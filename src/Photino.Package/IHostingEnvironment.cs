﻿using System;

namespace Photino.Blazor;

public interface IHostingEnvironment
{
    String ContentRootPath { get; }
    String EnvironmentName { get; }
}
