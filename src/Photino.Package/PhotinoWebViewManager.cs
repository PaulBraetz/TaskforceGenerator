﻿// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.

using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebView;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Options;

using PhotinoNET;

using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace Photino.Blazor;

public class PhotinoWebViewManager : WebViewManager
{
    private readonly PhotinoWindow _window;
    private readonly Channel<String> _channel;

    // On Windows, we can't use a custom scheme to host the initial HTML,
    // because webview2 won't let you do top-level navigation to such a URL.
    // On Linux/Mac, we must use a custom scheme, because their webviews
    // don't have a way to intercept http:// scheme requests.
    public static readonly String BlazorAppScheme = RuntimeInformation.IsOSPlatform(OSPlatform.Windows)
        ? "http"
        : "app";

    public static readonly String AppBaseUri = $"{BlazorAppScheme}://localhost/";

    public PhotinoWebViewManager(PhotinoWindow window, IServiceProvider provider, Dispatcher dispatcher,
        IFileProvider fileProvider, JSComponentConfigurationStore jsComponents, IOptions<PhotinoBlazorAppConfiguration> config)
        : base(provider, dispatcher, config.Value.AppBaseUri, fileProvider, jsComponents, config.Value.HostPage)
    {
        _window = window ?? throw new ArgumentNullException(nameof(window));

        // Create a scheduler that uses one thread.
        var sts = new SynchronousTaskScheduler();

        _window.WebMessageReceived += (sender, message) =>
        {
            // On some platforms, we need to move off the browser UI thread
            _ = Task.Factory.StartNew(message =>
            {
                // TODO: Fix this. Photino should ideally tell us the URL that the message comes from so we
                // know whether to trust it. Currently it's hardcoded to trust messages from any source, including
                // if the webview is somehow navigated to an external URL.
                var messageOriginUrl = new Uri(AppBaseUri);

                MessageReceived(messageOriginUrl, (String)message!);
            }, message, CancellationToken.None, TaskCreationOptions.DenyChildAttach, sts);
        };

        //Create channel and start reader
        _channel = Channel.CreateUnbounded<String>(new UnboundedChannelOptions() { SingleReader = true, SingleWriter = false, AllowSynchronousContinuations = false });
        _ = Task.Run(MessagePump);
    }

    public Stream HandleWebRequest(Object sender, String schema, String url, out String contentType)
    {
        // It would be better if we were told whether or not this is a navigation request, but
        // since we're not, guess.
        var localPath = new Uri(url).LocalPath;
        var hasFileExtension = localPath.LastIndexOf('.') > localPath.LastIndexOf('/');

        if(url.StartsWith(AppBaseUri, StringComparison.Ordinal)
            && TryGetResponseContent(url, !hasFileExtension, out _, out _,
                out var content, out var headers))
        {
            _ = headers.TryGetValue("Content-Type", out contentType);
            return content;
        } else
        {
            contentType = default;
            return null;
        }
    }

    protected override void NavigateCore(Uri absoluteUri) =>
        _window.Load(absoluteUri);

    protected override void SendMessage(String message)
    {
        while(!_channel.Writer.TryWrite(message))
        {
            Thread.Sleep(200);
        }
    }

    private async Task MessagePump()
    {
        var reader = _channel.Reader;
        try
        {
            while(true)
            {
                var message = await reader.ReadAsync();
                _window.SendWebMessage(message);
            }
        } catch(ChannelClosedException) { }
    }

    protected override ValueTask DisposeAsyncCore()
    {
        //complete channel
        try
        {
            _channel.Writer.Complete();
        } catch { }

        //continue disposing
        return base.DisposeAsyncCore();
    }
}