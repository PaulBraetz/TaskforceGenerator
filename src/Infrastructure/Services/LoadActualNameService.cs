﻿using OneOf;

using System.Text.RegularExpressions;

using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Requests;
using TaskforceGenerator.Domain.Core.Queries;
using TaskforceGenerator.Domain.Core.Results;
using TaskforceGenerator.Infrastructure.Queries;

namespace TaskforceGenerator.Infrastructure.Services;

/// <summary>
/// Service for loading a citizens actual name, as registered to RSI.
/// </summary>
public sealed partial class LoadActualNameService : IService<LoadActualName, OneOf<String, CitizenNotExistingResult>>
{
    private readonly IService<LoadCitizenPage, LoadCitizenPage.Result> _pageService;
    private readonly IService<CheckCitizenExists, Boolean> _checkService;

    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="pageService">The service used for loading the citizens page for parsing the actual name registered to RSI.</param>
    /// <param name="checkService">The service to use when checking if a name requested actually corresponds to an RSI registered citizen.</param>
    public LoadActualNameService(IService<LoadCitizenPage, LoadCitizenPage.Result> pageService,
                                 IService<CheckCitizenExists, Boolean> checkService)
    {
        _pageService = pageService;
        _checkService = checkService;
    }

    //TODO: https://html-agility-pack.net/
    [GeneratedRegex(@"(?<=<span class=""label"">Handle name<\/span>(\s|\r|\n)*<strong class=""value"">)[^<]*")]
    private partial Regex GetNamePattern();

    /// <inheritdoc/>
    public async ValueTask<OneOf<String, CitizenNotExistingResult>> Execute(LoadActualName query)
    {
        var exists = await new CheckCitizenExists(query.CitizenName, query.CancellationToken).Using(_checkService);
        if(!exists)
        {
            return CitizenNotExistingResult.Instance;
        }

        var page = await new LoadCitizenPage(query.CitizenName, query.CancellationToken).Using(_pageService);
        var result = GetNamePattern().Match(page).Value;

        return result;
    }
}
