﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Infrastructure.Queries;

namespace TaskforceGenerator.Infrastructure.Services;

/// <summary>
/// Service for loading a citizens RSI profile page.
/// </summary>
public sealed class LoadCitizenPageService : IService<LoadCitizenPage, LoadCitizenPage.Result>
{
    /// <inheritdoc/>
    public async ValueTask<LoadCitizenPage.Result> Execute(LoadCitizenPage query)
    {
        var pageUri = $"https://robertsspaceindustries.com/citizens/{query.CitizenName}";
        using var client = new HttpClient();
        var response = await client.GetAsync(pageUri, query.CancellationToken);
        if(!response.IsSuccessStatusCode)
        {
#pragma warning disable CA2208 // Instantiate argument exceptions correctly -> query request objects properties are to be treated as parameters
            throw new ArgumentException($"Unable to retrieve bio using the name provided: {query.CitizenName}", nameof(query.CitizenName));
#pragma warning restore CA2208 // Instantiate argument exceptions correctly
        }

        var result = await response.Content.ReadAsStringAsync(query.CancellationToken);

        return result;
    }
}
