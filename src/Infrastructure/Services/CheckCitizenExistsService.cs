﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Core.Queries;

namespace TaskforceGenerator.Infrastructure.Services;

/// <summary>
/// Service for checking whether a citizen actualy exists, that is, he is an RSI-registered citizen.
/// </summary>
public sealed class CheckCitizenExistsService : IService<CheckCitizenExists, Boolean>
{
    /// <inheritdoc/>
    public async ValueTask<Boolean> Execute(CheckCitizenExists query)
    {
        var pageUri = $"https://robertsspaceindustries.com/citizens/{query.CitizenName}";
        using var client = new HttpClient();
        var response = await client.GetAsync(pageUri, query.CancellationToken);
        var result = response.IsSuccessStatusCode;

        return result;
    }
}
