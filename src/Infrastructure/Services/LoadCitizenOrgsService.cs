﻿using System.Text.RegularExpressions;

using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Core.Queries;

namespace TaskforceGenerator.Infrastructure.Services;

/// <summary>
/// Service for loading a citizens public orgs.
/// </summary>
public sealed partial class LoadCitizenOrgsService : IService<LoadCitizenOrgs, IReadOnlySet<String>>
{
    [GeneratedRegex("(?<=Spectrum Identification \\(SID\\)<\\/span>(\\n|\\r|\\s)*<strong class=\"[a-zA-Z0-9\\s]*\">)[^>]*(?=<\\/strong>)")]
    private static partial Regex GetOrgsPatttteerrnn();

    /// <inheritdoc/>
    public async ValueTask<IReadOnlySet<String>> Execute(LoadCitizenOrgs query)
    {
        var pageUri = $"https://robertsspaceindustries.com/citizens/{query.CitizenName}/organizations";
        using var client = new HttpClient();
        var response = await client.GetAsync(pageUri, query.CancellationToken);
        if(!response.IsSuccessStatusCode)
#pragma warning disable CA2208 // Instantiate argument exceptions correctly -> query request objects properties are to be treated as parameters
            throw new ArgumentException($"Unable to retrieve orgs using the name provided: {query.CitizenName}", nameof(query.CitizenName));

        var page = await response.Content.ReadAsStringAsync(query.CancellationToken);

        //TODO: https://html-agility-pack.net/
        var result = GetOrgsPatttteerrnn().Matches(page)
            .Select(m => m.Value)
            .ToHashSet();

        return result;
    }
}
