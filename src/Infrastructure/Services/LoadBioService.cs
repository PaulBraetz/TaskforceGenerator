﻿using OneOf;

using System.Text.RegularExpressions;

using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Authentication.Requests;
using TaskforceGenerator.Domain.Core.Results;
using TaskforceGenerator.Infrastructure.Queries;

namespace TaskforceGenerator.Infrastructure.Services;

/// <summary>
/// Service for loading a citizen's bio.
/// </summary>
public sealed partial class LoadBioService : IService<LoadBio, OneOf<IBio, CitizenNotExistingResult>>
{
    private readonly IService<LoadCitizenPage, LoadCitizenPage.Result> _pageService;
    private readonly IService<CreateBio, IBio> _createService;
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="createService">The service to use when instantiating a bio after loading its text.</param>
    /// <param name="pageService">The service used when loading the citizens page for parsing the bio.</param>
    public LoadBioService(
        IService<CreateBio, IBio> createService,
        IService<LoadCitizenPage, LoadCitizenPage.Result> pageService)
    {
        _createService = createService;
        _pageService = pageService;
    }

    [GeneratedRegex(@"(?<=class=""entry\sbio"">)((?:(?!<\/div>).)|\r|\n)*(?=<\/div>)")]
    private partial Regex GetBioPattern();

    [GeneratedRegex("(?<=>)(?:(?!<[^>]*>)(.|\\r|\\n))*(?=<)")]
    private static partial Regex GetBioTexttPPaatttteerrnn();

    /// <inheritdoc/>
    public async ValueTask<OneOf<IBio, CitizenNotExistingResult>> Execute(LoadBio query)
    {
        var page = await new LoadCitizenPage(query.Name, query.CancellationToken).Using(_pageService);
        //TODO: https://html-agility-pack.net/
        var bio = GetBioPattern().Match(page).Value;
        var bioTextParts = GetBioTexttPPaatttteerrnn().Matches(bio)
            .Select(m => m.Value)
            .Where(v => !String.IsNullOrWhiteSpace(v));
        var bioText = String.Concat(bioTextParts);
        var result = await new CreateBio(bioText, query.CancellationToken).Using(_createService);

        return OneOf<IBio, CitizenNotExistingResult>.FromT0(result);
    }
}
