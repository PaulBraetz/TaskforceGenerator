﻿using OneOf;

using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Results;
using TaskforceGenerator.Domain.Authentication;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Authentication.Requests;
using TaskforceGenerator.Domain.Authentication.Results;
using TaskforceGenerator.Domain.Core.Queries;
using TaskforceGenerator.Domain.Core.Results;

namespace TaskforceGenerator.Infrastructure.Fakes;

/// <summary>
/// Service for setting a citizens password.
/// </summary>
public sealed class RepositoryFake :
    IService<CommitPasswordChange>,
    IService<ReconstituteConnection, OneOf<ICitizenConnection, CitizenNotRegisteredResult>>,
    IService<CommitBioCodeChange>,
    IService<CheckCitizenRegistered, Boolean>,
    IService<CommitConnection, OneOf<ServiceResult, ConnectionAlreadyCommittedResult>>
{
    private sealed class ConnectionFake : ICitizenConnection
    {
        public String CitizenName { get; set; } = String.Empty;
        public BioCode Code { get; set; }
        public Password Password { get; set; }
    }

    private readonly Dictionary<String, ConnectionFake> _citizens = new();

    private ConnectionFake LoadCitizen(String name)
    {
        return _citizens.TryGetValue(name, out var c) ?
            c :
            throw new InvalidOperationException($"Unable to locate citizen for name {name}.");
    }

    /// <inheritdoc/>
    public ValueTask<OneOf<ICitizenConnection, CitizenNotRegisteredResult>> Execute(ReconstituteConnection query)
    {
        var result = (ICitizenConnection)LoadCitizen(query.CitizenName);

        return ValueTask.FromResult(OneOf<ICitizenConnection, CitizenNotRegisteredResult>.FromT0(result));
    }
    /// <inheritdoc/>
    public ValueTask<ServiceResult> Execute(CommitPasswordChange command)
    {
        var citizen = LoadCitizen(command.CitizenName);
        citizen.Password = command.Password;

        return ServiceResult.Completed.Task.AsValueTask();
    }
    /// <inheritdoc/>
    public ValueTask<ServiceResult> Execute(CommitBioCodeChange command)
    {
        var citizen = LoadCitizen(command.CitizenName);
        citizen.Code = command.Code;

        return ServiceResult.Completed.Task.AsValueTask();
    }
    /// <inheritdoc/>
    public ValueTask<Boolean> Execute(CheckCitizenRegistered query)
    {
        var result = _citizens.ContainsKey(query.CitizenName);

        return ValueTask.FromResult(result);
    }
    /// <inheritdoc/>
    public ValueTask<OneOf<ServiceResult, ConnectionAlreadyCommittedResult>> Execute(CommitConnection command)
    {
        var clone = new ConnectionFake()
        {
            Code = command.Connection.Code,
            CitizenName = command.Connection.CitizenName,
            Password = command.Connection.Password,
        };

        _citizens.Add(clone.CitizenName, clone);
        return ValueTask.FromResult<OneOf<ServiceResult, ConnectionAlreadyCommittedResult>>(ServiceResult.Completed);
    }
}
