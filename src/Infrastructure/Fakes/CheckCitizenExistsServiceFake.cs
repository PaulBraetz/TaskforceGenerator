﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Core.Queries;

namespace TaskforceGenerator.Infrastructure.Fakes;

/// <summary>
/// Fake service for checking whether a citizen actualy exists, that is, he is an RSI-registered citizen.
/// The service will always report a citizen to exist.
/// </summary>
public sealed class CheckCitizenExistsServiceFake : IService<CheckCitizenExists, Boolean>
{
    /// <inheritdoc/>
    public ValueTask<Boolean> Execute(CheckCitizenExists query) => ValueTask.FromResult(true);
}
