﻿using TaskforceGenerator.Common.Abstractions;

using static TaskforceGenerator.Infrastructure.Queries.LoadCitizenPage;

namespace TaskforceGenerator.Infrastructure.Queries;

/// <summary>
/// Query for loading a citizens RSI page.
/// </summary>
/// <param name="CitizenName">The name of the citizen whose page to load.</param>
/// <param name="CancellationToken">The token used to signal the service execution to be cancelled.</param>
public readonly record struct LoadCitizenPage(String CitizenName, CancellationToken CancellationToken)
    : IServiceRequest<Result>
{
    /// <summary>
    /// The result of the query.
    /// </summary>
    /// <param name="Page">The page loaded for.</param>
    public readonly record struct Result(String Page)
    {
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
        public static implicit operator String(Result result) => result.Page;
        public static implicit operator Result(String page) => new(page);
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
    }
}
