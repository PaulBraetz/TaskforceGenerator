﻿#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member

using TaskforceGenerator.Common.Transactions.Abstractions;
using TaskforceGenerator.Common.Transactions;

namespace TaskforceGenerator.Common.Tests;

[TestClass]
public class MultipleChildrenTransactionTests
{
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
    private ObservableTransactionStateMachine Root { get; set; }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
    private IEqualityComparer<ITransactionStateMachine> Comparer { get; } = TransactionStateMachineEqualityComparer.Instance;

    [TestInitialize]
    public void Setup() => Root = new ObservableTransactionStateMachine(Comparer);

    [TestMethod]
    public async Task RootFlushRequestFlushesChildren()
    {
        //Arrange
        var child1 = new ObservableTransactionStateMachine(Comparer);
        var child2 = new ObservableTransactionStateMachine(Comparer);
        var child3 = new ObservableTransactionStateMachine(Comparer);

        await Root.AddChild(child1);
        await Root.AddChild(child2);
        await Root.AddChild(child3);

        //Act
        await Root.RequestFlush();

        //Assert
        Assert.AreEqual(TransactionState.Flushed, child1.State);
        Assert.AreEqual(TransactionState.Flushed, child2.State);
        Assert.AreEqual(TransactionState.Flushed, child3.State);
    }
    [TestMethod]
    public async Task UncommittedRootFlushRequestFlushesToRollbackOnUncommittedChildren()
    {
        //Arrange
        var child1 = new ObservableTransactionStateMachine(Comparer);
        var child2 = new ObservableTransactionStateMachine(Comparer);
        var child3 = new ObservableTransactionStateMachine(Comparer);

        await Root.AddChild(child1);
        await Root.AddChild(child2);
        await Root.AddChild(child3);

        //Act
        await Root.RequestFlush();

        //Assert
        var condition = await Root.GetIsCommit();
        Assert.IsFalse(condition);
    }
    [TestMethod]
    public async Task UncommittedRootFlushRequesteFlushesToRollbackOnPartiallyUncommittedChildren()
    {
        //Arrange
        var child1 = new ObservableTransactionStateMachine(Comparer);
        var child2 = new ObservableTransactionStateMachine(Comparer);
        var child3 = new ObservableTransactionStateMachine(Comparer);

        await Root.AddChild(child1);
        await Root.AddChild(child2);
        await Root.AddChild(child3);

        //Act
        await child2.Commit();
        await Root.RequestFlush();

        //Assert
        var condition = await Root.GetIsCommit();
        Assert.IsFalse(condition);
    }
    [TestMethod]
    public async Task UncommittedRootFlushRequestFlushesToRollbackOnRolledBackChildren()
    {
        //Arrange
        var child1 = new ObservableTransactionStateMachine(Comparer);
        var child2 = new ObservableTransactionStateMachine(Comparer);
        var child3 = new ObservableTransactionStateMachine(Comparer);

        await Root.AddChild(child1);
        await Root.AddChild(child2);
        await Root.AddChild(child3);

        //Act
        await child1.Rollback();
        await child2.Rollback();
        await child3.Rollback();
        await Root.RequestFlush();

        //Assert
        var condition = await Root.GetIsCommit();
        Assert.IsFalse(condition);
    }
    [TestMethod]
    public async Task UncommittedRootFlushRequestFlushesToRollbackOnPartiallyRolledBackChildren()
    {
        //Arrange
        var child1 = new ObservableTransactionStateMachine(Comparer);
        var child2 = new ObservableTransactionStateMachine(Comparer);
        var child3 = new ObservableTransactionStateMachine(Comparer);

        await Root.AddChild(child1);
        await Root.AddChild(child2);
        await Root.AddChild(child3);

        //Act
        await child1.Rollback();
        await child3.Rollback();
        await Root.RequestFlush();

        //Assert
        var condition = await Root.GetIsCommit();
        Assert.IsFalse(condition);
    }
    [TestMethod]
    public async Task UncommittedRootFlushRequestFlushesToRollbackOnCommittedChildren()
    {
        //Arrange
        var child1 = new ObservableTransactionStateMachine(Comparer);
        var child2 = new ObservableTransactionStateMachine(Comparer);
        var child3 = new ObservableTransactionStateMachine(Comparer);

        await Root.AddChild(child1);
        await Root.AddChild(child2);
        await Root.AddChild(child3);

        //Act
        await child1.Commit();
        await child2.Commit();
        await child3.Commit();
        await Root.RequestFlush();

        //Assert
        var condition = await Root.GetIsCommit();
        Assert.IsFalse(condition);
    }
    [TestMethod]
    public async Task UncommittedRootFlushRequestFlushesToRollbackOnPartiallyCommittedChildren()
    {
        //Arrange
        var child1 = new ObservableTransactionStateMachine(Comparer);
        var child2 = new ObservableTransactionStateMachine(Comparer);
        var child3 = new ObservableTransactionStateMachine(Comparer);

        await Root.AddChild(child1);
        await Root.AddChild(child2);
        await Root.AddChild(child3);

        //Act
        await child1.Commit();
        await child3.Commit();
        await Root.RequestFlush();

        //Assert
        var condition = await Root.GetIsCommit();
        Assert.IsFalse(condition);
    }
    [TestMethod]
    public async Task RolledBackRootFlushRequestFlushesToRollbackOnUncommittedChildren()
    {
        //Arrange
        var child1 = new ObservableTransactionStateMachine(Comparer);
        var child2 = new ObservableTransactionStateMachine(Comparer);
        var child3 = new ObservableTransactionStateMachine(Comparer);

        await Root.AddChild(child1);
        await Root.AddChild(child2);
        await Root.AddChild(child3);

        //Act
        await Root.Rollback();
        await Root.RequestFlush();

        //Assert
        var condition = await Root.GetIsCommit();
        Assert.IsFalse(condition);
    }
    [TestMethod]
    public async Task RolledBackRootFlushRequestFlushesToRollbackOnPartiallyUncommittedChildren()
    {
        //Arrange
        var child1 = new ObservableTransactionStateMachine(Comparer);
        var child2 = new ObservableTransactionStateMachine(Comparer);
        var child3 = new ObservableTransactionStateMachine(Comparer);

        await Root.AddChild(child1);
        await Root.AddChild(child2);
        await Root.AddChild(child3);

        //Act
        await child2.Commit();
        await Root.Rollback();
        await Root.RequestFlush();

        //Assert
        var condition = await Root.GetIsCommit();
        Assert.IsFalse(condition);
    }
    [TestMethod]
    public async Task RolledBackRootFlushRequestFlushesToRollbackOnRolledBackChildren()
    {
        //Arrange
        var root = new ObservableTransactionStateMachine(Comparer);
        var child = new ObservableTransactionStateMachine(Comparer);
        await root.AddChild(child);

        //Act
        await child.Rollback();
        await root.Rollback();
        await root.RequestFlush();

        //Assert
        var condition = await root.GetIsCommit();
        Assert.IsFalse(condition);
    }
    [TestMethod]
    public async Task RolledBackRootFlushRequestFlushesToRollbackOnCommittedChildren()
    {
        //Arrange
        var child1 = new ObservableTransactionStateMachine(Comparer);
        var child2 = new ObservableTransactionStateMachine(Comparer);
        var child3 = new ObservableTransactionStateMachine(Comparer);

        await Root.AddChild(child1);
        await Root.AddChild(child2);
        await Root.AddChild(child3);

        //Act
        await child1.Commit();
        await child2.Commit();
        await child3.Commit();
        await Root.Rollback();
        await Root.RequestFlush();

        //Assert
        var condition = await Root.GetIsCommit();
        Assert.IsFalse(condition);
    }
    [TestMethod]
    public async Task RolledBackRootFlushRequestFlushesToRollbackOnPartiallyCommittedChildren()
    {
        //Arrange
        var child1 = new ObservableTransactionStateMachine(Comparer);
        var child2 = new ObservableTransactionStateMachine(Comparer);
        var child3 = new ObservableTransactionStateMachine(Comparer);

        await Root.AddChild(child1);
        await Root.AddChild(child2);
        await Root.AddChild(child3);

        //Act
        await child1.Commit();
        await child3.Commit();
        await Root.Rollback();
        await Root.RequestFlush();

        //Assert
        var condition = await Root.GetIsCommit();
        Assert.IsFalse(condition);
    }
    [TestMethod]
    public async Task CommittedBackRootFlushRequestFlushesToRollbackOnUncommittedChildren()
    {
        //Arrange
        var child1 = new ObservableTransactionStateMachine(Comparer);
        var child2 = new ObservableTransactionStateMachine(Comparer);
        var child3 = new ObservableTransactionStateMachine(Comparer);

        await Root.AddChild(child1);
        await Root.AddChild(child2);
        await Root.AddChild(child3);

        //Act
        await Root.Commit();
        await Root.RequestFlush();

        //Assert
        var condition = await Root.GetIsCommit();
        Assert.IsFalse(condition);
    }
    [TestMethod]
    public async Task CommittedBackRootFlushRequestFlushesToRollbackOnPartiallyUncommittedChildren()
    {
        //Arrange
        var child1 = new ObservableTransactionStateMachine(Comparer);
        var child2 = new ObservableTransactionStateMachine(Comparer);
        var child3 = new ObservableTransactionStateMachine(Comparer);

        await Root.AddChild(child1);
        await Root.AddChild(child2);
        await Root.AddChild(child3);

        //Act
        await child2.Commit();
        await Root.Commit();
        await Root.RequestFlush();

        //Assert
        var condition = await Root.GetIsCommit();
        Assert.IsFalse(condition);
    }
    [TestMethod]
    public async Task CommittedRootFlushRequestFlushesToRollbackOnRolledBackChildren()
    {
        //Arrange
        var child1 = new ObservableTransactionStateMachine(Comparer);
        var child2 = new ObservableTransactionStateMachine(Comparer);
        var child3 = new ObservableTransactionStateMachine(Comparer);

        await Root.AddChild(child1);
        await Root.AddChild(child2);
        await Root.AddChild(child3);

        //Act
        await child1.Rollback();
        await child2.Rollback();
        await child3.Rollback();
        await Root.Commit();
        await Root.RequestFlush();

        //Assert
        var condition = await Root.GetIsCommit();
        Assert.IsFalse(condition);
    }
    [TestMethod]
    public async Task CommittedRootFlushRequestFlushesToRollbackOnPartiallyRolledBackChildren()
    {
        //Arrange
        var child1 = new ObservableTransactionStateMachine(Comparer);
        var child2 = new ObservableTransactionStateMachine(Comparer);
        var child3 = new ObservableTransactionStateMachine(Comparer);

        await Root.AddChild(child1);
        await Root.AddChild(child2);
        await Root.AddChild(child3);

        //Act
        await child1.Rollback();
        await child2.Commit();
        await child3.Rollback();
        await Root.Commit();
        await Root.RequestFlush();

        //Assert
        var condition = await Root.GetIsCommit();
        Assert.IsFalse(condition);
    }
    [TestMethod]
    public async Task CommittedRootFlushRequestFlushesToCommitOnCommittedChildren()
    {
        //Arrange
        var child1 = new ObservableTransactionStateMachine(Comparer);
        var child2 = new ObservableTransactionStateMachine(Comparer);
        var child3 = new ObservableTransactionStateMachine(Comparer);

        await Root.AddChild(child1);
        await Root.AddChild(child2);
        await Root.AddChild(child3);

        //Act
        await child1.Commit();
        await child2.Commit();
        await child3.Commit();
        await Root.Commit();
        await Root.RequestFlush();

        //Assert
        var condition = await Root.GetIsCommit();
        Assert.IsTrue(condition);
    }
    [TestMethod]
    public async Task CommittedRootFlushRequestFlushesToRollbackOnPartiallyCommittedChildren()
    {
        //Arrange
        var child1 = new ObservableTransactionStateMachine(Comparer);
        var child2 = new ObservableTransactionStateMachine(Comparer);
        var child3 = new ObservableTransactionStateMachine(Comparer);

        await Root.AddChild(child1);
        await Root.AddChild(child2);
        await Root.AddChild(child3);

        //Act
        await child1.Commit();
        await child3.Commit();
        await Root.Commit();
        await Root.RequestFlush();

        //Assert
        var condition = await Root.GetIsCommit();
        Assert.IsFalse(condition);
    }
}
