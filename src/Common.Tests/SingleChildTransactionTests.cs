﻿#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member

using TaskforceGenerator.Common.Transactions;
using TaskforceGenerator.Common.Transactions.Abstractions;

namespace TaskforceGenerator.Common.Tests;

[TestClass]
public class SingleChildTransactionTests
{
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
    private ObservableTransactionStateMachine Root { get; set; }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
    private IEqualityComparer<ITransactionStateMachine> Comparer { get; } = TransactionStateMachineEqualityComparer.Instance;

    [TestInitialize]
    public void Setup() => Root = new ObservableTransactionStateMachine(Comparer);

    [TestMethod]
    public async Task AddChildThrowsOnFlushedChild()
    {
        //Arrange
        var child = new ObservableTransactionStateMachine(Comparer);

        //Act
        await child.RequestFlush();

        //Assert
        _ = await Assert.ThrowsExceptionAsync<InvalidOperationException>(async () => await Root.AddChild(child));
    }
    [TestMethod]
    public async Task SetParentThrowsOnFlushedChild()
    {
        //Arrange
        var child = new ObservableTransactionStateMachine(Comparer);

        //Act
        await child.RequestFlush();

        //Assert
        _ = await Assert.ThrowsExceptionAsync<InvalidOperationException>(async () => await child.SetParent(Root));
    }
    [TestMethod]
    public async Task ChildFlushRequestFlushesToImmutable()
    {
        //Arrange
        var child = new ObservableTransactionStateMachine(Comparer);
        await Root.AddChild(child);

        //Act
        await child.RequestFlush();

        //Assert
        Assert.AreEqual(TransactionState.Immutable, child.State);
    }
    [TestMethod]
    public async Task RootFlushRequestFlushesChild()
    {
        //Arrange
        var child = new ObservableTransactionStateMachine(Comparer);
        await Root.AddChild(child);

        //Act
        await Root.RequestFlush();

        //Assert
        Assert.AreEqual(TransactionState.Flushed, child.State);
    }
    [TestMethod]
    public async Task UncommittedRootFlushRequestFlushesToRollbackOnUncommittedChild()
    {
        //Arrange
        var child = new ObservableTransactionStateMachine(Comparer);
        await Root.AddChild(child);

        //Act
        await Root.RequestFlush();

        //Assert
        var condition = await Root.GetIsCommit();
        Assert.IsFalse(condition);
    }
    [TestMethod]
    public async Task UncommittedRootFlushRequestFlushesToRollbackOnRolledBackChild()
    {
        //Arrange
        var child = new ObservableTransactionStateMachine(Comparer);
        await Root.AddChild(child);

        //Act
        await child.Rollback();
        await Root.RequestFlush();

        //Assert
        var condition = await Root.GetIsCommit();
        Assert.IsFalse(condition);
    }
    [TestMethod]
    public async Task UncommittedRootFlushRequestFlushesToRollbackOnCommittedChild()
    {
        //Arrange
        var child = new ObservableTransactionStateMachine(Comparer);
        await Root.AddChild(child);

        //Act
        await child.Commit();
        await Root.RequestFlush();

        //Assert
        var condition = await Root.GetIsCommit();
        Assert.IsFalse(condition);
    }
    [TestMethod]
    public async Task RolledBackRootFlushRequestFlushesToRollbackOnUncommittedChild()
    {
        //Arrange
        var child = new ObservableTransactionStateMachine(Comparer);
        await Root.AddChild(child);

        //Act
        await Root.Rollback();
        await Root.RequestFlush();

        //Assert
        var condition = await Root.GetIsCommit();
        Assert.IsFalse(condition);
    }
    [TestMethod]
    public async Task RolledBackRootFlushRequestFlushesToRollbackOnRolledBackChild()
    {
        //Arrange
        var child = new ObservableTransactionStateMachine(Comparer);
        await Root.AddChild(child);

        //Act
        await child.Rollback();
        await Root.Rollback();
        await Root.RequestFlush();

        //Assert
        var condition = await Root.GetIsCommit();
        Assert.IsFalse(condition);
    }
    [TestMethod]
    public async Task RolledBackRootFlushRequestFlushesToRollbackOnCommittedChild()
    {
        //Arrange
        var child = new ObservableTransactionStateMachine(Comparer);
        await Root.AddChild(child);

        //Act
        await child.Commit();
        await Root.Rollback();
        await Root.RequestFlush();

        //Assert
        var condition = await Root.GetIsCommit();
        Assert.IsFalse(condition);
    }
    [TestMethod]
    public async Task CommittedRootFlushRequestFlushesToRollbackOnUncommittedChild()
    {
        //Arrange
        var child = new ObservableTransactionStateMachine(Comparer);
        await Root.AddChild(child);

        //Act
        await Root.Commit();
        await Root.RequestFlush();

        //Assert
        var condition = await Root.GetIsCommit();
        Assert.IsFalse(condition);
    }
    [TestMethod]
    public async Task CommittedRootFlushRequestFlushesToRollbackOnRolledBackChild()
    {
        //Arrange
        var child = new ObservableTransactionStateMachine(Comparer);
        await Root.AddChild(child);

        //Act
        await child.Rollback();
        await Root.Commit();
        await Root.RequestFlush();

        //Assert
        var condition = await Root.GetIsCommit();
        Assert.IsFalse(condition);
    }
    [TestMethod]
    public async Task CommittedRootFlushRequestFlushesToCommitOnCommittedChild()
    {
        //Arrange
        var child = new ObservableTransactionStateMachine(Comparer);
        await Root.AddChild(child);

        //Act
        await child.Commit();
        await Root.Commit();
        await Root.RequestFlush();

        //Assert
        var condition = await Root.GetIsCommit();
        Assert.IsTrue(condition);
    }
}