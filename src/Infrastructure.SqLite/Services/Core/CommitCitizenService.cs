﻿using Microsoft.Data.Sqlite;

using OneOf;

using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Results;
using TaskforceGenerator.Common.Transactions;
using TaskforceGenerator.Common.Transactions.Abstractions;
using TaskforceGenerator.Domain.Core.Results;
using TaskforceGenerator.Infrastructure.SqLite.Abstractions;
using TaskforceGenerator.Infrastructure.SqLite.Schema;
using TaskforceGenerator.Domain.Core.Requests;

namespace TaskforceGenerator.Infrastructure.SqLite.Services.Core;

/// <summary>
/// Service for committing a citizen to the infrastructure.
/// </summary>
public sealed class CommitCitizenService :
    SqliteDatabaseServiceBase<Citizens>,
    IService<CommitCitizen, OneOf<ServiceResult, CitizenAlreadyCommittedResult>>
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="schema">The schema information required by the service.</param>
    /// <param name="transactionFactory">The factory to use when creating new or nested transactions.</param>
    public CommitCitizenService(
        Citizens schema,
        ITransactionFactory<ITransaction<SqliteTransaction>, SqliteTransaction> transactionFactory)
        : base(schema, transactionFactory)
    {
    }

    /// <inheritdoc/>
    public async ValueTask<OneOf<ServiceResult, CitizenAlreadyCommittedResult>> Execute(CommitCitizen command)
    {
        var transaction = await TransactionFactory.CreateRoot(command.CancellationToken);
        await using var _0 = transaction.AsAutoFlush();

        var comandText = CreateCommand();
        using var dbCommand = transaction.Context.Connection!.CreateCommand(comandText);

        var citizen = command.Citizen;
#pragma warning disable IDE0058 // Expression value is never used
        dbCommand.Parameters.AddWithValue(Schema.CitizenName.ParamName, citizen.Name);
#pragma warning restore IDE0058 // Expression value is never used

        if(command.CancellationToken.IsCancellationRequested)
        {
            return ServiceResult.CompliantlyCancelled;
        }

        await dbCommand.PrepareAsync(command.CancellationToken);
        try
        {
            _ = await dbCommand.ExecuteNonQueryAsync(command.CancellationToken);
            await transaction.Commit(command.CancellationToken);
        } catch(SqliteException ex)
        when(ex.SqliteExtendedErrorCode == 1555) //PK constraint ex
        {
            return CitizenAlreadyCommittedResult.Instance;
        }

        return ServiceResult.Completed;
    }

    private String CreateCommand()
    {
        var result = $"insert into {Schema.TableName}" +
            $"({Schema.CitizenName}" +
            $") values(" +
            $"{Schema.CitizenName.ParamName})";

        return result;
    }
}
