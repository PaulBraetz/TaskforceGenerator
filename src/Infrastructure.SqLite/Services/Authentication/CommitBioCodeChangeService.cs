﻿using Microsoft.Data.Sqlite;

using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Results;
using TaskforceGenerator.Common.Transactions;
using TaskforceGenerator.Common.Transactions.Abstractions;
using TaskforceGenerator.Domain.Authentication.Requests;
using TaskforceGenerator.Infrastructure.SqLite.Abstractions;
using TaskforceGenerator.Infrastructure.SqLite.Schema;

namespace TaskforceGenerator.Infrastructure.SqLite.Services.Authentication;

/// <summary>
/// Service for setting a citizens bio code.
/// </summary>
public sealed class CommitBioCodeChangeService :
    SqliteDatabaseServiceBase<CitizenConnections>,
    IService<CommitBioCodeChange>
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="schema">The schema information required by the service.</param>
    /// <param name="transactionFactory">The factory to use when creating new or nested transactions.</param>
    public CommitBioCodeChangeService(
        CitizenConnections schema,
        ITransactionFactory<ITransaction<SqliteTransaction>, SqliteTransaction> transactionFactory)
        : base(schema, transactionFactory)
    {
    }

    /// <inheritdoc/>
    public async ValueTask<ServiceResult> Execute(CommitBioCodeChange command)
    {
        if(command.CancellationToken.IsCancellationRequested)
        {
            return ServiceResult.CompliantlyCancelled;
        }

        var transaction = await TransactionFactory.CreateRoot(command.CancellationToken);
        await using var _0 = transaction.AsAutoFlush();

        var comandText = CreateCommand();
        using var dbCommand = transaction.Context.Connection!.CreateCommand(comandText);

#pragma warning disable IDE0058 // Expression value is never used
        dbCommand.Parameters.AddWithValue(Schema.CitizenName.ParamName, command.CitizenName);
        dbCommand.Parameters.AddWithValue(Schema.Code.ParamName, command.Code.Value);
#pragma warning restore IDE0058 // Expression value is never used

        await dbCommand.PrepareAsync(command.CancellationToken);
        _ = await dbCommand.ExecuteNonQueryAsync(command.CancellationToken);
        await transaction.Commit(command.CancellationToken);

        return ServiceResult.Completed;
    }

    private String CreateCommand()
    {
        var result = $"update {Schema.TableName} set " +
            $"{Schema.Code}={Schema.Code.ParamName} where " +
            $"{Schema.CitizenName}={Schema.CitizenName.ParamName}";

        return result;
    }
}
