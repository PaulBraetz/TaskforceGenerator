﻿using Microsoft.Data.Sqlite;

using System.Data;

using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Transactions;
using TaskforceGenerator.Common.Transactions.Abstractions;
using TaskforceGenerator.Domain.Authentication.Requests;
using TaskforceGenerator.Infrastructure.SqLite.Abstractions;
using TaskforceGenerator.Infrastructure.SqLite.Schema;

namespace TaskforceGenerator.Infrastructure.SqLite.Services.Authentication;
/// <summary>
/// Service for retrieving a citizenname for autocompletion.
/// </summary>
public sealed class LoadAutoCompleteCitizenNameService :
    SqliteDatabaseServiceBase<CitizenConnections>,
    IService<LoadAutoCompleteCitizenName, String?>
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="schema">The schema information required by the service.</param>
    /// <param name="transactionFactory">The factory to use when creating new or nested transactions.</param>
    public LoadAutoCompleteCitizenNameService(
        CitizenConnections schema,
        ITransactionFactory<ITransaction<SqliteTransaction>, SqliteTransaction> transactionFactory)
        : base(schema, transactionFactory)
    {
    }
    /// <inheritdoc/>
    public async ValueTask<String?> Execute(LoadAutoCompleteCitizenName query)
    {
        var transaction = await TransactionFactory.CreateRoot(query.CancellationToken);
        await using var _ = transaction.AsAutoFlush();

        var comandText = CreateCommand();
        using var dbCommand = transaction.Context.Connection!.CreateCommand(comandText);

#pragma warning disable IDE0058 // Expression value is never used
        dbCommand.Parameters.AddWithValue(Schema.CitizenName.ParamName, query.CitizenName);
#pragma warning restore IDE0058 // Expression value is never used

        await dbCommand.PrepareAsync(query.CancellationToken);

        using var reader = await dbCommand.ExecuteReaderAsync(query.CancellationToken);
        String? result = null;
        if(await reader.ReadAsync(query.CancellationToken))
        {
            result = reader.GetString(0);
        }

        return result;
    }

    private String CreateCommand()
    {
        var result = $"select {Schema.CitizenName.FieldName} from {Schema.TableName} " +
            $"where {Schema.CitizenName.FieldName} like {Schema.CitizenName.ParamName} || '%' " +
            $"order by {Schema.CitizenName.FieldName} " +
            $"limit 1";

        return result;
    }
}
