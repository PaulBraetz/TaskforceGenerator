﻿using Microsoft.Data.Sqlite;

using OneOf;

using System.Transactions;

using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Results;
using TaskforceGenerator.Common.Transactions;
using TaskforceGenerator.Common.Transactions.Abstractions;
using TaskforceGenerator.Domain.Authentication.Requests;
using TaskforceGenerator.Domain.Authentication.Results;
using TaskforceGenerator.Infrastructure.SqLite.Abstractions;
using TaskforceGenerator.Infrastructure.SqLite.Schema;

namespace TaskforceGenerator.Infrastructure.SqLite.Services.Authentication;

/// <summary>
/// Service for committing connections to the infrastructure.
/// </summary>
public sealed class CommitConnectionService :
    SqliteDatabaseServiceBase<CitizenConnections>,
    IService<CommitConnection, OneOf<ServiceResult, ConnectionAlreadyCommittedResult>>
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="schema">The schema information required by the service.</param>
    /// <param name="transactionFactory">The factory to use when creating new or nested transactions.</param>
    public CommitConnectionService(
        CitizenConnections schema,
        ITransactionFactory<ITransaction<SqliteTransaction>, SqliteTransaction> transactionFactory)
        : base(schema, transactionFactory)
    {
    }

    /// <inheritdoc/>
    public async ValueTask<OneOf<ServiceResult, ConnectionAlreadyCommittedResult>> Execute(CommitConnection command)
    {
        if(command.CancellationToken.IsCancellationRequested)
        {
            return ServiceResult.CompliantlyCancelled;
        }

        var transaction = await TransactionFactory.CreateRoot(command.CancellationToken);
        await using var _0 = transaction.AsAutoFlush();

        var comandText = CreateCommand();
        using var dbCommand = transaction.Context.Connection!.CreateCommand(comandText);

        var connection = command.Connection;
#pragma warning disable IDE0058 // Expression value is never used
        dbCommand.Parameters.AddWithValue(Schema.CitizenName.ParamName, connection.CitizenName);
        dbCommand.Parameters.AddWithValue(Schema.Code.ParamName, connection.Code.Value);
        dbCommand.Parameters.AddWithValue(Schema.PasswordHash.ParamName, connection.Password.Hash);

        var numerics = connection.Password.Parameters.Numerics;
        dbCommand.Parameters.AddWithValue(Schema.PasswordDegreeOfParallelism.ParamName, numerics.DegreeOfParallelism);
        dbCommand.Parameters.AddWithValue(Schema.PasswordIterations.ParamName, numerics.Iterations);
        dbCommand.Parameters.AddWithValue(Schema.PasswordMemorySize.ParamName, numerics.MemorySize);
        dbCommand.Parameters.AddWithValue(Schema.PasswordOutputLength.ParamName, numerics.OutputLength);

        var data = connection.Password.Parameters.Data;
        dbCommand.Parameters.AddWithValue(Schema.PasswordSalt.ParamName, data.Salt);
        dbCommand.Parameters.AddWithValue(Schema.PasswordAssociatedData.ParamName, data.AssociatedData);
        dbCommand.Parameters.AddWithValue(Schema.PasswordKnownSecret.ParamName, data.KnownSecret);
#pragma warning restore IDE0058 // Expression value is never used

        if(command.CancellationToken.IsCancellationRequested)
        {
            return ServiceResult.CompliantlyCancelled;
        }

        await dbCommand.PrepareAsync(command.CancellationToken);
        try
        {
            _ = await dbCommand.ExecuteNonQueryAsync(command.CancellationToken);
            await transaction.Commit(command.CancellationToken);
        } catch(SqliteException ex)
        when(ex.SqliteExtendedErrorCode == 1555) //PK constraint ex
        {
            return ConnectionAlreadyCommittedResult.Instance;
        }

        return ServiceResult.Completed;
    }

    private String CreateCommand()
    {
        var result = $"insert into {Schema.TableName}" +
            $"({Schema.CitizenName}," +
            $"{Schema.Code}," +
            $"{Schema.PasswordHash}," +
            $"{Schema.PasswordDegreeOfParallelism}," +
            $"{Schema.PasswordIterations}," +
            $"{Schema.PasswordMemorySize}," +
            $"{Schema.PasswordOutputLength}," +
            $"{Schema.PasswordSalt}," +
            $"{Schema.PasswordAssociatedData}," +
            $"{Schema.PasswordKnownSecret}" +
            $") values(" +
            $"{Schema.CitizenName.ParamName}," +
            $"{Schema.Code.ParamName}," +
            $"{Schema.PasswordHash.ParamName}," +
            $"{Schema.PasswordDegreeOfParallelism.ParamName}," +
            $"{Schema.PasswordIterations.ParamName}," +
            $"{Schema.PasswordMemorySize.ParamName}," +
            $"{Schema.PasswordOutputLength.ParamName}," +
            $"{Schema.PasswordSalt.ParamName}," +
            $"{Schema.PasswordAssociatedData.ParamName}," +
            $"{Schema.PasswordKnownSecret.ParamName})";

        return result;
    }
}
