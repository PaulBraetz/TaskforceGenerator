﻿using Microsoft.Data.Sqlite;

using OneOf;

using System.Data;

using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Transactions;
using TaskforceGenerator.Common.Transactions.Abstractions;
using TaskforceGenerator.Domain.Authentication.Abstractions;

using TaskforceGenerator.Domain.Authentication.Requests;
using TaskforceGenerator.Domain.Authentication.Results;
using TaskforceGenerator.Domain.Core.Results;
using TaskforceGenerator.Infrastructure.SqLite.Abstractions;
using TaskforceGenerator.Infrastructure.SqLite.Schema;

namespace TaskforceGenerator.Infrastructure.SqLite.Services.Authentication;

/// <summary>
/// Service for reconstituting a single citizen matching the query.
/// </summary>
public sealed partial class ReconstituteConnectionService :
    SqliteDatabaseServiceBase<CitizenConnections>,
    IService<ReconstituteConnection, OneOf<ICitizenConnection, CitizenNotRegisteredResult>>
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="schema">The schema information required by the service.</param>
    /// <param name="transactionFactory">The factory to use when creating new or nested transactions.</param>
    public ReconstituteConnectionService(
        CitizenConnections schema,
        ITransactionFactory<ITransaction<SqliteTransaction>, SqliteTransaction> transactionFactory)
        : base(schema, transactionFactory)
    {
    }

    /// <inheritdoc/>
    public async ValueTask<OneOf<ICitizenConnection, CitizenNotRegisteredResult>> Execute(ReconstituteConnection query)
    {
        var transaction = await TransactionFactory.CreateRoot(query.CancellationToken);
        await using var _ = transaction.AsAutoFlush();

        var comandText = CreateCommand();
        using var dbCommand = transaction.Context.Connection!.CreateCommand(comandText);

#pragma warning disable IDE0058 // Expression value is never used
        dbCommand.Parameters.AddWithValue(Schema.CitizenName.ParamName, query.CitizenName);
#pragma warning restore IDE0058 // Expression value is never used

        await dbCommand.PrepareAsync(query.CancellationToken);
        var reader = await dbCommand.ExecuteReaderAsync(query.CancellationToken);

        if(!reader.HasRows)
        {
            return CitizenNotRegisteredResult.Instance;
        }

        var row = reader.OfType<IDataRecord>().Single();
        var result = ConnectionEntity.Reconstitute(row, Schema);

        return OneOf<ICitizenConnection, CitizenNotRegisteredResult>.FromT0(result);
    }

    private String CreateCommand()
    {
        var result = $"select * from {Schema.TableName} where {Schema.CitizenName}={Schema.CitizenName.ParamName}";

        return result;
    }
}
