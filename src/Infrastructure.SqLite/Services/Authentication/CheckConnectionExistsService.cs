﻿using Microsoft.Data.Sqlite;

using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Transactions;
using TaskforceGenerator.Common.Transactions.Abstractions;
using TaskforceGenerator.Domain.Authentication.Queries;
using TaskforceGenerator.Domain.Authentication.Requests;
using TaskforceGenerator.Infrastructure.SqLite.Abstractions;
using TaskforceGenerator.Infrastructure.SqLite.Schema;

namespace TaskforceGenerator.Infrastructure.SqLite.Services.Authentication;

/// <summary>
/// Service for checking whether a citizens connection is registered to the system.
/// </summary>
public sealed class CheckConnectionExistsService : 
    SqliteDatabaseServiceBase<CitizenConnections>, 
    IService<CheckConnectionExists, Boolean>
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="schema">The schema information required by the service.</param>
    /// <param name="transactionFactory">The factory to use when creating new or nested transactions.</param>
    public CheckConnectionExistsService(
        CitizenConnections schema,
        ITransactionFactory<ITransaction<SqliteTransaction>, SqliteTransaction> transactionFactory)
        : base(schema, transactionFactory)
    {
    }

    /// <inheritdoc/>
    public async ValueTask<Boolean> Execute(CheckConnectionExists query)
    {
        var transaction = await TransactionFactory.CreateRoot(query.CancellationToken);
        await using var _ = transaction.AsAutoFlush();

        var comandText = CreateCommand();
        using var dbCommand = transaction.Context.Connection!.CreateCommand(comandText);

#pragma warning disable IDE0058 // Expression value is never used
        dbCommand.Parameters.AddWithValue(Schema.CitizenName.ParamName, query.CitizenName);
#pragma warning restore IDE0058 // Expression value is never used

        await dbCommand.PrepareAsync(query.CancellationToken);
        var count = await dbCommand.ExecuteScalarAsync(query.CancellationToken);
        var result = (Int64)count! > 0;

        return result;
    }

    private String CreateCommand()
    {
        var result = $"select count(*) from {Schema.TableName} where {Schema.CitizenName}={Schema.CitizenName.ParamName}";

        return result;
    }
}
