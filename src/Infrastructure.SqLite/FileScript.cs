﻿using Microsoft.Data.Sqlite;

using TaskforceGenerator.Infrastructure.SqLite.Abstractions;

namespace TaskforceGenerator.Infrastructure.SqLite;

/// <summary>
/// Represents an executable SQLite script file.
/// </summary>
public sealed class FileScript : IScript
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="scriptFilePath">The path to the script file to execute.</param>
    public FileScript(String scriptFilePath)
    {
        ScriptName = scriptFilePath;
    }

    /// <summary>
    /// Gets the path to the script to execute.
    /// </summary>
    public String ScriptName { get; }

    /// <summary>
    /// Executes the script on the connection provided.
    /// </summary>
    /// <param name="connection">The connection using which to execute the script.</param>
    public void Execute(SqliteConnection connection)
    {
        var fileInfo = new FileInfo(ScriptName);
        using var reader = fileInfo.OpenText();
        var script = reader.ReadToEnd();
        using var command = new SqliteCommand(script, connection);
        _ = command.ExecuteNonQuery();
    }
}
