﻿using Microsoft.Data.Sqlite;

using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Transactions;
using TaskforceGenerator.Common.Transactions.Abstractions;
using TaskforceGenerator.Infrastructure.SqLite.Abstractions;

using MsSqliteTransaction = Microsoft.Data.Sqlite.SqliteTransaction;

namespace TaskforceGenerator.Infrastructure.SqLite.Transactions;

sealed partial class SqliteTransaction
{
    /// <summary>
    /// Factory for instances of <see cref="SqliteTransaction"/>
    /// </summary>
    public sealed class Factory : ITransactionFactory<ITransaction<MsSqliteTransaction>, MsSqliteTransaction>, IDisposable
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="connectionString">The connection String to use when creating new connections.</param>
        /// <param name="memoryConnection">The optional connection for keeping alive in-memory connections.</param>
        /// <param name="stateMachineFactory">The factory to use when instantiating new state machines for transactions to use.</param>
        /// <param name="settingsFactory">The factory touse when creating new settings for new state machines.</param>
        private Factory(
            String connectionString,
            SqliteConnection? memoryConnection,
            IFactory<IObservableTransactionStateMachine, ITransactionStateMachineSettings> stateMachineFactory,
            IFactory<ITransactionStateMachineSettings> settingsFactory)
        {
            _connectionString = connectionString;
            _memoryConnection = memoryConnection;
            _stateMachineFactory = stateMachineFactory;
            _settingsFactory = settingsFactory;
        }

        private readonly String _connectionString;
        //keep alive for in-memory dbs
        private readonly SqliteConnection? _memoryConnection;
        private readonly IFactory<IObservableTransactionStateMachine, ITransactionStateMachineSettings> _stateMachineFactory;
        private readonly IFactory<ITransactionStateMachineSettings> _settingsFactory;

        /// <summary>
        /// Creates and initializes a new instance.
        /// </summary>
        /// <param name="connectionString">The connection String to use when creating new connections.</param>
        /// <param name="stateMachineFactory">The factory to use when instantiating new state machines for transactions to use.</param>
        /// <param name="settingsFactory">The factory touse when creating new settings for new state machines.</param>
        public static Factory Create(
            String connectionString,
            IFactory<IObservableTransactionStateMachine, ITransactionStateMachineSettings> stateMachineFactory,
            IFactory<ITransactionStateMachineSettings> settingsFactory)
        {
            var builder = new SqliteConnectionStringBuilder(connectionString);
            var inMemory =
                builder.Mode == SqliteOpenMode.Memory &&
                builder.Cache == SqliteCacheMode.Shared ||
                builder.DataSource == ":memory:";

            var memoryConnection = inMemory ?
                new SqliteConnection(connectionString) :
                null;
            memoryConnection?.Open();

            var result = new Factory(connectionString, memoryConnection, stateMachineFactory, settingsFactory);

            return result;
        }

        private async Task<SqliteTransaction> CreateInternal(Boolean isRoot, CancellationToken cancellationToken)
        {
            var connection = new SqliteConnection(_connectionString);
            await connection.OpenAsync(cancellationToken);
            var transaction = connection.BeginTransaction();

            var stateMachine = CreateStateMachine();

            var result = new SqliteTransaction(isRoot, transaction, stateMachine);
            await transaction.SaveAsync(result._savepointName, cancellationToken);

            stateMachine.Committed += result.OnCommitted;
            stateMachine.RolledBack += result.OnRolledBack;

            return result;
        }

        private IObservableTransactionStateMachine CreateStateMachine()
        {
            var settings = _settingsFactory.Create();
            settings.IgnoreMultipleFlushes = true;
            var result = _stateMachineFactory.Create(settings);

            return result;
        }

        /// <inheritdoc/>
        public async Task<ITransaction<MsSqliteTransaction>> CreateChild(ITransaction parent, CancellationToken cancellationToken)
        {
            var result = await CreateInternal(isRoot: false, cancellationToken);
            await parent.Attach(result, cancellationToken);

            return result;
        }
        /// <inheritdoc/>
        public async Task<ITransaction<MsSqliteTransaction>> CreateRoot(CancellationToken cancellationToken) =>
            await CreateInternal(isRoot: true, cancellationToken);
        /// <inheritdoc/>
        public Task<ITransaction<MsSqliteTransaction>> CreateChild(ITransaction<MsSqliteTransaction> parent, CancellationToken cancellationToken)
        {
            var stateMachine = CreateStateMachine();
            var result = new SqliteTransaction(false, parent.Context, stateMachine);

            return Task.FromResult<ITransaction<MsSqliteTransaction>>(result);
        }

        /// <summary>
        /// Initializes the database using the scripts provided.
        /// </summary>
        /// <param name="scripts">The initialization scripts to execute.</param>
        /// <param name="progress">The progress to use for reporting on the initialization process.</param>
        /// <returns></returns>
        public void Initialize(IEnumerable<IScript> scripts, IProgress<IScript>? progress = null)
        {
            SqliteConnection connection;
            if(_memoryConnection != null)
            {
                connection = _memoryConnection;
            } else
            {
                connection = new SqliteConnection(_connectionString);
                connection.Open();
            }
            
            try
            {
                foreach(var script in scripts)
                {
                    script.Execute(connection);
                    progress?.Report(script);
                }
            } finally
            {
                if(connection != _memoryConnection)
                {
                    connection.Dispose();
                }
            }
        }
        /// <inheritdoc/>
        public void Dispose() => _memoryConnection?.Dispose();
    }
}