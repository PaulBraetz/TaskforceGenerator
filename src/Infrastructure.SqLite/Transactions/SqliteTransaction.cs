﻿using Microsoft.Data.Sqlite;

using SQLitePCL;

using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Transactions.Abstractions;

using MsSqliteTransaction = Microsoft.Data.Sqlite.SqliteTransaction;

namespace TaskforceGenerator.Infrastructure.SqLite.Transactions;

/// <summary>
/// Sqlite transaction wrapper for instances of <see cref="SqliteConnection"/>.
/// </summary>
public sealed partial class SqliteTransaction : TransactionBase<MsSqliteTransaction>
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="isRoot">Indicates whether this instance is a roo transaction.</param>
    /// <param name="transaction">The underlying sqite transaction.</param>
    /// <param name="stateMachine">The state machine to use for managing the transactional state.</param>
    public SqliteTransaction(Boolean isRoot, MsSqliteTransaction transaction, ITransactionStateMachine stateMachine)
        : base(stateMachine, transaction)
    {
        if(transaction.Connection == null)
        {
            throw new ArgumentException("The transaction passed does not have a connection. It is possible that the transaction has already been committed or disposed.", nameof(transaction));
        }

        _isRoot = isRoot;
        _savepointName = $"Savepoint_Transaction_{Name}";
        _connection = transaction.Connection;
    }

    private async Task OnCommitted(Object? _, IAsyncEventArgs args)
    {
        await Context.ReleaseAsync(_savepointName, args.CancellationToken);

        if(_isRoot)
        {
            await Context.CommitAsync(args.CancellationToken);
        }
    }

    private Task OnRolledBack(Object? _, IAsyncEventArgs args) =>
        Context.RollbackAsync(_savepointName, args.CancellationToken);

    private readonly Boolean _isRoot;
    private readonly String _savepointName;
    private readonly SqliteConnection _connection;

    /// <inheritdoc/>
    public override async Task Flush(CancellationToken cancellationToken)
    {
        try
        {
            await ((ITransactionStateMachine)this).Flush(forceRollback: false, default);
        } finally
        {
            if(_isRoot)
            {
                await Context.DisposeAsync();
                await _connection.DisposeAsync();
            }
        }
    }
    /// <inheritdoc/>
    public override String ToString()
    {
        var rootString = _isRoot ? " (root)" : String.Empty;
        return $"{Name}{rootString}: {StateMachine}";
    }
}