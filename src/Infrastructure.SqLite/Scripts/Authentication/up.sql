--SQLite
create table if not exists CitizenConnections(
    CitizenName text primary key not null collate nocase, 
    Code text not null,
    Password_Hash blob not null, 
    Password_DegreeOfParallelism integer check(Password_DegreeOfParallelism > 0) , 
    Password_Iterations integer check(Password_Iterations > 0), 
    Password_MemorySize integer check(Password_MemorySize > 0), 
    Password_OutputLength integer check(Password_OutputLength > 32), 
    Password_Salt blob not null, 
    Password_AssociatedData blob not null, 
    Password_KnownSecret blob not null,
	foreign key(CitizenName) references Citizens(CitizenName))
