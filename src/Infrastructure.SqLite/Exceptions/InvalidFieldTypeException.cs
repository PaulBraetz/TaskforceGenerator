﻿namespace TaskforceGenerator.Infrastructure.SqLite.Exceptions;

/// <summary>
/// Thrown when a field does not have the type expected when reconstituting an entity or value.
/// </summary>
internal sealed class InvalidFieldTypeException : Exception
{
    public InvalidFieldTypeException(Type expectedType, Type actualType) : base($"Invalid field type encountered. Expected {expectedType}. Actual {actualType}")
    {
        ExpectedType = expectedType;
        ActualType = actualType;
    }

    public Type ExpectedType { get; }
    public Type ActualType { get; }
}
