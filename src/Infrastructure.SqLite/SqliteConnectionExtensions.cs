﻿using Microsoft.Data.Sqlite;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Infrastructure.SqLite;
/// <summary>
/// Contains extensions for <see cref="SqliteConnection"/>.
/// </summary>
public static class SqliteConnectionExtensions
{
    /// <summary>
    /// Creates a new command associated with the connection.
    /// </summary>
    /// <param name="connection">The connection using which to create the command.</param>
    /// <param name="commandText">The value to assign to the created commands <c>CommandText</c> property.</param>
    /// <returns>The new command.</returns>
    public static SqliteCommand CreateCommand(this SqliteConnection connection, String commandText)
    {
        var result = connection.CreateCommand();
        result.CommandText = commandText;

        return result;
    }
}
