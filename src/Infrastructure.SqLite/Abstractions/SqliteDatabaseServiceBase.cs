﻿using MsSqliteTransaction = Microsoft.Data.Sqlite.SqliteTransaction;

using TaskforceGenerator.Common.Transactions.Abstractions;

namespace TaskforceGenerator.Infrastructure.SqLite.Abstractions;
/// <summary>
/// Base class for types implementing a 
/// </summary>
/// <typeparam name="TSchema">The type of schema required by the service.</typeparam>
public abstract class SqliteDatabaseServiceBase<TSchema>
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="schema">The schema information required by the service.</param>
    /// <param name="transactionFactory">The factory to use when creating new or nested transactions.</param>
    protected SqliteDatabaseServiceBase(
        TSchema schema,
        ITransactionFactory<ITransaction<MsSqliteTransaction>, MsSqliteTransaction> transactionFactory)
    {
        Schema = schema;
        TransactionFactory = transactionFactory;
    }
    /// <summary>
    /// Gets the database.
    /// </summary>
    protected ITransactionFactory<ITransaction<MsSqliteTransaction>, MsSqliteTransaction> TransactionFactory { get; }
    /// <summary>
    /// Gets the required schema.
    /// </summary>
    protected TSchema Schema { get; }
}
