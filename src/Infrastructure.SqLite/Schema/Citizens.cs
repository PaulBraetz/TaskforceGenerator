﻿namespace TaskforceGenerator.Infrastructure.SqLite.Schema;

/// <summary>
/// Contains information on the column names of the <c>Citizens</c> table.
/// </summary>
public sealed class Citizens
{
    /// <summary>
    /// Gets the name of the table.
    /// </summary>
    public String TableName { get; init; } = "Citizens";
    /// <summary>
    /// Gets the name of the <c>CitizenName</c> column.
    /// </summary>
    public Field CitizenName { get; init; } = new("CitizenName", "citizenName");
}
