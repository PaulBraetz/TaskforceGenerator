﻿namespace TaskforceGenerator.Infrastructure.SqLite.Schema;

/// <summary>
/// Contains information on the names of tables and their fields.
/// </summary>
public sealed class Tables
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="citizenConnections">The citizen connections table schema.</param>
    /// <param name="citizens">The citizen table schema.</param>
    public Tables(CitizenConnections citizenConnections, Citizens citizens)
    {
        CitizenConnections = citizenConnections;
        Citizens = citizens;
    }

    /// <summary>
    /// Gets the column names for the <c>CitizenConnections</c> table.
    /// </summary>
    public CitizenConnections CitizenConnections { get; init; } = new();
    /// <summary>
    /// Gets the column names for the <c>Citizens</c> table.
    /// </summary>
    public Citizens Citizens { get; init; } = new();
}
