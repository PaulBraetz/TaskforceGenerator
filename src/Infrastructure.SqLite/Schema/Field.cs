﻿namespace TaskforceGenerator.Infrastructure.SqLite.Schema;

/// <summary>
/// Contains information on a single field.
/// </summary>
public sealed class Field
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="fieldName">The name of this field.</param>
    /// <param name="paramName">The default parameter name for parameterized queries involving this field.</param>
    public Field(String fieldName, String paramName)
    {
        FieldName = fieldName;
        ParamName = $"${paramName}";
    }

    /// <summary>
    /// Gets the name of this field.
    /// </summary>
    public String FieldName { get; }
    /// <summary>
    /// Gets the default parameter name for parameterized queries involving this field.
    /// </summary>
    public String ParamName { get; }
    /// <inheritdoc/>
    public override String ToString() => FieldName;
}
