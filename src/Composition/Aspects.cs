﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using OneOf;

using SimpleInjector;

using TaskforceGenerator.Application.Requests;
using TaskforceGenerator.Application.Results;
using TaskforceGenerator.Application.Services;
using TaskforceGenerator.Aspects;
using TaskforceGenerator.Aspects.Abstractions;
using TaskforceGenerator.Aspects.Decorators;
using TaskforceGenerator.Aspects.Proxies;
using TaskforceGenerator.Aspects.Services;
using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Formatters;
using TaskforceGenerator.Common.Results;
using TaskforceGenerator.Domain.Authentication;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Authentication.Requests;
using TaskforceGenerator.Domain.Authentication.Formatters;
using TaskforceGenerator.Domain.Authentication.Results;
using TaskforceGenerator.Domain.Core.Abstractions;
using TaskforceGenerator.Domain.Core.Queries;
using TaskforceGenerator.Domain.Core.Results;
using TaskforceGenerator.Infrastructure.Queries;
using TaskforceGenerator.Presentation.Models;
using TaskforceGenerator.Presentation.Models.Formatters;

namespace TaskforceGenerator.Composition;

/// <summary>
/// Contains Object tree definitions for cross-cutting concerns.
/// </summary>
internal static class Aspects
{
#pragma warning disable IDE0053 // Use expression body for lambda expression
#pragma warning disable IDE0200 // Remove unnecessary lambda expression
    public static IRoot LocalGui { get; } =
        Root.Create(c =>
        {
            RegisterCoreAspects(c);
        });
#pragma warning restore IDE0200 // Remove unnecessary lambda expression
#pragma warning restore IDE0053 // Use expression body for lambda expression
    /// <summary>
    /// Defines cross-cutting concerns.
    /// </summary>
    public static IRoot WebGui { get; } =
        Root.Create(c =>
        {
            RegisterCoreAspects(c);
            RegisterHttpContextLoggingDecorator(c);
        });

    private static void RegisterCoreAspects(Container c)
    {
        RegisterFormatters(c);
        RegisterLoggingService(c);

        RegisterConnectionEnsuringDecorators(c);
        RegisterCachedQueryProxies(c);
        RegisterServiceCancellationDecorators(c);
        RegisterNotificationDecorators(c);
        RegisterCitizenNameReplacementProxies(c);
        //Consider enabling this in contexts where the Ui sync context only allows for a single thread.
        //RegisterSyncContextInterceptionProxies(c);
        RegisterLoggingDecorators(c);
    }
    private static void RegisterServiceCancellationDecorators(Container c) =>
        c.RegisterDecorator(typeof(IService<>), typeof(ServiceCancellationDecorator<>), Lifestyle.Scoped);

    private static void RegisterNotificationDecorators(Container c)
    {
        c.RegisterDecorator(
            typeof(IService<,>),
            typeof(NotifySuccessServiceDecorator<,>),
            Lifestyle.Scoped,
            IsApplicationService);
        c.RegisterDecorator(
            typeof(IService<,>),
            typeof(NotifyExceptionServiceDecorator<,>),
            IsApplicationService);
    }
    private static void RegisterHttpContextLoggingDecorator(Container c)
    {
        c.RegisterDecorator(
            typeof(IService<,>),
            typeof(HttpContextConnectionIdLoggingServiceDecorator<,>),
            Lifestyle.Scoped,
            IsApplicationService);
    }

    private static void RegisterLoggingDecorators(Container c)
    {
        //timestamp->execution->thread->exception->time->service

        c.RegisterDecorator(typeof(IService<,>), typeof(ExecutionTimeLoggingServiceDecorator<,>), Lifestyle.Scoped);
        c.RegisterDecorator(typeof(IService<,>), typeof(ExceptionLoggingServiceDecorator<,>), Lifestyle.Scoped);
        c.RegisterDecorator(typeof(IService<,>), typeof(ThreadIdLoggingServiceDecorator<,>), Lifestyle.Scoped);
        c.RegisterDecorator(typeof(IService<,>), typeof(ExecutionLoggingServiceDecorator<,>), Lifestyle.Scoped);
        c.RegisterDecorator(typeof(IService<,>), ctx =>
        {
            var implementationType = ctx.ImplementationType;

            /*
            Not required for now, as proxied service is decorated also.
            Thus, both the proxy service as well as the proxied service will be logged in full.
            if(implementationType.IsConstructedGenericType)
            {
                var typeDef = implementationType.GetGenericTypeDefinition();
                if(typeDef == typeof(ProxyService<,,>))
                {
                    //replace impl type using proxied service type
                    implementationType = implementationType.GenericTypeArguments[2];
                }
            }
            */

            var serviceArgs = ctx.ServiceType.GenericTypeArguments;
            var result = typeof(ServiceTypeLoggingDecorator<,,>)
                .MakeGenericType(serviceArgs[0], serviceArgs[1], implementationType);

            return result;
        }, Lifestyle.Scoped, ctx => true);
        c.RegisterDecorator(
            typeof(IService<,>),
            typeof(TimestampLoggingServiceDecorator<,>),
            Lifestyle.Scoped,
            IsApplicationService);
    }

    private static void RegisterCitizenNameReplacementProxies(Container c)
    {
        RegisterReplacementStrategyConvention(c);

        c.RegisterDecorator(
            typeof(IService<,>),
            typeof(CitizenNameReplacementServiceProxy<,>),
            RequiresNameReplacement);
    }
    private static void RegisterReplacementStrategyConvention(Container c)
    {
        var implementationTypes = typeof(TaskforceGenerator.Aspects.Proxies.ReplacementStrategies.LoginStrategy).Assembly
            .GetTypes()
            .Where(t => t.Namespace == typeof(TaskforceGenerator.Aspects.Proxies.ReplacementStrategies.LoginStrategy).Namespace)
            .Where(t => t.IsPublic);

        c.Register(typeof(ICitizenNameReplacementStrategy<>), implementationTypes);
    }
    private static void RegisterExtractionStrategyConvention(Container c)
    {
        c.Register(typeof(ICitizenNameExtractionStrategy<>),
            typeof(TaskforceGenerator.Aspects.Decorators.ExtractionStrategies.LoginStrategy).Assembly
            .GetTypes()
            .Where(t => t.Namespace == typeof(TaskforceGenerator.Aspects.Decorators.ExtractionStrategies.LoginStrategy).Namespace));
    }
    private static void RegisterConnectionEnsuringDecorators(Container c)
    {
        RegisterExtractionStrategyConvention(c);

        registerQuery<Login, Login.Result>();
        registerQuery<SetPasswordForConnection, SetPasswordForConnection.Result>();
        registerQuery<GenerateBioCode, GenerateBioCode.Result>();

        void registerQuery<TRequest, TResult>()
            where TRequest : IServiceRequest<TResult>
        {
            c.RegisterDecorator<IService<TRequest, TResult>, EnsureCitizenConnectionServiceDecorator<TRequest, TResult>>(Lifestyle.Scoped);
            c.RegisterDecorator<IService<TRequest, TResult>, EnsureCitizenServiceDecorator<TRequest, TResult>>(Lifestyle.Scoped);
        }
    }

    private static void RegisterCachedQueryProxies(Container c)
    {
        RegisterResultCache<LoadCitizenPage, LoadCitizenPage.Result>(c);
        RegisterResultCache<LoadActualName, OneOf<String, CitizenNotExistingResult>>(c);
        RegisterResultCache<CheckCitizenExists, Boolean>(c);
        RegisterResultCache<LoadBio, OneOf<IBio, CitizenNotExistingResult>>(c);
    }

    private static void RegisterLoggingService(Container c)
    {
        c.Register(() => new LoggingService(c.GetRequiredService<ILoggerFactory>().CreateLogger<LoggingService>()), Lifestyle.Scoped);
        c.Register<ILoggingService>(c.GetRequiredService<LoggingService>, Lifestyle.Scoped);
    }

    private static void RegisterFormatters(Container c)
    {
        c.Register<EllipsisFormatter>(Lifestyle.Singleton);
        c.Register<IStaticFormatter<LoadCitizenPage.Result>>(() =>
        {
            var stringFormatter = c.GetInstance<EllipsisFormatter>();
            var result = new FormatterStrategy<LoadCitizenPage.Result>(r => stringFormatter.Format(r));
            return result;
        }, Lifestyle.Singleton);
        c.Register<IStaticFormatter<CreateBio>, CreateBioFormatter>(Lifestyle.Singleton);
        c.Register<IStaticFormatter<CreatePassword>, CreatePasswordFormatter>(Lifestyle.Singleton);
        c.Register<IStaticFormatter<Authenticate>, AuthenticateFormatter>(Lifestyle.Singleton);
        c.Register<IStaticFormatter<BioCodeMismatchResult>, BioCodeMismatchExceptionFormatter>(Lifestyle.Singleton);
        c.RegisterConditional(
            typeof(IStaticFormatter<SetPasswordForConnection>),
            c =>
            {
                var result =
                    c.Consumer != null &&
                    c.Consumer.ImplementationType == typeof(ToastsSuccessModel<SetPasswordForConnection>) ?
                typeof(TaskforceGenerator.Presentation.Models.Formatters.SetPasswordForConnectionFormatter) :
                typeof(TaskforceGenerator.Application.Formatters.SetPasswordForConnectionFormatter);

                return result;
            },
            Lifestyle.Singleton,
            c => c.ServiceType == typeof(IStaticFormatter<SetPasswordForConnection>));
        c.RegisterConditional(
            typeof(IStaticFormatter<Login>),
            c =>
            {
                var result =
                    c.Consumer != null &&
                    c.Consumer.ImplementationType == typeof(ToastsSuccessModel<Login>) ?
                typeof(TaskforceGenerator.Presentation.Models.Formatters.LoginFormatter) :
                typeof(TaskforceGenerator.Application.Formatters.LoginFormatter);

                return result;
            },
            Lifestyle.Singleton,
            c => c.ServiceType == typeof(IStaticFormatter<Login>));
        c.RegisterConditional(
            typeof(IStaticFormatter<>),
            typeof(SlotNameFormatter<>),
            Lifestyle.Singleton,
            c => c.ServiceType.GenericTypeArguments[0].IsAssignableTo(typeof(ISlot)));
        c.RegisterConditional(
            typeof(IStaticFormatter<>),
            typeof(SlotPreferenceNameFormatter<>),
            Lifestyle.Singleton,
            c => c.ServiceType.GenericTypeArguments[0].IsAssignableTo(typeof(ISlotPreference)));
        c.RegisterConditional(
            typeof(IStaticFormatter<>),
            typeof(VehicleNameFormatter<>),
            Lifestyle.Singleton,
            c => c.ServiceType.GenericTypeArguments[0].IsAssignableTo(typeof(IVehicle)));
        c.RegisterConditional(
            typeof(IStaticFormatter<>),
            typeof(TypeNameFormatter<>),
            Lifestyle.Singleton,
            c => !c.Handled && c.ServiceType.GenericTypeArguments[0].IsAssignableTo(typeof(Exception)));
        c.RegisterConditional(
            typeof(IStaticFormatter<>),
            typeof(ToStringFormatter<>),
            Lifestyle.Singleton,
            c => !c.Handled);
    }

    private static void RegisterResultCache<TRequest, TResult>(Container container)
        where TRequest : IServiceRequest<TResult>
    {
        container.Register<ICacheOptions<TRequest>>(
            () => container.GetInstance<IConfiguration>().GetRequiredCacheOptions<TRequest>(),
            Lifestyle.Singleton);
        container.Register<ICache<TRequest, Task<TResult>>, Cache<TRequest, Task<TResult>>>(Lifestyle.Singleton);
        container.RegisterDecorator<IService<TRequest, TResult>, ResultCacheServiceProxy<TRequest, TResult>>(Lifestyle.Scoped);
    }
    private static Boolean RequiresNameReplacement(DecoratorPredicateContext context) =>
        IsApplicationService(context) &&
        IsNotProxyService(context) &&
        !IsExcludedFromNameReplacement(context);

    private static readonly IReadOnlySet<Type> _nameReplacementExcludedServices =
        new HashSet<Type>()
        {
            typeof(CreateEventService)
        };
    private static Boolean IsExcludedFromNameReplacement(DecoratorPredicateContext context)
    {
        var result = _nameReplacementExcludedServices.Contains(context.ImplementationType);

        return result;
    }

    private static Boolean IsNotProxyService(DecoratorPredicateContext context)
    {
        var result = !context.ImplementationType.IsConstructedGenericType ||
            context.ImplementationType.GetGenericTypeDefinition() != typeof(ProxyService<,,>);

        return result;
    }
    private static Boolean IsApplicationService(DecoratorPredicateContext context)
    {
        var result = context.ImplementationType.Assembly ==
            typeof(SetPasswordForConnection).Assembly;

        return result;
    }
}
