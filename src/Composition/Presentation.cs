﻿namespace TaskforceGenerator.Composition;

/// <summary>
/// Contains Object tree definitions for the presentation layer.
/// </summary>
internal static partial class Presentation
{
    /// <summary>
    /// Defines the presentation layer for photino guis.
    /// </summary>
    public static IRoot WebGui { get; } =
        Root.Create(
            ConnectCitizen.Root,
            Login.Root,
            BuildTaskforce.Root,
            CreateEvent.Root,
            EnlistToEvent.Root,
            PresentationBase.Root
            );
    /// <summary>
    /// Defines the presentation layer for web guis.
    /// </summary>
    public static IRoot LocalGui { get; } =
        Root.Create(
            ConnectCitizen.Root,
            Login.Root,
            BuildTaskforce.Root,
            CreateEvent.Root,
            EnlistToEvent.Root,
            PresentationBase.Root
        );
}
