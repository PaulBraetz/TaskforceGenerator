﻿using Microsoft.AspNetCore.Mvc.ApplicationModels;

using OneOf;

using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Results;
using TaskforceGenerator.Domain.Authentication.Results;
using TaskforceGenerator.Domain.Core;
using TaskforceGenerator.Presentation.Models;
using TaskforceGenerator.Presentation.Models.Abstractions;
using TaskforceGenerator.Presentation.Models.Abstractions.CreateEvent;
using TaskforceGenerator.Presentation.Models.CreateEvent;
using TaskforceGenerator.Presentation.Models.Fakes;
using TaskforceGenerator.Presentation.Models.Formatters;

using CreateEvenTRequest = TaskforceGenerator.Application.Requests.CreateEvent;

namespace TaskforceGenerator.Composition;

internal static partial class Presentation
{
    /// <summary>
    /// Contains object tree definitions for the create event models and views.
    /// </summary>
    private static class CreateEvent
    {
        public static IRoot Root { get; } =
            Composition.Root.Create(c =>
            {
                c.Register<ICreateEventModel>(() =>
                {
                    var create = c.GetInstance<IButtonModel>();
                    var inputs = c.GetInstance<ICreateEventInputGroupModel>();
                    var service = c.GetInstance<
                        IService<CreateEvenTRequest, OneOf<InviteCode, CompliantlyCancelledResult, NotAuthenticatedResult>>>();
                    var hostingInfo = c.GetInstance<IHostingInformation>();
                    var formatter = new InviteCodeFormatter(hostingInfo);
                    var defaultValueProvider = c.GetInstance<IDefaultValueProvider<String?>>();
                    var inviteCode = new DisplayModel<String?>(defaultValueProvider, formatter);

                    var result = new CreateEventModel(create, inputs, service, inviteCode);

                    return result;
                });
                c.Register<ICreateEventInputGroupModel, CreateEventInputGroupModel>();
            });
    }
}
