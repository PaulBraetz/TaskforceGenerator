﻿using OneOf;

using TaskforceGenerator.Application.Requests;
using TaskforceGenerator.Application.Results;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Results;
using TaskforceGenerator.Domain.Authentication;
using TaskforceGenerator.Domain.Authentication.Results;
using TaskforceGenerator.Domain.Core.Results;
using TaskforceGenerator.Presentation.Models;
using TaskforceGenerator.Presentation.Models.Abstractions;
using TaskforceGenerator.Presentation.Models.Abstractions.ConnectCitizen;
using TaskforceGenerator.Presentation.Models.ConnectCitizen;

namespace TaskforceGenerator.Composition;

internal static partial class Presentation
{
    /// <summary>
    /// Contains object tree definitions for the connect citizen models and views.
    /// </summary>
    internal static class ConnectCitizen
    {
        public static IRoot Root { get; } =
                Composition.Root.Create(c =>
            {
                c.Register<IInputGroupModel>(() =>
                {
                    var nameInput = c.GetInstance<CitizenNameInputModel>();
                    var name = new InputGroupModel<String, String>(nameInput);

                    var passwordInput = c.GetInstance<PasswordCreationInputModel>();
                    var password = new InputGroupModel<String, PasswordValidity>(passwordInput);

                    var result = new InputGroupModel(name, password);

                    return result;
                });
                c.Register<IButtonGroupModel, ButtonGroupModel>();
                c.Register<ICodeModel, CodeModel>();
                c.Register<IConnectCitizenModel>(() =>
                {
                    var service = c.GetInstance<IService<SetPasswordForConnection, SetPasswordForConnection.Result>>();
                    var codeService = c.GetInstance<IService<GenerateBioCode, GenerateBioCode.Result>>();
                    var navigationManager = c.GetInstance<INavigationManager>();
                    var path = PathModel.Relative("/login");
                    var navigation = new NavigationModel(navigationManager, path);

                    var inputs = c.GetInstance<IInputGroupModel>();
                    var buttons = c.GetInstance<IButtonGroupModel>();
                    var code = c.GetInstance<ICodeModel>();

                    var userContext = c.GetInstance<IUserContextCitizenModel>();

                    var error = c.GetInstance<IDisplayModel<String?>>();

                    var result = ConnectCitizenModel.Create(
                        service,
                        codeService,
                        navigation,
                        inputs,
                        buttons,
                        code,
                        userContext,
                        error);

                    return result;
                });
            });
    }
}