﻿using SimpleInjector;
using TaskforceGenerator.Application.Requests;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Core;
using TaskforceGenerator.Domain.Core.Abstractions;
using TaskforceGenerator.Domain.Core.Queries;
using TaskforceGenerator.Presentation.Models;
using TaskforceGenerator.Presentation.Models.Abstractions;
using TaskforceGenerator.Presentation.Models.Abstractions.BuildTaskforce;
using TaskforceGenerator.Presentation.Models.BuildTaskforce;

namespace TaskforceGenerator.Composition;

internal static partial class Presentation
{
    /// <summary>
    /// Contains object tree definitions for the connect citizen models and views.
    /// </summary>
    private static class BuildTaskforce
    {
        public static IRoot Root { get; } =
            Composition.Root.Create(c =>
            {
                c.Register<IFactory<IOccupantModel>>(() =>
                {
                    var slotModelFactory = c.GetInstance<IFactory<ISelectInputGroupModel<ISlotPreference, String>>>();
                    var nameInputFactory = c.GetInstance<CitizenNameInputModelFactory>();
                    var nameFactory = new InputGroupModelFactory<String, String>(nameInputFactory);
                    var createService = c.GetInstance<IService<ProxyServiceRequest<CreateOccupant, IOccupant>, IOccupant>>();

                    var result = new OccupantModelFactory(nameFactory, slotModelFactory, createService);

                    return result;
                }, Lifestyle.Scoped);
                c.Register<IDynamicMultiControlModel<ISelectVehiclesModel>>(() =>
                {
                    var buttonModelFactory = c.GetInstance<IFactory<IButtonModel>>();
                    var vehiclesModelFactory = c.GetInstance<IFactory<ISelectVehiclesModel>>();

                    var vehiclesItemFactory = new DynamicMultiControlItemModelFactory<ISelectVehiclesModel>(buttonModelFactory, vehiclesModelFactory);
                    var addVehiclesButton = c.GetInstance<IButtonModel>();
                    var result = new DynamicMultiControlModel<ISelectVehiclesModel>(addVehiclesButton, vehiclesItemFactory);

                    return result;
                });
                c.Register<IDynamicMultiControlModel<IOccupantModel>>(() =>
                {
                    var buttonModelFactory = c.GetInstance<IFactory<IButtonModel>>();
                    var memberModelFactory = c.GetInstance<IFactory<IOccupantModel>>();

                    var memberItemFactory = new DynamicMultiControlItemModelFactory<IOccupantModel>(buttonModelFactory, memberModelFactory);
                    var addMemberButton = c.GetInstance<IButtonModel>();
                    var result = new DynamicMultiControlModel<IOccupantModel>(addMemberButton, memberItemFactory);

                    return result;
                });
                c.Register<IInputGroupModel>(() =>
                {
                    var schemeProvider = c.GetInstance<IValueSetProvider<ICrewingScheme>>();
                    var schemeNameProvider = new SchemeNameProvider(schemeProvider);
                    var schemeOptionsFactory = c.GetInstance<IOptionModelFactory<String>>();
                    var schemeOptionsProvider = new OptionsProviderAdapter<String>(schemeNameProvider, schemeOptionsFactory);

                    var selectInputModelFactory = new SelectInputModelFactory<String, String>(
                        schemeOptionsProvider,
                        c.GetInstance<IDefaultValueProviderFactory<IOptionModel<String>?>>(),
                        c.GetInstance<IDefaultValueProvider<String>>());

                    var schemeSelectModelFactory =
                        new SelectInputGroupModelFactory<String, String>(selectInputModelFactory);
                    var schemeSelectModel = schemeSelectModelFactory.Create();

                    var occupantControl = c.GetInstance<IDynamicMultiControlModel<IOccupantModel>>();
                    var vehicleControl = c.GetInstance<IDynamicMultiControlModel<ISelectVehiclesModel>>();

                    var result = new InputGroupModel(occupantControl, vehicleControl, schemeSelectModel);

                    return result;
                });
                c.Register<IBuildTaskforceModel>(() =>
                {
                    var button = c.GetInstance<IButtonModel>();
                    var inputGroup = c.GetInstance<IInputGroupModel>();

                    var result = BuildTaskforceModel.Create(button, inputGroup);

                    return result;
                });
            });
    }
}
