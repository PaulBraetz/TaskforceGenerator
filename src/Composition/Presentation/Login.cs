﻿using OneOf;

using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Results;
using TaskforceGenerator.Domain.Authentication.Results;
using TaskforceGenerator.Domain.Core.Results;
using TaskforceGenerator.Presentation.Models;
using TaskforceGenerator.Presentation.Models.Abstractions;
using TaskforceGenerator.Presentation.Models.Abstractions.Login;
using TaskforceGenerator.Presentation.Models.Login;

namespace TaskforceGenerator.Composition;

internal static partial class Presentation
{
    /// <summary>
    /// Contains object tree definitions for the login models and views.
    /// </summary>
    internal static class Login
    {
        public static IRoot Root { get; } =
            Composition.Root.Create(c =>
        {
            c.Register<IInputGroupModel>(() =>
            {
                var nameInput = c.GetInstance<CitizenNameInputModel>();
                var name = new InputGroupModel<String, String>(nameInput);

                var password = c.GetInstance<IInputGroupModel<String, String>>();

                var result = new InputGroupModel(name, password);

                return result;
            });
            c.Register<ILoginModel>(() =>
            {
                var navigationManager = c.GetInstance<INavigationManager>();
                var path = PathModel.Relative("/");
                var navigation = new NavigationModel(navigationManager, path);

                var loginService = c.GetInstance<
                    IService<TaskforceGenerator.Application.Requests.Login,
                    TaskforceGenerator.Application.Requests.Login.Result>>();
                var button = c.GetInstance<IButtonModel>();
                var inputs = c.GetInstance<IInputGroupModel>();

                var result = LoginModel.Create(
                    loginService,
                    navigation,
                    button,
                    inputs);

                return result;
            });
        });
    }
}
