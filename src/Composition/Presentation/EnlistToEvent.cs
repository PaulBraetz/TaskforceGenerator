﻿using TaskforceGenerator.Common;
using TaskforceGenerator.Presentation.Models;
using TaskforceGenerator.Presentation.Models.Abstractions;
using TaskforceGenerator.Presentation.Models.Abstractions.EnlistToEvent;
using TaskforceGenerator.Presentation.Models.EnlistToEvent;
using TaskforceGenerator.Presentation.Models.Fakes;

namespace TaskforceGenerator.Composition;

internal static partial class Presentation
{
    /// <summary>
    /// Contains object tree definitions for the create event models and views.
    /// </summary>
    private static class EnlistToEvent
    {
        public static IRoot Root { get; } =
            Composition.Root.Create(c =>
            {
                c.Register<IEnlistToEventModel, EnlistToEventModelFake>();
                c.Register<IDisplayGroupModel>(() =>
                {
                    var eventName = c.GetInstance<IDisplayModel<String>>();
                    var organizerName = c.GetInstance<IDisplayModel<String>>();
                    var valueDefaultProvider = c.GetInstance<IDefaultValueProvider<DateTime>>();
                    var formatter = new FormatterStrategy<DateTime>(v =>
                    {
                        var t = v - DateTime.Now;
                        return $"{v:f} (ETA {(Int64)t.TotalHours}h {t.Minutes}m)";
                    });
                    var eventDate = new DisplayModel<DateTime>(valueDefaultProvider, formatter);
                    var description = c.GetInstance<IDisplayModel<String>>();
                    var result = new DisplayGroupModel(eventName, organizerName, eventDate, description);

                    return result;
                });
                c.Register<IInviteLinkInputModel>(() =>
                {
                    var linkInput = c.GetInstance<IInputGroupModel<String, String>>();
                    var parsedCode = c.GetInstance<IDisplayModel<String>>();
                    var enlist = c.GetInstance<IButtonModel>();

                    var navigationManager = c.GetInstance<INavigationManager>();
                    var path = PathModel.Relative("/enlist");
                    var navigation = new NavigationModel(navigationManager, path);

                    var result = new InviteLinkInputModel(
                        linkInput,
                        parsedCode,
                        enlist,
                        navigation);

                    return result;
                });
            });
    }
}
