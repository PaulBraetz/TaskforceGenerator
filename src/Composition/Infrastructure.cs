﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using SimpleInjector;

using System.ComponentModel;

using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Transactions;
using TaskforceGenerator.Common.Transactions.Abstractions;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Core.Queries;
using TaskforceGenerator.Infrastructure.Fakes;
using TaskforceGenerator.Infrastructure.Queries;
using TaskforceGenerator.Infrastructure.Services;
using TaskforceGenerator.Infrastructure.SqLite;
using TaskforceGenerator.Infrastructure.SqLite.Abstractions;
using TaskforceGenerator.Infrastructure.SqLite.Schema;
using TaskforceGenerator.Infrastructure.SqLite.Services.Authentication;
using TaskforceGenerator.Infrastructure.SqLite.Services.Core;
using TaskforceGenerator.Infrastructure.SqLite.Transactions;

using MsSqliteTransaction = Microsoft.Data.Sqlite.SqliteTransaction;
using TaskforceGenerator.Domain.Core.Requests;
using TaskforceGenerator.Domain.Authentication.Requests;
using TaskforceGenerator.Domain.Authentication.Queries;
using OneOf;
using TaskforceGenerator.Common.Results;
using TaskforceGenerator.Domain.Core.Results;
using TaskforceGenerator.Domain.Authentication.Results;

namespace TaskforceGenerator.Composition;

/// <summary>
/// Contains Object tree definitions for the infrastructure layer.
/// </summary>
internal static class Infrastructure
{
    /// <summary>
    /// Defines the infrastructure layer.
    /// </summary>
    public static IRoot Root { get; } =
        Composition.Root.Create(c =>
        {
            c.Register<IFactory<ITransactionStateMachineSettings>>(() =>
            {
                var result = new FactoryStrategy<ITransactionStateMachineSettings>(() => new TransactionStateMachineSettings());

                return result;
            });
            c.Register<IFactory<IObservableTransactionStateMachine, ITransactionStateMachineSettings>, ObservableTransactionStateMachineFactory>(Lifestyle.Singleton);
            c.Register<ITransactionStateMachineSettings>(() =>
            {
                var config = c.GetInstance<IConfiguration>().GetRequiredSection("TransactionStateMachineSettings");
                var timeout = config.GetRequiredTimespanValue("FamilySynchronizationTimeout");
                var result = new TransactionStateMachineSettings()
                {
                    FamilySynchronizationTimeout = timeout
                };

                return result;
            });

            c.Register<IService<LoadAutoCompleteCitizenName, String?>,
                    LoadAutoCompleteCitizenNameService>
                (Lifestyle.Scoped);

            c.Register<
                IService<CommitCitizen, OneOf<ServiceResult, CitizenAlreadyCommittedResult>>,
                CommitCitizenService>(Lifestyle.Scoped);

            c.Register<IService<LoadCitizenPage, LoadCitizenPage.Result>, LoadCitizenPageService>(Lifestyle.Singleton);
            c.Register<IService<LoadCitizenOrgs, IReadOnlySet<String>>, LoadCitizenOrgsService>(Lifestyle.Scoped);
            c.Register<
                IService<LoadActualName, OneOf<String, CitizenNotExistingResult>>,
                LoadActualNameService>(Lifestyle.Scoped);
            c.Register<IService<LoadBio, OneOf<IBio, CitizenNotExistingResult>>, LoadBioService>(Lifestyle.Scoped);
            c.Register<IService<CheckCitizenExists, Boolean>, CheckCitizenExistsService>(Lifestyle.Singleton);

            c.Register<Tables>(Lifestyle.Singleton);
            c.Register<Citizens>(Lifestyle.Singleton);
            c.Register<CitizenConnections>(Lifestyle.Singleton);
            c.Register<ITransactionFactory<ITransaction<MsSqliteTransaction>, MsSqliteTransaction>>(() =>
                {
                    var config = c.GetRequiredService<IConfiguration>();
                    var connectionString = config.GetRequiredSection("ConnectionStrings")["SQLite"]
                        ?? throw new Exception("No connection String provided for 'SQLite'.");
                    var scripts = config.GetSection("SQLiteScripts")
                        .GetChildren()
                        .Select(s => s.Value)
                        .Where(s => s != null)
                        .Select(s => new FileScript(s!))
                        .ToArray();
                    var logger = c.GetRequiredService<ILoggerFactory>().CreateLogger<SqliteTransaction.Factory>();
                    var progress = new Progress<IScript>(s => logger.LogInformation("Executed {scriptPath}", s.ScriptName));

                    var stateMachineFactory = c.GetInstance<IFactory<IObservableTransactionStateMachine, ITransactionStateMachineSettings>>();
                    var settingsFactory = c.GetInstance<IFactory<ITransactionStateMachineSettings>>();

                    var result = SqliteTransaction.Factory.Create(
                        connectionString,
                        stateMachineFactory,
                        settingsFactory);

                    result.Initialize(scripts, progress);

                    return result;
                }, Lifestyle.Singleton);

            c.Register<IService<CheckConnectionExists, Boolean>, CheckConnectionExistsService>(Lifestyle.Scoped);
            c.Register<IService<CheckCitizenRegistered, Boolean>, CheckCitizenRegisteredService>(Lifestyle.Scoped);
            c.Register<
                IService<ReconstituteConnection, OneOf<ICitizenConnection, CitizenNotRegisteredResult>>,
                ReconstituteConnectionService>(Lifestyle.Scoped);
            c.Register<IService<CommitBioCodeChange>, CommitBioCodeChangeService>(Lifestyle.Scoped);
            c.Register<IService<CommitPasswordChange>, CommitPasswordChangeService>(Lifestyle.Scoped);
            c.Register<
                IService<CommitConnection, OneOf<ServiceResult, ConnectionAlreadyCommittedResult>>,
                CommitConnectionService>(Lifestyle.Scoped);
        });
}
