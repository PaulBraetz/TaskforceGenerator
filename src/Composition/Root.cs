﻿using SimpleInjector;

using TaskforceGenerator.Common;
using TaskforceGenerator.Composition.Domain;

namespace TaskforceGenerator.Composition;

/// <summary>
/// Contains  definitions for different application object graph roots.
/// </summary>
public static partial class Root
{
    /// <summary>
    /// Gets the composition root for a web gui.
    /// </summary>
    public static IRoot WebGui { get; } =
        Create(
            Infrastructure.Root,
            Core.Root,
            Authentication.Root,
            Application.Root,
            Presentation.WebGui,

            Aspects.WebGui
        );

    /// <summary>
    /// Gets the composition root for a Photino gui.
    /// </summary>
    public static IRoot LocalGui { get; } =
        Create(
            Infrastructure.Root,
            Core.Root,
            Authentication.Root,
            Application.Root,
            Presentation.LocalGui,

            Aspects.LocalGui
        );

    /// <summary>
    /// Creates a new composition root using the composition strategy provided.
    /// </summary>
    /// <param name="compose">The strategy using which to compose the application.</param>
    /// <returns>A new composition root based on the composition strategy passed.</returns>
    public static IRoot Create(Action<Container> compose) =>
        new Strategy(compose);
    /// <summary>
    /// Creates a new composite composition root using the roots provided.
    /// </summary>
    /// <param name="roots">The roots defining the composite composition.</param>
    /// <returns>A new composite composition root based on the roots provided.</returns>
    public static IRoot Create(params IRoot[] roots) =>
        new Strategy(c => roots.ForEach(r => r.Compose(c)));
}
