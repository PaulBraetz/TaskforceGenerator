﻿using SimpleInjector;

namespace TaskforceGenerator.Composition;

public static partial class Root
{
    private sealed class Strategy : IRoot
    {
        private readonly Action<Container> _strategy;

        public Strategy(Action<Container> strategy)
        {
            _strategy = strategy;
        }

        public void Compose(Container container) => _strategy.Invoke(container);
    }
}
