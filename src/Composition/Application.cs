﻿using OneOf;

using SimpleInjector;

using TaskforceGenerator.Application.Fakes;
using TaskforceGenerator.Application.Requests;
using TaskforceGenerator.Application.Results;
using TaskforceGenerator.Application.Services;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Results;
using TaskforceGenerator.Domain.Authentication;
using TaskforceGenerator.Domain.Authentication.Requests;
using TaskforceGenerator.Domain.Authentication.Results;
using TaskforceGenerator.Domain.Core;
using TaskforceGenerator.Domain.Core.Abstractions;
using TaskforceGenerator.Domain.Core.Queries;
using TaskforceGenerator.Domain.Core.Results;

namespace TaskforceGenerator.Composition;

/// <summary>
/// Contains Object tree definitions for the application layer.
/// </summary>
internal static class Application
{
    /// <summary>
    /// Defines the application layer.
    /// </summary>
    internal static IRoot Root { get; } =
            Composition.Root.Create(c =>
        {
            RegisterProxy(c, typeof(CheckCitizenExists), typeof(Boolean));
            RegisterProxy(c, typeof(LoadAutoCompleteCitizenName), typeof(String));
            RegisterProxy(c, typeof(CreateOccupant), typeof(IOccupant));

            c.Register<
                IService<Login, Login.Result>,
                LoginService>(Lifestyle.Scoped);
            c.Register<
                IService<SetPasswordForConnection, SetPasswordForConnection.Result>,
                SetPasswordForCitizenService>(Lifestyle.Scoped);
            c.Register<
                IService<GenerateBioCode, GenerateBioCode.Result>,
                GenerateBioCodeService>(Lifestyle.Scoped);
            c.Register<
                IService<TaskforceGenerator.Application.Requests.CreateEvent, OneOf<InviteCode, CompliantlyCancelledResult, NotAuthenticatedResult>>,
                CreateEventService>(Lifestyle.Scoped);
        });

    private static void RegisterProxy(Container c, Type requestType, Type resultType)
    {
        var serviceType = typeof(IService<,>).MakeGenericType(requestType, resultType);
        var implementationProducer = c.GetCurrentRegistrations()
            .SingleOrDefault(r => r.ServiceType == serviceType) ??
            throw new InvalidOperationException("No service implementation underlying the proxy has been registered.");

        var implementationType = implementationProducer.ImplementationType;

        var proxyRequestType = typeof(ProxyServiceRequest<,>).MakeGenericType(requestType, resultType);
        var proxyServiceType = typeof(IService<,>).MakeGenericType(proxyRequestType, resultType);
        var proxyImplementationType = typeof(ProxyService<,,>).MakeGenericType(requestType, resultType, implementationType);

        c.Register(proxyServiceType, proxyImplementationType, Lifestyle.Scoped);
    }
}
