﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using OneOf;

using SimpleInjector;

using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Results;
using TaskforceGenerator.Domain.Authentication;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Authentication.Requests;
using TaskforceGenerator.Domain.Authentication.Fakes;
using TaskforceGenerator.Domain.Authentication.Results;
using TaskforceGenerator.Domain.Authentication.Services;
using TaskforceGenerator.Domain.Core.Results;

namespace TaskforceGenerator.Composition.Domain;

/// <summary>
/// Contains Object tree definitions for the domain layer.
/// </summary>
internal static class Authentication
{

    /// <summary>
    /// Defines a domain layer based on default implementations.
    /// </summary>
    public static IRoot Root { get; } =
            Composition.Root.Create(c =>
        {
            c.Register<IPasswordGuideline>(() =>
            {
                var config = c.GetRequiredService<IConfiguration>();
                var guidelineConfig = config.GetRequiredSection("PasswordGuidline");
                var rules = guidelineConfig.GetSection("Patterns")
                    .GetChildren()
                    .Select(c =>
                    {
                        return (IPasswordRule)new RegexPasswordRule(
                            c.GetRequiredString("Name", "PasswordGuideline:Patterns"),
                            c.GetRequiredString("Description", "PasswordGuideline:Patterns"),
                            c.GetRequiredString("Pattern", "PasswordGuideline:Patterns"));
                    })
                    .ToHashSet();
                var result = new PasswordGuideline(rules);

                return result;
            }, Lifestyle.Singleton);
            c.Register<IService<CreatePasswordParameters, PasswordParameters>>(() =>
            {
                var config = c.GetRequiredService<IConfiguration>();
                var parametersConfig = config.GetSection("PasswordParameters");
                var saltLength = parametersConfig.GetRequiredInt32Value("SaltLength");
                var prototypeConfig = parametersConfig.GetRequiredSection("NumericsPrototype");
                var numericsPrototype = new PasswordParameterNumerics(
                    Iterations: prototypeConfig.GetRequiredInt32Value("Iterations"),
                    DegreeOfParallelism: prototypeConfig.GetRequiredInt32Value("DegreeOfParallelism"),
                    MemorySize: prototypeConfig.GetRequiredInt32Value("MemorySize"),
                    OutputLength: prototypeConfig.GetRequiredInt32Value("OutputLength"));

                var result = new CreatePasswordParametersService(saltLength, numericsPrototype);

                return result;
            }, Lifestyle.Singleton);
            c.RegisterDecorator<
                IService<CreatePassword, OneOf<Password, PasswordCreationGuidelineViolatedResult>>,
                CreatePasswordServiceValidation>(Lifestyle.Scoped);
            c.Register<IService<CreateBio, IBio>, BioFactory>(Lifestyle.Scoped);
            c.Register<
                IService<CreatePassword, OneOf<Password, PasswordCreationGuidelineViolatedResult>>,
                CreatePasswordService>(Lifestyle.Scoped);

            c.Register<IService<CreateConnection, ICitizenConnection>, CreateConnectionService>(Lifestyle.Scoped);

#if DEBUG
            c.Register<
                IService<VerifyBioCode, OneOf<VerifyBioCodeResult, CitizenNotRegisteredResult, CitizenNotExistingResult>>,
                VerifyBioCodeAlwaysMatchService>(Lifestyle.Scoped);
#else
            c.Register<
                IService<VerifyBioCode, OneOf<VerifyBioCodeResult, CitizenNotRegisteredResult, CitizenNotExistingResult>>,
                VerifyBioCodeService>(Lifestyle.Scoped);
#endif

            c.Register<IService<EnsureCitizenConnection, ServiceResult>, EnsureCitizenConnectionService>(Lifestyle.Scoped);
            c.Register<IService<OpenConnection, OneOf<ServiceResult, ConnectionAlreadyCommittedResult>>, OpenConnectionService>(Lifestyle.Scoped);
            c.Register<IService<GenerateRandomBioCode, BioCode>>(() =>
            {
                var bioCodeTokenCount = c.GetRequiredService<IConfiguration>().GetRequiredInt32Value("BioCodeTokenCount");
                var result = new GenerateRandomBioCodeService(bioCodeTokenCount);

                return result;
            }, Lifestyle.Scoped);
            c.Register<IService<GenerateInitialPassword, Password>>(() =>
            {
                var generatedPasswordsLength = c.GetRequiredService<IConfiguration>().GetRequiredInt32Value("GeneratedPasswordsLength");
                var parametersService = c.GetRequiredService<IService<CreatePasswordParameters, PasswordParameters>>();
                var passwordService = c.GetRequiredService<IService<CreatePassword, OneOf<Password, PasswordCreationGuidelineViolatedResult>>>();

                var result = new GenerateInitialPasswordService(passwordService, parametersService, generatedPasswordsLength);

                return result;
            }, Lifestyle.Scoped);

            c.Register<IObservableUserContext, UserContext>(Lifestyle.Scoped);
            c.Register<IUserContext, UserContext>(Lifestyle.Scoped);

            c.Register<IService<Authenticate, OneOf<ServiceResult, PasswordMismatchResult>>, AuthenticateService>(Lifestyle.Scoped);
            c.Register<IService<SetContextConnection>, SetContextConnectionService>(Lifestyle.Scoped);
            c.Register<
                IService<GetContextConnection, OneOf<ICitizenConnection, NotAuthenticatedResult>>,
                GetContextConnectionService>(Lifestyle.Scoped);
        });
}
