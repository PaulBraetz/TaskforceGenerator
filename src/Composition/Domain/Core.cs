﻿using SimpleInjector;

using TaskforceGenerator.Application.Services;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Core;
using TaskforceGenerator.Domain.Core.Abstractions;
using TaskforceGenerator.Domain.Core.Fakes;
using TaskforceGenerator.Domain.Core.Queries;
using TaskforceGenerator.Domain.Core.Services;
using TaskforceGenerator.Domain.Core.Requests;
using OneOf;
using TaskforceGenerator.Common.Results;
using TaskforceGenerator.Domain.Core.Results;

namespace TaskforceGenerator.Composition.Domain;

/// <summary>
/// Contains Object tree definitions for the domain layer.
/// </summary>
internal static class Core
{
    /// <summary>
    /// Defines a domain layer based on default implementations.
    /// </summary>
    public static IRoot Root { get; } =
            Composition.Root.Create(c =>
        {
            c.Register<ICrewingSchemeMap>(() =>
            {
                var provier = c.GetInstance<IValueSetProvider<ICrewingScheme>>();
                var result = CrewingSchemeMap.Create(provier);

                return result;
            }, Lifestyle.Singleton);
            c.Register<IValueSetProvider<ISlot>, SlotProvider>(Lifestyle.Singleton);
            c.Register<IValueSetProvider<ISlotPreference>, SlotPreferenceProvider>(Lifestyle.Singleton);
            c.Register<IValueSetProvider<IVehicle>, VehicleProvider>(Lifestyle.Singleton);
            c.Register<IValueSetProvider<ICareer>, CareerProvider>(Lifestyle.Singleton);
            c.Register<IValueSetProvider<ICrewingScheme>, SchemeProvider>(Lifestyle.Singleton);

            c.Register<
                IService<EnsureCitizen, OneOf<ServiceResult, CitizenNotExistingResult>>,
                EnsureCitizenService>(Lifestyle.Scoped);
            c.Register<IService<CommitEvent>, CommitEventServiceFake>(Lifestyle.Scoped);

            c.Register<IService<CreateCitizen, ICitizen>, CreateCitizenService>(Lifestyle.Scoped);
            c.Register<IService<CreateOccupant, IOccupant>, CreateOccupantService>(Lifestyle.Scoped);
            c.Register<IService<CreateEvent, IEvent>, CreateEventServiceFake>(Lifestyle.Scoped);
            c.Register<
                IService<CreateInviteCode, InviteCode>,
                CreateInviteCodeServiceFake>(Lifestyle.Scoped);
        });
}
