﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using TaskforceGenerator.Domain.Authentication;
using TaskforceGenerator.Domain.Authentication.Abstractions;

namespace TaskforceGenerator.Tests.Common;

/// <summary>
/// Contains helper methods and properties for creating citizens.
/// </summary>
public static class Data
{
    private sealed record Connection(String CitizenName, BioCode Code, Password Password) : ICitizenConnection;
    /// <summary>
    /// Gets a new collection of existing citizen connections, ready for use with <see cref="DynamicDataAttribute"/>.
    /// </summary>
    public static Object[][] Connections
    {
        get =>
            new[] { "SleepWellPupper", "Terrorente", "YokoArashi", "SpaceDirk", "Trinity", "rhobit", "The1Curly" }
            .Select(s => new[] { CreateConnection(s) })
            .ToArray();
    }
    /// <summary>
    /// Creates a new citizen connection.
    /// </summary>
    /// <param name="name">The name of the citizen whose connection to create.</param>
    /// <returns>A new instance of <see cref="ICitizenConnection"/>, initialized with a random password and bio code.</returns>
    public static ICitizenConnection CreateConnection(String name)
    {
        var bioCode = CreateBioCode();
        var password = CreatePassword();
        var result = new Connection(name, bioCode, password);

        return result;
    }
    /// <summary>
    /// Creates a new random bio code.
    /// </summary>
    /// <returns>A new bio code.</returns>
    public static BioCode CreateBioCode()
    {
        var result = new BioCode(Guid.NewGuid().ToString());

        return result;
    }
    /// <summary>
    /// Creates a new random password.
    /// </summary>
    /// <returns>A new random password.</returns>
    public static Password CreatePassword()
    {
        var result = new Password(Getbytes(), CreatePasswordParameters());

        return result;
    }
    /// <summary>
    /// Creates new random password parameters.
    /// </summary>
    /// <returns>A new random password parameters Object.</returns>
    public static PasswordParameters CreatePasswordParameters()
    {
        var result = new PasswordParameters(
                                new PasswordParameterNumerics(GetInt(), GetInt(), GetInt(), GetInt()),
                                new PasswordParameterData(Getbytes(), Getbytes(), Getbytes()));

        return result;
    }

    private static Byte[] Getbytes()
    {
        var result = new Byte[1024];
        Random.Shared.NextBytes(result);

        return result;
    }

    private static Int32 GetInt()
    {
        var result = Random.Shared.Next(1, 2048);

        return result;
    }
}
