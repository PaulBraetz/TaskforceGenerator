﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using TaskforceGenerator.Domain.Authentication.Abstractions;

namespace TaskforceGenerator.Tests.Common;

/// <summary>
/// Contains assertions for tests.
/// </summary>
public static class Assertions
{
    /// <summary>
    /// Asserts the equality of two citizens.
    /// </summary>
    /// <param name="expected">The expected value.</param>
    /// <param name="actual">The actual value.</param>
    public static void AreEqual(ICitizenConnection expected, ICitizenConnection actual)
    {
        Assert.AreEqual(expected.CitizenName, actual.CitizenName, "name");

        Assert.AreEqual(expected.Code, actual.Code, "code");

        Assert.IsTrue(expected.Password.Hash.SequenceEqual(actual.Password.Hash), "hash");

        Assert.IsTrue(expected.Password.Parameters.Data.KnownSecret.SequenceEqual(actual.Password.Parameters.Data.KnownSecret), "known secret");
        Assert.IsTrue(expected.Password.Parameters.Data.Salt.SequenceEqual(actual.Password.Parameters.Data.Salt), "salt");
        Assert.IsTrue(expected.Password.Parameters.Data.AssociatedData.SequenceEqual(actual.Password.Parameters.Data.AssociatedData), "associated data");

        Assert.AreEqual(expected.Password.Parameters.Numerics.Iterations, actual.Password.Parameters.Numerics.Iterations, "iterations");
        Assert.AreEqual(expected.Password.Parameters.Numerics.DegreeOfParallelism, actual.Password.Parameters.Numerics.DegreeOfParallelism, "degree of parallelism");
        Assert.AreEqual(expected.Password.Parameters.Numerics.MemorySize, actual.Password.Parameters.Numerics.MemorySize, "memory size");
        Assert.AreEqual(expected.Password.Parameters.Numerics.OutputLength, actual.Password.Parameters.Numerics.OutputLength, "output length");
    }

    /// <summary>
    /// Asserts that an action will throw an ArgumentNullException with an expected exception parameter name.
    /// </summary>
    /// <param name="expected">The expected exception parameter name.</param>
    /// <param name="action">The action to test if a ArgumentNullException is thrown.</param>
    public static void AssertArgumentNullException(String expected, Action action)
    {
        //Act
        var actual = String.Empty;
        try
        {
            action.Invoke();
        } catch(ArgumentNullException ex)
        {
            actual = ex.ParamName;
        }

        //Assert
        Assert.AreEqual(expected, actual);
    }
}
