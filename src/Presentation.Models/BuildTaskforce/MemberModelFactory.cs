﻿using TaskforceGenerator.Application.Requests;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Core.Abstractions;
using TaskforceGenerator.Domain.Core.Queries;
using TaskforceGenerator.Presentation.Models.Abstractions;
using TaskforceGenerator.Presentation.Models.Abstractions.BuildTaskforce;

namespace TaskforceGenerator.Presentation.Models.BuildTaskforce;

/// <summary>
/// Factory for producing instances of <see cref="OccupantModel"/>.
/// </summary>
public sealed class OccupantModelFactory : IFactory<IOccupantModel>
{
    private readonly IFactory<IInputGroupModel<String, String>> _nameModelFactory;
    private readonly IFactory<ISelectInputGroupModel<ISlotPreference, String>> _slotModelFactory;
    private readonly IService<ProxyServiceRequest<CreateOccupant, IOccupant>, IOccupant> _createService;
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="nameModelFactory">The factory to invoke when requiring a citizen name input model for new instances.</param>
    /// <param name="slotModelFactory">The factory to invoke when requiring a slot input model for new instances.</param>
    /// <param name="createService">The service to pass to results for producing instances of <see cref="IOccupant"/>.</param>
    public OccupantModelFactory(
        IFactory<IInputGroupModel<String, String>> nameModelFactory,
        IFactory<ISelectInputGroupModel<ISlotPreference, String>> slotModelFactory,
        IService<ProxyServiceRequest<CreateOccupant, IOccupant>, IOccupant> createService)
    {
        _nameModelFactory = nameModelFactory;
        _slotModelFactory = slotModelFactory;
        _createService = createService;
    }

    /// <inheritdoc/>
    public IOccupantModel Create()
    {
        var nameModel = _nameModelFactory.Create();
        var slotModel = _slotModelFactory.Create();

        nameModel.Input.Placeholder = "Member Name";

        var result = new OccupantModel(nameModel, slotModel, _createService);

        return result;
    }
}
