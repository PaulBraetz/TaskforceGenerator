﻿using TaskforceGenerator.Domain.Core.Abstractions;
using TaskforceGenerator.Presentation.Models.Abstractions;

namespace TaskforceGenerator.Presentation.Models.BuildTaskforce;

/// <summary>
/// Default implementation for <see cref="ISelectVehiclesModel"/>.
/// </summary>
public sealed class SelectVehiclesModel : ISelectVehiclesModel
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="vehicle">The type of vehicles to select.</param>
    /// <param name="count">The amount of this vehicle to select.</param>
    public SelectVehiclesModel(ISelectInputGroupModel<IVehicle, String> vehicle, IInputGroupModel<UInt16, String> count)
    {
        Vehicle = vehicle;
        Count = count;
    }
    /// <inheritdoc/>
    public ISelectInputGroupModel<IVehicle, String> Vehicle { get; }
    /// <inheritdoc/>
    public IInputGroupModel<UInt16, String> Count { get; }
}
