﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Core.Abstractions;
using TaskforceGenerator.Presentation.Models.Abstractions;

namespace TaskforceGenerator.Presentation.Models.BuildTaskforce;

/// <summary>
/// Factory for producing instances of <see cref="SelectVehiclesModel"/>.
/// </summary>
public sealed class SelectVehiclesModelFactory : IFactory<ISelectVehiclesModel>
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="vehicleFactory">The factory to invoke when creating new vehicle selects.</param>
    /// <param name="countModel">The factory to invoke when creating new count inputs.</param>
    public SelectVehiclesModelFactory(
        IFactory<ISelectInputGroupModel<IVehicle, String>> vehicleFactory,
        IFactory<IInputGroupModel<UInt16, String>> countModel)
    {
        _vehicleFactory = vehicleFactory;
        _countModel = countModel;
    }
    private readonly IFactory<ISelectInputGroupModel<IVehicle, String>> _vehicleFactory;
    private readonly IFactory<IInputGroupModel<UInt16, String>> _countModel;
    /// <inheritdoc/>
    public ISelectVehiclesModel Create()
    {
        var vehicle = _vehicleFactory.Create();
        var count = _countModel.Create();

        count.Input.Value = 1;
        count.Input.Placeholder = "Amount";

        var result = new SelectVehiclesModel(vehicle, count);

        return result;
    }
}
