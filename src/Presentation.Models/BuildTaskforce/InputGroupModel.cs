﻿using TaskforceGenerator.Presentation.Models.Abstractions;
using TaskforceGenerator.Presentation.Models.Abstractions.BuildTaskforce;

namespace TaskforceGenerator.Presentation.Models.BuildTaskforce;

/// <summary>
/// Default implementation of <see cref="IInputGroupModel"/>.
/// </summary>
public sealed class InputGroupModel : IInputGroupModel
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="occupants">The model used for obtaining the Occupant list.</param>
    /// <param name="vehicles">The model used for obtaining the vehicles to add to the taskforce.</param>
    /// <param name="scheme">The model responsible for obtaining the scheme name to use.</param>
    public InputGroupModel(
        IDynamicMultiControlModel<IOccupantModel> occupants,
        IDynamicMultiControlModel<ISelectVehiclesModel> vehicles,
        ISelectInputGroupModel<String, String> scheme)
    {
        Occupants = occupants;
        Vehicles = vehicles;
        Scheme = scheme;
    }

    /// <inheritdoc/>
    public IDynamicMultiControlModel<IOccupantModel> Occupants { get; }
    /// <inheritdoc/>
    public IDynamicMultiControlModel<ISelectVehiclesModel> Vehicles { get; }
    /// <inheritdoc/>
    public ISelectInputGroupModel<String, String> Scheme { get; }
}
