﻿using TaskforceGenerator.Application.Requests;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Requests;
using TaskforceGenerator.Domain.Core.Queries;
using TaskforceGenerator.Presentation.Models.Abstractions;

namespace TaskforceGenerator.Presentation.Models;

/// <summary>
/// Factory for producing instances of <see cref="CitizenNameInputModel"/>.
/// </summary>
public sealed class CitizenNameInputModelFactory : IFactory<IInputModel<String, String>>
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="existsService">The service used by models produced to validate the entered name.</param>
    /// <param name="autoCompleteService">The service used by models produced to generate autocompletions for registered citizen names.</param>
    /// <param name="valueDefaultProvider">The default value provider to use for initializing <see cref="IInputModel{TValue, TError}.Value"/>.</param>
    /// <param name="errorDefaultProvider">The default value provider to use for initializing <see cref="IInputModel{TValue, TError}.Error"/>.</param>
    public CitizenNameInputModelFactory(
        IService<ProxyServiceRequest<CheckCitizenExists, Boolean>, Boolean> existsService,
        IService<ProxyServiceRequest<LoadAutoCompleteCitizenName, String?>, String?> autoCompleteService,
        IDefaultValueProvider<String> valueDefaultProvider,
        IDefaultValueProvider<String> errorDefaultProvider)
    {
        _existsService = existsService;
        _valueDefaultProvider = valueDefaultProvider;
        _errorDefaultProvider = errorDefaultProvider;
        _autoCompleteService = autoCompleteService;
    }

    private readonly IService<ProxyServiceRequest<CheckCitizenExists, Boolean>, Boolean> _existsService;
    private readonly IService<ProxyServiceRequest<LoadAutoCompleteCitizenName, String?>, String?> _autoCompleteService;
    private readonly IDefaultValueProvider<String> _valueDefaultProvider;
    private readonly IDefaultValueProvider<String> _errorDefaultProvider;

    /// <inheritdoc/>
    public IInputModel<String, String> Create()
    {
        var result = new CitizenNameInputModel(
            _existsService,
            _autoCompleteService,
            _valueDefaultProvider,
            _errorDefaultProvider);

        return result;
    }
}
