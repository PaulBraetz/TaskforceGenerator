﻿using TaskforceGenerator.Presentation.Models.Abstractions;

namespace TaskforceGenerator.Presentation.Models;

/// <summary>
/// Bridge between <see cref="IToastModel"/> and <see cref="Common.Abstractions.IObserver{T}"/>.
/// Exceptions observed will be raised as error toasts.
/// </summary>
public sealed class ToastsErrorModel : Common.Abstractions.IObserver<Exception>, IToastsModel
{
    /// <summary> 
    /// Initializes a new instance.
    /// </summary>
    /// <param name="toastModelFactory">The factory to use when creating new toast models for exceptions received.</param>
    /// <param name="toastComparer">The comparer to use when relating a toasts <see cref="IToastModel.HasExpired"/> event to the toast to remove.</param>
    public ToastsErrorModel(IToastModelFactory toastModelFactory, IEqualityComparer<IToastModel> toastComparer)
    {
        _toastModelFactory = toastModelFactory;
        _toasts = new HashSet<IToastModel>(toastComparer);
    }

    private const Int32 CHAR_DISPLAY_MILLIS = 50;
    private const Int32 MIN_DISPLAY_MILLIS = 5_000;
    private const String TOAST_HEADER = "Error";

    private readonly HashSet<IToastModel> _toasts;

    private readonly IToastModelFactory _toastModelFactory;

    /// <inheritdoc/>
    public void Notify(Exception value)
    {
        var minDisplayMillis = value.Message.Length * CHAR_DISPLAY_MILLIS;
        var displayMillis = Math.Max(minDisplayMillis, MIN_DISPLAY_MILLIS);
        var lifespan = TimeSpan.FromMilliseconds(displayMillis);
        var toast = _toastModelFactory.Create(TOAST_HEADER, value.Message, ToastType.Error, lifespan);
        AddToast(toast);
    }

    private void AddToast(IToastModel toast)
    {
        var added = _toasts.Add(toast);
        if(added)
        {
            InvokeToastsChanged(ToastsChangedType.Added, toast);
            toast.HasExpired += (s, e) => RemoveToast(toast);
        }
    }
    private void RemoveToast(IToastModel toast)
    {
        var removed = _toasts.Remove(toast);
        if(removed)
        {
            InvokeToastsChanged(ToastsChangedType.Removed, toast);
        }
    }

    private void InvokeToastsChanged(ToastsChangedType type, IToastModel toastAffected) =>
        ToastsChanged?.Invoke(this, new ToastChangedEventArgs(type, toastAffected));

    /// <inheritdoc/>
    public ICollection<IToastModel> GetToasts()
    {
        var toastModels = _toasts.ToList();

        return toastModels;
    }

    /// <inheritdoc/>
    public event EventHandler<ToastChangedEventArgs>? ToastsChanged;
}
