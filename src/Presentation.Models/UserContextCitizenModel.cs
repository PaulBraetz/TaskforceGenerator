﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Presentation.Models.Abstractions;

namespace TaskforceGenerator.Presentation.Models;

/// <summary>
/// Model for displaying information on the currently authenticated citizen.
/// </summary>
public sealed class UserContextCitizenModel : HasObservableProperties, IUserContextCitizenModel
{
    private readonly IObservableUserContext _context;
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="context">The context monitored by this model.</param>
    public UserContextCitizenModel(IObservableUserContext context)
    {
        _context = context;
        _context.CitizenChanged += (s, e) =>
            InvokePropertyValueChanged(
                nameof(CitizenName),
                oldValue: e.OldValue,
                newValue: e.NewValue);
    }
    /// <inheritdoc/>
    public String? CitizenName => _context.Citizen?.CitizenName;
}
