﻿using Microsoft.AspNetCore.Http;

using TaskforceGenerator.Domain.Core;
using TaskforceGenerator.Presentation.Models.Abstractions;

namespace TaskforceGenerator.Presentation.Models;
/// <summary>
/// Navigates to a static path upon calling <see cref="Navigate"/>.
/// </summary>
public sealed class NavigationModel : INavigationModel
{
    private readonly INavigationManager _navigationManager;
    private readonly IPathModel _path;
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="navigationManager">The navigation manager to use when invoking <see cref="Navigate"/>.</param>
    /// <param name="path">The path to navigate to upon invoking <see cref="Navigate"/>.</param>
    public NavigationModel(INavigationManager navigationManager, IPathModel path)
    {
        _navigationManager = navigationManager;
        _path = path;
    }

    /// <inheritdoc/>
    public void Navigate(QueryString? query = null)
    {
        String path;
        if(_path.IsAbsolute)
        {
            var builder = new UriBuilder(_path.Path);
            if(query.HasValue)
            {
                var builderQuery = new QueryString(builder.Query);
                builder.Query = query.Value.Add(builderQuery).ToString();
            }

            path = builder.Uri.ToString();
        } else
        {
            //TODO: support fragments/user credentials
            path = query.HasValue ?
                _path.Path + query.Value.ToString() :
                _path.Path;
        }

        _navigationManager.NavigateTo(path);
    }
}
