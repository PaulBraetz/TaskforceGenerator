﻿namespace TaskforceGenerator.Presentation.Models.Abstractions;

/// <summary>
/// Contains toast type definitions intended to provide visual clues to the user.
/// </summary>
public enum ToastType
{
    /// <summary>
    /// The toast contains general information.
    /// </summary>
    Info,
    /// <summary>
    /// The toast contains warnings that inform about the state of the application or are intended to shift the users behaviour.
    /// </summary>
    Warning,
    /// <summary>
    /// The toast contains information about an error that occured during the applications execution.
    /// </summary>
    Error
}
/// <summary>
/// Represents a toast.
/// </summary>
public interface IToastModel
{
    /// <summary>
    /// Invoked after the toast has expired.
    /// </summary>
    event EventHandler? HasExpired;
    /// <summary>
    /// Gets a value indicating whether the toast has expired and should be hidden.
    /// </summary>
    Boolean Expired { get; }
    /// <summary>
    /// Expires the toast.
    /// </summary>
    void Expire();
    /// <summary>
    /// Gets the header to be displayed.
    /// </summary>
    String Header { get; }
    /// <summary>
    /// Gets the body to be displayed.
    /// </summary>
    String Body { get; }
    /// <summary>
    /// Gets the toasts type.
    /// </summary>
    ToastType Type { get; }
    /// <summary>
    /// Gets the lifespan of this toast.
    /// </summary>
    TimeSpan Lifespan { get; }
    /// <summary>
    /// Gets the time at which the toast was created.
    /// </summary>
    DateTimeOffset CreatedAt { get; }
}
