﻿using TaskforceGenerator.Domain.Core.Abstractions;

namespace TaskforceGenerator.Presentation.Models.Abstractions;

/// <summary>
/// Represents the input for selecting one or multiple vehicles.
/// </summary>
public interface ISelectVehiclesModel
{
    /// <summary>
    /// Gets the type of vehicles to select.
    /// </summary>
    ISelectInputGroupModel<IVehicle, String> Vehicle { get; }
    /// <summary>
    /// Gets the amount of this vehicle to select.
    /// </summary>
    IInputGroupModel<UInt16, String> Count { get; }
}
