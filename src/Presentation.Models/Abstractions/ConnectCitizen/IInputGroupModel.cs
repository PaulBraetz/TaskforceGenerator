﻿using TaskforceGenerator.Domain.Authentication;

namespace TaskforceGenerator.Presentation.Models.Abstractions.ConnectCitizen;

/// <summary>
/// Represents the inputs used to obtain input in a connect citizen form.
/// </summary>
public interface IInputGroupModel
{
    /// <summary>
    /// Gets the model used to obtain the citizens name.
    /// </summary>
    IInputGroupModel<String, String> Name { get; }
    /// <summary>
    /// Gets the model used to obtain the citizens password.
    /// </summary>
    IInputGroupModel<String, PasswordValidity> Password { get; }
}
