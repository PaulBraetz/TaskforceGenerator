﻿namespace TaskforceGenerator.Presentation.Models.Abstractions.ConnectCitizen;

/// <summary>
/// Represents a form for connecting a citizen.
/// </summary>
public interface IConnectCitizenModel
{
    /// <summary>
    /// Gets required input component model group.
    /// </summary>
    IInputGroupModel Inputs { get; }
    /// <summary>
    /// Gets the codedisplay component model.
    /// </summary>
    ICodeModel Code { get; }
    /// <summary>
    /// Gets the required button model group.
    /// </summary>
    IButtonGroupModel Buttons { get; }
    /// <summary>
    /// Gets the model responsible for displaying any errors.
    /// </summary>
    IDisplayModel<String?> Error { get; }
}
