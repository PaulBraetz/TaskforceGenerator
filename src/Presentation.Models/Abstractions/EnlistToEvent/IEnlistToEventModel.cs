﻿using TaskforceGenerator.Domain.Core.Abstractions;

namespace TaskforceGenerator.Presentation.Models.Abstractions.EnlistToEvent;

/// <summary>
/// Models for enlisting to an event.
/// </summary>
public interface IEnlistToEventModel
{
    /// <summary>
    /// Gets a value indicating whether an event has been loaded.
    /// </summary>
    Boolean IsLoaded { get; }
    /// <summary>
    /// Gets the display group containing informational displays about the currently loaded event.
    /// </summary>
    IDisplayGroupModel Displays { get; }
    /// <summary>
    /// Gets the model responsible for obtaining the vehicles owned by the citizen.
    /// </summary>
    IDynamicMultiControlModel<ISelectVehiclesModel> OwnedVehicles { get; }
    /// <summary>
    /// Gets the model responsible for obtaining the career preferrences.
    /// </summary>
    IMultiControlModel<ISelectInputModel<ICareer, String>> PreferredCareers { get; }
    /// <summary>
    /// Gets the model responsible for obtaining the crew mates the citizen would prefer to play with.
    /// </summary>
    IDynamicMultiControlModel<IInputModel<String, String>> PreferredCrewMates { get; }
    /// <summary>
    /// Gets the button responsible for enlisting the authenticated citizen to the event.
    /// </summary>
    IButtonModel Enlist { get; }
    /// <summary>
    /// Loads the event with the id provided.
    /// </summary>
    /// <param name="eventId">The id of the event to load.</param>
    /// <param name="cancellationToken">The token used to signal cancelling the operation.</param>
    Task LoadAsync(String? eventId, CancellationToken cancellationToken);
}
