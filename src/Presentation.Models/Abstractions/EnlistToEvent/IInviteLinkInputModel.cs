﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Presentation.Models.Abstractions.EnlistToEvent;

/// <summary>
/// Represents the page for entering an invite link manually.
/// </summary>
public interface IInviteLinkInputModel
{
    /// <summary>
    /// Gets the model responsible for obtaining the invite link, code or fragment.
    /// </summary>
    IInputGroupModel<String, String> LinkInput { get; }
    /// <summary>
    /// Gets the model responsible for displaying the parsed invite code.
    /// </summary>
    IDisplayModel<String> ParsedCode {get;}
    /// <summary>
    /// Gets the model responsible for navigating the user to the enlist page.
    /// </summary>
    IButtonModel Enlist { get; }
}
