﻿namespace TaskforceGenerator.Presentation.Models.Abstractions.EnlistToEvent;

/// <summary>
/// Models the information to display about an event when enlisting.
/// </summary>
public interface IDisplayGroupModel
{
    /// <summary>
    /// Gets the model responsible for displaying the name of the event.
    /// </summary>
    IDisplayModel<String> EventName { get; }
    /// <summary>
    /// Gets the model responsible for displaying the name of the organizer.
    /// </summary>
    IDisplayModel<String> OrganizerName { get; }
    /// <summary>
    /// Gets the model responsible for displaying the date of the event.
    /// </summary>
    IDisplayModel<DateTime> EventDate { get; }
    /// <summary>
    /// Gets the model responsible for displaying the description of the event.
    /// </summary>
    IDisplayModel<String> Description { get; }
}
