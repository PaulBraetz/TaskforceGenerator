﻿using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Presentation.Models.Abstractions;

/// <summary>
/// Abstract factory for producing instances of <see cref="IDynamicMultiControlItemModel{TSubControlModel}"/>.
/// </summary>
/// <typeparam name="TSubControlModel">The type of subcontrol encapsulated.</typeparam>
public interface IDynamicMultiControlItemModelFactory<TSubControlModel> :
    IFactory<IDynamicMultiControlItemModel<TSubControlModel>>
{ }
