﻿namespace TaskforceGenerator.Presentation.Models.Abstractions.CreateEvent;

/// <summary>
/// Models the input group of the create event component.
/// </summary>
public interface ICreateEventInputGroupModel
{
    /// <summary>
    /// Gets the model responsible for obtaining the event name.
    /// </summary>
    IInputGroupModel<String, String> EventName { get; }
    /// <summary>
    /// Gets the model responsible for obtaining the event description.
    /// </summary>
    IInputGroupModel<String, String> EventDescription { get; }
    /// <summary>
    /// Gets the model responsible for obtaining the date of the event.
    /// </summary>
    IInputGroupModel<DateTime, String> EventDate { get; }
    /// <summary>
    /// Gets the model responsible for obtaining the planned event vehicles.
    /// </summary>
    IDynamicMultiControlModel<ISelectVehiclesModel> PlannedVehicles { get; }
}
