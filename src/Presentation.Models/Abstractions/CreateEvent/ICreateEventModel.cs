﻿namespace TaskforceGenerator.Presentation.Models.Abstractions.CreateEvent;

/// <summary>
/// Models the event creation.
/// </summary>
public interface ICreateEventModel
{
    /// <summary>
    /// Gets the model responsible for displaying the invitation code 
    /// that was created after clicking the <see cref="Create"/> button.
    /// </summary>
    IDisplayModel<String?> InviteCode { get; }
    /// <summary>
    /// Gets the button model responsible for creating the event.
    /// </summary>
    IButtonModel Create { get; }
    /// <summary>
    /// Gets the input group responsible for configuring the event model.
    /// </summary>
    ICreateEventInputGroupModel Inputs { get; }
}
