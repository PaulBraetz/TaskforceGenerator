﻿using OneOf;

using System.ComponentModel;

using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Results;
using TaskforceGenerator.Domain.Authentication.Results;
using TaskforceGenerator.Domain.Core.Results;
using TaskforceGenerator.Presentation.Models.Abstractions;
using TaskforceGenerator.Presentation.Models.Abstractions.Login;

namespace TaskforceGenerator.Presentation.Models.Login;

/// <summary>
/// Default implementation of <see cref="ILoginModel"/>.
/// </summary>
public sealed class LoginModel : HasObservableProperties, ILoginModel
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="loginService">The service to use when invoking <see cref="Login"/>.</param>
    /// <param name="navigation">The navigation to invoke after successfully logging in.</param>
    /// <param name="login">the button for submitting the login request.</param>
    /// <param name="inputs">The model responsible for obtaining the required inputs.</param>
    public LoginModel(
        IService<Application.Requests.Login, Application.Requests.Login.Result> loginService,
        INavigationModel navigation,
        IButtonModel login,
        IInputGroupModel inputs)
    {
        _loginService = loginService;
        _navigation = navigation;

        Login = login;
        Inputs = inputs;
    }
    /// <summary>
    /// Creates, initializes and wires up a new instance.
    /// </summary>
    /// <param name="loginService">The service to use when invoking <see cref="Login"/>.</param>
    /// <param name="navigation">The navigation to invoke after successfully logging in.</param>
    /// <param name="login">the button for submitting the login request.</param>
    /// <param name="inputs">The model responsible for obtaining the required inputs.</param>
    /// <returns>A new, initialized and wired up instance of <see cref="LoginModel"/>.</returns>
    public static LoginModel Create(
        IService<Application.Requests.Login, Application.Requests.Login.Result> loginService,
        INavigationModel navigation,
        IButtonModel login,
        IInputGroupModel inputs)
    {
        var result = new LoginModel(
            loginService,
            navigation,
            login,
            inputs);

        result.Inputs.Name.PropertyValueChanged += result.OnNamePropertyChanged;
        result.Inputs.Name.Label = "Name";
        result.Inputs.Name.Description = "Enter the name of the citizen you would like to connect as.";
        result.Inputs.Name.Input.Entered += result.OnEntered;

        result.Inputs.Password.PropertyValueChanged += result.OnPasswordPropertyChanged;
        result.Inputs.Password.Label = "Password";
        result.Inputs.Password.Description = "Enter the password for the citizen you would like to connect as.";
        result.Inputs.Password.Input.Entered += result.OnEntered;

        result.Login.Clicked += result.OnLoginClick;
        result.Login.Label = "Login";

        return result;
    }

    private readonly IService<Application.Requests.Login, Application.Requests.Login.Result> _loginService;
    private readonly INavigationModel _navigation;

    /// <inheritdoc/>
    public IInputGroupModel Inputs { get; }
    /// <inheritdoc/>
    public IButtonModel Login { get; }

    private Task OnEntered(Object? _, IAsyncEventArgs<String> eventArgs)
    {
        var result = Login.Disabled ?
            Task.CompletedTask :
            Login.Click(eventArgs.CancellationToken);

        return result;
    }
    /// <inheritdoc/>
    private async Task OnLoginClick(Object? _0, IAsyncEventArgs _1)
    {
        if(Inputs.Name.Input.Validity == InputValidityType.Invalid ||
            Inputs.Password.Input.Validity == InputValidityType.Invalid)
        {
            return;
        }

        using(_ = Login.Disable())
        {
            var loginResult = await new Application.Requests.Login(
                Inputs.Name.Input.Value!,
                Inputs.Password.Input.Value!,
                CancellationToken.None).Using(_loginService);

            loginResult.Switch(
                s => s.Switch(
                    s => _navigation.Navigate(),
                    c => { }),
                notRegistered => Inputs.Name.Input.SetInvalid("The citizen provided does not seem to be registered to the system."),
                notExists => Inputs.Name.Input.SetInvalid("The citizen provided does not seem to be registered to RSI."),
                mismatch => Inputs.Password.Input.SetInvalid("The password provided was not correct."));
        }
    }
    private void OnNamePropertyChanged(Object? _, PropertyValueChangeArgsBase e)
    {
        if(e.PropertyName == nameof(Inputs.Name.Input.Value) &&
           !Inputs.Name.Input.IsInvalid())
        {
            UpdateButtonDisabled();
        }
    }
    private void OnPasswordPropertyChanged(Object? _, PropertyValueChangeArgsBase e)
    {
        if(e.PropertyName == nameof(Inputs.Password.Input.Value) &&
            !Inputs.Password.Input.IsInvalid())
        {
            UpdateButtonDisabled();
        }
    }

    private void UpdateButtonDisabled()
    {
        Login.Disabled = String.IsNullOrEmpty(Inputs.Name.Input.Value) ||
             !String.IsNullOrEmpty(Inputs.Name.Input.Error) ||
             String.IsNullOrEmpty(Inputs.Password.Input.Value) ||
             !String.IsNullOrEmpty(Inputs.Password.Input.Error);
    }
}
