﻿using TaskforceGenerator.Common;
using TaskforceGenerator.Domain.Authentication;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Presentation.Models.Abstractions;

namespace TaskforceGenerator.Presentation.Models;

/// <summary>
/// Self validating model for password creation inputs.
/// </summary>
public sealed class PasswordCreationInputModel : InputModel<String, PasswordValidity>
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="guideline">The guideline to use when assessing password inputs.</param>
    /// <param name="valueDefaultProvider">The default value provider to use for initializing <see cref="IInputModel{TValue, TError}.Value"/>.</param>
    /// <param name="errorDefaultProvider">The default value provider to use for initializing <see cref="IInputModel{TValue, TError}.Error"/>.</param>
    public PasswordCreationInputModel(
        IPasswordGuideline guideline,
        IDefaultValueProvider<String> valueDefaultProvider,
        IDefaultValueProvider<PasswordValidity> errorDefaultProvider) : base(valueDefaultProvider, errorDefaultProvider)
    {
        _guideline = guideline;
        UpdateError();
    }

    private readonly IPasswordGuideline _guideline;

    /// <inheritdoc/>
    protected override void OnPropertyValueChanged(PropertyValueChangeArgsBase args)
    {
        base.OnPropertyValueChanged(args);

        if(args.PropertyName != nameof(Value))
            return;

        UpdateError();
    }

    private void UpdateError()
    {
        var error = _guideline.Assess(Value);

        if(error.IsDefaultOrEmpty())
            SetValid();
        else
            SetInvalid(error);
    }
}
