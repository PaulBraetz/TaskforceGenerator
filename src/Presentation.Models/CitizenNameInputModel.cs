﻿using System.Reactive;
using System.Reactive.Linq;
using System.Reactive.Subjects;

using TaskforceGenerator.Application;
using TaskforceGenerator.Application.Requests;
using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Requests;
using TaskforceGenerator.Domain.Core.Queries;
using TaskforceGenerator.Presentation.Models.Abstractions;

namespace TaskforceGenerator.Presentation.Models;

/// <summary>
/// Self validating input model for citizen names.
/// </summary>
public sealed class CitizenNameInputModel : InputModel<String, String>
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="existsService">The service used to validate the entered name.</param>
    /// <param name="autoCompleteService">The service used to generate autocompletions for registered citizen names.</param>
    /// <param name="valueDefaultProvider">The default value provider to use for initializing <see cref="IInputModel{TValue, TError}.Value"/>.</param>
    /// <param name="errorDefaultProvider">The default value provider to use for initializing <see cref="IInputModel{TValue, TError}.Error"/>.</param>
    public CitizenNameInputModel(
        IService<ProxyServiceRequest<CheckCitizenExists, Boolean>, Boolean> existsService,
        IService<ProxyServiceRequest<LoadAutoCompleteCitizenName, String?>, String?> autoCompleteService,
        IDefaultValueProvider<String> valueDefaultProvider,
        IDefaultValueProvider<String> errorDefaultProvider)
        : base(valueDefaultProvider, errorDefaultProvider)
    {
        _existsService = existsService;
        _autoCompleteService = autoCompleteService;

        var observable = new Subject<String>();
        _valueObservable = observable;
        _valueObserver = observable
            .Throttle(TimeSpan.FromMilliseconds(250))
            .Select(OnNewValue)
            .Subscribe();

        _ = OnNewValue(Value);
    }

    /// <inheritdoc/>
    public override String Value
    {
        get => base.Value;
        set
        {
            base.Value = value;
            _valueObservable.OnNext(value);
        }
    }

    private readonly IService<ProxyServiceRequest<CheckCitizenExists, Boolean>, Boolean> _existsService;
    private readonly IService<ProxyServiceRequest<LoadAutoCompleteCitizenName, String?>, String?> _autoCompleteService;

    private readonly Subject<String> _valueObservable;
#pragma warning disable IDE0052 // Remove unread private members -> keep uncollected to keep observing
    private readonly IDisposable _valueObserver;
#pragma warning restore IDE0052 // Remove unread private members

    private async Task<Unit> OnNewValue(String value)
    {
        var exists = await new CheckCitizenExists(value, CancellationToken.None)
            .AsProxyQuery<CheckCitizenExists, Boolean>()
            .Using(_existsService);

        if(exists)
            SetValid();
        else
            SetInvalid("The name entered does not appear to be a registered RSI citizen.");

        var autoCompleteResponse = await new LoadAutoCompleteCitizenName(value, CancellationToken.None)
            .AsProxyQuery<LoadAutoCompleteCitizenName, String?>()
            .Using(_autoCompleteService);

        var enabled = !String.IsNullOrEmpty(autoCompleteResponse);
        if(enabled)
            AutoCompleteValue = autoCompleteResponse!;

        AutoCompleteEnabled = enabled;

        return Unit.Default;
    }
}
