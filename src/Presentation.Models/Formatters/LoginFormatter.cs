﻿using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Presentation.Models.Formatters;

/// <summary>
/// Toast-specific formatter for login successes.
/// </summary>
public sealed class LoginFormatter : IStaticFormatter<Application.Requests.Login>
{
    /// <inheritdoc/>
    public String Format(Application.Requests.Login value) => $"Logged in as '{value.CitizenName}' successfully.";
}
