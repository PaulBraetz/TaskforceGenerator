﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Core.Abstractions;

namespace TaskforceGenerator.Presentation.Models.Formatters;

/// <summary>
/// Formatter for instances of type <see cref="ICareer"/> that yields the career name upon formatting.
/// </summary>
/// <typeparam name="TCareer">The type of career to format.</typeparam>
public sealed class CareerNameFormatter<TCareer> : IStaticFormatter<TCareer>
    where TCareer : ICareer
{
    /// <inheritdoc/>
    public String Format(TCareer value) => value.Name;
}
