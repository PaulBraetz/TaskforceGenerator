﻿using TaskforceGenerator.Application.Results;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Requests;

namespace TaskforceGenerator.Presentation.Models.Formatters;

/// <summary>
/// Ui-friendly formatter for instances of <see cref="BioCodeMismatchResult"/>.
/// </summary>
public sealed class BioCodeMismatchExceptionFormatter : IStaticFormatter<BioCodeMismatchResult>
{
    /// <inheritdoc/>
    public String Format(BioCodeMismatchResult value)
    {
        var result = value.MismatchType == VerifyBioCodeResult.CitizenMismatch ?
                "The code generated does not match the code associated with the citizen anymore. Please generate a new code." :
                "The code generated does not match the code posted in the citizens bio. Post the code in the citizens bio in order to get verified.";

        return result;
    }
}
