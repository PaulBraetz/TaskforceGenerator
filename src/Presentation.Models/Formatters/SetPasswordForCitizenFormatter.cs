﻿using TaskforceGenerator.Application.Requests;
using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Presentation.Models.Formatters;

/// <summary>
/// Toast-specific formatter for password set successes.
/// </summary>
public sealed class SetPasswordForConnectionFormatter : IStaticFormatter<SetPasswordForConnection>
{
    /// <inheritdoc/>
    public String Format(SetPasswordForConnection value) => $"Password for '{value.CitizenName}' set successfully.";
}
