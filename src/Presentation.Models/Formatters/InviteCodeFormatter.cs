﻿using Microsoft.AspNetCore.Http;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Presentation.Models.Abstractions;

namespace TaskforceGenerator.Presentation.Models.Formatters;
/// <summary>
/// Formatter for formatting invite codes into hosting-specific links.
/// </summary>
public sealed class InviteCodeFormatter : IStaticFormatter<String?>
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="hostingInformation">The hosting information used to create a link upon formatting.</param>
    public InviteCodeFormatter(IHostingInformation hostingInformation)
    {
        _hostingInformation = hostingInformation;
    }
    private readonly IHostingInformation _hostingInformation;
    /// <inheritdoc/>
    public String Format(String? value) =>
        _hostingInformation.CreateUri("/enlist", QueryString.Create("eventId", value))
        .ToString();
}
