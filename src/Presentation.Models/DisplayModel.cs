﻿using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Presentation.Models.Abstractions;

namespace TaskforceGenerator.Presentation.Models;

/// <summary>
/// Default implementation of <see cref="IDisplayModel{TValue}"/>.
/// </summary>
/// <typeparam name="TValue">The type of value to display.</typeparam>
public class DisplayModel<TValue> : HasObservableProperties, IDisplayModel<TValue>
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="valueDefaultProvider">The default value provider to use for initializing <see cref="Value"/>.</param>
    /// <param name="formatter">The formatter to format a String representation of the value.</param>
    public DisplayModel(IDefaultValueProvider<TValue> valueDefaultProvider, IStaticFormatter<TValue> formatter)
    {
        _value = valueDefaultProvider.GetDefault();
        _formatter = formatter;
    }

    private TValue _value;
    private String _placeholder = String.Empty;
    private String? _displayText;
    private readonly IStaticFormatter<TValue> _formatter;

    /// <inheritdoc/>
    public TValue Value
    {
        get => _value;
        set
        {
            _ = ExchangeBackingField(ref _value, value);
            DisplayText = _formatter.Format(value);
        }
    }
    /// <inheritdoc/>
    public String DisplayText
    {
        get => _displayText ?? Placeholder;
        private set => ExchangeBackingField(ref _displayText, value);
    }
    /// <inheritdoc/>
    public String Placeholder
    {
        get => _placeholder;
        set => ExchangeBackingField(ref _placeholder, value);
    }
    /// <inheritdoc/>
    protected override void OnPropertyValueChanged(PropertyValueChangeArgsBase args)
    {
        if(args.PropertyName is nameof(Value) or nameof(Placeholder))
        {
            InvokePropertyValueChanged(nameof(DisplayText), DisplayText, DisplayText);
        }
    }
}
