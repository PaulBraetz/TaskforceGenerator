﻿using OneOf;

using System.ComponentModel;
using System.Text;

using TaskforceGenerator.Application.Requests;
using TaskforceGenerator.Application.Results;
using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Results;
using TaskforceGenerator.Domain.Authentication;
using TaskforceGenerator.Domain.Authentication.Results;
using TaskforceGenerator.Domain.Core.Results;
using TaskforceGenerator.Presentation.Models.Abstractions;
using TaskforceGenerator.Presentation.Models.Abstractions.ConnectCitizen;

namespace TaskforceGenerator.Presentation.Models.ConnectCitizen;

/// <summary>
/// Model for connecting citizens.
/// </summary>
public sealed class ConnectCitizenModel : HasObservableProperties, IConnectCitizenModel
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="service">The service to use when connecting a citizen.</param>
    /// <param name="codeService">The service to use for requesting a new bio code.</param>
    /// <param name="successNavigation">The navigation to invoke after successfully connecting a citizen.</param>
    /// <param name="inputs">The required input component model group.</param>
    /// <param name="buttons">The required button model group.</param>
    /// <param name="code">The codedisplay component model.</param>
    /// <param name="error">The error to display, if any.</param>
    public ConnectCitizenModel(
        IService<SetPasswordForConnection, SetPasswordForConnection.Result> service,
        IService<GenerateBioCode, GenerateBioCode.Result> codeService,
        INavigationModel successNavigation,
        IInputGroupModel inputs,
        IButtonGroupModel buttons,
        ICodeModel code,
        IDisplayModel<String?> error)
    {
        _connectService = service;
        _codeService = codeService;
        _successNavigation = successNavigation;

        Inputs = inputs;
        Buttons = buttons;
        Code = code;
        Error = error;
    }
    /// <summary>
    /// Creates, initializes and wires up a new instance.
    /// </summary>
    /// <param name="service">The service to use when connecting a citizen.</param>
    /// <param name="codeService">The service to use for requesting a new bio code.</param>
    /// <param name="successNavigation">The navigation to invoke after successfully connecting a citizen.</param>
    /// <param name="inputs">The required input component model group.</param>
    /// <param name="buttons">The required button model group.</param>
    /// <param name="code">The codedisplay component model.</param>
    /// <param name="userContext">The user context, used to set the default citizen name.</param>
    /// <param name="error">The error to display, if any.</param>
    /// <returns>A new, initialized and wired up instance of <see cref="ConnectCitizenModel"/>.</returns>
    public static ConnectCitizenModel Create(
        IService<SetPasswordForConnection, SetPasswordForConnection.Result> service,
        IService<GenerateBioCode, GenerateBioCode.Result> codeService,
        INavigationModel successNavigation,
        IInputGroupModel inputs,
        IButtonGroupModel buttons,
        ICodeModel code,
        IUserContextCitizenModel userContext,
        IDisplayModel<String?> error)
    {
        var result = new ConnectCitizenModel(
            service,
            codeService,
            successNavigation,
            inputs,
            buttons,
            code,
            error);

        result.Inputs.Password.Label = "Password";
        result.Inputs.Password.Description = "Set the password for your citizen connection. This may be changed at any time later.";
        result.Inputs.Password.PropertyValueChanged += result.OnInputPropertyValueChanged;
        result.Inputs.Password.Input.Entered += result.OnEnterPressed;

        result.Inputs.Name.Input.Value = userContext.CitizenName ?? String.Empty;
        result.Inputs.Name.Label = "Name";
        result.Inputs.Name.Description = "Enter the name of the citizen you would like to connect.";
        result.Inputs.Name.PropertyValueChanged += (s, e) =>
        {
            result.Code.Value = BioCode.Empty;
            result.OnInputPropertyValueChanged(s, e);
        };
        result.Inputs.Name.Input.Entered += result.OnEnterPressed;

        result.Buttons.ConnectCitizen.Clicked += result.OnConnectClicked;
        result.Buttons.GenerateBioCode.Clicked += result.OnGenerateClicked;

        result.UpdateButtonDisabledState();

        return result;
    }

    private readonly IService<SetPasswordForConnection, SetPasswordForConnection.Result> _connectService;
    private readonly IService<GenerateBioCode, GenerateBioCode.Result> _codeService;

    private readonly INavigationModel _successNavigation;

    /// <inheritdoc/>
    public IButtonGroupModel Buttons { get; }
    /// <inheritdoc/>
    public IInputGroupModel Inputs { get; }
    /// <inheritdoc/>
    public ICodeModel Code { get; }
    /// <inheritdoc/>
    public IDisplayModel<String?> Error { get; }

    private Task OnEnterPressed(Object? _, IAsyncEventArgs<String> eventAags)
    {
        var result = !Buttons.ConnectCitizen.Disabled ?
            Buttons.ConnectCitizen.Click(eventAags.CancellationToken) :
            !Buttons.GenerateBioCode.Disabled ?
            Buttons.GenerateBioCode.Click(eventAags.CancellationToken) :
            Task.CompletedTask;

        return result;
    }

    private async Task OnConnectClicked(Object? _0, IAsyncEventArgs _1)
    {
        if(Inputs.Name.Input.Validity == InputValidityType.Invalid ||
            Inputs.Password.Input.Validity == InputValidityType.Invalid)
        {
            return;
        }

        using var _ = Buttons.ConnectCitizen.Disable();

        var setResult = await new SetPasswordForConnection(
                       Inputs.Name.Input.Value,
                       Inputs.Password.Input.Value,
                       Code.Value,
                       CancellationToken.None).Using(_connectService);

        setResult.Switch(
            r => r.Switch(
                s => _successNavigation.Navigate(),
                c => { }),
            mismatch => Error.Value = mismatch.Message,
            guideline => Error.Value = String.Join(", ", guideline.Validity.RulesViolated.Select(r => r.Description)),
            notExists => Error.Value = "The citizen provided does not seem to be registered to RSI.",
            notRegistered => Error.Value = "The citizen provided does not seem to be registered to the system.");
    }

    private async Task OnGenerateClicked(Object? _0, IAsyncEventArgs _1)
    {
        if(Inputs.Name.Input.Validity == InputValidityType.Invalid)
        {
            return;
        }

        using(_ = Buttons.GenerateBioCode.Disable())
        {
            var generateResult = await new GenerateBioCode(
                Inputs.Name.Input.Value,
                CancellationToken.None).Using(_codeService);

            generateResult.Switch(
                c => Code.Value = c,
                notRegistered => Error.Value = "The citizen provided does not seem to be registered to the system.",
                notExists => Error.Value= "The citizen provided does not seem to be registered to RSI.",
                c => { });
        }

        UpdateButtonDisabledState();
    }

    private void OnInputPropertyValueChanged(Object? _0, PropertyValueChangeArgsBase _1) =>
        UpdateButtonDisabledState();
    private void UpdateButtonDisabledState()
    {
        var nameInvalid = Inputs.Name.Input.IsInvalid();
        var passwordInvalid = Inputs.Password.Input.IsInvalid();
        var codeNotGenerated = Code.Value.IsDefaultOrEmpty();

        Buttons.GenerateBioCode.Disabled = nameInvalid;
        Buttons.ConnectCitizen.Disabled = nameInvalid || passwordInvalid || codeNotGenerated;
    }
}
