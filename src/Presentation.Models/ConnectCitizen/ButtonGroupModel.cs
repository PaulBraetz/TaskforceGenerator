﻿using TaskforceGenerator.Presentation.Models.Abstractions;
using TaskforceGenerator.Presentation.Models.Abstractions.ConnectCitizen;

namespace TaskforceGenerator.Presentation.Models.ConnectCitizen;

/// <summary>
/// Default implementation of <see cref="IButtonGroupModel"/>.
/// </summary>
public sealed class ButtonGroupModel : IButtonGroupModel
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="generateBioCode">The button model for generating a bio code.</param>
    /// <param name="connectCitizen">The button model for connecting a citizen.</param>
    public ButtonGroupModel(IButtonModel generateBioCode, IButtonModel connectCitizen)
    {
        GenerateBioCode = generateBioCode;
        ConnectCitizen = connectCitizen;

        GenerateBioCode.Label = "Generate Bio Code";
        ConnectCitizen.Label = "Connect Citizen";
    }

    /// <inheritdoc/>
    public IButtonModel GenerateBioCode { get; }
    /// <inheritdoc/>
    public IButtonModel ConnectCitizen { get; }
}
