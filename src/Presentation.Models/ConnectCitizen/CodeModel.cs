﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication;
using TaskforceGenerator.Presentation.Models.Abstractions.ConnectCitizen;

namespace TaskforceGenerator.Presentation.Models.ConnectCitizen;

/// <summary>
/// Default implementation of <see cref="ICodeModel"/>.
/// </summary>
public sealed class CodeModel : HasObservableProperties, ICodeModel
{
    private String _codeError = String.Empty;
    private BioCode _value;

    /// <inheritdoc/>
    public String CodeError
    {
        get => _codeError;
        set => ExchangeBackingField(ref _codeError, value);
    }
    /// <inheritdoc/>
    public BioCode Value
    {
        get => _value;
        set => ExchangeBackingField(ref _value, value);
    }
}
