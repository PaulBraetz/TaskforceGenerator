﻿using Microsoft.AspNetCore.Http;

using System.Text.RegularExpressions;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Presentation.Models.Abstractions;
using TaskforceGenerator.Presentation.Models.Abstractions.EnlistToEvent;

namespace TaskforceGenerator.Presentation.Models.EnlistToEvent;

/// <summary>
/// Default implementation of <see cref="IInviteLinkInputModel"/>.
/// </summary>
public sealed partial class InviteLinkInputModel : HasObservableProperties, IInviteLinkInputModel
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="linkInput">The model responsible for obtaining the invite link, code or fragment.</param>
    /// <param name="parsedCode">The model responsible for displaying the parsed invite code.</param>
    /// <param name="enlist">The model responsible for navigating the user to the enlist page.</param>
    /// <param name="navigation">The parameterized navigation using which to react to clicking <see cref="Enlist"/>.</param>
    public InviteLinkInputModel(
        IInputGroupModel<String, String> linkInput,
        IDisplayModel<String> parsedCode,
        IButtonModel enlist,
        INavigationModel navigation)
    {
        LinkInput = linkInput;
        ParsedCode = parsedCode;
        Enlist = enlist;

        LinkInput.Label = "Invite Link or Code";
        LinkInput.Description = "Enter the invite link or the code contained within to enlist in an event.";
        LinkInput.Input.PropertyValueChanged += (s, e) =>
        {
            if(e.PropertyName == nameof(LinkInput.Input.Value))
            {
                //banged because name has been verified to belong to string type
                ParsedCode.Value = Parse(e.As<String>()!.NewValue);
            }
        };

        Enlist.Label = "Enlist";
        Enlist.Clicked += (s, e) =>
        {
            var query = new QueryString($"?eventId={parsedCode.Value}");
            navigation.Navigate(query);
            return Task.CompletedTask;
        };
    }

    /// <inheritdoc/>
    public IInputGroupModel<String, String> LinkInput { get; }
    /// <inheritdoc/>
    public IDisplayModel<String> ParsedCode { get; }
    /// <inheritdoc/>
    public IButtonModel Enlist { get; }

    [GeneratedRegex("(?<=.*(\\?|&)eventId=)[^&/\\s\\r\\n]*", RegexOptions.IgnoreCase, "en-US")]
    private static partial Regex EventIdRegex();

    private static String Parse(String linkInputValue)
    {
        var match = EventIdRegex().Match(linkInputValue);

        var result = match.Success ?
            match.Value :
            linkInputValue;

        return result;
    }
}
