﻿using TaskforceGenerator.Presentation.Models.Abstractions;
using TaskforceGenerator.Presentation.Models.Abstractions.EnlistToEvent;

namespace TaskforceGenerator.Presentation.Models.EnlistToEvent;

/// <summary>
/// Default implementation of <see cref="IDisplayGroupModel"/>.
/// </summary>
public sealed class DisplayGroupModel : IDisplayGroupModel
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="eventName">The model responsible for displaying the name of the event.</param>
    /// <param name="organizerName">The model responsible for displaying the name of the organizer.</param>
    /// <param name="eventDate">The model responsible for displaying the date of the event.</param>
    /// <param name="description">The model responsible for displaying the description of the event.</param>
    public DisplayGroupModel(IDisplayModel<String> eventName, IDisplayModel<String> organizerName, IDisplayModel<DateTime> eventDate, IDisplayModel<String> description)
    {
        EventName = eventName;
        OrganizerName = organizerName;
        EventDate = eventDate;
        Description = description;
    }

    /// <inheritdoc/>
    public IDisplayModel<String> EventName { get; }
    /// <inheritdoc/>
    public IDisplayModel<String> OrganizerName { get; }
    /// <inheritdoc/>
    public IDisplayModel<DateTime> EventDate { get; }
    /// <inheritdoc/>
    public IDisplayModel<String> Description { get; }
}
