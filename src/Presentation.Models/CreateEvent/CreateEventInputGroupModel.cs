﻿using TaskforceGenerator.Presentation.Models.Abstractions;
using TaskforceGenerator.Presentation.Models.Abstractions.CreateEvent;

namespace TaskforceGenerator.Presentation.Models.CreateEvent;

/// <summary>
/// Empty implementation of <see cref="ICreateEventInputGroupModel"/>.
/// </summary>
public sealed class CreateEventInputGroupModel : ICreateEventInputGroupModel
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="eventName">The model responsible for obtaining the event name.</param>
    /// <param name="eventDescription">The model responsible for obtaining the event description.</param>
    /// <param name="plannedVehicles">The model responsible for obtaining the planned event vehicles.</param>
    /// <param name="eventDate">The model responsible for obtaining the date of the event.</param>
    public CreateEventInputGroupModel(
        IInputGroupModel<String, String> eventName,
        IInputGroupModel<String, String> eventDescription,
        IDynamicMultiControlModel<ISelectVehiclesModel> plannedVehicles,
        IInputGroupModel<DateTime, String> eventDate)
    {
        EventName = eventName;
        EventDescription = eventDescription;
        PlannedVehicles = plannedVehicles;
        EventDate = eventDate;
    }
    /// <inheritdoc/>
    public IInputGroupModel<String, String> EventName { get; }
    /// <inheritdoc/>
    public IInputGroupModel<String, String> EventDescription { get; }
    /// <inheritdoc/>
    public IDynamicMultiControlModel<ISelectVehiclesModel> PlannedVehicles { get; }
    /// <inheritdoc/>
    public IInputGroupModel<DateTime, String> EventDate { get; }
}
