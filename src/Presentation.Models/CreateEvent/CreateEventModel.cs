﻿using OneOf;

using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Results;
using TaskforceGenerator.Domain.Authentication.Results;
using TaskforceGenerator.Domain.Core;
using TaskforceGenerator.Presentation.Models.Abstractions;
using TaskforceGenerator.Presentation.Models.Abstractions.CreateEvent;

namespace TaskforceGenerator.Presentation.Models.CreateEvent;

/// <summary>
/// Empty implementation of <see cref="ICreateEventModel"/>.
/// </summary>
public sealed class CreateEventModel : HasObservableProperties, ICreateEventModel
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="create">The button model responsible for creating the event.</param>
    /// <param name="inputs">The input group responsible for configuring the event model.</param>
    /// <param name="service">The service to use when creating an event.</param>
    /// <param name="inviteCode">The model responsible for displaying the invitation code.</param>
    public CreateEventModel(
        IButtonModel create,
        ICreateEventInputGroupModel inputs,
        IService<
            Application.Requests.CreateEvent,
            OneOf<InviteCode, CompliantlyCancelledResult, NotAuthenticatedResult>> service,
        IDisplayModel<String?> inviteCode)
    {
        Create = create;
        Inputs = inputs;
        _service = service;

        Create.Label = "Create";
        Create.Clicked += OnCreate;

        Inputs.EventName.Label = "Name";
        Inputs.EventName.Description = "Give your event a name.";

        Inputs.EventDescription.Label = "Description";
        Inputs.EventDescription.Description = "Give your event a description.";

        Inputs.EventDate.Label = "Date";
        Inputs.EventDate.Description = "Schedule your event.";

        Inputs.PlannedVehicles.Label = "Planned vehicles";
        Inputs.PlannedVehicles.Description = "Select the vehicles you are planning on using.";

        InviteCode = inviteCode;
    }

    private readonly IService<
        Application.Requests.CreateEvent,
        OneOf<InviteCode, CompliantlyCancelledResult, NotAuthenticatedResult>> _service;

    private async Task OnCreate(Object? _0, IAsyncEventArgs _1)
    {
        var eventName = Inputs.EventName.Input.Value;
        var eventDescription = Inputs.EventDescription.Input.Value;
        var eventDate = Inputs.EventDate.Input.Value.ToUniversalTime();
        var eventDateOffset = new DateTimeOffset(eventDate);

        var plannedVehicles = Inputs.PlannedVehicles.Items
            .Select(i => i.Control)
            .Select(i => (count: i.Count.Input.Value, vehicle: i.Vehicle.Input.Value?.Value))
            .Where(t => t.vehicle != null)
            .GroupBy(t => t.vehicle!)
            .ToDictionary(g => g.Key, g => g.Sum(t => t.count));

        var inviteCode = await new Application.Requests.CreateEvent(
            eventName,
            eventDescription,
            eventDateOffset,
            plannedVehicles,
            CancellationToken.None).Using(_service);

        inviteCode.Switch(
            newValue =>
            {
                var oldValue = InviteCode.Value;
                InviteCode.Value = newValue;
                InvokePropertyValueChanged(
                    nameof(InviteCode),
                    InviteCode,
                    InviteCode);
            },
            c => { },
            e => throw new NotImplementedException()); //TODO: handle not authenticated case (should not occur anyway)

    }

    /// <inheritdoc/>
    public IButtonModel Create { get; }
    /// <inheritdoc/>
    public ICreateEventInputGroupModel Inputs { get; }
    /// <inheritdoc/>
    public IDisplayModel<String?> InviteCode { get; }
}
