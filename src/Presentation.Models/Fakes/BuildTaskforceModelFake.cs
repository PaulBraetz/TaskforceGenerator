﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Core.Abstractions;
using TaskforceGenerator.Presentation.Models.Abstractions;
using TaskforceGenerator.Presentation.Models.Abstractions.BuildTaskforce;

namespace TaskforceGenerator.Presentation.Models.Fakes;

/// <summary>
/// Fake implementation of <see cref="IBuildTaskforceModel"/>.
/// </summary>
public sealed class BuildTaskforceModelFake : IBuildTaskforceModel
{
    private BuildTaskforceModelFake(IButtonModel build, IInputGroupModel inputs)
    {
        Inputs = inputs;
        Build = build;
    }
    /// <summary>
    /// Creates and initializes a new instance.
    /// </summary>
    /// <param name="build">The button to use to initiate building the taskforce.</param>
    /// <param name="inputs">The inputs responsible for obtaining the build request data.</param>
    /// <returns>The new model instance, initialized and ready.</returns>
    public static BuildTaskforceModelFake Create(IInputGroupModel inputs, IButtonModel build)
    {

        var result = new BuildTaskforceModelFake(build, inputs);

        result.Inputs.Occupants.Label = "Add Occupants";
        result.Inputs.Occupants.Description = "Add Occupants to your taskforce.";

        result.Inputs.Vehicles.Label = "Add Vehicles";
        result.Inputs.Vehicles.Description = "Add vehicles to your taskforce.";

        result.Inputs.Scheme.Label = "Select Scheme";
        result.Inputs.Scheme.Description = "Select the scheme algorithm to build your taskforce.";

        result.Build.Label = "Build (Fake)";
        result.Build.Clicked += result.OnClicked;

        return result;
    }
    /// <inheritdoc/>
    public IInputGroupModel Inputs { get; }
    /// <inheritdoc/>
    public ITaskforce? Taskforce { get; }
    /// <inheritdoc/>
    public IButtonModel Build { get; }
    /// <inheritdoc/>
    public String BuildError { get; set; } = String.Empty;

    private Task OnClicked(Object? _0, IAsyncEventArgs _1) => Task.Delay(5000);
}
