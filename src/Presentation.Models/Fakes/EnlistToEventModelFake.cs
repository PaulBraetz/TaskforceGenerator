﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Core;
using TaskforceGenerator.Domain.Core.Abstractions;
using TaskforceGenerator.Presentation.Models.Abstractions;
using TaskforceGenerator.Presentation.Models.Abstractions.EnlistToEvent;

namespace TaskforceGenerator.Presentation.Models.Fakes;

/// <summary>
/// Empty implementation of <see cref="IEnlistToEventModel"/>.
/// </summary>
public sealed class EnlistToEventModelFake : HasObservableProperties, IEnlistToEventModel
{
    /// <summary>
    /// Initializes as new Instance.
    /// </summary>
    /// <param name="enlist">The button responsible for enlisting the authenticated citizen to the event.</param>
    /// <param name="displays">The display group containing informational displays about the currently loaded event.</param>
    /// <param name="ownedVehicles">The model responsible for obtaining the vehicles owned by the citizen.</param>
    /// <param name="preferredCareers">The model responsible for obtaining the career preferrences.</param>
    /// <param name="selectCareerFactory">The factory to use when adding a model to <see cref="PreferredCareers"/>.</param>
    /// <param name="preferredCrewMates">The model responsible for obtaining the crew mates the citizen would prefer to play with.</param>
    public EnlistToEventModelFake(
        IButtonModel enlist,
        IDisplayGroupModel displays,
        IDynamicMultiControlModel<ISelectVehiclesModel> ownedVehicles,
        IMultiControlModel<ISelectInputModel<ICareer, String>> preferredCareers,
        IFactory<ISelectInputModel<ICareer, String>> selectCareerFactory,
        IDynamicMultiControlModel<IInputModel<String, String>> preferredCrewMates)
    {
        Enlist = enlist;
        Enlist.Label = "Enlist";
        Enlist.Clicked += (s, e) => Task.Delay(2500);

        Displays = displays;
        OwnedVehicles = ownedVehicles;
        OwnedVehicles.Label = "your own vehicles";
        PreferredCareers = preferredCareers;
        PreferredCareers.Label = "your preferred careers";
        _selectCareerFactory = selectCareerFactory;
        PreferredCrewMates = preferredCrewMates;
        PreferredCrewMates.Label = "the mates i wish to play with";
    }

    private Boolean _isLoaded = false;
    private readonly IFactory<ISelectInputModel<ICareer, String>> _selectCareerFactory;

    /// <inheritdoc/>
    public Boolean IsLoaded { get => _isLoaded; private set => ExchangeBackingField(ref _isLoaded, value); }
    /// <inheritdoc/>
    public IDisplayGroupModel Displays { get; }
    /// <inheritdoc/>
    public IDynamicMultiControlModel<ISelectVehiclesModel> OwnedVehicles { get; }
    /// <inheritdoc/>
    public IMultiControlModel<ISelectInputModel<ICareer, String>> PreferredCareers { get; }
    /// <inheritdoc/>
    public IDynamicMultiControlModel<IInputModel<String, String>> PreferredCrewMates { get; }
    /// <inheritdoc/>
    public IButtonModel Enlist { get; }
    /// <inheritdoc/>
    public async Task LoadAsync(String? eventId, CancellationToken cancellationToken)
    {
        IsLoaded = false;

        await Task.Delay(2500, cancellationToken);

        Displays.EventName.Value = "Lorem ipsum dolor sit amet";
        Displays.OrganizerName.Value = "YokoArashi";
        Displays.EventDate.Value = DateTime.Now.AddHours(1);
        Displays.Description.Value = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.";

        OwnedVehicles.Items.Clear();
        var vehicles = new[] { Vehicle.Redeemer, Vehicle.Pisces };
        foreach(var vehicle in vehicles)
        {
            await OwnedVehicles.Add.Click(cancellationToken);
            var item = OwnedVehicles.Items.Last();
            item.Control.Count.Input.Value = 0;
            var option = item.Control.Vehicle.Input.GetOptions()
                .Single(o => o.Value.Name == vehicle.Name);
            option.IsSelected = true;
        }

        PreferredCareers.Items.Clear();
        for(var i = 0; i < 3; i++)
        {
            var item = _selectCareerFactory.Create();
            PreferredCareers.Items.Add(item);
        }

        PreferredCrewMates.Items.Clear();
        for(var i = 0; i < 3; i++)
        {
            await PreferredCrewMates.Add.Click(cancellationToken);
            var item = PreferredCrewMates.Items.Last();
            item.Control.Value = $"Member {i}";
        }

        IsLoaded = true;
    }
}
