﻿using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Presentation.Models.Abstractions;

namespace TaskforceGenerator.Presentation.Models;

/// <summary>
/// Default implementation of <see cref="IAuthenticationModel"/>.
/// </summary>
public sealed class AuthenticationModel : HasObservableProperties, IAuthenticationModel
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="context">The context monitored by this model.</param>
    public AuthenticationModel(IObservableUserContext context)
    {
        _context = context;
        _context.CitizenChanged += OnCitizenChanged;
    }
    private readonly IObservableUserContext _context;
    /// <inheritdoc/>
    public Boolean IsAuthenticated => BooleanState.ToBooleanState(_isAuthenticated);

    private Int32 _isAuthenticated = BooleanState.FALSE_STATE;

    private void OnCitizenChanged(Object? _, PropertyValueChangeArgs<ICitizenConnection?> args)
    {
        var newState = args.NewValue != null ?
            BooleanState.TRUE_STATE :
            BooleanState.FALSE_STATE;

        if(Interlocked.Exchange(ref _isAuthenticated, newState) != newState)
        {
            InvokePropertyValueChanged(
                nameof(IsAuthenticated),
                oldValue: !BooleanState.ToBooleanState(newState),
                newValue: BooleanState.ToBooleanState(newState));
        }
    }
}
