﻿using Microsoft.AspNetCore.Components;

namespace TaskforceGenerator.Presentation.Views.Blazor.DependencyInjection;

/// <summary>
/// Base component that integrates with SimpleIjector.
/// </summary>
public abstract class SimpleInjectorIntegratedComponent : ComponentBase, IHandleEvent
{
    /// <summary>
    /// Gets or sets the service scope applier to use when handling events.
    /// This property should not be set or used in client code.
    /// </summary>
    [Injected] public ServiceScopeApplier Applier { get; set; }

    Task IHandleEvent.HandleEventAsync(EventCallbackWorkItem callback, Object arg)
    {
        Applier.ApplyServiceScope();

        var task = callback.InvokeAsync(arg);
        var shouldAwaitTask = task.Status is not TaskStatus.RanToCompletion and not TaskStatus.Canceled;

        StateHasChanged();

        return shouldAwaitTask ?
            CallStateHasChangedOnAsyncCompletion(task) :
            Task.CompletedTask;
    }

    private async Task CallStateHasChangedOnAsyncCompletion(Task task)
    {
        try
        {
            await task;
        } catch
        {
            if(task.IsCanceled)
            {
                return;
            }

            throw;
        }

        base.StateHasChanged();
    }
}
