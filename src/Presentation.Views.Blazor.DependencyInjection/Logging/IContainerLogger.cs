﻿using Microsoft.Extensions.Logging;

using SimpleInjector;

namespace TaskforceGenerator.Presentation.Views.Blazor.DependencyInjection.Logging;

/// <summary>
/// Common interface for logging composition information.
/// </summary>
public interface IContainerLogger
{
    /// <summary>
    /// Logs information about the composition.
    /// </summary>
    /// <param name="container">The container to log information about.</param>
    /// <param name="logger">The logger which to log messages about the container to.</param>
    void Log(Container container, ILogger logger);
}
