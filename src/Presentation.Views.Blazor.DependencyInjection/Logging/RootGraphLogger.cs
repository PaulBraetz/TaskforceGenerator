﻿using Microsoft.Extensions.Logging;

using SimpleInjector;

namespace TaskforceGenerator.Presentation.Views.Blazor.DependencyInjection.Logging;

/// <summary>
/// Logs the root Object graphs constructed by a container.
/// </summary>
public sealed class RootGraphLogger : IContainerLogger
{
    /// <inheritdoc/>
    public void Log(Container container, ILogger logger)
    {
        var roots = container.GetRootRegistrations();
        foreach(var root in roots)
        {
            var graph = root.VisualizeObjectGraph();
            logger.LogInformation(
                "Injection info: Graph for root {serviceType}: {graph}",
                root.ServiceType.FullName,
                graph);
        }
    }
}
