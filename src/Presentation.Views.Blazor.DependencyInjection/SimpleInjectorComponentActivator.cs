﻿using Microsoft.AspNetCore.Components;

using SimpleInjector;

namespace TaskforceGenerator.Presentation.Views.Blazor.DependencyInjection;

/// <summary>
/// Blazor component activator that integrates with SimpleInjector.
/// </summary>
public sealed class SimpleInjectorComponentActivator : IComponentActivator
{
    private readonly ServiceScopeApplier _applier;
    private readonly Container _container;
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="applier">The ambient scope applier to use.</param>
    /// <param name="container">The container using which to resolve components.</param>
    public SimpleInjectorComponentActivator(
        ServiceScopeApplier applier, Container container)
    {
        _applier = applier;
        _container = container;
    }

    /// <inheritdoc/>
    public IComponent CreateInstance(Type type)
    {
        _applier.ApplyServiceScope();

        IServiceProvider provider = _container;
        var component = provider.GetService(type) ?? Activator.CreateInstance(type);
        return (IComponent)component;
    }
}
