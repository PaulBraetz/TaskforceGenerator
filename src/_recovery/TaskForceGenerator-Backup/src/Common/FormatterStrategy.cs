﻿using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Common
{
    /// <summary>
    /// Strategy based implementation of <see cref="IStaticFormatter{T}"/>.
    /// </summary>
    /// <typeparam name="T">The type of Object to format.</typeparam>
    public sealed class FormatterStrategy<T> : IStaticFormatter<T>
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="strategy">The strategy used when invoking <see cref="Format(T)"/>.</param>
        public FormatterStrategy(Func<T, String> strategy)
        {
            _strategy = strategy;
        }

        private readonly Func<T, String> _strategy;
        /// <inheritdoc/>
        public String Format(T value)
        {
            var result = _strategy.Invoke(value);

            return result;
        }
    }
}
