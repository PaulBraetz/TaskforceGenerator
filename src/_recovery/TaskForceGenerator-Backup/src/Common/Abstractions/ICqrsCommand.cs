﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Common.Abstractions
{
    /// <summary>
    /// Represents a CQRS command request object.
    /// </summary>
    public interface ICqrsCommand
    {
        /// <summary>
        /// The token used to signal the command execution to be cancelled.
        /// </summary>
        CancellationToken CancellationToken { get; }
    }
}
