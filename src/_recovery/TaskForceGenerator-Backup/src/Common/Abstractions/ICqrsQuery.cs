﻿namespace TaskforceGenerator.Common.Abstractions
{
    /// <summary>
    /// Represents CQRS query parameter object for a potentially long running query.
    /// </summary>
    public interface ICqrsQuery
    {
        /// <summary>
        /// The token used to signal the query execution to be cancelled.
        /// </summary>
        CancellationToken CancellationToken { get; }
    }
}
