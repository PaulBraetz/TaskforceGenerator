﻿namespace TaskforceGenerator.Common.Abstractions
{
    /// <summary>
    /// Represents a query whose dependencies are fully captured.
    /// </summary>
    /// <typeparam name="TResult">The type of result to produce.</typeparam>
    public interface IAsyncQuery<TResult>
    {
        /// <summary>
        /// Executes the query.
        /// </summary>
        /// <returns>The result of the query.</returns>
        ValueTask<TResult> Execute();
    }
}