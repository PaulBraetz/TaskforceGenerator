﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Common.Abstractions
{
    /// <summary>
    /// Service implementing the request Object pattern for cqrs-command requests.
    /// </summary>
    /// <typeparam name="TCommand">The type of command to execute.</typeparam>
    public interface IAsyncCqrsCommandService<TCommand>
        where TCommand : ICqrsCommand
    {
        /// <summary>
        /// Executes a command.
        /// </summary>
        /// <param name="command">The command to execute.</param>
        ValueTask Execute(TCommand command);
    }
}
