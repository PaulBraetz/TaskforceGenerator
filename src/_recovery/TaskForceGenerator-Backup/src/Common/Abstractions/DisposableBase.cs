﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Common.Abstractions
{
    /// <summary>
    /// Base class for types implementing <see cref="IDisposable"/>.
    /// Instances may only be disposed once. Concurrent calls to <see cref="Dispose()"/> will
    /// be ignored; only one call may enter the method. Exceptions thrown during disposal will cause
    /// the instance state to revert to 'not-disposed'.
    /// </summary>
    public abstract class DisposableBase : IDisposable
    {
        private const Int32 DISPOSED_STATE = 1;
        private const Int32 NOT_DISPOSED_STATE = 0;
        private const Int32 DISPOSING_STATE = -1;

        private Int32 _disposedValue = NOT_DISPOSED_STATE;

        /// <summary>
        /// Disposes the instance.
        /// </summary>
        /// <param name="disposing">Should be set to <see langword="true"/> if disposing from a <see cref="Dispose()"/> call; otherwise, <see langword="false"/> (occurs during finalization).</param>
        private void Dispose(Boolean disposing)
        {
            if(Interlocked.CompareExchange(ref _disposedValue, DISPOSING_STATE, NOT_DISPOSED_STATE) != NOT_DISPOSED_STATE)
            {
                return;
            }

            try
            {
                if(disposing)
                {
                    DisposeManaged();
                }

                DisposeUnmanaged();
                _disposedValue = DISPOSED_STATE;
            } catch
            {
                _disposedValue = NOT_DISPOSED_STATE;
                throw;
            }
        }
        /// <summary>
        /// Disposes of managed state.
        /// </summary>
        protected virtual void DisposeManaged()
        {

        }
        /// <summary>
        /// Disposes unmanaged state and sets large fields to null. 
        /// When overriding this method, make sure to also implement a finalizer that calls <see cref="DisposeFinalize"/>.
        /// </summary>
        protected virtual void DisposeUnmanaged() { }
        /// <summary>
        /// This method should only be invoked in the finalizer of classes that also override <see cref="DisposeUnmanaged"/>.
        /// </summary>
        protected void DisposeFinalize() => Dispose(false);

        /// <inheritdoc/>
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
