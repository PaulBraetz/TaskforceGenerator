﻿namespace TaskforceGenerator.Common.Abstractions
{
    /// <summary>
    /// Service implementing the request Object pattern for cqrs-query requests.
    /// </summary>
    /// <typeparam name="TQuery">The type of query to execute.</typeparam>
    /// <typeparam name="TResult">The type of result yielded by executing a query.</typeparam>
    public interface IAsyncCqrsQueryService<TQuery, TResult>
        where TQuery : ICqrsQuery
    {
        /// <summary>
        /// Executes a query.
        /// </summary>
        /// <param name="query">The query to execute.</param>
        /// <returns>The result of the query.</returns>
        ValueTask<TResult> Execute(TQuery query);
    }
}
