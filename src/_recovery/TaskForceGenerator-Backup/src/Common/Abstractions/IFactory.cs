﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Common.Abstractions
{
    /// <summary>
    /// Abstract factory for creating instances of <typeparamref name="T"/>.
    /// </summary>
    /// <typeparam name="T">The type of object to create.</typeparam>
    public interface IFactory<T>
    {
        /// <summary>
        /// Creates a new instance of <typeparamref name="T"/>.
        /// </summary>
        /// <returns>A new instance of <typeparamref name="T"/>.</returns>
        T Create();
    }
}
