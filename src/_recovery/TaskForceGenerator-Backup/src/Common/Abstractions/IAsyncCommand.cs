﻿namespace TaskforceGenerator.Common.Abstractions
{
    /// <summary>
    /// Represents a command whose dependencies are fully captured.
    /// </summary>
    public interface IAsyncCommand
    {
        /// <summary>
        /// Executes a command.
        /// </summary>
        ValueTask Execute();
    }
}