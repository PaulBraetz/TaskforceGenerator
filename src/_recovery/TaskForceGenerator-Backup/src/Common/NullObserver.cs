﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Common
{
    /// <summary>
    /// Observer that provides no reaction to values published.
    /// </summary>
    public sealed class NullObserver<T> : Abstractions.IObserver<T>
    {
        /// <summary>
        /// Initializes a new instance. If possible, use <see cref="Instance"/> instead.
        /// </summary>
        public NullObserver() { }
        /// <summary>
        /// Gets the singleton instance.
        /// </summary>
        public static NullObserver<T> Instance { get; } = new();
        /// <inheritdoc/>
        public void Notify(T value) { }
    }
}
