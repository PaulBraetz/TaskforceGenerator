﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Common
{
    /// <summary>
    /// Strategy based implementatio of <see cref="IEqualityComparer{T}"/>.
    /// </summary>
    /// <typeparam name="T">The type of objects to compare.</typeparam>
    public sealed class EqualityComparerStrategy<T> : IEqualityComparer<T>
    {
        private readonly Func<T, T, Boolean> _equalityStrategy;
        private readonly Func<T, Int32> _hashCodeStrategy;
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="equalityStrategy">The strategy to use when invoking <see cref="Equals(T?, T?)"/>.</param>
        /// <param name="hashCodeStrategy">The strategy to use when invoking <see cref="GetHashCode(T)"/>.</param>
        public EqualityComparerStrategy(Func<T, T, Boolean> equalityStrategy, Func<T, Int32> hashCodeStrategy)
        {
            _equalityStrategy = equalityStrategy;
            _hashCodeStrategy = hashCodeStrategy;
        }

        /// <inheritdoc/>
        public Boolean Equals(T? x, T? y)
        {
#pragma warning disable IDE0046 // Convert to conditional expression
            if(x == null)
            {
                return y == null;
            }

            if(y == null)
            {
                return x == null;
            }

            return _equalityStrategy(x, y);
#pragma warning restore IDE0046 // Convert to conditional expression
        }
        /// <inheritdoc/>
        public Int32 GetHashCode([DisallowNull] T obj)
        {
            _ = obj ?? throw new ArgumentNullException(nameof(obj));

            return _hashCodeStrategy(obj);
        }
    }
}
