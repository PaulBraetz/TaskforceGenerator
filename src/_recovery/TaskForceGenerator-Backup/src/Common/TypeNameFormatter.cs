﻿using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Common
{
    /// <summary>
    /// Formats values into an the formatted values type name.
    /// </summary>
    /// <typeparam name="T">The type of object to format.</typeparam>
    public sealed class TypeNameFormatter<T> : IStaticFormatter<T>
    {
        /// <inheritdoc/>
        public String Format(T value) => value?.GetType().Name ?? String.Empty;
    }
}
