﻿using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Exceptions;

namespace TaskforceGenerator.Aspects
{
    public static partial class AmbientCallbackStateQueue<T>
    {
        private sealed partial class Context
        {
            public struct Scope : IDisposable
            {
                private Int32 _disposedValue = BooleanState.FALSE_STATE;

                public Scope()
                {
                }

                public static Scope Create(T item, Action<IEnumerable<T>> callback)
                {
                    var result = new Scope();

                    Instance.OnScopeCreated();

                    Instance.SetCallback(callback);
                    Instance.Enqueue(item);

                    return result;
                }
                public void Dispose()
                {
                    if(Interlocked.CompareExchange(
                        ref _disposedValue,
                        BooleanState.TRUE_STATE,
                        BooleanState.FALSE_STATE) == BooleanState.FALSE_STATE)
                    {
                        Instance.OnScopeDisposed();
                    } else
                    {
                        throw Throw.ObjectDisposedException.Scope;
                    }
                }
            }
        }
    }
}
