﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Exceptions;

namespace TaskforceGenerator.Common
{
    /// <summary>
    /// A closure around a command and the service using which it should be executed.
    /// </summary>
    /// <typeparam name="TCommand">The type of command captured.</typeparam>
    public readonly struct CommandClosure<TCommand> : IAsyncCommand
        where TCommand : ICqrsCommand
    {
        private readonly TCommand _command;
        private readonly IAsyncCqrsCommandService<TCommand> _service;
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="command">The command captured.</param>
        /// <param name="service">The service captured.</param>
        public CommandClosure(TCommand command, IAsyncCqrsCommandService<TCommand> service)
        {
            _command = command;
            _service = service;
        }

        /// <summary>
        /// Executes the captured command using the captured service.
        /// </summary>
        public ValueTask Execute() => _service.Execute(_command);
        /// <inheritdoc/>
        public override Boolean Equals(Object? obj) => throw Throw.NotSupportedException.ClosureEquals;
        /// <inheritdoc/>
        public override Int32 GetHashCode() => throw Throw.NotSupportedException.ClosureGetHashCode;
        /// 
        public static Boolean operator ==(CommandClosure<TCommand> left, CommandClosure<TCommand> right) =>
            left.Equals(right);
        /// 
        public static Boolean operator !=(CommandClosure<TCommand> left, CommandClosure<TCommand> right) =>
            !(left == right);
    }
}
