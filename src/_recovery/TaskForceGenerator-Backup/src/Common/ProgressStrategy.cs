﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Common
{
    /// <summary>
    /// Strategy-based implementation of <see cref="IProgress{T}"/>.
    /// </summary>
    /// <typeparam name="T">The type of progress update value.</typeparam>
    public sealed class ProgressStrategy<T> : IProgress<T>
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="strategy">The strategy to use when invoking <see cref="Report(T)"/>.</param>
        public ProgressStrategy(Action<T> strategy)
        {
            _strategy = strategy;
        }

        private readonly Action<T> _strategy;
        /// <inheritdoc/>
        public void Report(T value) => _strategy.Invoke(value);
    }
}
