﻿namespace TaskforceGenerator.Common
{
    /// <summary>
    /// Helper class for converting boolean states between boolean and integer representations for the purpose of enabling atomic exchange operations.
    /// </summary>
    public static class BooleanState
    {
        /// <summary>
        /// Gets the value an instance of <see cref="Int32"/> must contain in order to be considered <see langword="true"/>.
        /// </summary>
        public const Int32 TRUE_STATE = 1;
        /// <summary>
        /// Gets the value an instance of <see cref="Int32"/> must contain in order to be considered <see langword="false"/>.
        /// </summary>
        public const Int32 FALSE_STATE = 0;
        /// <summary>
        /// Converts an integer state its boolean state representation.
        /// </summary>
        /// <param name="state">The integer state to convert.</param>
        /// <returns>The boolean state representation of <paramref name="state"/>.</returns>
        public static Boolean ToBooleanState(Int32 state)
        {
            CheckStateValidity(state);
            return state == TRUE_STATE;
        }
        /// <summary>
        /// Converts a boolean state representation to its integer state representation.
        /// </summary>
        /// <param name="state">The boolean state to convert.</param>
        /// <returns>The integer state representation of <paramref name="state"/>.</returns>
        public static Int32 FromBooleanState(Boolean state) => state ? TRUE_STATE : FALSE_STATE;
        /// <summary>
        /// Gets a value indicating whether an instance of <see cref="Int32"/> represents a valid state, that is, 
        /// its value is either <see cref="TRUE_STATE"/> or <see cref="FALSE_STATE"/>.
        /// </summary>
        /// <param name="state">The integer state to assess.</param>
        /// <returns><see langword="true"/> if the value is either <see cref="TRUE_STATE"/> or <see cref="FALSE_STATE"/>; otherwise, <see langword="false"/>.</returns>
        public static Boolean IsValidState(Int32 state) => state is TRUE_STATE or FALSE_STATE;
        private static void CheckStateValidity(Int32 state)
        {
            if(!IsValidState(state))
                throw new ArgumentOutOfRangeException(nameof(state), state, $"{nameof(state)} must be either {TRUE_STATE} or {FALSE_STATE}.");
        }
    }
}
