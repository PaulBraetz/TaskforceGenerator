﻿using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Common
{
    /// <summary>
    /// Extensions for the <c>TaskforceGenerator.Common</c> namespace.
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Evaluates an enumeration by executing an action over all items contained.
        /// </summary>
        /// <typeparam name="T">The type of item enumerated.</typeparam>
        /// <param name="items">The enumeration to evaluate.</param>
        /// <param name="action">The action to invoke on every item enumerated.</param>
        public static void ForEach<T>(this IEnumerable<T> items, Action<T> action)
        {
            foreach (var item in items)
                action(item);
        }
        /// <summary>
        /// Executes a command on the service provided.
        /// </summary>
        /// <typeparam name="TCommand">The type of command to execute.</typeparam>
        /// <param name="command">The command to execute.</param>
        /// <param name="service">The service to execute the command on.</param>
        /// <returns>A task representing the commands execution.</returns>
        public static ValueTask Using<TCommand>(this TCommand command, IAsyncCqrsCommandService<TCommand> service)
            where TCommand : ICqrsCommand
        {
            var result = service.Execute(command);

            return result;
        }
        /// <summary>
        /// Captures a cqrs command and the service to execute it into a closure command.
        /// </summary>
        /// <typeparam name="TCommand">The type of command to capture.</typeparam>
        /// <param name="command">The command to capture.</param>
        /// <param name="service">The service to capture.</param>
        /// <returns>A closure command capturing the command and the service using which to execute it.</returns>
        public static IAsyncCommand Capture<TCommand>(this TCommand command, IAsyncCqrsCommandService<TCommand> service)
            where TCommand : ICqrsCommand
        {
            var result = new CommandClosure<TCommand>(command, service);

            return result;
        }
        /// <summary>
        /// Executes a query on the service provided.
        /// </summary>
        /// <typeparam name="TQuery">The type of query to execute.</typeparam>
        /// <typeparam name="TResult">The type of result produced by the query.</typeparam>
        /// <param name="query">The query to execute.</param>
        /// <param name="service">The service on which toexecute the query.</param>
        /// <returns>The result of the query.</returns>
        public static ValueTask<TResult> Using<TQuery, TResult>(this TQuery query, IAsyncCqrsQueryService<TQuery, TResult> service)
            where TQuery : ICqrsQuery
        {
            var result = service.Execute(query);

            return result;
        }
        /// <summary>
        /// Captures a cqrs query and the service to execute it into a closure query.
        /// </summary>
        /// <typeparam name="TQuery">The type of query to capture.</typeparam>
        /// <typeparam name="TResult">The type of result produced by executing the query.</typeparam>
        /// <param name="query">The query to capture.</param>
        /// <param name="service">The service to capture.</param>
        /// <returns>A closure query capturing the query and the service using which to execute it.</returns>
        public static IAsyncQuery<TResult> Capture<TQuery, TResult>(this TQuery query, IAsyncCqrsQueryService<TQuery, TResult> service)
            where TQuery : ICqrsQuery
        {
            var result = new QueryClosure<TQuery, TResult>(query, service);

            return result;
        }
    }
}
