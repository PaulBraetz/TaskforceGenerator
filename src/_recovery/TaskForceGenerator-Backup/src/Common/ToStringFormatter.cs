﻿using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Common
{
    /// <summary>
    /// <see cref="Object.ToString"/>-based implementation of <see cref="IStaticFormatter{T}"/>.
    /// </summary>
    /// <typeparam name="T">The type of object to format.</typeparam>
    public sealed class ToStringFormatter<T> : IStaticFormatter<T>
    {
        /// <inheritdoc/>
        public String Format(T value) => value?.ToString() ?? String.Empty;
    }
}
