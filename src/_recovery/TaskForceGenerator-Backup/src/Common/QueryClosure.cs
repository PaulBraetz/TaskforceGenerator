﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Exceptions;

namespace TaskforceGenerator.Common
{
    /// <summary>
    /// A closure around a query and the service using which it should be executed.
    /// </summary>
    /// <typeparam name="TQuery">The type of query captured.</typeparam>
    /// <typeparam name="TResult">The type of result produced.</typeparam>
    public readonly struct QueryClosure<TQuery, TResult> : IAsyncQuery<TResult> 
        where TQuery : ICqrsQuery
    {
        private readonly TQuery _query;
        private readonly IAsyncCqrsQueryService<TQuery, TResult> _service;
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="query">The query captured.</param>
        /// <param name="service">The service captured.</param>
        public QueryClosure(TQuery query, IAsyncCqrsQueryService<TQuery, TResult> service)
        {
            _query = query;
            _service = service;
        }

        /// <summary>
        /// Executes the captured query using the captured service.
        /// </summary>
        public ValueTask<TResult> Execute() => _service.Execute(_query);
        /// <inheritdoc/>
        public override Boolean Equals(Object? obj) => throw Throw.NotSupportedException.ClosureEquals;
        /// <inheritdoc/>
        public override Int32 GetHashCode() => throw Throw.NotSupportedException.ClosureGetHashCode;
        /// 
        public static Boolean operator ==(QueryClosure<TQuery, TResult> left, QueryClosure<TQuery, TResult> right) =>
            left.Equals(right);
        /// 
        public static Boolean operator !=(QueryClosure<TQuery, TResult> left, QueryClosure<TQuery, TResult> right) =>
            !(left == right);
    }
}
