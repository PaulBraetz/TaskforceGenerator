﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Common
{
    /// <summary>
    /// Event handler that performs an asynchronous handling operation.
    /// </summary>
    /// <typeparam name="T">The type of event argument supported by the handler.</typeparam>
    /// <param name="value">The event argument.</param>
    /// <returns>A task representing the handlers asynchronous execution.</returns>
    public delegate Task AsynchronousEventHandler<in T>(T value);
    /// <summary>
    /// Event handler that performs an asynchronous handling operation.
    /// </summary>
    /// <returns>A task representing the handlers asynchronous execution.</returns>
    public delegate Task AsynchronousEventHandler();
    /// <summary>
    /// Contains extensions for <see cref="AsynchronousEventHandler"/>.
    /// </summary>
    public static class AsynchronousEventHandlerExtensions
    {
        /// <summary>
        /// Asynchronously invokes all delegates registered to the handler.
        /// </summary>
        /// <param name="handler">The event handler whose delegates to invoke.</param>
        /// <returns>A task that will complete upon all of the handlers delegates having completed, or a completed task if the handler was <see langword="null"/>.</returns>
        public static async Task InvokeAsync(this AsynchronousEventHandler? handler)
        {
            if(handler == null)
                return;

            var delegates = handler.GetInvocationList()
                .Cast<AsynchronousEventHandler>()
                .Select(h => h.Invoke());

            await Task.Run(() => Task.WhenAll(delegates));
        }
    }
}
