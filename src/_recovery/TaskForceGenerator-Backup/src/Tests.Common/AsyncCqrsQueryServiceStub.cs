﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Tests.Common
{
    /// <summary>
    /// Stub for query service.
    /// </summary>
    /// <typeparam name="TQuery"></typeparam>
    /// <typeparam name="TResult"></typeparam>
    public sealed class AsyncCqrsQueryServiceStub<TQuery, TResult>
        : IAsyncCqrsQueryService<TQuery, TResult>
        where TQuery : ICqrsQuery
    {
        /// <summary>
        /// Gets a value indicating whether <see cref="Execute(TQuery)"/> has been called.
        /// </summary>
        public Boolean ExecuteCalled { get; private set; }
        /// <summary>
        /// Gets or sets the strategy to use when invoking <see cref="Execute(TQuery)"/>.
        /// </summary>
        public Func<TQuery, ValueTask<TResult>> ExecuteStrategy { get; set; } =
            q => throw new NotImplementedException();
        /// <inheritdoc/>
        public ValueTask<TResult> Execute(TQuery query)
        {
            ExecuteCalled = true;
            return ExecuteStrategy(query);
        }
    }
}
