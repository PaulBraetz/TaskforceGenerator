﻿using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Tests.Common
{
    /// <summary>
    /// Stub for command services.
    /// </summary>
    /// <typeparam name="TCommand"></typeparam>
    public sealed class AsyncCqrsCommandServiceStub<TCommand>
        : IAsyncCqrsCommandService<TCommand>
        where TCommand : ICqrsCommand
    {
        /// <summary>
        /// Gets a value indicating whether <see cref="Execute(TCommand)"/> has been called.
        /// </summary>
        public Boolean ExecuteCalled { get; private set; }
        /// <summary>
        /// Gets or sets the strategy to use when invoking <see cref="Execute(TCommand)"/>.
        /// </summary>
        public Func<TCommand, ValueTask> ExecuteStrategy { get; set; } =
            q => ValueTask.CompletedTask;
        /// <inheritdoc/>
        public ValueTask Execute(TCommand command)
        {
            ExecuteCalled = true;
            return ExecuteStrategy(command);
        }
    }
}
