﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Photino.Blazor
{
    internal class SynchronousTaskScheduler : TaskScheduler
    {
        public override System.Int32 MaximumConcurrencyLevel => 1;

        protected override void QueueTask(Task task) => 
            TryExecuteTask(task);

        protected override System.Boolean TryExecuteTaskInline(Task task, System.Boolean taskWasPreviouslyQueued) =>
            TryExecuteTask(task);

        protected override IEnumerable<Task> GetScheduledTasks() =>
            Enumerable.Empty<Task>();
    }
}
