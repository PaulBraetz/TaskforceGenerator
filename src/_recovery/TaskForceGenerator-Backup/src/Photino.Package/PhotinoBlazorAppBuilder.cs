﻿using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;

using PhotinoNET;

using System;
using System.Net.Http;
using System.IO;

namespace Photino.Blazor
{
    public class PhotinoBlazorAppBuilder
    {
        private PhotinoBlazorAppBuilder(
            IServiceCollection services,
            IHostingEnvironment environment,
            IConfigurationBuilder configuration,
            LoggingBuilder logging)
        {
            RootComponents = new RootComponentList();
            Services = services;
            Environment = environment;
            Configuration = configuration;
            Logging = logging;
        }

        public static PhotinoBlazorAppBuilder CreateDefault(String[] args = default)
        {
            var environment = CreateEnvironment();
            var configBuilder = CreateConfiguration(environment);
            var services = CreateServices(environment, configBuilder);
            var logging = CreateLogging(services);
            var builder = new PhotinoBlazorAppBuilder(services, environment, configBuilder, logging);

            return builder;
        }

        private static ServiceCollection CreateServices(HostingEnvironment environment, IConfigurationBuilder configBuilder)
        {
            var result = new ServiceCollection();

            _ = result
                .AddSingleton<IHostingEnvironment>(environment)
                .AddSingleton((p) => configBuilder.Build())
                .AddSingleton<IConfiguration>(s => s.GetRequiredService<IConfigurationRoot>())
                .AddOptions<PhotinoBlazorAppConfiguration>()
                .Configure(opts =>
                {
                    opts.AppBaseUri = new Uri(PhotinoWebViewManager.AppBaseUri);
                    opts.HostPage = "index.html";
                });

            _ = result
                .AddScoped(sp =>
                {
                    var handler = sp.GetService<PhotinoHttpHandler>();
                    return new HttpClient(handler) { BaseAddress = new Uri(PhotinoWebViewManager.AppBaseUri) };
                })
                .AddSingleton(sp =>
                {
                    var manager = sp.GetService<PhotinoWebViewManager>();
                    var store = sp.GetService<JSComponentConfigurationStore>();

                    return new BlazorWindowRootComponents(manager, store);
                })
                .AddSingleton<Dispatcher, PhotinoDispatcher>()
                .AddSingleton<IFileProvider>(_ =>
                {
                    var root = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "wwwroot");
                    return new PhysicalFileProvider(root);
                })
                .AddSingleton<JSComponentConfigurationStore>()
                .AddSingleton<PhotinoBlazorApp>()
                .AddSingleton<PhotinoHttpHandler>()
                .AddSingleton<PhotinoSynchronizationContext>()
                .AddSingleton<PhotinoWebViewManager>()
                .AddSingleton(new PhotinoWindow())
                .AddBlazorWebView();

            return result;
        }
        private static LoggingBuilder CreateLogging(ServiceCollection services) =>
            new(services);
        private static IConfigurationBuilder CreateConfiguration(HostingEnvironment environment) =>
            new ConfigurationBuilder()
                .SetBasePath(environment.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{environment.EnvironmentName}.json", optional: true);
        private static HostingEnvironment CreateEnvironment() => new();

        public RootComponentList RootComponents { get; }
        public IConfigurationBuilder Configuration { get; private set; }
        public ILoggingBuilder Logging { get; private set; }
        public IServiceCollection Services { get; }
        public IHostingEnvironment Environment { get; }

        public PhotinoBlazorApp Build(Action<IServiceProvider> serviceProviderOptions = null)
        {
            // register root components with DI container
            // Services.AddSingleton(RootComponents);

            var sp = Services.BuildServiceProvider();
            var app = sp.GetRequiredService<PhotinoBlazorApp>();

            serviceProviderOptions?.Invoke(sp);

            app.Initialize(sp, RootComponents);

            var env = app.Services.GetRequiredService<IHostingEnvironment>();
            app.Services.GetRequiredService<ILoggerFactory>()
                .CreateLogger<IHostingEnvironment>()
                .LogInformation("Environment: {environment}", env.EnvironmentName);

            return app;
        }
    }
}
