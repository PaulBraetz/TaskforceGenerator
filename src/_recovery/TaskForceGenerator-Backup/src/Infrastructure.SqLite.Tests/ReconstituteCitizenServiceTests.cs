﻿#pragma warning disable IDE0022 // Use expression body for methods
using TaskforceGenerator.Common;
using System.Data;
using TaskforceGenerator.Infrastructure.SqLite.Tests.Abstractions;
using TaskforceGenerator.Tests.Common;
using TaskforceGenerator.Infrastructure.SqLite.Services.Authentication;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Authentication.Queries;
using TaskforceGenerator.Domain.Core.Exceptions;

namespace TaskforceGenerator.Infrastructure.SqLite.Tests
{
    /// <summary>
    /// Contains tests for <see cref="ReconstituteConnectionService"/>.
    /// </summary>
    [TestClass]
    [TestCategory("Unit Infrastructure.SqLite")]
    public class ReconstituteCitizenServiceTests : DatabaseCommitRequiredTestBase<ReconstituteConnectionService>
    {
        private static Object[][] Citizens => Data.Connections;

        /// <summary>
        /// Tests whether the service throws a <see cref="ConnectionNotExistingException"/> when requesting a citizen that has not been inserted before.
        /// </summary>
        /// <param name="citizen">The citizen to request, but not insert.</param>
        [TestMethod]
        [DynamicData(nameof(Citizens))]
        [ExpectedException(typeof(ConnectionNotExistingException))]
        public async Task ThrowsCNEEWhenCitizenNotInserted(ICitizenConnection citizen)
        {
            _ = await new ReconstituteConnection(citizen.CitizenName, CancellationToken.None).Using(Service);
        }

        /// <summary>
        /// Tests whether the service reconstitutes citizens previously inserted into the database.
        /// </summary>
        /// <param name="citizen">The ciitzen to create and reconstitute.</param>
        [TestMethod]
        [DynamicData(nameof(Citizens))]
        public async Task ReconstitutesCitizenInsertedBefore(ICitizenConnection citizen)
        {
            await Commit(citizen);

            var reconstituted = await new ReconstituteConnection(citizen.CitizenName, CancellationToken.None).Using(Service);

            Assertions.AreEqual(citizen, reconstituted);
        }
        /// <summary>
        /// Tests whether the service throws a <see cref="CitizenNotRegisteredException"/> upon attempting to reconstitute a citizen with an invalid name.
        /// </summary>
        /// <param name="name">The name passed to the service.</param>
        [TestMethod]
        [DataRow("SleepWellPupper")]
        [DataRow("YokoArashi")]
        [DataRow("Terrorente")]
        [DataRow("SpaceDirk")]
        [ExpectedException(typeof(ConnectionNotExistingException))]
        public async Task ThrowsCNREForNotRegisteredCitizens(String name)
        {
            _ = await new ReconstituteConnection(name, CancellationToken.None).Using(Service);
        }
        /// <summary>
        /// Tests whether the service throws a <see cref="OperationCanceledException"/> when passed a cancelled token.
        /// </summary>
        /// <param name="name">The name passed to the service.</param>
        [TestMethod]
        [DataRow("SleepWellPupper")]
        [DataRow("YokoArashi")]
        [DataRow("Terrorente")]
        [DataRow("SpaceDirk")]
        [ExpectedException(typeof(OperationCanceledException), AllowDerivedTypes = true)]
        public async Task ThrowsOCEForNotRegisteredCitizens(String name)
        {
            var token = new CancellationToken(true);
            _ = await new ReconstituteConnection(name, token).Using(Service);
        }
    }
}

#pragma warning restore IDE0022 // Use expression body for methods