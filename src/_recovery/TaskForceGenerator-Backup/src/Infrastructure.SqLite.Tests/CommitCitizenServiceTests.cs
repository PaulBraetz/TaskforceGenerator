﻿using TaskforceGenerator.Common;
using TaskforceGenerator.Infrastructure.SqLite.Tests.Abstractions;
using SimpleInjector;
using TaskforceGenerator.Tests.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Infrastructure.SqLite.Services.Authentication;
using TaskforceGenerator.Domain.Authentication.Queries;
using System.Data;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Authentication.Commands;

namespace TaskforceGenerator.Infrastructure.SqLite.Tests
{
    /// <summary>
    /// Contains tests for <see cref="CommitConnectionService"/>.
    /// </summary>
    [TestClass]
    [TestCategory("Unit Infrastructure.SqLite")]
    public class CommitCitizenServiceTests : DatabaseTestBase<CommitConnectionService>
    {
        private static Object[][] Citizens => Data.Connections;

        private IAsyncCqrsQueryService<ReconstituteConnection, ICitizenConnection> ReconstituteService { get; set; }
        /// <inheritdoc/>
        protected override void Configure(Container container)
        {
            base.Configure(container);
            container.Register<IAsyncCqrsQueryService<ReconstituteConnection, ICitizenConnection>, ReconstituteConnectionService>();
        }
        /// <inheritdoc/>
        protected override void OnAfterConfigure(Container container)
        {
            base.OnAfterConfigure(container);
            ReconstituteService = container.GetInstance<IAsyncCqrsQueryService<ReconstituteConnection, ICitizenConnection>>();
        }

        /// <summary>
        /// Tests whether the service commits citizens to the database.
        /// </summary>
        /// <param name="citizen">The citizen to commit.</param>
        [TestMethod]
        [DynamicData(nameof(Citizens))]
        public async Task CommitsCitizenPassed(ICitizenConnection citizen)
        {
            await new CommitConnection(citizen, CancellationToken.None).Using(Service);

            var reconstituted = await new ReconstituteConnection(citizen.CitizenName, CancellationToken.None).Using(ReconstituteService);

            Assertions.AreEqual(citizen, reconstituted);
        }

        /// <summary>
        /// Tests whether the service throws an <see cref="OperationCanceledException"/> upon receiving a cancelled token.
        /// Since the operation being cancelled before initiating the transaction yields a valid state (citizen does not get inserted), the service should simply return to the caller.
        /// </summary>
        /// <param name="citizen">The citizen to commit.</param>
        [TestMethod]
        [DynamicData(nameof(Citizens))]
        public async Task DoesNotThrowOCEWhenPassedCancelledToken(ICitizenConnection citizen)
        {
            var token = new CancellationToken(true);
            await new CommitConnection(citizen, token).Using(Service);

            var commandText = $"select Count(*) from {Schema.CitizenConnections.TableName} where " +
                $"{Schema.CitizenConnections.CitizenName}='{citizen.CitizenName}'";
            using var command = Database.CreateCommand(commandText);
            var count = await command.ExecuteScalarAsync();

            Assert.AreEqual(0, (Int32)(Int64)count!);
        }
    }
}
