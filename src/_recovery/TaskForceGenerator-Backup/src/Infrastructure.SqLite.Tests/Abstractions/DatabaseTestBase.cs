﻿using TaskforceGenerator.Infrastructure.SqLite.Abstractions;
using TaskforceGenerator.Infrastructure.SqLite.Schema;
using TaskforceGenerator.Tests.Common;

using Infrastructure.SqLite;

using Microsoft.Data.Sqlite;

using SimpleInjector;

namespace TaskforceGenerator.Infrastructure.SqLite.Tests.Abstractions
{
    /// <summary>
    /// Base class for tests whose service require a database to execute.
    /// </summary>
    /// <typeparam name="TService"></typeparam>
    public abstract class DatabaseTestBase<TService> : ServiceTestBase<TService>
        where TService : class
    {
        private sealed class MemoryScript : IScript
        {
            private readonly String _commandText;

            public MemoryScript(String scriptName, String commandText)
            {
                _commandText = commandText;
                ScriptName = scriptName;
            }

            public String ScriptName { get; }

            public void Execute(SqliteConnection connection)
            {
                using var command = new SqliteCommand(_commandText, connection);
                _ = command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Gets the database registered for injection into the service.
        /// </summary>
        protected Database Database { get; private set; }
        /// <summary>
        /// Gets the database schema.
        /// </summary>
        protected Tables Schema { get; private set; }
        /// <inheritdoc/>
        protected override void OnAfterConfigure(Container container)
        {
            Database = container.GetInstance<Database>();
            Schema = container.GetInstance<Tables>();
        }
        /// <inheritdoc/>
        protected override void Configure(Container container)
        {
            base.Configure(container);
            container.Register<CitizenConnections>(Lifestyle.Singleton);
            container.Register<Citizens>(Lifestyle.Singleton);
            container.Register<Tables>(Lifestyle.Singleton);

            container.Register(() =>
            {
                var tables = container.GetInstance<Tables>();
                var result = Database.Create("Data Source=:memory:", scripts: new IScript[]
                {
                    new MemoryScript("Up",
@$"create table if not exists {tables.CitizenConnections.TableName}(
    {tables.CitizenConnections.CitizenName} text primary key, 
    {tables.CitizenConnections.Code} text,
    {tables.CitizenConnections.PasswordHash} blob, 
    {tables.CitizenConnections.PasswordDegreeOfParallelism} integer, 
    {tables.CitizenConnections.PasswordIterations} integer, 
    {tables.CitizenConnections.PasswordMemorySize} integer, 
    {tables.CitizenConnections.PasswordOutputLength} integer, 
    {tables.CitizenConnections.PasswordSalt} blob, 
    {tables.CitizenConnections.PasswordAssociatedData} blob, 
    {tables.CitizenConnections.PasswordKnownSecret} blob)")
                });

                return result;
            }, Lifestyle.Singleton);
        }
    }
}
