﻿using TaskforceGenerator.Common;
using SimpleInjector;
using TaskforceGenerator.Domain.Authentication.Commands;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Infrastructure.SqLite.Services.Authentication;
using TaskforceGenerator.Domain.Authentication.Abstractions;

namespace TaskforceGenerator.Infrastructure.SqLite.Tests.Abstractions
{
    /// <summary>
    /// Base class for tests whose service require a database to execute. A service for committing citizens is provided via the <see cref="CommitService"/> property.
    /// </summary>
    /// <typeparam name="TService">The type of service tested.</typeparam>
    public abstract class DatabaseCommitRequiredTestBase<TService> : DatabaseTestBase<TService>
        where TService : class
    {
        /// <summary>
        /// Gets the service used for committing citizens to the database.
        /// </summary>
        protected IAsyncCqrsCommandService<CommitConnection> CommitService { get; set; }

        /// <inheritdoc/>
        protected override void Configure(Container container)
        {
            base.Configure(container);
            container.Register<IAsyncCqrsCommandService<CommitConnection>, CommitConnectionService>(Lifestyle.Transient);
        }

        /// <inheritdoc/>
        protected override void OnAfterConfigure(Container container)
        {
            base.OnAfterConfigure(container);
            CommitService = container.GetInstance<IAsyncCqrsCommandService<CommitConnection>>();
        }
        /// <summary>
        /// Commits a citizen to the database using <see cref="CommitService"/>.
        /// </summary>
        /// <param name="connection">The cicitzen to submit.</param>
        protected ValueTask Commit(ICitizenConnection connection) => 
            new CommitConnection(connection, CancellationToken.None).Using(CommitService);
    }
}
