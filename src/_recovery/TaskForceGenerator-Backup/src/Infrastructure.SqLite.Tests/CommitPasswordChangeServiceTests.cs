﻿using TaskforceGenerator.Common;
using System.Data;
using TaskforceGenerator.Infrastructure.SqLite.Tests.Abstractions;
using SimpleInjector;
using TaskforceGenerator.Tests.Common;
using TaskforceGenerator.Domain.Authentication.Commands;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Infrastructure.SqLite.Services.Authentication;
using TaskforceGenerator.Domain.Authentication.Queries;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Authentication;

namespace TaskforceGenerator.Infrastructure.SqLite.Tests
{
    /// <summary>
    /// Contains tests for <see cref="CommitPasswordChangeService"/>.
    /// </summary>
    [TestClass]
    [TestCategory("Unit Infrastructure.SqLite")]
    public class CommitPasswordChangeServiceTests : DatabaseCommitRequiredTestBase<CommitPasswordChangeService>
    {
        private static Object[][] PasswordData => Data.Connections.Select(p => p.Append(Data.CreatePassword()).ToArray()).ToArray();

        private IAsyncCqrsQueryService<ReconstituteConnection, ICitizenConnection> ReconstituteService { get; set; }
        /// <inheritdoc/>
        protected override void Configure(Container container)
        {
            base.Configure(container);
            container.Register<IAsyncCqrsQueryService<ReconstituteConnection, ICitizenConnection>, ReconstituteConnectionService>();
        }
        /// <inheritdoc/>
        protected override void OnAfterConfigure(Container container)
        {
            base.OnAfterConfigure(container);
            ReconstituteService = container.GetInstance<IAsyncCqrsQueryService<ReconstituteConnection, ICitizenConnection>>();
        }

        /// <summary>
        /// Tests whether the service will correctly commit password changes to the database.
        /// </summary>
        /// <param name="citizen">The citizen to create and whose password to set.</param>
        /// <param name="newPassword">The new password to commit using the service.</param>
        [TestMethod]
        [DynamicData(nameof(PasswordData))]
        public async Task CommitsPasswordChanges(ICitizenConnection citizen, Password newPassword)
        {
            await Commit(citizen);

            await new CommitPasswordChange(citizen.CitizenName, citizen.Code, newPassword, CancellationToken.None).Using(Service);

            var reconstituted = await new ReconstituteConnection(citizen.CitizenName, CancellationToken.None).Using(ReconstituteService);

            Assert.IsTrue(reconstituted.Password.Hash.SequenceEqual(newPassword.Hash), "hash");

            Assert.IsTrue(reconstituted.Password.Parameters.Data.KnownSecret.SequenceEqual(newPassword.Parameters.Data.KnownSecret), "known secret");
            Assert.IsTrue(reconstituted.Password.Parameters.Data.Salt.SequenceEqual(newPassword.Parameters.Data.Salt), "salt");
            Assert.IsTrue(reconstituted.Password.Parameters.Data.AssociatedData.SequenceEqual(newPassword.Parameters.Data.AssociatedData), "associated data");

            Assert.AreEqual(reconstituted.Password.Parameters.Numerics.Iterations, newPassword.Parameters.Numerics.Iterations, "iterations");
            Assert.AreEqual(reconstituted.Password.Parameters.Numerics.DegreeOfParallelism, newPassword.Parameters.Numerics.DegreeOfParallelism, "degree of parallelism");
            Assert.AreEqual(reconstituted.Password.Parameters.Numerics.MemorySize, newPassword.Parameters.Numerics.MemorySize, "memory size");
            Assert.AreEqual(reconstituted.Password.Parameters.Numerics.OutputLength, newPassword.Parameters.Numerics.OutputLength, "output length");
        }

        /// <summary>
        /// Tests whether the service will correctly commit password changes to the database.
        /// </summary>
        /// <param name="citizen">The citizen to create and whose password to set.</param>
        /// <param name="newPassword">The new password to commit using the service.</param>
        [TestMethod]
        [DynamicData(nameof(PasswordData))]
        public async Task DoesNotThrowOCEWhenPassedCancelledToken(ICitizenConnection citizen, Password newPassword)
        {
            await Commit(citizen);

            var token = new CancellationToken(true);
            await new CommitPasswordChange(citizen.CitizenName, citizen.Code, newPassword, token).Using(Service);

            var reconstituted = await new ReconstituteConnection(citizen.CitizenName, CancellationToken.None).Using(ReconstituteService);

            Assertions.AreEqual(citizen, reconstituted);
        }
    }
}
