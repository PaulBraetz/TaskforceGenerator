﻿using TaskforceGenerator.Application.Commands;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication;
using TaskforceGenerator.Presentation.Models;
using TaskforceGenerator.Presentation.Models.Abstractions;
using TaskforceGenerator.Presentation.Views.Blazor;
using TaskforceGenerator.Presentation.Views.Blazor.Abstractions;
using TaskforceGenerator.Presentation.Views.Blazor.Interop;

using Microsoft.Extensions.Configuration;

using SimpleInjector;
using SimpleInjector.Diagnostics;

using TaskforceGenerator.Common;

namespace TaskforceGenerator.Composition
{
    /// <summary>
    /// Contains Object tree definitions for the presentation layer.
    /// </summary>
    internal static partial class Presentation
    {
        /// <summary>
        /// Defines the presentation layer for photino guis.
        /// </summary>
        public static IRoot WebGui { get; } =
            Root.Create(
                ConnectCitizen.Root,
                Login.Root,
                BuildTaskforce.Root,
                CreateEvent.Root,
                PresentationBase.Root
                );
        /// <summary>
        /// Defines the presentation layer for web guis.
        /// </summary>
        public static IRoot LocalGui { get; } =
            Root.Create(
                ConnectCitizen.Root,
                Login.Root,
                BuildTaskforce.Root,
                CreateEvent.Root,
                PresentationBase.Root
            );
    }
}
