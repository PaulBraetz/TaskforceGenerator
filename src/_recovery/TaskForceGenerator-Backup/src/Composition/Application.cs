﻿using TaskforceGenerator.Application.Commands;
using TaskforceGenerator.Application.Queries;
using TaskforceGenerator.Application.Services;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication;

using SimpleInjector;
using TaskforceGenerator.Domain.Core.Queries;

namespace TaskforceGenerator.Composition
{
    /// <summary>
    /// Contains Object tree definitions for the application layer.
    /// </summary>
    internal static class Application
    {
        /// <summary>
        /// Defines the application layer.
        /// </summary>
        internal static IRoot Root { get; } =
			Composition.Root.Create(c =>
            {
                c.Register<
                    IAsyncCqrsQueryService<ProxyQuery<CheckCitizenExists>, CheckCitizenExists.Result>,
                    ProxyQueryService<CheckCitizenExists, CheckCitizenExists.Result>>
                    (Lifestyle.Scoped);
                c.Register<
                    IAsyncCqrsQueryService<ProxyQuery<CreateOccupant>, IOccupant>,
                    ProxyQueryService<CreateOccupant, IOccupant>>
                    (Lifestyle.Scoped);
                c.Register<IAsyncCqrsQueryService<BuildTaskforce, ITaskforce>, BuildTaskforceService>(Lifestyle.Scoped);
                c.RegisterDecorator<IAsyncCqrsQueryService<BuildTaskforce, ITaskforce>, BuildTaskforceServiceValidation>(Lifestyle.Scoped);
                c.Register<IAsyncCqrsCommandService<Login>, LoginService>(Lifestyle.Scoped);
                c.Register<IAsyncCqrsCommandService<SetPasswordForConnection>, SetPasswordForCitizenService>(Lifestyle.Scoped);
                c.Register<IAsyncCqrsQueryService<GenerateBioCode, BioCode>, GenerateBioCodeService>(Lifestyle.Scoped);
            });
    }
}
