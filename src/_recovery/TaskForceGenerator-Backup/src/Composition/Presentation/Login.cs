﻿using SimpleInjector;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Presentation.Models.Abstractions;

using TaskforceGenerator.Presentation.Models;
using TaskforceGenerator.Presentation.Models.Abstractions.Login;
using TaskforceGenerator.Presentation.Models.Login;
using TaskforceGenerator.Domain.Core.Queries;
using TaskforceGenerator.Domain.Core.Exceptions;
using TaskforceGenerator.Domain.Authentication.Exceptions;

namespace TaskforceGenerator.Composition
{
    internal static partial class Presentation
    {
        /// <summary>
        /// Contains object tree definitions for the login models and views.
        /// </summary>
        internal static class Login
        {
            public static IRoot Root { get; } =
			Composition.Root.Create(c =>
            {
                c.Register<IInputGroupModel>(() =>
                {
                    var nameInput = c.GetInstance<CitizenNameInputModel>();
                    var name = new InputGroupModel<String, String>(nameInput);

                    var password = c.GetInstance<IInputGroupModel<String, String>>();

                    var result = new InputGroupModel(name, password);

                    return result;
                });
                c.Register<ILoginModel>(() =>
                {
                    var navigationManager = c.GetInstance<INavigationManager>();
                    var navigation = new NavigationModel(navigationManager, "/");
                    
                    var loginService = c.GetInstance<IAsyncCqrsCommandService<TaskforceGenerator.Application.Commands.Login>>();
                    var button = c.GetInstance<IButtonModel>();
                    var inputs = c.GetInstance<IInputGroupModel>();

                    var notExistingExceptionFormatter = c.GetInstance<IStaticFormatter<CitizenNotExistingException>>();
                    var notRegisteredExceptionFormatter = c.GetInstance<IStaticFormatter<CitizenNotRegisteredException>>();
                    var passwordExceptionFormatter = c.GetInstance<IStaticFormatter<PasswordMismatchException>>();
                    var handler = new ExceptionHandler(
                        inputs,
                        notExistingExceptionFormatter,
                        notRegisteredExceptionFormatter,
                        passwordExceptionFormatter);

                    var result = LoginModel.Create(
                        loginService,
                        navigation,
                        button,
                        handler,
                        inputs);

                    return result;
                });
            });
        }
    }
}
