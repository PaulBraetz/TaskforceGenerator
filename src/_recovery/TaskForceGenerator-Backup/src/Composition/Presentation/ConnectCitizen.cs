﻿using SimpleInjector;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskforceGenerator.Application.Commands;
using TaskforceGenerator.Application.Queries;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication;
using TaskforceGenerator.Presentation.Models.Abstractions.ConnectCitizen;
using TaskforceGenerator.Presentation.Models.Abstractions;

using TaskforceGenerator.Presentation.Models;
using TaskforceGenerator.Presentation.Models.ConnectCitizen;
using TaskforceGenerator.Domain.Core.Queries;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Authentication.Exceptions;
using TaskforceGenerator.Domain.Core.Exceptions;

namespace TaskforceGenerator.Composition
{
    internal static partial class Presentation
    {
        /// <summary>
        /// Contains object tree definitions for the connect citizen models and views.
        /// </summary>
        internal static class ConnectCitizen
        {
            public static IRoot Root { get; } =
				Composition.Root.Create(c =>
                {
                    c.Register<IInputGroupModel>(() =>
                    {
                        var nameInput = c.GetInstance<CitizenNameInputModel>();
                        var name = new InputGroupModel<String, String>(nameInput);

                        var passwordInput = c.GetInstance<PasswordCreationInputModel>();
                        var password = new InputGroupModel<String, PasswordValidity>(passwordInput);

                        var result = new InputGroupModel(name, password);

                        return result;
                    });
                    c.Register<IButtonGroupModel, ButtonGroupModel>();
                    c.Register<ICodeModel, CodeModel>();
                    c.Register<IConnectCitizenModel>(() =>
                    {
                        var service = c.GetInstance<IAsyncCqrsCommandService<SetPasswordForConnection>>();
                        var codeService = c.GetInstance<IAsyncCqrsQueryService<GenerateBioCode, BioCode>>();
                        var navigationManager = c.GetInstance<INavigationManager>();
                        var navigation = new NavigationModel(navigationManager, "/login");

                        var inputs = c.GetInstance<IInputGroupModel>();
                        var buttons = c.GetInstance<IButtonGroupModel>();
                        var code = c.GetInstance<ICodeModel>();

                        var notExistingExceptionFormatter = c.GetInstance<IStaticFormatter<CitizenNotExistingException>>();
                        var notRegisteredExceptionFormatter = c.GetInstance<IStaticFormatter<CitizenNotRegisteredException>>();
                        var passwordExceptionFormatter = c.GetInstance<IStaticFormatter<PasswordCreationGuidelineException>>();
                        var handler = new ExceptionHandler(
                            inputs,
                            notExistingExceptionFormatter,
                            notRegisteredExceptionFormatter,
                            passwordExceptionFormatter);

                        var result = ConnectCitizenModel.Create(
                            service,
                            codeService,
                            navigation,
                            inputs,
                            buttons,
                            code,
                            handler);

                        return result;
                    });
                });
        }
    }
}