﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TaskforceGenerator.Presentation.Models.Abstractions.CreateEvent;
using TaskforceGenerator.Presentation.Models.Fakes;

namespace TaskforceGenerator.Composition
{
    internal static partial class Presentation
    {
        /// <summary>
        /// Contains object tree definitions for the create event models and views.
        /// </summary>
        private static class CreateEvent
        {
            public static IRoot Root { get; } =
                Composition.Root.Create(c =>
                {
                    c.Register<ICreateEventModel, CreateEventModelFake>();
                    c.Register<ICreateEventInputGroupModel, CreateEventInputGroupModelFake>();
                });
        }
    }
}
