﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using SimpleInjector;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Authentication;
using TaskforceGenerator.Domain.Authentication.Queries;
using TaskforceGenerator.Domain.Authentication.Services;
using TaskforceGenerator.Domain.Authentication.Fakes;
using TaskforceGenerator.Domain.Authentication.Commands;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Application.Commands;
using TaskforceGenerator.Aspects.Decorators;
using TaskforceGenerator.Application.Queries;

namespace TaskforceGenerator.Composition.Domain
{
    /// <summary>
    /// Contains Object tree definitions for the domain layer.
    /// </summary>
    internal static class Authentication
    {

        /// <summary>
        /// Defines a domain layer based on default implementations.
        /// </summary>
        public static IRoot Root { get; } =
			Composition.Root.Create(c =>
            {
                c.Register<IPasswordGuideline>(() =>
                {
                    var config = c.GetRequiredService<IConfiguration>();
                    var guidelineConfig = config.GetRequiredSection("PasswordGuidline");
                    var rules = guidelineConfig.GetSection("Patterns")
                        .GetChildren()
                        .Select(c =>
                        {
                            return (IPasswordRule)new RegexPasswordRule(
                                c.GetRequiredSection("Name").Value,
                                c.GetRequiredSection("Description").Value,
                                c.GetRequiredSection("Pattern").Value);
                        })
                        .ToHashSet();
                    var result = new PasswordGuideline(rules);

                    return result;
                }, Lifestyle.Singleton);
                c.Register<IAsyncCqrsQueryService<CreatePasswordParameters, PasswordParameters>>(() =>
                {
                    var config = c.GetRequiredService<IConfiguration>();
                    var parametersConfig = config.GetSection("PasswordParameters");
                    var saltLength = parametersConfig.GetRequiredInt32Value("SaltLength");
                    var prototypeConfig = parametersConfig.GetRequiredSection("NumericsPrototype");
                    var numericsPrototype = new PasswordParameterNumerics(
                        Iterations: prototypeConfig.GetRequiredInt32Value("Iterations"),
                        DegreeOfParallelism: prototypeConfig.GetRequiredInt32Value("DegreeOfParallelism"),
                        MemorySize: prototypeConfig.GetRequiredInt32Value("MemorySize"),
                        OutputLength: prototypeConfig.GetRequiredInt32Value("OutputLength"));

                    var result = new CreatePasswordParametersService(saltLength, numericsPrototype);

                    return result;
                }, Lifestyle.Singleton);
                c.RegisterDecorator<IAsyncCqrsQueryService<CreatePassword, Password>, CreatePasswordServiceValidation>();
                c.Register<IAsyncCqrsQueryService<CreateBio, IBio>, BioFactory>(Lifestyle.Scoped);
                c.Register<IAsyncCqrsQueryService<CreatePassword, Password>, CreatePasswordService>(Lifestyle.Scoped);

                c.Register<IAsyncCqrsQueryService<CreateConnection, ICitizenConnection>, CreateConnectionService>(Lifestyle.Scoped);

#if DEBUG
                c.Register<IAsyncCqrsQueryService<VerifyBioCode, VerifyBioCode.Result>, VerifyBioCodeAlwaysMatchService>(Lifestyle.Scoped);
#else
                c.Register<IAsyncCqrsQueryService<VerifyBioCode, VerifyBioCode.Result>, VerifyBioCodeService>(Lifestyle.Scoped);
#endif

                c.Register<IAsyncCqrsCommandService<EnsureCitizenConnection>, EnsureCitizenConnectionService>(Lifestyle.Scoped);
                c.Register<IAsyncCqrsCommandService<OpenConnection>, OpenConnectionService>(Lifestyle.Scoped);
                c.Register<IAsyncCqrsQueryService<GenerateRandomBioCode, BioCode>>(() =>
                {
                    var bioCodeTokenCount = c.GetRequiredService<IConfiguration>().GetRequiredInt32Value("BioCodeTokenCount");
                    var result = new GenerateRandomBioCodeService(bioCodeTokenCount);

                    return result;
                }, Lifestyle.Scoped);
                c.Register<IAsyncCqrsQueryService<GenerateInitialPassword, Password>>(() =>
                {
                    var generatedPasswordsLength = c.GetRequiredService<IConfiguration>().GetRequiredInt32Value("GeneratedPasswordsLength");
                    var parametersService = c.GetRequiredService<IAsyncCqrsQueryService<CreatePasswordParameters, PasswordParameters>>();
                    var passwordService = c.GetRequiredService<IAsyncCqrsQueryService<CreatePassword, Password>>();

                    var result = new GenerateInitialPasswordService(passwordService, parametersService, generatedPasswordsLength);

                    return result;
                }, Lifestyle.Scoped);

                c.Register<IObservableUserContext, UserContext>(Lifestyle.Scoped);
                c.Register<IUserContext, UserContext>(Lifestyle.Scoped);

                c.Register<IAsyncCqrsCommandService<Authenticate>, AuthenticateService>(Lifestyle.Scoped);
                c.Register<IAsyncCqrsCommandService<SetContextConnection>, SetContextConnectionService>(Lifestyle.Scoped);
                c.Register<IAsyncCqrsQueryService<GetContextConnection, ICitizenConnection>, GetContextConnectionService>(Lifestyle.Scoped);
            });
    }
}
