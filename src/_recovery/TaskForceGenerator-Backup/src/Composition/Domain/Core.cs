﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Authentication.Queries;
using TaskforceGenerator.Domain.Authentication.Services;
using TaskforceGenerator.Domain.Core.Abstractions;
using TaskforceGenerator.Domain.Core.Commands;
using TaskforceGenerator.Domain.Core.Queries;
using TaskforceGenerator.Domain.Core.Services;

using SimpleInjector;
using TaskforceGenerator.Domain.Core;
using TaskforceGenerator.Domain.Core.Fakes;

namespace TaskforceGenerator.Composition.Domain
{
    /// <summary>
    /// Contains Object tree definitions for the domain layer.
    /// </summary>
    internal static class Core
    {
        /// <summary>
        /// Defines a domain layer based on default implementations.
        /// </summary>
        public static IRoot Root { get; } =
			Composition.Root.Create(c =>
            {
                c.Register<ICrewingSchemeMap>(() =>
                {
                    var provier = c.GetInstance<IValueSetProvider<ICrewingScheme>>();
                    var result = CrewingSchemeMap.Create(provier);

                    return result;
                }, Lifestyle.Singleton);
                c.Register<IValueSetProvider<ISlot>, SlotProvider>(Lifestyle.Singleton);
                c.Register<IValueSetProvider<ISlotPreference>, SlotPreferenceProvider>(Lifestyle.Singleton);
                c.Register<IValueSetProvider<IVehicle>, VehicleProvider>(Lifestyle.Singleton);
                c.Register<IValueSetProvider<ICrewingScheme>, SchemeProvider>(Lifestyle.Singleton);
                c.Register<IAsyncCqrsCommandService<EnsureCitizen>, EnsureCitizenService>(Lifestyle.Scoped);
                c.Register<IAsyncCqrsQueryService<CreateCitizen, ICitizen>, CreateCitizenService>(Lifestyle.Scoped);
                c.Register<IAsyncCqrsQueryService<CreateOccupant, IOccupant>, CreateOccupantService>(Lifestyle.Scoped);
            });
    }
}
