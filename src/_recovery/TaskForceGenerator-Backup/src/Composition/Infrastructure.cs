﻿using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Authentication.Commands;
using TaskforceGenerator.Domain.Authentication.Queries;
using TaskforceGenerator.Domain.Core.Commands;
using TaskforceGenerator.Domain.Core.Queries;
using TaskforceGenerator.Infrastructure.Queries;
using TaskforceGenerator.Infrastructure.Services;
using TaskforceGenerator.Infrastructure.SqLite.Abstractions;
using TaskforceGenerator.Infrastructure.SqLite.Schema;
using TaskforceGenerator.Infrastructure.SqLite.Services.Authentication;
using TaskforceGenerator.Infrastructure.SqLite.Services.Core;
using Infrastructure.SqLite;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using SimpleInjector;

namespace TaskforceGenerator.Composition
{
    /// <summary>
    /// Contains Object tree definitions for the infrastructure layer.
    /// </summary>
    internal static class Infrastructure
    {
        /// <summary>
        /// Defines the infrastructure layer.
        /// </summary>
        public static IRoot Root { get; } =
            Composition.Root.Create(c =>
            {
                c.Register<IAsyncCqrsCommandService<CommitCitizen>, CommitCitizenService>(Lifestyle.Scoped);

                c.Register<IAsyncCqrsQueryService<LoadCitizenPage, LoadCitizenPage.Result>, LoadCitizenPageService>(Lifestyle.Singleton);
                c.Register<IAsyncCqrsQueryService<LoadCitizenOrgs, LoadCitizenOrgs.Result>, LoadCitizenOrgsService>(Lifestyle.Scoped);
                c.Register<IAsyncCqrsQueryService<LoadActualName, LoadActualName.Result>, LoadActualNameService>(Lifestyle.Scoped);
                c.Register<IAsyncCqrsQueryService<LoadBio, IBio>, LoadBioService>(Lifestyle.Scoped);
                c.Register<IAsyncCqrsQueryService<CheckCitizenExists, CheckCitizenExists.Result>, CheckCitizenExistsService>(Lifestyle.Singleton);

                c.Register<Tables>(Lifestyle.Singleton);
                c.Register<Citizens>(Lifestyle.Singleton);
                c.Register<CitizenConnections>(Lifestyle.Singleton);
                c.Register(() =>
                {
                    var config = c.GetRequiredService<IConfiguration>();
                    var connectionString = config.GetRequiredSection("ConnectionStrings")["SQLite"];
                    var scripts = config.GetSection("SQLiteScripts")
                        .GetChildren()
                        .Select(s => s.Value)
                        .Select(s => new FileScript(s))
                        .ToArray();
                    var logger = c.GetRequiredService<ILoggerFactory>().CreateLogger<Database>();
                    var progress = new ProgressStrategy<IScript>(s => logger.LogInformation("Executed {scriptPath}", s.ScriptName));
                    var result = Database.Create(connectionString, progress, scripts);

                    return result;
                }, Lifestyle.Singleton);

                c.Register<IAsyncCqrsQueryService<CheckConnectionExists, CheckConnectionExists.Result>, CheckConnectionExistsService>(Lifestyle.Scoped);
                c.Register<IAsyncCqrsQueryService<CheckCitizenRegistered, CheckCitizenRegistered.Result>, CheckCitizenRegisteredService>(Lifestyle.Scoped);
                c.Register<IAsyncCqrsQueryService<ReconstituteConnection, ICitizenConnection>, ReconstituteConnectionService>(Lifestyle.Scoped);
                c.Register<IAsyncCqrsCommandService<CommitBioCodeChange>, CommitBioCodeChangeService>(Lifestyle.Scoped);
                c.Register<IAsyncCqrsCommandService<CommitPasswordChange>, CommitPasswordChangeService>(Lifestyle.Scoped);
                c.Register<IAsyncCqrsCommandService<CommitConnection>, CommitConnectionService>(Lifestyle.Scoped);
            });
    }
}
