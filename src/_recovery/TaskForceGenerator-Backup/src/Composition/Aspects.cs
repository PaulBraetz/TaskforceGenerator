﻿using TaskforceGenerator.Application.Commands;
using TaskforceGenerator.Application.Formatters;
using TaskforceGenerator.Application.Queries;
using TaskforceGenerator.Aspects;
using TaskforceGenerator.Aspects.Abstractions;
using TaskforceGenerator.Aspects.Decorators;
using TaskforceGenerator.Aspects.Proxies;
using TaskforceGenerator.Aspects.Services;
using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Authentication.Commands;
using TaskforceGenerator.Domain.Authentication.Formatters;
using TaskforceGenerator.Domain.Authentication.Queries;
using TaskforceGenerator.Domain.Core.Exceptions;
using TaskforceGenerator.Domain.Core.Queries;
using TaskforceGenerator.Infrastructure.Formatters;
using TaskforceGenerator.Infrastructure.Queries;
using TaskforceGenerator.Presentation.Models;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using SimpleInjector;
using TaskforceGenerator.Presentation.Models.Formatters;
using TaskforceGenerator.Application.Exceptions;
using TaskforceGenerator.Application.Services;
using TaskforceGenerator.Domain.Authentication.Exceptions;

namespace TaskforceGenerator.Composition
{
    /// <summary>
    /// Contains Object tree definitions for cross-cutting concerns.
    /// </summary>
    internal static class Aspects
    {
#pragma warning disable IDE0053 // Use expression body for lambda expression
        public static IRoot LocalGui { get; } =
            Root.Create(c =>
            {
                RegisterCoreAspects(c);
            });
#pragma warning restore IDE0053 // Use expression body for lambda expression
        /// <summary>
        /// Defines cross-cutting concerns.
        /// </summary>
        public static IRoot WebGui { get; } =
            Root.Create(c =>
            {
                RegisterCoreAspects(c);
                RegisterHttpContextLoggingDecorator(c);
            });

        private static void RegisterCoreAspects(Container c)
        {
            RegisterFormatters(c);
            RegisterLoggingService(c);
            RegisterGenerateBioCodeShortCircuit(c);

            RegisterConnectionEnsuringDecorators(c);
            RegisterCachedQueryProxies(c);
            RegisterQueryCancellationDecorators(c);
            RegisterNotificationDecorators(c);
            RegisterCitizenNameReplacementProxies(c);
            //Consider enabling this in contexts where the Ui sync context only allows for a single thread.
            //RegisterSyncContextInterceptionProxies(c);
            RegisterLoggingDecorators(c);
        }

        private static void RegisterQueryCancellationDecorators(Container c) =>
            c.RegisterDecorator(typeof(IAsyncCqrsQueryService<,>), typeof(QueryCancellationDecorator<,>), Lifestyle.Scoped);

        private static void RegisterNotificationDecorators(Container c)
        {
            c.RegisterDecorator(
                typeof(IAsyncCqrsCommandService<>),
                typeof(NotifySuccessCommandServiceDecorator<>),
                Lifestyle.Scoped,
                IsApplicationService);
            c.RegisterDecorator(typeof(IAsyncCqrsCommandService<>),
                typeof(NotifyExceptionCommandServiceDecorator<>),
                Lifestyle.Scoped,
                IsApplicationService);

            c.RegisterDecorator(
                typeof(IAsyncCqrsQueryService<,>),
                typeof(NotifyExceptionQueryServiceDecorator<,>),
                IsApplicationService);
        }
        private static void RegisterHttpContextLoggingDecorator(Container c)
        {
            c.RegisterDecorator(
                typeof(IAsyncCqrsCommandService<>),
                typeof(HttpContextConnectionIdLoggingCommandServiceDecorator<>),
                Lifestyle.Scoped,
                IsApplicationService);

            c.RegisterDecorator(
                typeof(IAsyncCqrsQueryService<,>),
                typeof(HttpContextConnectionIdLoggingQueryServiceDecorator<,>),
                Lifestyle.Scoped,
                IsApplicationService);
        }
        private static void RegisterLoggingDecorators(Container c)
        {
            //timestamp->execution->thread->exception->time->service

            c.RegisterDecorator(typeof(IAsyncCqrsCommandService<>), typeof(ExecutionTimeLoggingCommandServiceDecorator<>), Lifestyle.Scoped);
            c.RegisterDecorator(typeof(IAsyncCqrsCommandService<>), typeof(ExceptionLoggingCommandServiceDecorator<>), Lifestyle.Scoped);
            c.RegisterDecorator(typeof(IAsyncCqrsCommandService<>), typeof(ThreadIdLoggingCommandServiceDecorator<>), Lifestyle.Scoped);
            c.RegisterDecorator(typeof(IAsyncCqrsCommandService<>), typeof(ExecutionLoggingCommandServiceDecorator<>), Lifestyle.Scoped);
            c.RegisterDecorator(
                typeof(IAsyncCqrsCommandService<>),
                typeof(TimestampLoggingCommandServiceDecorator<>),
                Lifestyle.Scoped,
                IsApplicationService);

            c.RegisterDecorator(typeof(IAsyncCqrsQueryService<,>), typeof(ExecutionTimeLoggingQueryServiceDecorator<,>), Lifestyle.Scoped);
            c.RegisterDecorator(typeof(IAsyncCqrsQueryService<,>), typeof(ExceptionLoggingQueryServiceDecorator<,>), Lifestyle.Scoped);
            c.RegisterDecorator(typeof(IAsyncCqrsQueryService<,>), typeof(ThreadIdLoggingQueryServiceDecorator<,>), Lifestyle.Scoped);
            c.RegisterDecorator(typeof(IAsyncCqrsQueryService<,>), typeof(ExecutionLoggingQueryServiceDecorator<,>), Lifestyle.Scoped);
            c.RegisterDecorator(
                typeof(IAsyncCqrsQueryService<,>),
                typeof(TimestampLoggingQueryServiceDecorator<,>),
                Lifestyle.Scoped,
                IsApplicationService);
        }
#pragma warning disable IDE0051 // Remove unused private members
        private static void RegisterSyncContextInterceptionProxies(Container c)
        {
            c.RegisterDecorator(
                typeof(IAsyncCqrsQueryService<,>),
                typeof(SyncContextInterceptionQueryServiceProxy<,>),
                Lifestyle.Scoped,
                IsApplicationService);

            c.RegisterDecorator(
                typeof(IAsyncCqrsCommandService<>),
                typeof(SyncContextInterceptionCommandServiceProxy<>),
                Lifestyle.Scoped,
                IsApplicationService);
        }
#pragma warning restore IDE0051 // Remove unused private members
        private static void RegisterCitizenNameReplacementProxies(Container c)
        {
            RegisterReplacementStrategyConvention(c);

            c.RegisterDecorator(typeof(IAsyncCqrsCommandService<>), typeof(CitizenNameReplacementCommandProxy<>), RequiresNameReplacement);
            c.RegisterDecorator(typeof(IAsyncCqrsQueryService<,>), typeof(CitizenNameReplacementQueryProxy<,>), RequiresNameReplacement);
        }
        private static void RegisterExtractionStrategyConvention(Container c)
        {
            c.Register(typeof(ICitizenNamesExtractionStrategy<>),
                typeof(TaskforceGenerator.Aspects.Decorators.ExtractionStrategies.LoginStrategy).Assembly
                .GetTypes()
                .Where(t => t.Namespace == typeof(TaskforceGenerator.Aspects.Decorators.ExtractionStrategies.LoginStrategy).Namespace));
        }
        private static void RegisterConnectionEnsuringDecorators(Container c)
        {
            RegisterExtractionStrategyConvention(c);

            registerCommand<Login>();
            registerCommand<SetPasswordForConnection>();

            registerQuery<GenerateBioCode, BioCode>();

            void registerCommand<TCommad>()
                where TCommad : ICqrsCommand
            {
                c.RegisterDecorator<IAsyncCqrsCommandService<TCommad>, EnsureCitizenConnectionCommandServiceDecorator<TCommad>>(Lifestyle.Scoped);
                c.RegisterDecorator<IAsyncCqrsCommandService<TCommad>, EnsureCitizenCommandServiceDecorator<TCommad>>(Lifestyle.Scoped);
            }

            void registerQuery<TQuery, TResult>()
                where TQuery : ICqrsQuery
            {
                c.RegisterDecorator<IAsyncCqrsQueryService<TQuery, TResult>, EnsureCitizenConnectionQueryServiceDecorator<TQuery, TResult>>(Lifestyle.Scoped);
                c.RegisterDecorator<IAsyncCqrsQueryService<TQuery, TResult>, EnsureCitizenQueryServiceDecorator<TQuery, TResult>>(Lifestyle.Scoped);
            }
        }

        private static void RegisterGenerateBioCodeShortCircuit(Container c)
        {
            c.Register<ICacheOptions<GenerateBioCode>>(
                                () => c.GetInstance<IConfiguration>().GetRequiredCacheOptions(new EqualityComparerStrategy<GenerateBioCode>(
                                    (x, y) => x.CitizenName == y.CitizenName,
                                    obj => HashCode.Combine(obj.CitizenName))),
                                Lifestyle.Singleton);
            c.Register<ICache<GenerateBioCode, ShortCircuitCacheEntry<CitizenNotExistingException>>, Cache<GenerateBioCode, ShortCircuitCacheEntry<CitizenNotExistingException>>>(Lifestyle.Singleton);
            c.RegisterDecorator<IAsyncCqrsQueryService<GenerateBioCode, BioCode>, ShortCircuitQueryServiceProxy<GenerateBioCode, BioCode, CitizenNotExistingException>>(Lifestyle.Scoped);
        }

        private static void RegisterReplacementStrategyConvention(Container c)
        {
            var implementationTypes = typeof(TaskforceGenerator.Aspects.Proxies.ReplacementStrategies.LoginStrategy).Assembly
                .GetTypes()
                .Where(t => t.Namespace == typeof(TaskforceGenerator.Aspects.Proxies.ReplacementStrategies.LoginStrategy).Namespace)
                .Where(t => t.IsPublic);
            c.Register(typeof(ICitizenNamesReplacementStrategy<>), implementationTypes);
        }

        private static void RegisterCachedQueryProxies(Container c)
        {
            RegisterResultCache<LoadCitizenPage, LoadCitizenPage.Result>(c);
            RegisterResultCache<LoadActualName, LoadActualName.Result>(c);
            RegisterResultCache<CheckCitizenExists, CheckCitizenExists.Result>(c);
            RegisterResultCache<LoadBio, IBio>(c);
        }

        private static void RegisterLoggingService(Container c)
        {
            c.Register(() => new LoggingService(c.GetRequiredService<ILoggerFactory>().CreateLogger<LoggingService>()), Lifestyle.Scoped);
            c.Register<ILoggingService>(() => c.GetRequiredService<LoggingService>(), Lifestyle.Scoped);
        }

        private static void RegisterFormatters(Container c)
        {
            c.Register<IStaticFormatter<LoadCitizenPage.Result>, LoadCitizenPageResultFormatter>(Lifestyle.Singleton);
            c.Register<IStaticFormatter<CreateBio>, CreateBioFormatter>(Lifestyle.Singleton);
            c.Register<IStaticFormatter<CreatePassword>, CreatePasswordFormatter>(Lifestyle.Singleton);
            c.Register<IStaticFormatter<Authenticate>, AuthenticateFormatter>(Lifestyle.Singleton);
            c.Register<IStaticFormatter<BioCodeMismatchException>, BioCodeMismatchExceptionFormatter>(Lifestyle.Singleton);
            c.Register<IStaticFormatter<PasswordMismatchException>, PasswordMismatchExceptionFormatter>(Lifestyle.Singleton);
            c.RegisterConditional(
                typeof(IStaticFormatter<SetPasswordForConnection>),
                c =>
                {
                    var result =
                        c.Consumer != null &&
                        c.Consumer.ImplementationType == typeof(ToastsSuccessModel<SetPasswordForConnection>) ?
                    typeof(TaskforceGenerator.Presentation.Models.Formatters.SetPasswordForConnectionFormatter) :
                    typeof(TaskforceGenerator.Application.Formatters.SetPasswordForConnectionFormatter);

                    return result;
                },
                Lifestyle.Singleton,
                c => c.ServiceType == typeof(IStaticFormatter<SetPasswordForConnection>));
            c.RegisterConditional(
                typeof(IStaticFormatter<Login>),
                c =>
                {
                    var result =
                        c.Consumer != null &&
                        c.Consumer.ImplementationType == typeof(ToastsSuccessModel<Login>) ?
                    typeof(TaskforceGenerator.Presentation.Models.Formatters.LoginFormatter) :
                    typeof(TaskforceGenerator.Application.Formatters.LoginFormatter);

                    return result;
                },
                Lifestyle.Singleton,
                c => c.ServiceType == typeof(IStaticFormatter<Login>));
            c.RegisterConditional(
                typeof(IStaticFormatter<>),
                typeof(SlotNameFormatter<>),
                Lifestyle.Singleton,
                c => c.ServiceType.GenericTypeArguments[0].IsAssignableTo(typeof(ISlot)));
            c.RegisterConditional(
                typeof(IStaticFormatter<>),
                typeof(SlotPreferenceNameFormatter<>),
                Lifestyle.Singleton,
                c => c.ServiceType.GenericTypeArguments[0].IsAssignableTo(typeof(ISlotPreference)));
            c.RegisterConditional(
                typeof(IStaticFormatter<>),
                typeof(VehicleNameFormatter<>),
                Lifestyle.Singleton,
                c => c.ServiceType.GenericTypeArguments[0].IsAssignableTo(typeof(IVehicle)));
            c.RegisterConditional(
                typeof(IStaticFormatter<>),
                typeof(TypeNameFormatter<>),
                Lifestyle.Singleton,
                c => !c.Handled && c.ServiceType.GenericTypeArguments[0].IsAssignableTo(typeof(Exception)));
            c.RegisterConditional(
                typeof(IStaticFormatter<>),
                typeof(ToStringFormatter<>),
                Lifestyle.Singleton,
                c => !c.Handled);
        }

        private static void RegisterResultCache<TQuery, TResult>(Container container)
            where TQuery : ICqrsQuery
        {
            container.Register<ICacheOptions<TQuery>>(
                () => container.GetInstance<IConfiguration>().GetRequiredCacheOptions<TQuery>(),
                Lifestyle.Singleton);
            container.Register<ICache<TQuery, Task<TResult>>, Cache<TQuery, Task<TResult>>>(Lifestyle.Singleton);
            container.RegisterDecorator<IAsyncCqrsQueryService<TQuery, TResult>, ResultCacheServiceProxy<TQuery, TResult>>(Lifestyle.Scoped);
        }
        private static Boolean RequiresNameReplacement(DecoratorPredicateContext context) => IsApplicationService(context) && IsNotProxService(context);
        private static Boolean IsNotProxService(DecoratorPredicateContext context)
        {
            if(!context.ImplementationType.IsConstructedGenericType)
                return true;

            var genericTypeDefinition = context.ImplementationType.GetGenericTypeDefinition();
            var result = genericTypeDefinition != typeof(ProxyCommandService<>) &&
                genericTypeDefinition != typeof(ProxyQueryService<,>);

            return result;
        }
        private static Boolean IsApplicationService(DecoratorPredicateContext context)
        {
            var result = context.ImplementationType.Assembly ==
                typeof(SetPasswordForConnection).Assembly;

            return result;
        }
    }
}
