﻿using System.ComponentModel;

using TaskforceGenerator.Common;
using TaskforceGenerator.Presentation.Models.Abstractions;

namespace TaskforceGenerator.Presentation.Models.Tests.Stubs
{
    internal sealed class InputModel<TValue, TError> : IInputModel<TValue, TError>
    {
        private TError _error;
        private TValue _value;
        private InputValidityType _validity;
        private String _placeholder;

        public InputValidityType Validity
        {
            get => _validity;
            private set
            {
                _validity = value;
                InvokePropertyChanged(nameof(Validity));
            }
        }

        public void SetInvalid(TError error)
        {
            Validity = InputValidityType.Invalid;
            Error = error;
        }
        public void SetValid() => Validity = InputValidityType.Valid;

        public void UnsetValidity() => Validity = InputValidityType.None;

        public TValue Value
        {
            get => _value;
            set
            {
                _value = value;
                InvokePropertyChanged(nameof(Value));
            }
        }
        public String Placeholder{
            get=>_placeholder;
            set{
                _placeholder = value;
                InvokePropertyChanged(nameof(Placeholder));
            }
        }
        public TError Error
        {
            get => _error;
            private set
            {
                _error = value;
                InvokePropertyChanged(nameof(Error));
            }
        }

        public Boolean EnterCalled { get; private set; }
        public Func<Task> EnterStrategy { get; set; } = () => Task.CompletedTask;

        public async Task Enter()
        {
            EnterCalled = true;
            await InvokeEntered();
            await EnterStrategy();
        }

        public void InvokePropertyChanged(String propertyName) =>
            PropertyChanged?.Invoke(this, new(propertyName));
        public Task InvokeEntered() =>
            Entered.InvokeAsync();

        public event AsynchronousEventHandler Entered;
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
