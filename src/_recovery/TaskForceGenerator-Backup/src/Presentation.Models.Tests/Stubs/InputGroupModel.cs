﻿using System.ComponentModel;

using TaskforceGenerator.Presentation.Models.Abstractions;

namespace TaskforceGenerator.Presentation.Models.Tests.Stubs
{
    internal sealed class InputGroupModel<TValue, TError> : IInputGroupModel<TValue, TError>
    {
        private String _label = String.Empty;
        private String _description = String.Empty;

        public InputGroupModel(InputModel<TValue, TError> inputStub)
        {
            InputStub = inputStub;
            InputStub.PropertyChanged += (s, e) =>
            {
                InvokePropertyChanged(nameof(InputStub));
                InvokePropertyChanged(nameof(Input));
            };
        }

        public InputModel<TValue, TError> InputStub { get; }
        public IInputModel<TValue, TError> Input => InputStub;
        public String Label
        {
            get => _label;
            set
            {
                _label = value;
                InvokePropertyChanged(nameof(Label));
            }
        }
        public String Description
        {
            get => _description;
            set
            {
                _description = value;
                InvokePropertyChanged(nameof(Description));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        public void InvokePropertyChanged(String propertyName) =>
            PropertyChanged?.Invoke(this, new(propertyName));
    }
}
