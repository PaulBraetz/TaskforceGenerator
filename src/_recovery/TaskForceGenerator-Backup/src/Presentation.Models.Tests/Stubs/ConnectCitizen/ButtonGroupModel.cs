﻿using TaskforceGenerator.Presentation.Models.Abstractions;
using TaskforceGenerator.Presentation.Models.Abstractions.ConnectCitizen;

namespace TaskforceGenerator.Presentation.Models.Tests.Stubs.ConnectCitizen
{
    internal sealed class ButtonGroupModel : IButtonGroupModel
    {
        public ButtonGroupModel(ButtonModel generateBioCodeStub, ButtonModel connectCitizenStub)
        {
            GenerateBioCodeStub = generateBioCodeStub;
            ConnectCitizenStub = connectCitizenStub;
        }

        public ButtonModel GenerateBioCodeStub { get; }
        public ButtonModel ConnectCitizenStub { get; }

        public IButtonModel GenerateBioCode => GenerateBioCodeStub;
        public IButtonModel ConnectCitizen => ConnectCitizenStub;
    }
}
