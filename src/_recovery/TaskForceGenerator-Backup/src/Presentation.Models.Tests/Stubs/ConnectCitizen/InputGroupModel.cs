﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TaskforceGenerator.Domain.Authentication;
using TaskforceGenerator.Presentation.Models.Abstractions;
using TaskforceGenerator.Presentation.Models.Abstractions.ConnectCitizen;

namespace TaskforceGenerator.Presentation.Models.Tests.Stubs.ConnectCitizen
{
    internal sealed class InputGroupModel : IInputGroupModel
    {
        public IInputGroupModel<String, String> Name { get; }
        public IInputGroupModel<String, PasswordValidity> Password { get; }
    }
}
