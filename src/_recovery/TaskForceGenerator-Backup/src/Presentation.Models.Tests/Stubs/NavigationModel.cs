﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TaskforceGenerator.Presentation.Models.Abstractions;

namespace TaskforceGenerator.Presentation.Models.Tests.Stubs
{
    internal sealed class NavigationModel : INavigationModel
    {
        public Boolean Navigated { get; private set; }
        public void Navigate() => Navigated = true;
    }
}
