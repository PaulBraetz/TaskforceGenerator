﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using SimpleInjector;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TaskforceGenerator.Application.Commands;
using TaskforceGenerator.Application.Queries;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication;
using TaskforceGenerator.Presentation.Models.Abstractions;
using TaskforceGenerator.Presentation.Models.Abstractions.ConnectCitizen;
using TaskforceGenerator.Presentation.Models.ConnectCitizen;
using TaskforceGenerator.Presentation.Models.Tests.Stubs;
using TaskforceGenerator.Tests.Common;

namespace TaskforceGenerator.Presentation.Models.Tests
{
    /// <summary>
    /// Contains tests for <see cref="ConnectCitizenModel"/>.
    /// </summary>
    [TestClass]
    public class ConectCitizenModelTests : ServiceTestBase<ConnectCitizenModel>
    {
        /// <inheritdoc/>
        protected override void Configure(Container container)
        {
            base.Configure(container);

            container.Register<Stubs.ButtonModel>();
            container.Register<IButtonModel, Stubs.ButtonModel>();

            container.Register<Stubs.NavigationModel>();
            container.Register<INavigationModel, Stubs.NavigationModel>();

            container.Register<Stubs.ExceptionHandler>();
            container.Register<IExceptionHandler, Stubs.ExceptionHandler>();

            container.Register<Stubs.ConnectCitizen.ButtonGroupModel>();
            container.Register<IButtonGroupModel, Stubs.ConnectCitizen.ButtonGroupModel>();

            container.Register<Stubs.ConnectCitizen.InputGroupModel>();
            container.Register<IInputGroupModel, Stubs.ConnectCitizen.InputGroupModel>();

            container.Register<Stubs.ConnectCitizen.CodeModel>();
            container.Register<ICodeModel, Stubs.ConnectCitizen.CodeModel>();

            container.Register<AsyncCqrsQueryServiceStub<GenerateBioCode, BioCode>>();
            container.Register<
                IAsyncCqrsQueryService<GenerateBioCode, BioCode>,
                AsyncCqrsQueryServiceStub<GenerateBioCode, BioCode>>();

            container.Register<AsyncCqrsCommandServiceStub<SetPasswordForConnection>>();
            container.Register<
                IAsyncCqrsCommandService<SetPasswordForConnection>,
                AsyncCqrsCommandServiceStub<SetPasswordForConnection>>();
        }
        /// <summary>
        /// Asserts ?
        /// </summary>
        [TestMethod]
        public void MyTestMethod()
        {

        }
    }
}
