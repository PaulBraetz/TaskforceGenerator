﻿using TaskforceGenerator.Domain.Authentication.Queries;
using TaskforceGenerator.Domain.Authentication;

namespace TaskforceGenerator.Application.Exceptions
{
    /// <summary>
    /// Exception thrown when a bio code provided does not match the one expected when executing <see cref="VerifyBioCode"/>.
    /// </summary>
    public sealed class BioCodeMismatchException : Exception
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="code">The invalid code provided.</param>
        /// <param name="mismatchType">The mismatch type causing the exception.</param>
        /// <param name="message">The exception message.</param>
        public BioCodeMismatchException(BioCode code, VerifyBioCode.Result mismatchType, String message) : base(message)
        {
            Code = code;
            MismatchType = mismatchType;
        }
        /// <summary>
        /// Creates and initializes a new instance.
        /// </summary>
        /// <param name="code">The </param>
        /// <param name="mismatchType"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public static BioCodeMismatchException Create(BioCode code, VerifyBioCode.Result mismatchType)
        {
            var message = mismatchType switch
            {
                VerifyBioCode.Result.BioMismatch => $"The required code does not match the bio retrieved for the citizen. Make sure the required code is posted to the citizens RSI bio.",
                VerifyBioCode.Result.CitizenMismatch => $"The required code does not match the system entities code. Please try again.",
                _ => throw new ArgumentException($"Invalid mismatch type received: {mismatchType}", nameof(mismatchType))
            };
            var result = new BioCodeMismatchException(code, mismatchType, message);

            return result;
        }
        /// <summary>
        /// Gets the invalid code provided.
        /// </summary>
        public BioCode Code { get; }
        /// <summary>
        /// Gets the mismatch type causing the exception.
        /// </summary>
        public VerifyBioCode.Result MismatchType { get; }
    }
}
