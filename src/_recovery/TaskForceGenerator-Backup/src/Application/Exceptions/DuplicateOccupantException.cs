﻿namespace TaskforceGenerator.Application.Exceptions
{
	/// <summary>
	/// Exception throwns when a taskforce is attempted to be built using duplicate occupants.
	/// </summary>
	public sealed class DuplicateOccupantException:ApplicationException
    {
		/// <summary>
		/// Initializes a new instance.
		/// </summary>
		/// <param name="name">The name of the duplicate occupant.</param>
		/// <param name="count">The amount of duplicate occupants passed.</param>
		public DuplicateOccupantException(String name, Int32 count)
			:base($"The occupant '{name}' was supplied {count} times.")
		{
			Name = name;
			Count = count;
		}
		/// <summary>
		/// Gets the name of the duplicate occupant.
		/// </summary>
		public String Name { get; }
		/// <summary>
		/// Gets the amount of duplicate occupants passed.
		/// </summary>
        public Int32 Count { get; }
    }
}
