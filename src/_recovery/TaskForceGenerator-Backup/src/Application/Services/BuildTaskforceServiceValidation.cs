﻿using TaskforceGenerator.Application.Exceptions;
using TaskforceGenerator.Application.Queries;
using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Application.Services
{
	/// <summary>
	/// Decorator for validating <see cref="BuildTaskforce"/> executions.
	/// </summary>
	public sealed class BuildTaskforceServiceValidation : IAsyncCqrsQueryService<BuildTaskforce, ITaskforce>
	{
		private readonly IAsyncCqrsQueryService<BuildTaskforce, ITaskforce> _decorated;

		/// <summary>
		/// Initializes a new instance.
		/// </summary>
		/// <param name="decorated">The service whose queries to validate.</param>
		public BuildTaskforceServiceValidation(IAsyncCqrsQueryService<BuildTaskforce, ITaskforce> decorated)
		{
			_decorated = decorated;
		}

		/// <inheritdoc/>
		public ValueTask<ITaskforce> Execute(BuildTaskforce query)
		{
			var (name, count) = query.Occupants.GroupBy(o => o.CitizenName)
				.Select(g => (name: g.Key, count: g.Count()))
				.Where(t => t.count > 1)
				.FirstOrDefault();

			var result = count > 0 ?
				throw new DuplicateOccupantException(name, count) :
				_decorated.Execute(query);

			return result;
		}
	}
}
