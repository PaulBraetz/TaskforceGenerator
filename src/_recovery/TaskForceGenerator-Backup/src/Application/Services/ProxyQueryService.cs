﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TaskforceGenerator.Application.Queries;
using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Application.Services
{
    /// <summary>
    /// General proxy query service.
    /// Queries should be proxied in order to force dependency injection of application services instead of domain or infrsatructure services.
    /// This leads to proper application of cross-cutting concerns, even though the underlying executing service might have been injected directly instead.
    /// </summary>
    /// <typeparam name="TQuery">The type of query to proxy.</typeparam>
    /// <typeparam name="TResult">The type of result yielded by executing a query.</typeparam>
    public sealed class ProxyQueryService<TQuery, TResult> : IAsyncCqrsQueryService<ProxyQuery<TQuery>, TResult>
        where TQuery : ICqrsQuery
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="service">The proxied service.</param>
        public ProxyQueryService(IAsyncCqrsQueryService<TQuery, TResult> service)
        {
            _service = service;
        }

        private readonly IAsyncCqrsQueryService<TQuery, TResult> _service;

        /// <inheritdoc/>
        public ValueTask<TResult> Execute(ProxyQuery<TQuery> query) => _service.Execute(query.Query);
    }
}
