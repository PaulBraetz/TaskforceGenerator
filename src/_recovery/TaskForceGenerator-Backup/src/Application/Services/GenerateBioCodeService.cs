﻿using TaskforceGenerator.Application.Queries;
using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Authentication.Commands;
using TaskforceGenerator.Domain.Authentication.Queries;

namespace TaskforceGenerator.Application.Services
{
    /// <summary>
    /// Service for generating new bio codes. New bio codes are registered to the citizen connection they are generated for.
    /// This service resides in the application layer because it violates the CQRS principle by simultaneously retrieving a new bio code (query) and assigning it to a connection (command).
    /// In that sense, it also acts as <c>Facade</c>, as it orchestrates domain services.
    /// </summary>
    public sealed class GenerateBioCodeService : IAsyncCqrsQueryService<GenerateBioCode, BioCode>
    {
        private readonly IAsyncCqrsCommandService<CommitBioCodeChange> _commitService;
        private readonly IAsyncCqrsQueryService<ReconstituteConnection, ICitizenConnection> _citizenService;
        private readonly IAsyncCqrsQueryService<GenerateRandomBioCode, BioCode> _generateCodeService;

        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="citizenService">The service used for retrieving the citizen for whom to generate a new code.</param>
        /// <param name="codeService">The service to set the citizens bio code to after generating it.</param>
        /// <param name="generateCodeService">The service to use when generating new bio codes.</param>
        public GenerateBioCodeService(IAsyncCqrsQueryService<ReconstituteConnection, ICitizenConnection> citizenService,
                                      IAsyncCqrsCommandService<CommitBioCodeChange> codeService,
                                      IAsyncCqrsQueryService<GenerateRandomBioCode, BioCode> generateCodeService)
        {
            _citizenService = citizenService;
            _commitService = codeService;
            _generateCodeService = generateCodeService;
        }
        /// <inheritdoc/>
        public async ValueTask<BioCode> Execute(GenerateBioCode query)
        {
            var citizen = await new ReconstituteConnection(query.CitizenName, query.CancellationToken).Using(_citizenService);
            var previousCode = citizen.Code;
            BioCode result;
            do
            {
                result = await new GenerateRandomBioCode().Using(_generateCodeService);
            } while(previousCode == result);

            await new CommitBioCodeChange(query.CitizenName, result, query.CancellationToken).Using(_commitService);

            return result;
        }
    }
}
