﻿using TaskforceGenerator.Application.Commands;
using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Authentication.Commands;
using TaskforceGenerator.Domain.Authentication.Queries;

namespace TaskforceGenerator.Application.Services
{
    /// <summary>
    /// Service for logging in a user.
    /// This service resides in the application layer because it coordinates domain actions.
    /// </summary>
    public sealed class LoginService : IAsyncCqrsCommandService<Login>
    {
        private readonly IAsyncCqrsQueryService<ReconstituteConnection, ICitizenConnection> _repository;
        private readonly IAsyncCqrsCommandService<Authenticate> _authenticateService;
        private readonly IAsyncCqrsCommandService<SetContextConnection> _contextService;

        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="repository">The repository to use when reconstituting the requested citizen.</param>
        /// <param name="authenticateService">The service used to authenticate the citizen with the password provided.</param>
        /// <param name="contextService">The service used to set the authenticated citizen.</param>
        public LoginService(
            IAsyncCqrsQueryService<ReconstituteConnection, ICitizenConnection> repository,
            IAsyncCqrsCommandService<Authenticate> authenticateService,
            IAsyncCqrsCommandService<SetContextConnection> contextService)
        {
            _repository = repository;
            _authenticateService = authenticateService;
            _contextService = contextService;
        }

        /// <inheritdoc/>
        public async ValueTask Execute(Login command)
        {
            var connection = await new ReconstituteConnection(command.CitizenName, command.CancellationToken).Using(_repository);
            await new Authenticate(connection, command.ClearPassword, command.CancellationToken).Using(_authenticateService);
            await new SetContextConnection(connection, command.CancellationToken).Using(_contextService);
        }
    }
}
