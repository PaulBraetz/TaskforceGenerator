﻿using TaskforceGenerator.Application.Commands;
using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Application.Services
{
    /// <summary>
    /// General proxy command service.
    /// Commands should be proxied in order to force dependency injection of application services instead of domain or infrsatructure services.
    /// This leads to proper application of cross-cutting concerns, even though the underlying executing service might have been injected directly instead.
    /// </summary>
    /// <typeparam name="TCommand">The type of command to proxy.</typeparam>
    public sealed class ProxyCommandService<TCommand> : IAsyncCqrsCommandService<ProxyCommand<TCommand>>
        where TCommand : ICqrsCommand
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="service">The proxied service.</param>
        public ProxyCommandService(IAsyncCqrsCommandService<TCommand> service)
        {
            _service = service;
        }

        private readonly IAsyncCqrsCommandService<TCommand> _service;

        /// <inheritdoc/>
        public ValueTask Execute(ProxyCommand<TCommand> query) => _service.Execute(query.Command);
    }
}
