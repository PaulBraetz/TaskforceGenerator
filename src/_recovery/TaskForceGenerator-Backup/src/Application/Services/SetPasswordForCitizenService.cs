﻿using TaskforceGenerator.Application.Commands;
using TaskforceGenerator.Application.Exceptions;
using TaskforceGenerator.Common;
using TaskforceGenerator.Domain.Authentication.Commands;
using TaskforceGenerator.Domain.Authentication.Queries;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication;

namespace TaskforceGenerator.Application.Services
{
    /// <summary>
    /// Service for setting a citizens password.
    /// This service resides in the application layer because it coordinates domain actions.
    /// </summary>
    public sealed class SetPasswordForCitizenService : IAsyncCqrsCommandService<SetPasswordForConnection>
    {
        private readonly IAsyncCqrsQueryService<VerifyBioCode, VerifyBioCode.Result> _verifyService;
        private readonly IAsyncCqrsQueryService<CreatePasswordParameters, PasswordParameters> _parametersService;
        private readonly IAsyncCqrsQueryService<CreatePassword, Password> _passwordService;
        private readonly IAsyncCqrsCommandService<CommitPasswordChange> _commitService;

        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="verifyService">The service used for verifying password setting commands.</param>
        /// <param name="passwordService">The service used to create a new password.</param>
        /// <param name="commitService">The service used to commit the new password to the infrastructure.</param>
        /// <param name="parametersService">The service to use when creating new password parameters.</param>
        public SetPasswordForCitizenService(IAsyncCqrsQueryService<VerifyBioCode, VerifyBioCode.Result> verifyService,
                                            IAsyncCqrsQueryService<CreatePassword, Password> passwordService,
                                            IAsyncCqrsCommandService<CommitPasswordChange> commitService,
                                            IAsyncCqrsQueryService<CreatePasswordParameters, PasswordParameters> parametersService)
        {
            _verifyService = verifyService;
            _passwordService = passwordService;
            _commitService = commitService;
            _parametersService = parametersService;
        }
        /// <inheritdoc/>
        public async ValueTask Execute(SetPasswordForConnection command)
        {
            var (name, password, code, token) = command;
            var verifyResult = await new VerifyBioCode(code, name, token).Using(_verifyService);
            if(verifyResult != VerifyBioCode.Result.Match)
            {
                throw BioCodeMismatchException.Create(code, verifyResult);
            }

            var passwordParameters = await new CreatePasswordParameters().Using(_parametersService);
            var newPassword = await new CreatePassword(
                password,
                passwordParameters,
                IsInitialPassword: true,
                token).Using(_passwordService);

            await new CommitPasswordChange(name, code, newPassword, token).Using(_commitService);
        }
    }
}
