﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TaskforceGenerator.Application.Exceptions;
using TaskforceGenerator.Application.Queries;
using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Application.Services
{
	/// <summary>
	/// Service for requesting a taskforce to be built.
	/// </summary>
	public sealed class BuildTaskforceService : IAsyncCqrsQueryService<BuildTaskforce, ITaskforce>
	{
		private readonly ICrewingSchemeMap _schemeMap;
		/// <summary>
		/// Initializes a new instance.
		/// </summary>
		/// <param name="schemeMap">The map to use when obtaining a crewing scheme based on the name provided in the query.</param>
		public BuildTaskforceService(ICrewingSchemeMap schemeMap)
		{
			_schemeMap = schemeMap;
		}
		/// <inheritdoc/>
		public ValueTask<ITaskforce> Execute(BuildTaskforce query)
		{
			var scheme = _schemeMap.TryGetScheme(query.SchemeName, out var s) ?
				s! :
				throw new InvalidSchemeNameException(query.SchemeName);

			var builder = new TaskforceBuilder()
				.SetCrewingScheme(scheme);

			foreach (var vehicle in query.Vehicles)
				_ = builder.AddVehicle(vehicle);

			foreach (var occupant in query.Occupants)
				_ = builder.AddOccupant(occupant);

			var result = builder.Build();

			return ValueTask.FromResult(result);
		}
	}
}
