﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Core.Queries;

namespace TaskforceGenerator.Application.Queries
{
    /// <summary>
    /// Represents a proxy request for query execution. 
    /// The query is thus signalled to be executed by a proxy wrapping the actual service.
    /// Queries should be proxied in order to force dependency injection of application services instead of domain or infrastructure services.
    /// This leads to proper application of cross-cutting concerns, even though the underlying executing service might have been injected directly instead.
    /// </summary>
    /// <param name="Query">The proxied query. </param>
    /// <typeparam name="TQuery">The type of query to proxy.</typeparam>
    public readonly record struct ProxyQuery<TQuery>(TQuery Query) : ICqrsQuery
            where TQuery : ICqrsQuery
    {
        /// <inheritdoc/>
        public CancellationToken CancellationToken => Query.CancellationToken;
    }
}
