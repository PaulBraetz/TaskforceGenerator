﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Application.Queries
{
    /// <summary>
    /// Query for requesting a taskforce to be built.
    /// </summary>
    /// <param name="CancellationToken">The token used to signal the query execution to be cancelled.</param>
    /// <param name="Occupants">The occupants to crew vehicles with.</param>
    /// <param name="SchemeName">The name of the scheme to utilize.</param>
    /// <param name="Vehicles">The vehicles to crew.</param>
    public readonly record struct BuildTaskforce(
        IEnumerable<IVehicle> Vehicles,
        IEnumerable<IOccupant> Occupants,
        String SchemeName,
        CancellationToken CancellationToken) : ICqrsQuery;
}
