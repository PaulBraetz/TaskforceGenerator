﻿using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Application.Queries
{
    /// <summary>
    /// Query for getting new bio codes.
    /// </summary>
    /// <param name="CitizenName">The citizen whose connection to get a new bio code for.</param>
    /// <param name="CancellationToken">The token used to signal the query execution to be cancelled.</param>
    public readonly record struct GenerateBioCode(String CitizenName, CancellationToken CancellationToken) : ICqrsQuery;
}