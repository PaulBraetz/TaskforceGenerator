﻿using TaskforceGenerator.Application.Commands;
using TaskforceGenerator.Common.Abstractions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Application.Fakes
{
    /// <summary>
    /// Fake service for logging in.
    /// </summary>
    public sealed class LoginServiceFake : IAsyncCqrsCommandService<Login>
    {
        /// <inheritdoc/>
        public ValueTask Execute(Login command) => ValueTask.CompletedTask;
    }
}
