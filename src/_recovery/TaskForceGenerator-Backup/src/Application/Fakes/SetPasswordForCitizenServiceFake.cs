﻿using TaskforceGenerator.Application.Commands;
using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Application.Fakes
{
    /// <summary>
    /// Fake service for setting a citizens connection password.
    /// </summary>
    public sealed class SetPasswordForCitizenServiceFake : IAsyncCqrsCommandService<SetPasswordForConnection>
    {
        /// <inheritdoc/>
        public ValueTask Execute(SetPasswordForConnection command) => ValueTask.CompletedTask;
    }
}
