﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskforceGenerator.Application.Commands;
using TaskforceGenerator.Application.Queries;
using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Application
{
    /// <summary>
    /// Extensions for the <c>TaskforceGenerator.Application</c> namespace.
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Wraps a query in a proxy request.
        /// </summary>
        /// <typeparam name="TQuery">The type of query to proxy.</typeparam>
        /// <param name="query">The query to proxy.</param>
        /// <returns>A new instance of <see cref="ProxyQuery{TQuery}"/>, wrapping the query.</returns>
        public static ProxyQuery<TQuery> AsProxyQuery<TQuery>(this TQuery query)
            where TQuery : ICqrsQuery => new(query);
        /// <summary>
        /// Wraps a command in a proxy request.
        /// </summary>
        /// <typeparam name="TCommand">The type of command to proxy.</typeparam>
        /// <param name="command">The command to proxy.</param>
        /// <returns>A new instance of <see cref="ProxyCommand{TCommand}"/>, wrapping the command.</returns>
        public static ProxyCommand<TCommand> AsProxyCommand<TCommand>(this TCommand command)
            where TCommand : ICqrsCommand => new(command);
    }
}
