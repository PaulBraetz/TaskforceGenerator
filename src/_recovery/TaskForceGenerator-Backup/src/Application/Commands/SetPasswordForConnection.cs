﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication;

namespace TaskforceGenerator.Application.Commands
{
    /// <summary>
    /// Command for setting a citizens connection password.
    /// </summary>
    /// <param name="CitizenName">The name of the citizen whose connection to modify.</param>
    /// <param name="Password">The password to associate to the connection.</param>
    /// <param name="Code">The bio code to verify the password change with.</param>
    /// <param name="CancellationToken">The token used to signal the command execution to be cancelled.</param>
    public readonly record struct SetPasswordForConnection(String CitizenName, String Password, BioCode Code, CancellationToken CancellationToken) : ICqrsCommand;
}