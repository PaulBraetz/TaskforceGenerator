﻿using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Application.Commands
{
    /// <summary>
    /// Command for authenticating the user for using a citizen connection.
    /// </summary>
    /// <param name="CitizenName">The name of the citizen for which to authenticate the user.</param>
    /// <param name="ClearPassword">The password provided by the user.</param>
    /// <param name="CancellationToken">The token used to signal the command execution to be cancelled.</param>
    public readonly record struct Login(String CitizenName, String ClearPassword, CancellationToken CancellationToken) : ICqrsCommand;
}
