﻿using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Application.Commands
{
    /// <summary>
    /// Represents a proxy request for command execution. 
    /// The command is thus signalled to be executed by a proxy wrapping the actual service.
    /// Commands should be proxied in order to force dependency injection of application services instead of domain or infrsatructure services.
    /// This leads to proper application of cross-cutting concerns, even though the underlying executing service might have been injected directly instead.
    /// </summary>
    /// <typeparam name="TCommand">The type of command to proxy.</typeparam>
    public readonly struct ProxyCommand<TCommand> : ICqrsCommand
        where TCommand : ICqrsCommand
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="command">The proxied command.</param>
        public ProxyCommand(TCommand command)
        {
            Command = command;
        }

        /// <summary>
        /// Gets the proxied command.
        /// </summary>
        public readonly TCommand Command { get; }
        /// <inheritdoc/>
        public CancellationToken CancellationToken => Command.CancellationToken;
    }
}
