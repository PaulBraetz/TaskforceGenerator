﻿namespace TaskforceGenerator.Domain.Core.Exceptions
{
    /// <summary>
    /// Exception thrown when a connection that is not known to the system is attempted to be accessed.
    /// </summary>
    public sealed class ConnectionNotExistingException : Exception
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="name">The name of the citizen whose connection was attempted to be accessed.</param>
        public ConnectionNotExistingException(String name)
        {
            Name = name;
        }
        /// <summary>
        /// Gets the name of the citizen whose connection was attempted to be accessed.
        /// </summary>
        public String Name { get; } = String.Empty;
    }
}
