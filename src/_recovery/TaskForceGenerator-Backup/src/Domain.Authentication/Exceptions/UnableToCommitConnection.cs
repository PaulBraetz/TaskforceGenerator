﻿namespace TaskforceGenerator.Domain.Core.Exceptions
{
    /// <summary>
    /// Exception thrown when the system is unable to commit a connection to the infrastructure.
    /// </summary>
    public sealed class ConnectionCommitException : Exception
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="name">The name of the connection that was attempted to be commited.</param>
        /// <param name="innerException">The exception prohibiting committing the connection.</param>
        public ConnectionCommitException(String name, Exception? innerException) : base($"Unable to commit connection '{name}'", innerException)
        {
            Name = name;
        }
        /// <summary>
        /// Gets the name of the connection that was attempted to be commited.
        /// </summary>
        public String Name { get; } = String.Empty;
    }
}
