﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Domain.Authentication.Exceptions
{
    /// <summary>
    /// Thrown if a citizen is requested from the user context but none have been authenticated.
    /// </summary>
    public sealed class NotAuthenticatedException:Exception
    {

    }
}
