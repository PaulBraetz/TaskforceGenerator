﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Domain.Authentication.Exceptions
{
    /// <summary>
    /// Thrown when a password passed for authentication does not match the password set for a citizen.
    /// </summary>
    public sealed class PasswordMismatchException : Exception
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="citizenName">The name of the citizen for whose connection an authentication was attempted.</param>
        public PasswordMismatchException(String citizenName) : base($"The password provided for '{citizenName}' is incorrect.")
        {
            CitizenName = citizenName;
        }
        /// <summary>
        /// Gets the name of the citizen for whose connection an authentication was attempted.
        /// </summary>
        public String CitizenName { get; }
    }
}
