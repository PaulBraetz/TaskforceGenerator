﻿namespace TaskforceGenerator.Domain.Authentication.Exceptions
{
    /// <summary>
    /// Exception thrown when a new password is attempted to be created with an invalid clear password.
    /// </summary>
    public sealed class PasswordCreationGuidelineException : Exception
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="validity">The validity of the clear password.</param>
        public PasswordCreationGuidelineException(PasswordValidity validity)
        {
            Validity = validity;
        }
        /// <summary>
        /// Gets the validity of the clear password.
        /// </summary>
        public PasswordValidity Validity { get; }
    }
}
