﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Authentication.Exceptions;
using TaskforceGenerator.Domain.Authentication.Queries;
using TaskforceGenerator.Domain.Authentication;

namespace TaskforceGenerator.Domain.Authentication.Services
{
    /// <summary>
    /// Decorator for validating <see cref="CreatePassword"/> executions.
    /// </summary>
    public sealed class CreatePasswordServiceValidation : IAsyncCqrsQueryService<CreatePassword, Password>
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="passwordGuideline">The password guideline to apply to clear passwords provided.</param>
        /// <param name="decorated">The decorated service.</param>
        public CreatePasswordServiceValidation(IPasswordGuideline passwordGuideline, IAsyncCqrsQueryService<CreatePassword, Password> decorated)
        {
            _passwordGuideline = passwordGuideline;
            _decorated = decorated;
        }

        private readonly IPasswordGuideline _passwordGuideline;
        private readonly IAsyncCqrsQueryService<CreatePassword, Password> _decorated;

        /// <inheritdoc/>
        public ValueTask<Password> Execute(CreatePassword command)
        {
            var validity = command.IsInitialPassword ?
                PasswordValidity.Empty :
                _passwordGuideline.Assess(command.ClearPassword);
            if (validity.RulesViolated.Length > 0)
            {
                throw new PasswordCreationGuidelineException(validity);
            }

            if (command.Parameters.Numerics.Iterations < 1)
            {
                throw new Exception($"Must have at least one iteration when hashing passwords.");
            }

            var result = _decorated.Execute(command);

            return result;
        }
    }
}
