﻿using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Commands;
using TaskforceGenerator.Domain.Authentication.Exceptions;
using TaskforceGenerator.Domain.Authentication.Queries;
using TaskforceGenerator.Domain.Core.Commands;

namespace TaskforceGenerator.Domain.Authentication.Services
{
    /// <summary>
    /// Service for ensuring a citizen is available to the system.
    /// </summary>
    public sealed class EnsureCitizenConnectionService : IAsyncCqrsCommandService<EnsureCitizenConnection>
    {
        private readonly IAsyncCqrsQueryService<CheckConnectionExists, CheckConnectionExists.Result> _existsService;
        private readonly IAsyncCqrsCommandService<OpenConnection> _openConnectionService;
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="openConnectionService">The service to use when opening a connection.</param>
        /// <param name="existsService">The service to use when first checking whether a connection already exists.</param>
        public EnsureCitizenConnectionService(IAsyncCqrsCommandService<OpenConnection> openConnectionService, IAsyncCqrsQueryService<CheckConnectionExists, CheckConnectionExists.Result> existsService)
        {
            _openConnectionService = openConnectionService;
            _existsService = existsService;
        }

        /// <inheritdoc/>
        public async ValueTask Execute(EnsureCitizenConnection command)
        {
            var (name, token) = command;
            if(token.IsCancellationRequested)
            {
                return;
            }

            var exists = (await new CheckConnectionExists(name, token).Using(_existsService)).Exists;
            if(exists)
            {
                return;
            }

            await new OpenConnection(name, token).Using(_openConnectionService);
        }
    }
}
