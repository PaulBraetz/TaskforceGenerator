﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Authentication.Commands;
using TaskforceGenerator.Domain.Core.Abstractions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Domain.Authentication.Services
{
    /// <summary>
    /// Service for setting the connection on user contexts.
    /// </summary>
    public sealed class SetContextConnectionService : IAsyncCqrsCommandService<SetContextConnection>
    {
        private readonly IUserContext _context;

        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="context">The context for which to set the citizen.</param>
        public SetContextConnectionService(IUserContext context)
        {
            _context = context;
        }
        /// <inheritdoc/>
        public ValueTask Execute(SetContextConnection command)
        {
            if(!command.CancellationToken.IsCancellationRequested)
            {
                _context.Citizen = command.Connection;
            }

            return ValueTask.CompletedTask;
        }
    }
}
