﻿using TaskforceGenerator.Common;
using TaskforceGenerator.Domain.Authentication;
using TaskforceGenerator.Domain.Authentication.Queries;
using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Domain.Authentication.Services
{
    /// <summary>
    /// Service for generating initial citizen passwords.
    /// </summary>
    public sealed class GenerateInitialPasswordService : IAsyncCqrsQueryService<GenerateInitialPassword, Password>
    {
        private readonly IAsyncCqrsQueryService<CreatePassword, Password> _passwordService;
        private readonly IAsyncCqrsQueryService<CreatePasswordParameters, PasswordParameters> _parametersService;
        private readonly Int32 _generatedPasswordLength;

        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="passwordService">The service to use when creating a new password.</param>
        /// <param name="parametersService">The service to use when creating new password parameters.</param>
        /// <param name="generatedPasswordLength">The length of generated (random) passwords.</param>
        public GenerateInitialPasswordService(IAsyncCqrsQueryService<CreatePassword, Password> passwordService,
                                              IAsyncCqrsQueryService<CreatePasswordParameters, PasswordParameters> parametersService,
                                              Int32 generatedPasswordLength)
        {
            _passwordService = passwordService;
            _parametersService = parametersService;
            _generatedPasswordLength = generatedPasswordLength;
        }

        /// <inheritdoc/>
        public async ValueTask<Password> Execute(GenerateInitialPassword query)
        {
            var parameters = await new CreatePasswordParameters().Using(_parametersService);
            var clearPassword = GenerateRandomPassword();
            var password = await new CreatePassword(
                clearPassword,
                parameters,
                IsInitialPassword: true,
                query.CancellationToken).Using(_passwordService);

            return password;
        }

        private String GenerateRandomPassword()
        {
            var chars = Enumerable
                .Repeat(0, _generatedPasswordLength)
                .Select(i => Random.Shared.Next('A', 'Z'))
                .Select(i => (Char)i);
            var result = String.Concat(chars);

            return result;
        }
    }
}
