﻿using TaskforceGenerator.Common;
using TaskforceGenerator.Domain.Authentication.Commands;
using TaskforceGenerator.Domain.Authentication.Queries;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Abstractions;

namespace TaskforceGenerator.Domain.Authentication.Services
{
    /// <summary>
    /// Service for creating and committing a citizen connection entity.
    /// </summary>
    public sealed class OpenConnectionService : IAsyncCqrsCommandService<OpenConnection>
    {
        private readonly IAsyncCqrsQueryService<GenerateInitialPassword, Password> _passwordService;
        private readonly IAsyncCqrsQueryService<GenerateRandomBioCode, BioCode> _randomBioCodeService;
        private readonly IAsyncCqrsQueryService<CreateConnection, ICitizenConnection> _connectionService;
        private readonly IAsyncCqrsCommandService<CommitConnection> _commitService;

        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="passwordService">The service to use when generating the initial password.</param>
        /// <param name="randomBioCodeService">The service to use when generating the initial bio code.</param>
        /// <param name="citizenService">The service to use when creating a new citizen.</param>
        /// <param name="commitService">The service to use when committing a new citizen to the infrastructure.</param>
        public OpenConnectionService(IAsyncCqrsQueryService<GenerateInitialPassword, Password> passwordService,
                                      IAsyncCqrsQueryService<GenerateRandomBioCode, BioCode> randomBioCodeService,
                                      IAsyncCqrsQueryService<CreateConnection, ICitizenConnection> citizenService,
                                      IAsyncCqrsCommandService<CommitConnection> commitService)
        {
            _passwordService = passwordService;
            _randomBioCodeService = randomBioCodeService;
            _connectionService = citizenService;
            _commitService = commitService;
        }

        /// <inheritdoc/>
        public async ValueTask Execute(OpenConnection command)
        {
            var password = await new GenerateInitialPassword().Using(_passwordService);
            var code = await new GenerateRandomBioCode().Using(_randomBioCodeService);
            var connection = await new CreateConnection(command.CitizenName, code, password, command.CancellationToken).Using(_connectionService);
            await new CommitConnection(connection, command.CancellationToken).Using(_commitService);
        }
    }
}
