﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Authentication.Queries;

namespace TaskforceGenerator.Domain.Authentication.Services
{
    /// <summary>
    /// Factory for creating instances of <see cref="IBio"/>.
    /// </summary>
    public sealed class BioFactory : IAsyncCqrsQueryService<CreateBio, IBio>
    {
        /// <inheritdoc/>
        public ValueTask<IBio> Execute(CreateBio query)
        {
            IBio result = new Bio(query.BioText);

            return ValueTask.FromResult(result);
        }
    }
}
