﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Authentication.Exceptions;
using TaskforceGenerator.Domain.Authentication.Queries;

namespace TaskforceGenerator.Domain.Authentication.Services
{
    /// <summary>
    /// Service for retrieving the citizen connection associated with the users context.
    /// </summary>
    public sealed class GetContextConnectionService : IAsyncCqrsQueryService<GetContextConnection, ICitizenConnection>
    {
        private readonly IUserContext _context;

        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="context">The context for which to set the citizen.</param>
        public GetContextConnectionService(IUserContext context)
        {
            _context = context;
        }
        /// <inheritdoc/>
        public ValueTask<ICitizenConnection> Execute(GetContextConnection query)
        {
            query.CancellationToken.ThrowIfCancellationRequested();
            var result = _context.Citizen;

            return result == null ?
                throw new NotAuthenticatedException() :
                ValueTask.FromResult(result);
        }
    }
}
