﻿using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Commands;
using TaskforceGenerator.Domain.Authentication.Exceptions;
using TaskforceGenerator.Domain.Authentication.Queries;
using TaskforceGenerator.Domain.Authentication;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Domain.Authentication.Services
{
    /// <summary>
    /// Service for authenticating passwords against passwords.
    /// </summary>
    public sealed class AuthenticateService : IAsyncCqrsCommandService<Authenticate>
    {
        private readonly IAsyncCqrsQueryService<CreatePassword, Password> _passwordService;
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="passwordService">The service to use when creating a password based on the clear password passed for authentication.</param>
        public AuthenticateService(IAsyncCqrsQueryService<CreatePassword, Password> passwordService)
        {
            _passwordService = passwordService;
        }

        /// <inheritdoc/>
        public async ValueTask Execute(Authenticate command)
        {
            var (citizen, clearPassword, token) = command;
            var passwordPassed = await new CreatePassword(clearPassword, citizen.Password.Parameters, false, token).Using(_passwordService);
            var match = passwordPassed.Hash.SequenceEqual(citizen.Password.Hash);
            if(!match)
            {
                throw new PasswordMismatchException(citizen.CitizenName);
            }
        }
    }
}
