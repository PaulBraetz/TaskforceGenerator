﻿using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Core.Abstractions;
using TaskforceGenerator.Domain.Authentication;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Domain.Authentication
{
    /// <summary>
    /// Default implementation of <see cref="IBio"/>.
    /// </summary>
    public sealed class Bio : IBio
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="bioText">The text posted in the citizen's bio.</param>
        public Bio(String bioText)
        {
            _bioText = bioText;
        }

        private readonly String _bioText;
        /// <inheritdoc/>
        public Boolean Contains(BioCode code) => _bioText.Contains(code);
    }
}
