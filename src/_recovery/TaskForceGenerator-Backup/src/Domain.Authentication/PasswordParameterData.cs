﻿namespace TaskforceGenerator.Domain.Authentication
{
    /// <summary>
    /// Data blocks to be applied to the hash calculation.
    /// </summary>
    /// <param name="AssociatedData">Any extra associated data to use while hashing the password.</param>
    /// <param name="KnownSecret">An optional secret to use while hashing the password.</param>
    /// <param name="Salt">The password hashing salt.</param>
    public readonly record struct PasswordParameterData(Byte[] AssociatedData, Byte[] KnownSecret, Byte[] Salt);
}
