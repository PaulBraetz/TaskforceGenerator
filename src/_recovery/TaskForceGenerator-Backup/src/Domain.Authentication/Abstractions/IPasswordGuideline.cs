﻿namespace TaskforceGenerator.Domain.Authentication.Abstractions
{
    /// <summary>
    /// represents a guideline for assessing password validity; a rule composite.
    /// </summary>
    public interface IPasswordGuideline
    {
        /// <summary>
        /// Assesses the validity of a password.
        /// </summary>
        /// <param name="clearPassword">The password to assess.</param>
        /// <returns>The validity of the password, that is, which rules represented by the guideline are being violated by the password.</returns>
        PasswordValidity Assess(String clearPassword);
    }
}