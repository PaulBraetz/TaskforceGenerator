﻿namespace TaskforceGenerator.Domain.Authentication.Abstractions
{
    /// <summary>
    /// Defines a rule that a password may either match or not.
    /// </summary>
    public interface IPasswordRule
    {
        /// <summary>
        /// Gets the name of the rule.
        /// </summary>
        String Name { get; }
        /// <summary>
        /// Gets the description of the rule.
        /// </summary>
        String Description { get; }
        /// <summary>
        /// Evaluates whether a clear pasword matches the rule.
        /// </summary>
        /// <param name="clearPassword">The password to assess.</param>
        /// <returns><see langword="true"/> if the password matches this rule; otherwise <see langword="false"/>.</returns>
        Boolean Matches(String clearPassword);
    }
}
