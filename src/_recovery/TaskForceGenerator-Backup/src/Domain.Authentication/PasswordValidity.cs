﻿using TaskforceGenerator.Domain.Authentication.Abstractions;

namespace TaskforceGenerator.Domain.Authentication
{
    /// <summary>
    /// The result of a password guideline assessment.
    /// </summary>
    public readonly struct PasswordValidity : IEquatable<PasswordValidity>
    {
        /// <summary>
        /// Gets the rules violated by the password assessed.
        /// </summary>
        public IPasswordRule[] RulesViolated => _rulesViolated ?? Array.Empty<IPasswordRule>();
        private readonly IPasswordRule[]? _rulesViolated;
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="rulesViolated">The violated rules to be represented by this instance.</param>
        public PasswordValidity(IPasswordRule[] rulesViolated)
        {
            _rulesViolated = rulesViolated;
        }
        /// <summary>
        /// An empty instance of <see cref="PasswordValidity"/>.
        /// </summary>
        public static readonly PasswordValidity Empty = new(Array.Empty<IPasswordRule>());

        /// <summary>
        /// Indicates whether the validity is equal to the default value of <see cref="PasswordValidity"/> or <see cref="Empty"/>.
        /// </summary>
        /// <returns><see langword="true"/> if the validity is equal to the default value of <see cref="PasswordValidity"/> or <see cref="Empty"/>; otherwise, <see langword="false"/>.</returns>
        public Boolean IsDefaultOrEmpty()
        {
            var result = _rulesViolated == null || RulesViolated.Length == 0;

            return result;
        }

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
        public override Boolean Equals(Object? obj) => obj is PasswordValidity validity && Equals(validity);
        public Boolean Equals(PasswordValidity other) => RulesViolated.SequenceEqual(other.RulesViolated);
        public override Int32 GetHashCode() =>
            RulesViolated.Aggregate(new HashCode(), (h, r) =>
            {
                h.Add(r);
                return h;
            }).ToHashCode();
        public static Boolean operator ==(PasswordValidity left, PasswordValidity right) => left.Equals(right);
        public static Boolean operator !=(PasswordValidity left, PasswordValidity right) => !(left == right);
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
    }
}
