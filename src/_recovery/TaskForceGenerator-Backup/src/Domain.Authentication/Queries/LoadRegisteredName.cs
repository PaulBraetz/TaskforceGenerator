﻿using TaskforceGenerator.Common.Abstractions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Domain.Authentication.Queries
{
    /// <summary>
    /// Query for loading a citizens actual name. Citizens may be specified using case-insensitivity, however they should always be represented using the RSI-registered typing.
    /// </summary>
    /// <param name="CitizenName">The name of the citizen whose actual name to load.</param>
    /// <param name="CancellationToken">The token used to signal the query execution to be cancelled.</param>
    public readonly record struct LoadActualName(String CitizenName, CancellationToken CancellationToken) : ICqrsQuery
    {
        /// <summary>
        /// The result of the query.
        /// </summary>
        /// <param name="ActualName">The actual name registered to RSI.</param>
        public readonly record struct Result(String ActualName);
    }
}
