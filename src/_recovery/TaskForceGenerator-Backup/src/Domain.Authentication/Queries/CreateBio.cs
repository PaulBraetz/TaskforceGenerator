﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Abstractions;

namespace TaskforceGenerator.Domain.Authentication.Queries
{
    /// <summary>
    /// Query for creating instances of <see cref="IBio"/>.
    /// </summary>
    /// <param name="BioText">The text representing the ctizens bio.</param>
    /// <param name="CancellationToken">The token used to signal the query execution to be cancelled.</param>
    public readonly record struct CreateBio(String BioText, CancellationToken CancellationToken) : ICqrsQuery;
}
