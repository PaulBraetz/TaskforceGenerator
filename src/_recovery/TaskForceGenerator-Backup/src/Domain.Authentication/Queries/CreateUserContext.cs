﻿using TaskforceGenerator.Common.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Domain.Authentication.Queries
{
    /// <summary>
    /// Query for creating a user context.
    /// </summary>
    /// <param name="CitizenName">The name of the citizen for which the user is authenticating.</param>
    /// <param name="Password">The password using which the user is attempting to authenticate.</param>
    /// <param name="CancellationToken">The token used to signal the query execution to be cancelled.</param>
    public readonly record struct CreateUserContext(String CitizenName, String Password, CancellationToken CancellationToken) : ICqrsQuery;
}
