﻿using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Domain.Authentication.Queries
{
    /// <summary>
    /// Query for creating new password parameters.
    /// </summary>
    public readonly record struct CreatePasswordParameters(CancellationToken CancellationToken) : ICqrsQuery;
}
