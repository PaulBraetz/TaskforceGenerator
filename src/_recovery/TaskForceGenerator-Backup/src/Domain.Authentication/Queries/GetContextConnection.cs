﻿using TaskforceGenerator.Common.Abstractions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Domain.Authentication.Queries
{
    /// <summary>
    /// Query for retrieving the citizen connection associated with the users context.
    /// </summary>
    /// <param name="CancellationToken">The token used to signal the query execution to be cancelled.</param>
    public readonly record struct GetContextConnection(CancellationToken CancellationToken) : ICqrsQuery;
}
