﻿using TaskforceGenerator.Common.Abstractions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Domain.Authentication.Queries
{
    /// <summary>
    /// Query for checking whether a citizens connection already exists.
    /// </summary>
    /// <param name="CitizenName">The name of the citizen whose connection to check for.</param>
    /// <param name="CancellationToken">The token used to signal the query execution to be cancelled.</param>
    public readonly record struct CheckConnectionExists(String CitizenName, CancellationToken CancellationToken):ICqrsQuery
    {
        /// <summary>
        /// The result of the query.
        /// </summary>
        /// <param name="Exists">Indicates whether the a connection for the citizen requested exists.</param>
        public readonly record struct Result(Boolean Exists);
    }
}
