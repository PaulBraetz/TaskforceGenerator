﻿using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Domain.Authentication.Queries
{
    /// <summary>
    /// Query for creating a random bio code.
    /// </summary>
    public readonly record struct GenerateRandomBioCode(CancellationToken CancellationToken) : ICqrsQuery
    {
        /// <summary>
        /// The result of the query.
        /// </summary>
        /// <param name="Code">The random code generated.</param>
        public readonly record struct Result(BioCode Code);
    }
}
