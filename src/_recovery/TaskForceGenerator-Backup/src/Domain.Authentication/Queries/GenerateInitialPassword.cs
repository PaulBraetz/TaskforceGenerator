﻿using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Domain.Authentication.Queries
{
    /// <summary>
    /// Query for generating initial citizen passwords.
    /// </summary>
    public readonly record struct GenerateInitialPassword(CancellationToken CancellationToken) : ICqrsQuery;
}
