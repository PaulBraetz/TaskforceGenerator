﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Core.Abstractions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Domain.Authentication.Queries
{
    /// <summary>
    /// Query for loading a citizen's bio.
    /// </summary>
    /// <param name="Name">The name of the citizen whose bio to load.</param>
    /// <param name="CancellationToken">The token used to signal the query execution to be cancelled.</param>
    public readonly record struct LoadBio(String Name, CancellationToken CancellationToken) : ICqrsQuery;
}
