﻿using TaskforceGenerator.Common.Abstractions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Domain.Authentication.Commands
{
    /// <summary>
    /// Command for creating and committing a citizen connection entity.
    /// </summary>
    /// <param name="CitizenName">The name of the citizen whose connection to create and commit.</param>
    /// <param name="CancellationToken">The token used to signal the command execution to be cancelled.</param>
    public readonly record struct OpenConnection(String CitizenName, CancellationToken CancellationToken) : ICqrsCommand;
}
