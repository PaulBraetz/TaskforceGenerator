﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Core.Abstractions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Domain.Authentication.Commands
{
    /// <summary>
    /// Command for committing connections to the infrastructure.
    /// </summary>
    /// <param name="Connection">The connection to commit.</param>
    /// <param name="CancellationToken">The token used to signal the command execution to be cancelled.</param>
    public readonly record struct CommitConnection(ICitizenConnection Connection, CancellationToken CancellationToken) : ICqrsCommand;
}
