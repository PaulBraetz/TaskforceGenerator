﻿namespace TaskforceGenerator.Domain.Authentication
{
    /// <summary>
    /// The parameters used to create a password hash.
    /// </summary>
    /// <param name="Numerics">Gets numerics associated with the hash calculation.</param>
    /// <param name="Data">Gets data associated with the hash calculation.</param>
    public readonly record struct PasswordParameters(PasswordParameterNumerics Numerics, PasswordParameterData Data);
}
