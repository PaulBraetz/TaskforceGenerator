﻿using TaskforceGenerator.Domain.Authentication.Abstractions;

using System.Text.RegularExpressions;

namespace TaskforceGenerator.Domain.Authentication
{
    /// <summary>
    /// A pattern-based implementation of <see cref="IPasswordRule"/>.
    /// </summary>
    public readonly struct RegexPasswordRule : IPasswordRule, IEquatable<RegexPasswordRule>
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <param name="pattern"></param>
        public RegexPasswordRule(String name, String description, String pattern)
        {
            Name = name;
            Description = description;
            _pattern = pattern;
        }
        /// <inheritdoc/>
        public String Name { get; }
        /// <inheritdoc/>
        public String Description { get; }

        private readonly String _pattern;

        /// <inheritdoc/>
        public Boolean Matches(String clearPassword)
        {
            var result = Regex.IsMatch(clearPassword, _pattern);

            return result;
        }

        /// <inheritdoc/>
        public override Boolean Equals(Object? obj) => obj is RegexPasswordRule rule && Equals(rule);

        /// <inheritdoc/>
        public Boolean Equals(RegexPasswordRule other) => _pattern == other._pattern;

        /// <inheritdoc/>
        public override Int32 GetHashCode() => HashCode.Combine(_pattern);

        /// <inheritdoc/>
        public static Boolean operator ==(RegexPasswordRule left, RegexPasswordRule right) => left.Equals(right);

        /// <inheritdoc/>
        public static Boolean operator !=(RegexPasswordRule left, RegexPasswordRule right) => !(left == right);
    }
}
