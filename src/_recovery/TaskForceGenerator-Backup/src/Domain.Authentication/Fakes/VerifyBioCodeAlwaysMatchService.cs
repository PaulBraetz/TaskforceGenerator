﻿using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Authentication.Queries;

namespace TaskforceGenerator.Domain.Authentication.Fakes
{
    /// <summary>
    /// Fake service for verifying a bio code.
    /// </summary>
    public sealed class VerifyBioCodeAlwaysMatchService : IAsyncCqrsQueryService<VerifyBioCode, VerifyBioCode.Result>
    {
        private readonly IAsyncCqrsQueryService<LoadBio, IBio> _bioService;
        private readonly IAsyncCqrsQueryService<ReconstituteConnection, ICitizenConnection> _connectionService;

        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="bioService">The service to use for retrieving a citizens bio.</param>
        /// <param name="citizenService">The service to use for retrieving a citizen with a given name.</param>
        public VerifyBioCodeAlwaysMatchService(IAsyncCqrsQueryService<LoadBio, IBio> bioService,
                                    IAsyncCqrsQueryService<ReconstituteConnection, ICitizenConnection> citizenService)
        {
            _bioService = bioService;
            _connectionService = citizenService;
        }

        /// <inheritdoc/>
        public async ValueTask<VerifyBioCode.Result> Execute(VerifyBioCode query)
        {
            _ = await new ReconstituteConnection(query.Name, query.CancellationToken).Using(_connectionService);
            _ = await new LoadBio(query.Name, query.CancellationToken).Using(_bioService);

            return VerifyBioCode.Result.Match;
        }
    }
}
