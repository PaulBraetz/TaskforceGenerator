﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Authentication.Queries;

namespace TaskforceGenerator.Domain.Authentication.Fakes
{
    /// <summary>
    /// Fake implementation for loading a citizen's bio.
    /// </summary>
    public sealed class LoadBioServiceFake : IAsyncCqrsQueryService<LoadBio, IBio>
    {
        /// <inheritdoc/>
        public ValueTask<IBio> Execute(LoadBio query)
        {
            var result = (IBio)new Bio($"FakeBioCode{Random.Shared.Next(100)}");

            return ValueTask.FromResult(result);
        }
    }
}
