﻿using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Core.Abstractions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Domain.Authentication
{
    /// <summary>
    /// Default implementation of <see cref="IUserContext"/>.
    /// </summary>
    public sealed class UserContext : IUserContext, IObservableUserContext
    {
        private ICitizenConnection? _citizen;
        
        /// <inheritdoc/>
        public ICitizenConnection? Citizen {
            get => _citizen;
            set {
                _citizen = value;
                CitizenChanged?.Invoke(this, value);
            }
        }
        /// <inheritdoc/>
        public event EventHandler<ICitizenConnection?>? CitizenChanged;
    }
}
