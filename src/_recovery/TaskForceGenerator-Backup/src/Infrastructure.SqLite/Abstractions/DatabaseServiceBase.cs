﻿using Infrastructure.SqLite;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Infrastructure.SqLite.Abstractions
{
    /// <summary>
    /// Base class for types implementing a 
    /// </summary>
    /// <typeparam name="TSchema">The type of schema required by the service.</typeparam>
    public abstract class DatabaseServiceBase<TSchema>
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="schema">The schema information required by the service.</param>
        /// <param name="database">The database providing infrastructure access.</param>
        protected DatabaseServiceBase(TSchema schema, Database database)
        {
            Schema = schema;
            Database = database;
        }
        /// <summary>
        /// Gets the database.
        /// </summary>
        protected Database Database { get; }
        /// <summary>
        /// Gets the required schema.
        /// </summary>
        protected TSchema Schema { get; }
    }
}
