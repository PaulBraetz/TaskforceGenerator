﻿using Microsoft.Data.Sqlite;

namespace TaskforceGenerator.Infrastructure.SqLite.Abstractions
{
    /// <summary>
    /// Represents an executable SQLite script.
    /// </summary>
    public interface IScript
    {
        /// <summary>
        /// Gets the path to the script to execute.
        /// </summary>
        String ScriptName { get; }
        /// <summary>
        /// Executes the script on the connection provided.
        /// </summary>
        /// <param name="connection">The connection using which to execute the script.</param>
        void Execute(SqliteConnection connection);
    }
}