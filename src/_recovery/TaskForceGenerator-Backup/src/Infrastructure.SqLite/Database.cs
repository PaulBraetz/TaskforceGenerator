﻿using TaskforceGenerator.Infrastructure.SqLite.Abstractions;
using TaskforceGenerator.Infrastructure.SqLite.Schema;

using Microsoft.Data.Sqlite;

namespace Infrastructure.SqLite
{
    /// <summary>
    /// The database representation to use when accessing the actual SQLite server.
    /// </summary>
    public sealed class Database : IDisposable
    {
        private Database(SqliteConnection connection)
        {
            _connection = connection;
        }

        private readonly SqliteConnection _connection;
        /// <summary>
        /// Creates a new database command.
        /// </summary>
        /// <returns>A new database command.</returns>
        public SqliteCommand CreateCommand(String commandText)
        {
            var result = _connection.CreateCommand();
            result.CommandText = commandText;

            return result;
        }

        /// <summary>
        /// Factory method for creating instances of <see cref="Database"/>.
        /// </summary>
        /// <param name="connectionString">The connection String using which to access the database.</param>
        /// <param name="executionProgress">Optonal progress to repost on the last script executed.</param>
        /// <param name="scripts">The scripts to execute after initializing the connection.</param>
        /// <returns>A new instance of <see cref="Database"/>.</returns>
        public static Database Create(String connectionString, IProgress<IScript>? executionProgress = null, IScript[]? scripts = null)
        {
            var connection = new SqliteConnection(connectionString);
            connection.Open();

            ExecuteScripts(connection, executionProgress, scripts);

            var result = new Database(connection);

            return result;
        }

        private static void ExecuteScripts(SqliteConnection connection, IProgress<IScript>? executionProgress, IScript[]? scripts)
        {
            foreach(var script in scripts ?? Array.Empty<FileScript>())
            {
                script.Execute(connection);
                executionProgress?.Report(script);
            }
        }

        /// <inheritdoc/>
        public void Dispose() => _connection.Dispose();
    }
}
