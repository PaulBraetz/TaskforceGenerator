﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Queries;
using TaskforceGenerator.Domain.Core.Queries;
using TaskforceGenerator.Infrastructure.SqLite.Abstractions;
using TaskforceGenerator.Infrastructure.SqLite.Schema;

using Infrastructure.SqLite;

namespace TaskforceGenerator.Infrastructure.SqLite.Services.Core
{
    /// <summary>
    /// Service for checking whether a cititzen is registered to the system.
    /// </summary>
    public sealed class CheckCitizenRegisteredService : DatabaseServiceBase<Citizens>, IAsyncCqrsQueryService<CheckCitizenRegistered, CheckCitizenRegistered.Result>
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="schema">The schema information required by the service.</param>
        /// <param name="database">The database providing infrastructure access.</param>
        public CheckCitizenRegisteredService(Citizens schema, Database database) : base(schema, database)
        {
        }

        /// <inheritdoc/>
        public async ValueTask<CheckCitizenRegistered.Result> Execute(CheckCitizenRegistered query)
        {
            var commandText = CreateCommand();
            using var dbCommand = Database.CreateCommand(commandText);

#pragma warning disable IDE0058 // Expression value is never used
            dbCommand.Parameters.AddWithValue(Schema.CitizenName.ParamName, query.CitizenName);
#pragma warning restore IDE0058 // Expression value is never used

            await dbCommand.PrepareAsync(query.CancellationToken);
            var count = await dbCommand.ExecuteScalarAsync(query.CancellationToken);
            var registered = (Int64)count! > 0;
            var result = new CheckCitizenRegistered.Result(registered);

            return result;
        }

        private String CreateCommand()
        {
            var result = $"select count(*) from {Schema.TableName} where {Schema.CitizenName}={Schema.CitizenName.ParamName}";

            return result;
        }
    }
}
