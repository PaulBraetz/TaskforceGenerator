﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Core.Commands;
using TaskforceGenerator.Domain.Core.Exceptions;
using TaskforceGenerator.Infrastructure.SqLite.Abstractions;
using TaskforceGenerator.Infrastructure.SqLite.Schema;

using Infrastructure.SqLite;

using Microsoft.Data.Sqlite;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Infrastructure.SqLite.Services.Core
{
    /// <summary>
    /// Service for committing a citizen to the infrastructure.
    /// </summary>
    public sealed class CommitCitizenService : DatabaseServiceBase<Citizens>, IAsyncCqrsCommandService<CommitCitizen>
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="schema">The schema information required by the service.</param>
        /// <param name="database">The database providing infrastructure access.</param>
        public CommitCitizenService(Citizens schema, Database database) : base(schema, database)
        {
        }

        /// <inheritdoc/>
        public async ValueTask Execute(CommitCitizen command)
        {
            var commandText = CreateCommandText();
            using var dbCommand = Database.CreateCommand(commandText);

            var citizen = command.Citizen;
#pragma warning disable IDE0058 // Expression value is never used
            dbCommand.Parameters.AddWithValue(Schema.CitizenName.ParamName, citizen.Name);
#pragma warning restore IDE0058 // Expression value is never used

            if(command.CancellationToken.IsCancellationRequested)
            {
                return;
            }

            await dbCommand.PrepareAsync(command.CancellationToken);
            try
            {
                _ = await dbCommand.ExecuteNonQueryAsync(command.CancellationToken);
            } catch(SqliteException ex)
            {
                throw new CitizenCommitException(command.Citizen.Name, ex);
            }
        }

        private String CreateCommandText()
        {
            var result = $"insert into {Schema.TableName}" +
                $"({Schema.CitizenName}" +
                $") values(" +
                $"{Schema.CitizenName.ParamName})";

            return result;
        }
    }
}
