﻿using TaskforceGenerator.Infrastructure.SqLite.Schema;
using System.Data;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Authentication;

namespace TaskforceGenerator.Infrastructure.SqLite.Services.Authentication
{
    public sealed partial class ReconstituteConnectionService
    {
        private sealed record ConnectionEntity(String CitizenName, BioCode Code, Password Password) : ICitizenConnection
        {
            public static ICitizenConnection Reconstitute(IDataRecord row, CitizenConnections schema)
            {
                var result = new ConnectionEntity(
                    row.Read<String>(schema.CitizenName),
                    ReconstituteBioCode(row, schema),
                    ReconstitutePassword(row, schema));

                return result;
            }
            private static Password ReconstitutePassword(IDataRecord row, CitizenConnections schema)
            {
                var result = new Password(
                    row.Read<Byte[]>(schema.PasswordHash),
                    new PasswordParameters(
                        new PasswordParameterNumerics(
                            row.ReadInt32(schema.PasswordIterations),
                            row.ReadInt32(schema.PasswordDegreeOfParallelism),
                            row.ReadInt32(schema.PasswordMemorySize),
                            row.ReadInt32(schema.PasswordOutputLength)),
                        new PasswordParameterData(
                            row.Read<Byte[]>(schema.PasswordAssociatedData),
                            row.Read<Byte[]>(schema.PasswordKnownSecret),
                            row.Read<Byte[]>(schema.PasswordSalt))));

                return result;
            }
            private static BioCode ReconstituteBioCode(IDataRecord row, CitizenConnections schema)
            {
                var result = new BioCode(row.Read<String>(schema.Code));

                return result;
            }
        }
    }
}
