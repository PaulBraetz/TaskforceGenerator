﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Commands;
using TaskforceGenerator.Infrastructure.SqLite.Abstractions;
using TaskforceGenerator.Infrastructure.SqLite.Schema;

using Infrastructure.SqLite;

namespace TaskforceGenerator.Infrastructure.SqLite.Services.Authentication
{
    /// <summary>
    /// Service for setting a citizens bio code.
    /// </summary>
    public sealed class CommitBioCodeChangeService : DatabaseServiceBase<CitizenConnections>, IAsyncCqrsCommandService<CommitBioCodeChange>
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="schema">The schema information required by the service.</param>
        /// <param name="database">The database providing infrastructure access.</param>
        public CommitBioCodeChangeService(CitizenConnections schema, Database database) : base(schema, database)
        {
        }

        /// <inheritdoc/>
        public async ValueTask Execute(CommitBioCodeChange command)
        {
            if(command.CancellationToken.IsCancellationRequested)
            {
                return;
            }

            var commandText = CreateCommandText();
            using var dbCommand = Database.CreateCommand(commandText);

#pragma warning disable IDE0058 // Expression value is never used
            dbCommand.Parameters.AddWithValue(Schema.CitizenName.ParamName, command.CitizenName);
            dbCommand.Parameters.AddWithValue(Schema.Code.ParamName, command.Code.Value);
#pragma warning restore IDE0058 // Expression value is never used

            await dbCommand.PrepareAsync(command.CancellationToken);
            _ = await dbCommand.ExecuteNonQueryAsync(command.CancellationToken);
        }

        private String CreateCommandText()
        {
            var result = $"update {Schema.TableName} set " +
                $"{Schema.Code}={Schema.Code.ParamName} where " +
                $"{Schema.CitizenName}={Schema.CitizenName.ParamName}";

            return result;
        }
    }
}
