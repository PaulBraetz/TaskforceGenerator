﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Commands;
using TaskforceGenerator.Domain.Core.Exceptions;
using TaskforceGenerator.Infrastructure.SqLite.Abstractions;
using TaskforceGenerator.Infrastructure.SqLite.Schema;

using Infrastructure.SqLite;

using Microsoft.Data.Sqlite;

namespace TaskforceGenerator.Infrastructure.SqLite.Services.Authentication
{
    /// <summary>
    /// Service for committing connections to the infrastructure.
    /// </summary>
    public sealed class CommitConnectionService : DatabaseServiceBase<CitizenConnections>, IAsyncCqrsCommandService<CommitConnection>
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="schema">The schema information required by the service.</param>
        /// <param name="database">The database providing infrastructure access.</param>
        public CommitConnectionService(CitizenConnections schema, Database database) : base(schema, database)
        {
        }

        /// <inheritdoc/>
        public async ValueTask Execute(CommitConnection command)
        {
            var commandText = CreateCommandText();
            using var dbCommand = Database.CreateCommand(commandText);

            var connection = command.Connection;
#pragma warning disable IDE0058 // Expression value is never used
            dbCommand.Parameters.AddWithValue(Schema.CitizenName.ParamName, connection.CitizenName);
            dbCommand.Parameters.AddWithValue(Schema.Code.ParamName, connection.Code.Value);
            dbCommand.Parameters.AddWithValue(Schema.PasswordHash.ParamName, connection.Password.Hash);

            var numerics = connection.Password.Parameters.Numerics;
            dbCommand.Parameters.AddWithValue(Schema.PasswordDegreeOfParallelism.ParamName, numerics.DegreeOfParallelism);
            dbCommand.Parameters.AddWithValue(Schema.PasswordIterations.ParamName, numerics.Iterations);
            dbCommand.Parameters.AddWithValue(Schema.PasswordMemorySize.ParamName, numerics.MemorySize);
            dbCommand.Parameters.AddWithValue(Schema.PasswordOutputLength.ParamName, numerics.OutputLength);

            var data = connection.Password.Parameters.Data;
            dbCommand.Parameters.AddWithValue(Schema.PasswordSalt.ParamName, data.Salt);
            dbCommand.Parameters.AddWithValue(Schema.PasswordAssociatedData.ParamName, data.AssociatedData);
            dbCommand.Parameters.AddWithValue(Schema.PasswordKnownSecret.ParamName, data.KnownSecret);
#pragma warning restore IDE0058 // Expression value is never used

            if(command.CancellationToken.IsCancellationRequested)
            {
                return;
            }

            await dbCommand.PrepareAsync(command.CancellationToken);
            try
            {
                _ = await dbCommand.ExecuteNonQueryAsync(command.CancellationToken);
            } catch(SqliteException ex)
            {
                throw new ConnectionCommitException(command.Connection.CitizenName, ex);
            }
        }

        private String CreateCommandText()
        {
            var result = $"insert into {Schema.TableName}" +
                $"({Schema.CitizenName}," +
                $"{Schema.Code}," +
                $"{Schema.PasswordHash}," +
                $"{Schema.PasswordDegreeOfParallelism}," +
                $"{Schema.PasswordIterations}," +
                $"{Schema.PasswordMemorySize}," +
                $"{Schema.PasswordOutputLength}," +
                $"{Schema.PasswordSalt}," +
                $"{Schema.PasswordAssociatedData}," +
                $"{Schema.PasswordKnownSecret}" +
                $") values(" +
                $"{Schema.CitizenName.ParamName}," +
                $"{Schema.Code.ParamName}," +
                $"{Schema.PasswordHash.ParamName}," +
                $"{Schema.PasswordDegreeOfParallelism.ParamName}," +
                $"{Schema.PasswordIterations.ParamName}," +
                $"{Schema.PasswordMemorySize.ParamName}," +
                $"{Schema.PasswordOutputLength.ParamName}," +
                $"{Schema.PasswordSalt.ParamName}," +
                $"{Schema.PasswordAssociatedData.ParamName}," +
                $"{Schema.PasswordKnownSecret.ParamName})";

            return result;
        }
    }
}
