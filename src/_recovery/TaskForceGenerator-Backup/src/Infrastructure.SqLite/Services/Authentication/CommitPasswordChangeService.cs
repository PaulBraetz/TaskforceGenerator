﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Commands;
using TaskforceGenerator.Infrastructure.SqLite.Abstractions;
using TaskforceGenerator.Infrastructure.SqLite.Schema;

using Infrastructure.SqLite;

namespace TaskforceGenerator.Infrastructure.SqLite.Services.Authentication
{
    /// <summary>
    /// Service for setting a citizens password.
    /// </summary>
    public sealed class CommitPasswordChangeService : DatabaseServiceBase<CitizenConnections>, IAsyncCqrsCommandService<CommitPasswordChange>
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="schema">The schema information required by the service.</param>
        /// <param name="database">The database providing infrastructure access.</param>
        public CommitPasswordChangeService(CitizenConnections schema, Database database) : base(schema, database)
        {
        }

        /// <inheritdoc/>
        public async ValueTask Execute(CommitPasswordChange command)
        {
            if(command.CancellationToken.IsCancellationRequested)
            {
                return;
            }

            var commandText = CreateCommandText();
            using var dbCommand = Database.CreateCommand(commandText);

#pragma warning disable IDE0058 // Expression value is never used
            dbCommand.Parameters.AddWithValue(Schema.CitizenName.ParamName, command.CitizenName);
            dbCommand.Parameters.AddWithValue(Schema.Code.ParamName, command.Code.Value);

            dbCommand.Parameters.AddWithValue(Schema.PasswordHash.ParamName, command.Password.Hash);

            var numerics = command.Password.Parameters.Numerics;
            dbCommand.Parameters.AddWithValue(Schema.PasswordDegreeOfParallelism.ParamName, numerics.DegreeOfParallelism);
            dbCommand.Parameters.AddWithValue(Schema.PasswordIterations.ParamName, numerics.Iterations);
            dbCommand.Parameters.AddWithValue(Schema.PasswordMemorySize.ParamName, numerics.MemorySize);
            dbCommand.Parameters.AddWithValue(Schema.PasswordOutputLength.ParamName, numerics.OutputLength);

            var data = command.Password.Parameters.Data;
            dbCommand.Parameters.AddWithValue(Schema.PasswordSalt.ParamName, data.Salt);
            dbCommand.Parameters.AddWithValue(Schema.PasswordAssociatedData.ParamName, data.AssociatedData);
            dbCommand.Parameters.AddWithValue(Schema.PasswordKnownSecret.ParamName, data.KnownSecret);
#pragma warning restore IDE0058 // Expression value is never used

            await dbCommand.PrepareAsync(command.CancellationToken);
            _ = await dbCommand.ExecuteNonQueryAsync(command.CancellationToken);
        }

        private String CreateCommandText()
        {
            var result = $"update {Schema.TableName} set " +
                $"{Schema.PasswordHash}={Schema.PasswordHash.ParamName}," +
                $"{Schema.PasswordDegreeOfParallelism}={Schema.PasswordDegreeOfParallelism.ParamName}," +
                $"{Schema.PasswordIterations}={Schema.PasswordIterations.ParamName}," +
                $"{Schema.PasswordMemorySize}={Schema.PasswordMemorySize.ParamName}," +
                $"{Schema.PasswordOutputLength}={Schema.PasswordOutputLength.ParamName}," +
                $"{Schema.PasswordSalt}={Schema.PasswordSalt.ParamName}," +
                $"{Schema.PasswordAssociatedData}={Schema.PasswordAssociatedData.ParamName}," +
                $"{Schema.PasswordKnownSecret}={Schema.PasswordKnownSecret.ParamName} " +
                $"where " +
                $"{Schema.CitizenName}={Schema.CitizenName.ParamName} and " +
                $"{Schema.Code}={Schema.Code.ParamName}";

            return result;
        }
    }
}
