﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Queries;
using TaskforceGenerator.Domain.Core.Queries;
using TaskforceGenerator.Infrastructure.SqLite.Abstractions;
using TaskforceGenerator.Infrastructure.SqLite.Schema;
using Infrastructure.SqLite;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Infrastructure.SqLite.Services.Authentication
{
    /// <summary>
    /// Service for checking whether a citizens connection is registered to the system.
    /// </summary>
    public sealed class CheckConnectionExistsService : DatabaseServiceBase<CitizenConnections>, IAsyncCqrsQueryService<CheckConnectionExists, CheckConnectionExists.Result>
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="schema">The schema information required by the service.</param>
        /// <param name="database">The database providing infrastructure access.</param>
        public CheckConnectionExistsService(CitizenConnections schema, Database database) : base(schema, database)
        {
        }

        /// <inheritdoc/>
        public async ValueTask<CheckConnectionExists.Result> Execute(CheckConnectionExists query)
        {
            var commandText = CreateCommand();
            using var dbCommand = Database.CreateCommand(commandText);

#pragma warning disable IDE0058 // Expression value is never used
            dbCommand.Parameters.AddWithValue(Schema.CitizenName.ParamName, query.CitizenName);
#pragma warning restore IDE0058 // Expression value is never used

            await dbCommand.PrepareAsync(query.CancellationToken);
            var count = await dbCommand.ExecuteScalarAsync(query.CancellationToken);
            var registered = (Int64)count! > 0;
            var result = new CheckConnectionExists.Result(registered);

            return result;
        }

        private String CreateCommand()
        {
            var result = $"select count(*) from {Schema.TableName} where {Schema.CitizenName}={Schema.CitizenName.ParamName}";

            return result;
        }
    }
}
