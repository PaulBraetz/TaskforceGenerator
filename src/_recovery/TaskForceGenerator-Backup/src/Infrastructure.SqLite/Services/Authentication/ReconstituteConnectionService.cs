﻿using TaskforceGenerator.Infrastructure.SqLite;
using TaskforceGenerator.Infrastructure.SqLite.Schema;
using System.Data;
using TaskforceGenerator.Domain.Authentication.Exceptions;
using TaskforceGenerator.Common.Abstractions;
using Infrastructure.SqLite;
using TaskforceGenerator.Domain.Authentication.Queries;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Infrastructure.SqLite.Abstractions;
using TaskforceGenerator.Domain.Core.Exceptions;

namespace TaskforceGenerator.Infrastructure.SqLite.Services.Authentication
{
    /// <summary>
    /// Service for reconstituting a single citizen matching the query.
    /// </summary>
    public sealed partial class ReconstituteConnectionService : DatabaseServiceBase<CitizenConnections>, IAsyncCqrsQueryService<ReconstituteConnection, ICitizenConnection>
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="schema">The schema information required by the service.</param>
        /// <param name="database">The database providing infrastructure access.</param>
        public ReconstituteConnectionService(CitizenConnections schema, Database database) : base(schema, database)
        {
        }

        /// <inheritdoc/>
        public async ValueTask<ICitizenConnection> Execute(ReconstituteConnection query)
        {
            var commandText = CreateCommandText();
            using var dbCommand = Database.CreateCommand(commandText);

#pragma warning disable IDE0058 // Expression value is never used
            dbCommand.Parameters.AddWithValue(Schema.CitizenName.ParamName, query.CitizenName);
#pragma warning restore IDE0058 // Expression value is never used

            await dbCommand.PrepareAsync(query.CancellationToken);
            var reader = await dbCommand.ExecuteReaderAsync(query.CancellationToken);

            if(!reader.HasRows)
            {
                throw new ConnectionNotExistingException(query.CitizenName);
            }

            var row = reader.OfType<IDataRecord>().Single();
            var result = ConnectionEntity.Reconstitute(row, Schema);

            return result;
        }

        private String CreateCommandText()
        {
            var result = $"select * from {Schema.TableName} where {Schema.CitizenName}={Schema.CitizenName.ParamName}";

            return result;
        }
    }
}
