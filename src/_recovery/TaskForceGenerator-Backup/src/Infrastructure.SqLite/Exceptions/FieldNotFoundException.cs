﻿namespace TaskforceGenerator.Infrastructure.SqLite.Exceptions
{
    /// <summary>
    /// Thrown when a field does not exist but is required in order to reconstitute an entity or value.
    /// </summary>
    internal sealed class FieldNotFoundException : Exception
    {
        public FieldNotFoundException(String name)
        {
            Name = name;
        }

        public String Name { get; }
    }
}
