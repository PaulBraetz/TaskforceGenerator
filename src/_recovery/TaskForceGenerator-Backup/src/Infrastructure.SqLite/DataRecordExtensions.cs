﻿using TaskforceGenerator.Infrastructure.SqLite.Exceptions;
using TaskforceGenerator.Infrastructure.SqLite.Schema;

using System.Data;

namespace TaskforceGenerator.Infrastructure.SqLite
{
    internal static class DataRecordExtensions
    {
        public static T Read<T>(this IDataRecord row, Field field)
        {
            var result = row.Read<T>(field.FieldName);

            return result;
        }
        public static T Read<T>(this IDataRecord row, String name)
        {
            var result = row.Read(name, f => f is T, f => (T)f!);

            return result;
        }
        public static T Read<T>(this IDataRecord row, String name, Func<Object?, Boolean> assertType, Func<Object?, T> parse)
        {
            var field = row[name];
            var result = assertType.Invoke(field) ?
                parse.Invoke(field) :
                throw new InvalidFieldTypeException(typeof(T), field.GetType());

            return result;
        }
        public static Int32 ReadInt32(this IDataRecord row, Field field)
        {
            var result = row.ReadInt32(field.FieldName);

            return result;
        }
        public static Int32 ReadInt32(this IDataRecord row, String name)
        {
            var result = row.Read(
                name,
                f => f is Int64 int64Field &&
                    int64Field <= Int32.MaxValue &&
                    int64Field >= Int32.MinValue,
                f => (Int32)(Int64)f!);

            return result;
        }
    }
}
