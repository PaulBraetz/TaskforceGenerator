﻿namespace TaskforceGenerator.Infrastructure.SqLite.Schema
{
    /// <summary>
    /// Contains information on the column names of the <c>CitizenConnections</c> table.
    /// </summary>
    public sealed class CitizenConnections
    {
        /// <summary>
        /// Gets the name of the table.
        /// </summary>
        public String TableName { get; init; } = "CitizenConnections";
        /// <summary>
        /// Gets the name of the <c>CitizenName</c> column.
        /// </summary>
        public Field CitizenName { get; init; } = new ("CitizenName", "citizenName");
        /// <summary>
        /// Gets the name of the <c>Code</c> column.
        /// </summary>
        public Field Code { get; init; } = new ("Code", "code");
        /// <summary>
        /// Gets the name of the <c>Password_Hash</c> column.
        /// </summary>
        public Field PasswordHash { get; init; } = new ("Password_Hash", "password_Hash");
        /// <summary>
        /// Gets the name of the <c>Password_Iterations</c> column.
        /// </summary>
        public Field PasswordIterations { get; init; } = new ("Password_Iterations", "password_Iterations");
        /// <summary>
        /// Gets the name of the <c>Password_OutputLength</c> column.
        /// </summary>
        public Field PasswordOutputLength { get; init; } = new ("Password_OutputLength", "password_OutputLength");
        /// <summary>
        /// Gets the name of the <c>Password_MemorySize</c> column.
        /// </summary>
        public Field PasswordMemorySize { get; init; } = new ("Password_MemorySize", "password_MemorySize");
        /// <summary>
        /// Gets the name of the <c>Password_DegreeOfParallelism</c> column.
        /// </summary>
        public Field PasswordDegreeOfParallelism { get; init; } = new ("Password_DegreeOfParallelism", "password_DegreeOfParallelism");
        /// <summary>
        /// Gets the name of the <c>Password_Salt</c> column.
        /// </summary>
        public Field PasswordSalt { get; init; } = new ("Password_Salt", "password_Salt");
        /// <summary>
        /// Gets the name of the <c>Password_KnownSecret</c> column.
        /// </summary>
        public Field PasswordKnownSecret { get; init; } = new ("Password_KnownSecret", "password_KnownSecret");
        /// <summary>
        /// Gets the name of the <c>Password_AssociatedData</c> column.
        /// </summary>
        public Field PasswordAssociatedData { get; init; } = new ("Password_AssociatedData", "password_AssociatedData");
    }
}
