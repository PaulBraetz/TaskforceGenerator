﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Core.Queries;

namespace TaskforceGenerator.Infrastructure.Services
{
    /// <summary>
    /// Service for checking whether a citizen actualy exists, that is, he is an RSI-registered citizen.
    /// </summary>
    public sealed class CheckCitizenExistsService : IAsyncCqrsQueryService<CheckCitizenExists, CheckCitizenExists.Result>
    {
        /// <inheritdoc/>
        public async ValueTask<CheckCitizenExists.Result> Execute(CheckCitizenExists query)
        {
            var pageUri = $"https://robertsspaceindustries.com/citizens/{query.CitizenName}";
            using var client = new HttpClient();
            var response = await client.GetAsync(pageUri, query.CancellationToken);
            var exists = response.IsSuccessStatusCode;
            var result = new CheckCitizenExists.Result(exists);

            return result;
        }
    }
}
