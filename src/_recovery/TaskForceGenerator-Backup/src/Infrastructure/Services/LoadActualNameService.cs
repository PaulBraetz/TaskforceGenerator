﻿using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Queries;
using TaskforceGenerator.Infrastructure.Queries;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TaskforceGenerator.Domain.Core.Queries;
using TaskforceGenerator.Domain.Core.Exceptions;

namespace TaskforceGenerator.Infrastructure.Services
{
    /// <summary>
    /// Service for loading a citizens actual name, as registered to RSI.
    /// </summary>
    public sealed class LoadActualNameService : IAsyncCqrsQueryService<LoadActualName, LoadActualName.Result>
    {
        private readonly IAsyncCqrsQueryService<LoadCitizenPage, LoadCitizenPage.Result> _pageService;
        private readonly IAsyncCqrsQueryService<CheckCitizenExists, CheckCitizenExists.Result> _checkService;

        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="pageService">The service used for loading the citizens page for parsing the actual name registered to RSI.</param>
        /// <param name="checkService">The service to use when checking if a name requested actually corresponds to an RSI registered citizen.</param>
        public LoadActualNameService(IAsyncCqrsQueryService<LoadCitizenPage, LoadCitizenPage.Result> pageService,
                                     IAsyncCqrsQueryService<CheckCitizenExists, CheckCitizenExists.Result> checkService)
        {
            _pageService = pageService;
            _checkService = checkService;
        }
        /// <inheritdoc/>
        public async ValueTask<LoadActualName.Result> Execute(LoadActualName query)
        {
            var exists = (await new CheckCitizenExists(query.CitizenName, query.CancellationToken).Using(_checkService)).Exists;
            if (!exists)
            {
                throw new CitizenNotExistingException(query.CitizenName);
            }

            var page = (await new LoadCitizenPage(query.CitizenName, query.CancellationToken).Using(_pageService)).Page;
            //TODO: https://html-agility-pack.net/
            var actualName = Regex.Match(page, @"(?<=<span class=""label"">Handle name<\/span>(\s|\r|\n)*<strong class=""value"">)[^<]*").Value;
            var result = new LoadActualName.Result(actualName);

            return result;
        }
    }
}
