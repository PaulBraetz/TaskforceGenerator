﻿using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Authentication.Queries;
using TaskforceGenerator.Infrastructure.Queries;

using System.Text.RegularExpressions;

namespace TaskforceGenerator.Infrastructure.Services
{
    /// <summary>
    /// Service for loading a citizen's bio.
    /// </summary>
    public sealed class LoadBioService : IAsyncCqrsQueryService<LoadBio, IBio>
    {
        private readonly IAsyncCqrsQueryService<LoadCitizenPage, LoadCitizenPage.Result> _pageService;
        private readonly IAsyncCqrsQueryService<CreateBio, IBio> _createService;
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="createService">The service to use when instantiating a bio after loading its text.</param>
        /// <param name="pageService">The service used when loading the citizens page for parsing the bio.</param>
        public LoadBioService(IAsyncCqrsQueryService<CreateBio, IBio> createService, IAsyncCqrsQueryService<LoadCitizenPage, LoadCitizenPage.Result> pageService)
        {
            _createService = createService;
            _pageService = pageService;
        }

        /// <inheritdoc/>
        public async ValueTask<IBio> Execute(LoadBio query)
        {
            var page = (await new LoadCitizenPage(query.Name, query.CancellationToken).Using(_pageService)).Page;
            //TODO: https://html-agility-pack.net/
            var bio = Regex.Match(page, @"(?<=class=""entry\sbio"">)((?:(?!<\/div>).)|\r|\n)*(?=<\/div>)").Value;
            var bioTextParts = Regex.Matches(bio, @"(?<=>)(?:(?!<[^>]*>)(.|\r|\n))*(?=<)")
                .Select(m => m.Value)
                .Where(v => !String.IsNullOrWhiteSpace(v));
            var bioText = String.Concat(bioTextParts);
            var result = await new CreateBio(bioText, query.CancellationToken).Using(_createService);

            return result;
        }
    }
}
