﻿using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Infrastructure.Queries;
using TaskforceGenerator.Domain.Core.Queries;
using System.Text.RegularExpressions;

namespace TaskforceGenerator.Infrastructure.Services
{
    /// <summary>
    /// Service for loading a citizens public orgs.
    /// </summary>
    public sealed class LoadCitizenOrgsService : IAsyncCqrsQueryService<LoadCitizenOrgs, LoadCitizenOrgs.Result>
    {
        /// <inheritdoc/>
        public async ValueTask<LoadCitizenOrgs.Result> Execute(LoadCitizenOrgs query)
        {
            var pageUri = $"https://robertsspaceindustries.com/citizens/{query.CitizenName}/organizations";
            using var client = new HttpClient();
            var response = await client.GetAsync(pageUri, query.CancellationToken);
            if(!response.IsSuccessStatusCode)
            {
#pragma warning disable CA2208 // Instantiate argument exceptions correctly -> query request objects properties are to be treated as parameters
                throw new ArgumentException($"Unable to retrieve orgs using the name provided: {query.CitizenName}", nameof(query.CitizenName));
#pragma warning restore CA2208 // Instantiate argument exceptions correctly
            }

            var page = await response.Content.ReadAsStringAsync(query.CancellationToken);

            //TODO: https://html-agility-pack.net/
            var orgs = Regex.Matches(page, @"(?<=Spectrum Identification \(SID\)<\/span>(\n|\r|\s)*<strong class=""[a-zA-Z0-9\s]*"">)[^>]*(?=<\/strong>)")
                .Select(m => m.Value)
                .ToHashSet();
            var result = new LoadCitizenOrgs.Result(orgs);
            return result;
        }
    }
}
