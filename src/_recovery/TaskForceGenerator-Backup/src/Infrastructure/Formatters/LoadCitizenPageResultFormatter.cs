﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Queries;
using TaskforceGenerator.Infrastructure.Queries;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Infrastructure.Formatters
{
	/// <summary>
	/// Ellipsis based formatter for <see cref="LoadCitizenPage.Result"/>.
	/// </summary>
	public sealed class LoadCitizenPageResultFormatter : IStaticFormatter<LoadCitizenPage.Result>
	{
		private const Int32 ELLIPSIS_LIMIT = 10;
		/// <inheritdoc/>
		public String Format(LoadCitizenPage.Result value)
		{
			var result =
				$"{nameof(LoadCitizenPage.Result)} {{ " +
					$"{nameof(LoadCitizenPage.Result.Page)} = ... " +
				$"}}";

			return result;
		}
	}
}
