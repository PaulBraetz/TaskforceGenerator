﻿using TaskforceGenerator.Common.Abstractions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Infrastructure.Queries
{
    /// <summary>
    /// Query for loading a citizens RSI page.
    /// </summary>
    /// <param name="CitizenName">The name of the citizen whose page to load.</param>
    /// <param name="CancellationToken">The token used to signal the query execution to be cancelled.</param>
    public readonly record struct LoadCitizenPage(String CitizenName, CancellationToken CancellationToken) : ICqrsQuery
    {
        /// <summary>
        /// The result of the query.
        /// </summary>
        /// <param name="Page">The page loaded.</param>
        public readonly record struct Result(String Page);
    }
}
