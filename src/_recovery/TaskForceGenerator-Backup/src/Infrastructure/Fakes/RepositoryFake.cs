﻿using TaskforceGenerator.Domain.Authentication.Commands;
using TaskforceGenerator.Domain.Authentication.Queries;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Authentication;
using TaskforceGenerator.Domain.Core.Queries;

namespace TaskforceGenerator.Infrastructure.Fakes
{
    /// <summary>
    /// Service for setting a citizens password.
    /// </summary>
    public sealed class RepositoryFake :
        IAsyncCqrsCommandService<CommitPasswordChange>,
        IAsyncCqrsQueryService<ReconstituteConnection, ICitizenConnection>,
        IAsyncCqrsCommandService<CommitBioCodeChange>,
        IAsyncCqrsQueryService<CheckCitizenRegistered, CheckCitizenRegistered.Result>,
        IAsyncCqrsCommandService<CommitConnection>
    {
        private sealed class ConnectionFake : ICitizenConnection
        {
            public String CitizenName { get; set; } = String.Empty;
            public BioCode Code { get; set; }
            public Password Password { get; set; }
        }

        private readonly Dictionary<String, ConnectionFake> _citizens = new();

        private ConnectionFake LoadCitizen(String name)
        {
            return _citizens.TryGetValue(name, out var c) ?
                c :
                throw new InvalidOperationException($"Unable to locate citizen for name {name}.");
        }

        /// <inheritdoc/>
        public ValueTask<ICitizenConnection> Execute(ReconstituteConnection query)
        {
            var result = (ICitizenConnection)LoadCitizen(query.CitizenName);

            return ValueTask.FromResult(result);
        }
        /// <inheritdoc/>
        public ValueTask Execute(CommitPasswordChange command)
        {
            var citizen = LoadCitizen(command.CitizenName);
            citizen.Password = command.Password;

            return ValueTask.CompletedTask;
        }
        /// <inheritdoc/>
        public ValueTask Execute(CommitBioCodeChange command)
        {
            var citizen = LoadCitizen(command.CitizenName);
            citizen.Code = command.Code;

            return ValueTask.CompletedTask;
        }
        /// <inheritdoc/>
        public ValueTask<CheckCitizenRegistered.Result> Execute(CheckCitizenRegistered query)
        {
            var registered = _citizens.ContainsKey(query.CitizenName);
            var result = new CheckCitizenRegistered.Result(registered);

            return ValueTask.FromResult(result);
        }
        /// <inheritdoc/>
        public ValueTask Execute(CommitConnection command)
        {
            var clone = new ConnectionFake()
            {
                Code = command.Connection.Code,
                CitizenName = command.Connection.CitizenName,
                Password = command.Connection.Password,
            };

            _citizens.Add(clone.CitizenName, clone);
            return ValueTask.CompletedTask;
        }
    }
}
