﻿using Microsoft.AspNetCore.SignalR;

using SimpleInjector;

namespace TaskforceGenerator.Presentation.Views.Blazor.DependencyInjection
{
    /// <summary>
    /// Hub activator for integrating SimpleInjector into Blazor.
    /// </summary>
    /// <typeparam name="T">The type of hub to activate.</typeparam>
    public sealed class SimpleInjectorBlazorHubActivator<T>
        : IHubActivator<T> where T : Hub
    {
        private readonly ServiceScopeApplier _applier;
        private readonly Container _container;

        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="applier">The ambient scope applier to use.</param>
        /// <param name="container">The container using which to resolve the hub.</param>
        public SimpleInjectorBlazorHubActivator(
            ServiceScopeApplier applier, Container container)
        {
            _applier = applier;
            _container = container;
        }
        /// <inheritdoc/>
        public T Create()
        {
            _applier.ApplyServiceScope();
            var result = _container.GetInstance<T>();
            return result;
        }
        /// <inheritdoc/>
        public void Release(T hub) { }
    }
}
