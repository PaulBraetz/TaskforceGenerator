﻿using SimpleInjector;

namespace TaskforceGenerator.Presentation.Views.Blazor.DependencyInjection
{
    /// <summary>
    /// Helper class for integrating SimpleInjector with Blazor.
    /// </summary>
    public sealed class ScopeAccessor : IAsyncDisposable, IDisposable
    {
        /// <summary>
        /// Gets or sets the current Scope.
        /// </summary>
        public Scope Scope { get; set; }
        /// <inheritdoc/>
        public ValueTask DisposeAsync() => Scope?.DisposeAsync() ?? default;

        /// <inheritdoc/>
        public void Dispose() => Scope?.Dispose();
    }
}
