﻿using SimpleInjector;
using SimpleInjector.Advanced;

using System.Reflection;

namespace TaskforceGenerator.Presentation.Views.Blazor.DependencyInjection
{
    internal sealed class DependencyAttributePropertySelectionBehavior : IPropertySelectionBehavior
    {
        public Boolean SelectProperty(Type type, PropertyInfo prop)
        {
            var result = prop.GetCustomAttributes(typeof(InjectedAttribute)).Any();

            return result;
        }
    }
}
