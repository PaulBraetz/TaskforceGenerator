﻿using SimpleInjector;
using Microsoft.Extensions.Logging;

namespace TaskforceGenerator.Presentation.Views.Blazor.DependencyInjection.Logging
{
    /// <summary>
    /// Logs detected fakes in the service registered to the container.
    /// </summary>
    public sealed class FakeWarningsLogger : IContainerLogger
    {
        /// <inheritdoc/>
        public void Log(Container container, ILogger logger)
        {
            var fakes = container.GetCurrentRegistrations()
                .Where(r =>
                    r.ImplementationType.Name.Contains("fake", StringComparison.InvariantCultureIgnoreCase) ||
                    r.ImplementationType.Namespace != null && 
                    r.ImplementationType.Namespace.Contains("fake", StringComparison.InvariantCultureIgnoreCase));
            foreach(var fake in fakes)
            {
                logger.LogWarning(
                    "Injection warning: Fake detected: {serviceType}->{implementationType}",
                    fake.ServiceType.FullName,
                    fake.ImplementationType.FullName);
            }
        }
    }
}
