﻿using Microsoft.Extensions.Logging;

using SimpleInjector;

namespace TaskforceGenerator.Presentation.Views.Blazor.DependencyInjection.Logging
{
    /// <summary>
    /// Composite logger for logging different types of information about a container.
    /// </summary>
    public sealed class CompositeContainerLogger : IContainerLogger
    {
        private readonly IEnumerable<IContainerLogger> _children;
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="loggers">The loggers used when calling <see cref="Log(Container, ILogger)"/>.</param>
        public CompositeContainerLogger(IEnumerable<IContainerLogger> loggers)
        {
            _children = loggers;
        }
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="loggers">The loggers used when calling <see cref="Log(Container, ILogger)"/>.</param>
        public CompositeContainerLogger(params IContainerLogger[] loggers) : this((IEnumerable<IContainerLogger>)loggers)
        {

        }

        /// <inheritdoc/>
        public void Log(Container container, ILogger logger)
        {
            logger.Log(LogLevel.Information,
                       default,
                       _children,
                       null,
                       (c, e) => $"Container loggers used: {String.Join(", ", c.Select(c => c.GetType().Name))}");
            foreach (var child in _children)
            {
                child.Log(container, logger);
            }
        }
    }
}
