﻿using SimpleInjector.Diagnostics;
using SimpleInjector;
using Microsoft.Extensions.Logging;

namespace TaskforceGenerator.Presentation.Views.Blazor.DependencyInjection.Logging
{
    /// <summary>
    /// Logs diagnostics provided by the container.
    /// </summary>
    public sealed class ContainerDiagnosticsLogger : IContainerLogger
    {
        /// <inheritdoc/>
        public void Log(Container container, ILogger logger)
        {
            var analysisResults = Analyzer.Analyze(container);
            foreach(var result in analysisResults)
            {
                switch(result.Severity)
                {
                    case DiagnosticSeverity.Warning:
                        logger.LogWarning("Injection warning: {diagnostic}", result.Description);
                        break;
                    case DiagnosticSeverity.Information:
                        logger.LogWarning("Injection info: {diagnostic}", result.Description);
                        break;
                }
            }
        }
    }
}
