﻿using Microsoft.Extensions.Logging;

using SimpleInjector;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Presentation.Views.Blazor.DependencyInjection.Logging
{
    /// <summary>
    /// Implements the null logging pattern for <see cref="IContainerLogger"/>.
    /// </summary>
    public sealed class NullContainerLogger : IContainerLogger
    {
        private NullContainerLogger() { }
        /// <summary>
        /// Gets the singleton instance of <see cref="NullContainerLogger"/>.
        /// </summary>
        public static NullContainerLogger Instance { get; } = new();
        /// <inheritdoc/>
        public void Log(Container container, ILogger logger) { }
    }
}
