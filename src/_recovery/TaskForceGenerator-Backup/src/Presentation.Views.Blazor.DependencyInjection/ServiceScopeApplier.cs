﻿using Microsoft.Extensions.DependencyInjection;

using SimpleInjector;
using SimpleInjector.Integration.ServiceCollection;
using SimpleInjector.Lifestyles;

namespace TaskforceGenerator.Presentation.Views.Blazor.DependencyInjection
{
    /// <summary>
    /// Helper class for applying the request scope to the scope accessor.
    /// </summary>
    public sealed class ServiceScopeApplier
    {
        private static readonly AsyncScopedLifestyle _lifestyle = new();

        private readonly IServiceScope _serviceScope;
        private readonly ScopeAccessor _accessor;
        private readonly Container _container;

        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="requestServices">The container with which to integrate.</param>
        /// <param name="accessor">The accessor whose scope to apply.</param>
        /// <param name="container">The container which to integrate.</param>
        public ServiceScopeApplier(
            IServiceProvider requestServices, ScopeAccessor accessor, Container container)
        {
            _serviceScope = (IServiceScope)requestServices;
            _accessor = accessor;
            _container = container;
        }
        /// <summary>
        /// Applies the ambient scope to the accessor.
        /// </summary>
        public void ApplyServiceScope()
        {
            if(_accessor.Scope is null)
            {
                var scope = AsyncScopedLifestyle.BeginScope(_container);

                _accessor.Scope = scope;

                scope.GetInstance<ServiceScopeProvider>().ServiceScope = _serviceScope;
            } else
            {
                _lifestyle.SetCurrentScope(_accessor.Scope);
            }
        }
    }
}
