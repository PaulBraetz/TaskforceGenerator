﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Authentication.Queries;

namespace Infrastructure.Tests
{
    internal sealed class CreateBioService : IAsyncCqrsQueryService<CreateBio, IBio>
    {
        private sealed class Bio : IBio
        {
            private readonly String _bio;

            public Bio(String bio)
            {
                _bio = bio;
            }

            public Boolean Contains(BioCode code) => _bio.Contains(code);
        }
        public ValueTask<IBio> Execute(CreateBio query)
        {
            IBio result = new Bio(query.BioText);

            return ValueTask.FromResult(result);
        }
    }
}