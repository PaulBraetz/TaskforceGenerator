using TaskforceGenerator.Common;
using TaskforceGenerator.Domain.Authentication.Queries;
using TaskforceGenerator.Domain.Core.Queries;
using TaskforceGenerator.Infrastructure.Services;
using TaskforceGenerator.Tests.Common;

namespace Infrastructure.Tests
{
    /// <summary>
    /// Contains tests for <see cref="CheckCitizenExistsService"/>.
    /// </summary>
    [TestClass]
    [TestCategory("Unit Infrastructure")]
    public class CheckCitizenExistsServiceTests : ServiceTestBase<CheckCitizenExistsService>
    {
        /// <summary>
        /// Tests whether the service yields a positive result for citizens that are registered to RSI.
        /// </summary>
        /// <param name="name">The name of the citizen passed to the service.</param>
        [TestMethod]
        [DataRow("SleepWellPupper")]
        [DataRow("YokoArashi")]
        [DataRow("Terrorente")]
        [DataRow("SpaceDirk")]
        public async Task ReturnsExistsForExistingCitizens(String name)
        {
            var result = await new CheckCitizenExists(name, CancellationToken.None).Using(Service);

            Assert.IsTrue(result.Exists);
        }
        /// <summary>
        /// Tests whether the service will throw an <see cref="OperationCanceledException"/> upon being called with a cancelled token.
        /// There is no valid default result to be returned, thus the exception must be thrown.
        /// </summary>
        /// <param name="name">The name of the citizen passed to the service.</param>
        [TestMethod]
        [DataRow("SleepWellPupper")]
        [DataRow("YokoArashi")]
        [DataRow("Terrorente")]
        [DataRow("SpaceDirk")]
        [DataRow("")]
        [DataRow("ok")]
        [DataRow("-")]
        [DataRow("B95686B7-C7FA-4C1D-9663-1370A287AE63")]
        [ExpectedException(typeof(OperationCanceledException), AllowDerivedTypes = true)]
        public async Task ThrowsOCEWhenCancelled(String name)
        {
            var token = new CancellationToken(true);
            _ = await new CheckCitizenExists(name, token).Using(Service);
        }
        /// <summary>
        /// Tests whether the service yields a negative result for citizens that are not registered to RSI.
        /// </summary>
        /// <param name="name">The name of the citizen passed to the service.</param>
        [TestMethod]
        [DataRow("")]
        [DataRow("ok")]
        [DataRow("-")]
        [DataRow("B95686B7-C7FA-4C1D-9663-1370A287AE63")]
        public async Task ReturnsNotExistsForNotExistingCitizens(String name)
        {
            var result = await new CheckCitizenExists(name, CancellationToken.None).Using(Service);

            Assert.IsFalse(result.Exists);
        }
    }
}