﻿using SimpleInjector;

using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Core.Queries;
using TaskforceGenerator.Infrastructure.Queries;
using TaskforceGenerator.Infrastructure.Services;
using TaskforceGenerator.Tests.Common;

namespace TaskforceGenerator.Infrastructure.Tests
{
    /// <summary>
    /// Contains tests for <see cref="LoadCitizenOrgsService"/>.
    /// </summary>
    [TestClass]
    public class LoadCitizenOrgsTests : ServiceTestBase<LoadCitizenOrgsService>
    {
        /// <inheritdoc/>
        protected override void Configure(Container container)
        {
            container.Register<IAsyncCqrsQueryService<LoadCitizenPage, LoadCitizenPage.Result>, LoadCitizenPageService>(Lifestyle.Singleton);
            base.Configure(container);
        }

        /// <summary>
        /// Asserts that the public orgs loaded match the actual ones.
        /// </summary>
        [TestMethod]
        [DataRow("Sleepwellpupper", "KRT")]
        [DataRow("rhobit", "KRT", "QISS", "AVOCADO", "SC4")]
        public async Task LoadsSleepWellPupperOrgsCorrectly(String citizenName, params String[] orgNames)
        {
            //Arrange
            var orgNameSet = orgNames.OfType<String>().ToHashSet();

            //Act
            var orgsResult = await new LoadCitizenOrgs(citizenName, default)
                .Using(Service);
            var orgs = orgsResult.PublicOrgNames;

            //Assert
            Assert.IsTrue(orgNameSet.All(orgs.Contains));
        }
    }
}
