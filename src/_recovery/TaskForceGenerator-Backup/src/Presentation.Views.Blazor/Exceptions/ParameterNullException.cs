﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Presentation.Views.Blazor.Exceptions
{
	/// <summary>
	/// The <see cref="ParameterNullException"/> is thrown when a parameter is <see langword="null"/> when it shouldn't be.
	/// </summary>
	public sealed class ParameterNullException : Exception
	{
		/// <summary>
		/// Initializes a new instance.
		/// </summary>
		/// <param name="parameterName">The name of the parameter that was <see langword="null"/> when it shouldn't have been.</param>
		/// <param name="componentType">The type of the component the exception was thrown by.</param>
		public ParameterNullException(String parameterName, Type componentType) : base($"The component parameter named '{parameterName}' for component of type {componentType.Name} cannot be null.")
		{
			ParameterName = parameterName;
			ComponentType = componentType;
		}
		/// <summary>
		/// Gets the name of the parameter that was <see langword="null"/> when it shouldn't have been.
		/// </summary>
		public String ParameterName { get; }
		/// <summary>
		/// Gets the type of the component the exception was thrown by.
		/// </summary>
		public Type ComponentType { get; }
	}
}
