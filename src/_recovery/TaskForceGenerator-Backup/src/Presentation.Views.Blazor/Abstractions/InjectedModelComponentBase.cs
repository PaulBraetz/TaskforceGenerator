﻿using TaskforceGenerator.Presentation.Views.Blazor.DependencyInjection;

namespace TaskforceGenerator.Presentation.Views.Blazor.Abstractions
{
    /// <summary>
    /// Base component view for a model of <typeparamref name="TModel"/>. The model will be injected.
    /// </summary>
    /// <typeparam name="TModel">The type of model to render a component for.</typeparam>
    public abstract class InjectedModelComponentBase<TModel> : ModelComponentBase<TModel>, IDisposable
    {
        /// <inheritdoc/>
        [Injected]
        public override TModel Value { get => base.Value; set => base.Value = value; }

        /// <summary>
        /// 
        /// </summary>
        [Injected]
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
        public IDisposeStrategy<TModel> ModelDisposeStrategy { get; set; }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

        private const Int32 DISPOSED_STATE = 1;
        private const Int32 NOT_DISPOSED_STATE = 0;
        private const Int32 DISPOSING_STATE = -1;

        private Int32 _disposedValue = NOT_DISPOSED_STATE;

        /// <summary>
        /// Disposes the instance.
        /// </summary>
        /// <param name="disposing">Should be set to <see langword="true"/> if disposing from a <see cref="Dispose()"/> call; otherwise, <see langword="false"/> (occurs during finalization).</param>
        private void Dispose(Boolean disposing)
        {
            if(Interlocked.CompareExchange(ref _disposedValue, DISPOSING_STATE, NOT_DISPOSED_STATE) != NOT_DISPOSED_STATE)
            {
                return;
            }

            try
            {
                if(disposing)
                {
                    ModelDisposeStrategy.Dispose(Value);
                }

                _disposedValue = DISPOSED_STATE;
            } catch
            {
                _disposedValue = NOT_DISPOSED_STATE;
                throw;
            }
        }
        /// <inheritdoc/>
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
