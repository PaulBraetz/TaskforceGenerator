﻿using Microsoft.AspNetCore.Components;

using System.ComponentModel.DataAnnotations;
using System.Reflection;

using TaskforceGenerator.Presentation.Views.Blazor.DependencyInjection;
using TaskforceGenerator.Presentation.Views.Blazor.Exceptions;

namespace TaskforceGenerator.Presentation.Views.Blazor.Abstractions
{
    /// <summary>
    /// Base component providing non captured parameters via <see cref="Attributes"/>
    /// as well as providing automated checking for required parameters in <see cref="OnParametersSet"/>.
    /// Required parameters must be annotated using the <see cref="ParameterAttribute"/> and the <see cref="RequiredAttribute"/>.
    /// Upon encountering a required parameter with value <see langword="null"/>,
    /// a <see cref="ParameterNullException"/> will be thrown.
    /// </summary>
    public abstract class ComponentBase : SimpleInjectorIntegratedComponent
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        public ComponentBase()
        {
            _requiredParameters = new Lazy<PropertyInfo[]>(GetRequiredParameters);
            _attributes = new();
#if DEBUG
            _attributes.Add("component-type", GetType().Name);
#endif
        }

        private readonly Lazy<PropertyInfo[]> _requiredParameters;
        private Dictionary<String, Object> _attributes;

        /// <summary>
        /// Gets or sets the otherwise unmatched attributes passed to the component.
        /// </summary>
        [Parameter(CaptureUnmatchedValues = true)]
        [Required]
        public Dictionary<String, Object> Attributes
        {
            get => _attributes;
            set
            {
#if DEBUG
                // Allow assigning null value in order to force
                // ParameterNullException in OnParametersSet and avoiding
                // NullReferenceException here
                _ = value?.TryAdd("component-type", GetType().Name);
#endif
                _attributes = value!;
            }
        }

        /// <summary>
        /// Ensures that each of the class names provided are present in the <c>class</c> attribute.
        /// </summary>
        /// <param name="classNames">
        /// The class names ensure are present in the <c>class</c> attribute.
        /// </param>
        protected void EnsureClassAttribute(IEnumerable<String> classNames)
        {
            var trimmedClassNames = classNames.Select(n => n.Trim());
            if(!Attributes.TryGetValue("class", out var existingClassNames) ||
                existingClassNames == null ||
                existingClassNames is not String)
            {
                Attributes["class"] = String.Join(' ', trimmedClassNames);
                return;
            }

            var classSet = (existingClassNames as String)!
                .Split(' ', StringSplitOptions.TrimEntries)
                .ToHashSet();

            foreach(var className in trimmedClassNames)
            {
                _ = classSet.Add(className);
            }

            var newClassNames = String.Join(' ', classSet);
            Attributes["class"] = newClassNames;
        }
        /// <summary>
        /// Ensures that each of the class names provided are present in the <c>class</c> attribute.
        /// </summary>
        /// <param name="classNames">
        /// The class names ensure are present in the <c>class</c> attribute.
        /// </param>
        protected void EnsureClassAttribute(params String[] classNames) =>
            EnsureClassAttribute((IEnumerable<String>)classNames);
        /// <inheritdoc/>
        protected override void OnParametersSet() => CheckNullParameters();
        private void CheckNullParameters()
        {
            var nullValueNames = GetNullRequiredParameters();
            var type = GetType();
            foreach(var name in nullValueNames)
            {
                throw new ParameterNullException(name, type);
            }
        }
        private PropertyInfo[] GetRequiredParameters()
        {
            var requiredParameterAttributes = new[]
                        {
                typeof(ParameterAttribute),
                typeof(RequiredAttribute)
            };

            var result = GetType()
                .GetProperties()
                .Where(p => p.CustomAttributes.Any(a => a.AttributeType == typeof(ParameterAttribute)))
                .Where(p => p.CustomAttributes.Any(a => a.AttributeType == typeof(RequiredAttribute)))
                .ToArray();

            return result;
        }
        private IEnumerable<String> GetNullRequiredParameters()
        {
            var result = _requiredParameters.Value
                .Where(p => p.GetValue(this) == null)
                .Select(p => p.Name);

            return result;
        }
    }
}
