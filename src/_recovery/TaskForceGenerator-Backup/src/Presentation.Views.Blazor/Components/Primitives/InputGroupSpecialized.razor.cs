﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Rendering;

using System.ComponentModel.DataAnnotations;

using TaskforceGenerator.Presentation.Models.Abstractions;
using TaskforceGenerator.Presentation.Views.Blazor.Abstractions;

namespace TaskforceGenerator.Presentation.Views.Blazor.Components.Primitives
{
    /// <summary>
    /// Base class for input controls that provide a label, description and error.
    /// </summary>
    /// <typeparam name="TValue">The type of value obtained by the model.</typeparam>
    /// <typeparam name="TError">The type of error displayed by the model.</typeparam>
    /// <typeparam name="TInput">The type of input model used to obtain input.</typeparam>
    public partial class InputGroupSpecialized<TInput, TValue, TError> :
        ModelComponentBase<IInputGroupModel<TInput, TValue, TError>>
        where TInput : IInputModel<TValue, TError>
    {
        private RenderFragment? _label;
        private RenderFragment? _description;

        /// <summary>
        /// Gets or sets the input template.
        /// </summary>
        [Parameter]
        [Required]
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
        public RenderFragment<TInput> Input { get; set; }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
        /// <summary>
        /// Gets or sets the error template.
        /// </summary>
        [Parameter]
        [Required]
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
        public RenderFragment<TError> Error { get; set; }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
        /// <summary>
        /// Gets or sets the label template.
        /// </summary>
        [Parameter]
        [Required]
        public RenderFragment Label
        {
            get => _label ?? DefaultLabel;
            set => _label = value;
        }
        /// <summary>
        /// Gets or sets the description template.
        /// </summary>
        [Parameter]
        [Required]
        public RenderFragment Description
        {
            get => _description ?? DefaultDescription;
            set => _description = value;
        }
        /// <summary>
        /// Renders the default label component that should be rendered if no value is provided for <see cref="Label"/>.
        /// </summary>
        /// <param name="builder">The <see cref="RenderTreeBuilder"/> to which the content should be written.</param>
        protected virtual void DefaultLabel(RenderTreeBuilder builder)
        {
            if(String.IsNullOrEmpty(Model.Label))
                return;
            builder.OpenElement(0, "div");
            builder.AddAttribute(1, "class", "mb-2 text-center font-light text-krtWhite text-2xl");
            builder.AddContent(2, Model.Label);
            builder.CloseComponent();
        }
        /// <summary>
        /// Renders the default description component that should be rendered if no value is provided for <see cref="Description"/>.
        /// </summary>
        /// <param name="builder">The <see cref="RenderTreeBuilder"/> to which the content should be written.</param>
        protected virtual void DefaultDescription(RenderTreeBuilder builder)
        {
            if(String.IsNullOrEmpty(Model.Description))
                return;
            builder.OpenElement(0, "div");
            builder.AddAttribute(1, "class", "input-group-description");
            builder.AddContent(2, Model.Description);
            builder.CloseComponent();
        }
    }
}
