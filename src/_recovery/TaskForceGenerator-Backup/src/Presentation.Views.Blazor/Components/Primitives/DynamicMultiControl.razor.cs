﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Rendering;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TaskforceGenerator.Presentation.Models.Abstractions;
using TaskforceGenerator.Presentation.Views.Blazor.Abstractions;

namespace TaskforceGenerator.Presentation.Views.Blazor.Components.Primitives
{
    /// <summary>
    /// 
    /// </summary>
    public partial class DynamicMultiControl<TSubControlModel> : ModelComponentBase<IDynamicMultiControlModel<TSubControlModel>>
    {
        private static RenderFragment DefaultAddButton(IButtonModel value)
        {
            return result;
            void result(RenderTreeBuilder builder)
            {
                builder.OpenComponent(0, typeof(Button));
                builder.AddAttribute(1, "Value", value);
                builder.CloseComponent();
            }
        }
        private static RenderFragment DefaultLabel(String value)
        {
            return result;
            void result(RenderTreeBuilder builder)
            {
                if(String.IsNullOrEmpty(value))
                    return;
                builder.OpenElement(0, "div");
                builder.AddAttribute(1, "class", "text-krtWhite text-2xl mb-2 text-center font-light");
                builder.AddContent(2, value);
                builder.CloseComponent();
            }
        }
        private static RenderFragment DefaultDescription(String value)
        {
            return result;
            void result(RenderTreeBuilder builder)
            {
                if(String.IsNullOrEmpty(value))
                    return;
                builder.OpenElement(0, "div");
                builder.AddAttribute(1, "class", "input-group-description");
                builder.AddContent(2, value);
                builder.CloseComponent();
            }
        }
    }
}
