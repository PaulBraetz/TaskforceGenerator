﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Rendering;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TaskforceGenerator.Presentation.Models.Abstractions;
using TaskforceGenerator.Presentation.Views.Blazor.Abstractions;
using TaskforceGenerator.Presentation.Views.Blazor.Exceptions;

namespace TaskforceGenerator.Presentation.Views.Blazor.Components.Primitives
{
	/// <summary>
	/// </summary>
	/// <typeparam name="TSubControlModel"></typeparam>
	public partial class DynamicMultiControlItem<TSubControlModel> : ModelComponentBase<IDynamicMultiControlItemModel<TSubControlModel>>
	{
		private static RenderFragment DefaultRemoveButton(IButtonModel button)
		{
			return result;
			void result(RenderTreeBuilder b)
			{
				b.OpenElement(0, "div");
				b.AddAttribute(1, "class", "mr-1 flex items-center");
				b.OpenComponent<Button>(2);
				b.AddAttribute(3, "Value", button);
				b.CloseComponent();
				b.CloseElement();
			}
		}
	}
}
