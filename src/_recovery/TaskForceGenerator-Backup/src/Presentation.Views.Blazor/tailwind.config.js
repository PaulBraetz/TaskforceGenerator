/** @type {import('tailwindcss').Config} */

module.exports = {
	content: ["./**/*.{html,razor,cshtml,razor.cs,cs}"],
	theme: {
		extend: {},
		colors: {
			transparent: "transparent",
			current: "currentColor",
			alert: "#db495a",
			ok: "#9cf251",
			dark: "#1e2537",
			krtPrimary: "#e77e23",
			krtLight: "#e6b184",
			krtDark: "#c45c00",
			krtWhite: "#ffffff",
			krtBlack: "#000000",
			krtGrey1: "#d2d2d2",
			krtGrey2: "#646464",
			krtGrey3: "#282828",
			krtGrey4: "#141414",
			krtRaum: "#37BBC0",
			krtSub: "A3000A",
			krtMarine: "#7a5e96",
			krtForschung: "#355ddc",
			krtProfit: "#239e33",
			krtSar: "#ffd23f",
		},
	},
	plugins: [],
};
