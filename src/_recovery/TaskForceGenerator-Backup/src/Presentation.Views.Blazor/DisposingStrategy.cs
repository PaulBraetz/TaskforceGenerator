﻿using TaskforceGenerator.Presentation.Views.Blazor.Abstractions;

namespace TaskforceGenerator.Presentation.Views.Blazor
{
    /// <summary>
    /// Dispose strategy that disposes instances every time <see cref="Dispose(T)"/> is called.
    /// </summary>
    /// <typeparam name="T">The type of objects to dispose of.</typeparam>
    public sealed class DisposingStrategy<T> : IDisposeStrategy<T>
    {
        /// <summary>
        /// Initializes a new instance. If possible, use <see cref="Instance"/> instead.
        /// </summary>
        public DisposingStrategy() { }
        /// <summary>
        /// Gets the singleton instance.
        /// </summary>
        public static DisposingStrategy<T> Instance { get; } = new();
        /// <inheritdoc/>
        public void Dispose(T obj)
        {
            if (obj is IDisposable disposable)
            {
                disposable.Dispose();
            }
        }
    }
}
