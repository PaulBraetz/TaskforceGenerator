﻿namespace TaskforceGenerator
{
    /// <summary>
    /// Default implementation of <see cref="ISlot"/>.
    /// </summary>
    /// <param name="Name">The name of the slot.</param>
    public record Slot(String Name) : ISlot
    {
        /// <summary>
        /// Gets the passenger slot.
        /// </summary>
        public static ISlot Passenger { get; } = new Slot("Passenger");
        /// <summary>
        /// Gets the pilot slot.
        /// </summary>
        public static ISlot Pilot { get; } = new Slot("Pilot");
        /// <summary>
        /// Gets the gunner slot.
        /// </summary>
        public static ISlot Gunner { get; } = new Slot("Gunner");
        /// <inheritdoc/>
        public IOccupiedSlot Occupy(IOccupant occupant)
        {
            var result = new OccupiedSlot(Name, occupant);

            return result;
        }
    }
}
