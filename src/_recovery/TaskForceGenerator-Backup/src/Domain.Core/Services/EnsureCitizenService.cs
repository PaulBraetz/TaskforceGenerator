﻿using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Core.Abstractions;
using TaskforceGenerator.Domain.Core.Commands;
using TaskforceGenerator.Domain.Core.Exceptions;
using TaskforceGenerator.Domain.Core.Queries;

namespace TaskforceGenerator.Domain.Core.Services
{
    /// <summary>
    /// Service for ensuring a citizen exists withing the application.
    /// </summary>
    public sealed class EnsureCitizenService : IAsyncCqrsCommandService<EnsureCitizen>
    {
        private readonly IAsyncCqrsQueryService<CheckCitizenRegistered, CheckCitizenRegistered.Result> _registeredService;
        private readonly IAsyncCqrsQueryService<CheckCitizenExists, CheckCitizenExists.Result> _existsService;
        private readonly IAsyncCqrsQueryService<CreateCitizen, ICitizen> _createService;
        private readonly IAsyncCqrsCommandService<CommitCitizen> _commitService;

        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="registeredService">The service to use when checking whether the citizen is already registered.</param>
        /// <param name="existsService">The service to use when checking whether the citizen actually exists (registered to RSI).</param>
        /// <param name="commitService">The service to use when committing a newly created citizn to the infrastructure.</param>
        /// <param name="createService">The service to use when creating a new citizen.</param>
        public EnsureCitizenService(IAsyncCqrsQueryService<CheckCitizenRegistered, CheckCitizenRegistered.Result> registeredService,
                                              IAsyncCqrsQueryService<CheckCitizenExists, CheckCitizenExists.Result> existsService,
                                              IAsyncCqrsCommandService<CommitCitizen> commitService,
                                              IAsyncCqrsQueryService<CreateCitizen, ICitizen> createService)
        {
            _registeredService = registeredService;
            _existsService = existsService;
            _commitService = commitService;
            _createService = createService;
        }
        /// <inheritdoc/>
        public async ValueTask Execute(EnsureCitizen command)
        {
            var (name, token) = command;
            var registered = (await new CheckCitizenRegistered(name, token).Using(_registeredService)).Registered;
            if(registered)
            {
                return;
            }

            var exists = (await new CheckCitizenExists(name, token).Using(_existsService)).Exists;
            if(!exists)
            {
                throw new CitizenNotExistingException(name);
            }

            var citizen = await new CreateCitizen(name, token).Using(_createService);
            await new CommitCitizen(citizen, token).Using(_commitService);
        }
    }
}
