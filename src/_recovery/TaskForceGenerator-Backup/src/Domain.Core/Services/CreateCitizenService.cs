﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Core.Abstractions;
using TaskforceGenerator.Domain.Core.Queries;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskforceGenerator.Common;

namespace TaskforceGenerator.Domain.Core.Services
{
    /// <summary>
    /// Service for creating a citizen.
    /// </summary>
    public sealed class CreateCitizenService : IAsyncCqrsQueryService<CreateCitizen, ICitizen>
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="orgsService">The service to use when loading a citizens public orgs.</param>
        public CreateCitizenService(IAsyncCqrsQueryService<LoadCitizenOrgs, LoadCitizenOrgs.Result> orgsService)
        {
            _orgsService = orgsService;
        }
        private readonly IAsyncCqrsQueryService<LoadCitizenOrgs, LoadCitizenOrgs.Result> _orgsService;
        /// <inheritdoc/>
        public async ValueTask<ICitizen> Execute(CreateCitizen query)
        {
            query.CancellationToken.ThrowIfCancellationRequested();

            var orgs = (await new LoadCitizenOrgs(query.CitizenName, query.CancellationToken).Using(_orgsService)).PublicOrgNames;
            ICitizen result = new Citizen(query.CitizenName, orgs);

            return result;
        }
    }
}
