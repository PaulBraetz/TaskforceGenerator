﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Core.Queries;

namespace TaskforceGenerator.Domain.Core.Services
{
    /// <summary>
    /// Service for creating occupants.
    /// </summary>
    public sealed class CreateOccupantService : IAsyncCqrsQueryService<CreateOccupant, IOccupant>
    {
        /// <inheritdoc/>
        public ValueTask<IOccupant> Execute(CreateOccupant query) =>
            ValueTask.FromResult<IOccupant>(new Occupant(query.CitizenName, query.SlotPreference));
    }
}
