﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Core.Abstractions;

namespace TaskforceGenerator.Domain.Core.Commands
{
    /// <summary>
    /// Command for committing a citizen to the infrastructure.
    /// </summary>
    /// <param name="Citizen">The citizen to commit.</param>
    /// <param name="CancellationToken">The token used to signal the command execution to be cancelled.</param>
    public readonly record struct CommitCitizen(ICitizen Citizen, CancellationToken CancellationToken) : ICqrsCommand;
}
