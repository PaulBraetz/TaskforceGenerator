﻿using TaskforceGenerator.Common.Abstractions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Domain.Core.Commands
{
    /// <summary>
    /// Command for ensuring a citizen exists withing the application.
    /// </summary>
    /// <param name="CitizenName">The name of the citizen whose existence in the application to ensure.</param>
    /// <param name="CancellationToken">The token used to signal the command execution to be cancelled.</param>
    public readonly record struct EnsureCitizen(String CitizenName, CancellationToken CancellationToken) : ICqrsCommand;
}
