﻿namespace TaskforceGenerator.Domain.Core.Exceptions
{
    /// <summary>
    /// Exception thrown when a citizen that is not registered to the system is attempted to be accessed.
    /// </summary>
    public sealed class CitizenNotRegisteredException : Exception
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="name">The name of the citizen attempted to be accessed.</param>
        public CitizenNotRegisteredException(String name)
        {
            Name = name;
        }
        /// <summary>
        /// Gets the name of the citizen attempted to be accessed.
        /// </summary>
        public String Name { get; } = String.Empty;
    }
}
