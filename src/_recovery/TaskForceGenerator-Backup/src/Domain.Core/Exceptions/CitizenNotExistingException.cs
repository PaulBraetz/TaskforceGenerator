﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Domain.Core.Exceptions
{
    /// <summary>
    /// Exception thrown when a citizen that does not exist (is not registered to RSI) is attempted to be accessed.
    /// </summary>
    public sealed class CitizenNotExistingException : Exception
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="name">The name of the citizen attempted to be accessed.</param>
        public CitizenNotExistingException(String name) : base($"The citizen named '{name}' does not appear to be registered to RSI.")
        {
            Name = name;
        }
        /// <summary>
        /// Gets the name of the citizen attempted to be accessed.
        /// </summary>
        public String Name { get; } = String.Empty;
    }
}
