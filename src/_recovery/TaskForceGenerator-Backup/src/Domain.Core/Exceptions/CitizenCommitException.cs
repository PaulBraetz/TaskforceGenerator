﻿namespace TaskforceGenerator.Domain.Core.Exceptions
{
    /// <summary>
    /// Exception thrown when the system is unable to commit a citizen to the infrastructure.
    /// </summary>
    public sealed class CitizenCommitException : Exception
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="name">The name of the citizen attempted to be committed.</param>
        /// <param name="innerException">The exception prohibiting committing the citizen.</param>
        public CitizenCommitException(String name, Exception? innerException=null):base($"Unable to commit citizen '{name}'", innerException)
        {
            Name = name;
        }
        /// <summary>
        /// Gets the name of the citizen attempted to be committed.
        /// </summary>
        public String Name { get; } = String.Empty;
    }
}
