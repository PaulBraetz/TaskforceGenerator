﻿using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Domain.Core
{
    /// <summary>
    /// Default implementation of <see cref="ICrewingSchemeMap"/>.
    /// </summary>
    public sealed class CrewingSchemeMap : ICrewingSchemeMap
    {
        private CrewingSchemeMap(IDictionary<String, ICrewingScheme> map)
        {
            _map = map;
        }
        /// <summary>
        /// Creates a new instance based on the schemes made available by the scheme provider.
        /// </summary>
        /// <param name="schemeProvider">The scheme provider defining the available schemes.</param>
        /// <returns>A new map of the schemes provided by the provider, mapped onto their names.</returns>
        public static CrewingSchemeMap Create(IValueSetProvider<ICrewingScheme> schemeProvider)
        {
            var map = schemeProvider.GetValues().ToDictionary(s => s.Name, s => s);

            var result = new CrewingSchemeMap(map);

            return result;
        }

        private readonly IDictionary<String, ICrewingScheme> _map;

        /// <inheritdoc/>
        public Boolean TryGetScheme(String name, out ICrewingScheme? scheme) => _map.TryGetValue(name, out scheme);
    }
}
