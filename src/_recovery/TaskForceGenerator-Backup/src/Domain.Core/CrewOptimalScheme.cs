﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TaskforceGenerator
{
	/// <summary>
	/// Implementation of <see cref="ICrewingScheme"/> that attempts to optimally accomodate the citizens preferences.
	/// </summary>
	public sealed class CrewOptimalScheme : ICrewingScheme
	{
		/// <summary>
		/// Initializes a new instance. If possible, use <see cref="Instance"/> instead.
		/// </summary>
		public CrewOptimalScheme() { }
		/// <summary>
		/// Gets the singleton instance.
		/// </summary>
		public static CrewOptimalScheme Instance { get; } = new();
		/// <inheritdoc/>
		public IEnumerable<ICrewedVehicle> CrewVehicles(IReadOnlyList<IVehicle> vehicles, IReadOnlyList<IOccupant> occupants)
		{
			_ = vehicles ?? throw new ArgumentNullException(nameof(vehicles));
			_ = occupants ?? throw new ArgumentNullException(nameof(occupants));

			var unassignedOccupants = Enumerable.Range(0, occupants.Count).ToHashSet();
			var crews = vehicles.Select(v => new IOccupiedSlot[v.AvailableSlots.Count]).ToArray();

			// add occupants to preferred slots
			OccupySlots(crews, unassignedOccupants, vehicles, occupants, checkForPreferredSlot: true);

			// add leftover occupants to any available slots
			OccupySlots(crews, unassignedOccupants, vehicles, occupants, checkForPreferredSlot: false);

			var result = vehicles.Select((v, i) => v.Crew(crews[i].Where(s => s != null)));

			return result;
		}

		private static void OccupySlots(IOccupiedSlot[][] crews,
			ISet<Int32> unassignedOccupants,
			IReadOnlyList<IVehicle> vehicles,
			IReadOnlyList<IOccupant> occupants,
			Boolean checkForPreferredSlot)
		{
			for (var occupantIndex = 0; occupantIndex < occupants.Count; occupantIndex++)
			{
				var occupant = occupants[occupantIndex];
				for (var vehicleIndex = 0; vehicleIndex < vehicles.Count && unassignedOccupants.Contains(occupantIndex); vehicleIndex++)
				{
					var (availableSlot, slotIndex) = vehicles[vehicleIndex].AvailableSlots
						.Select((s, i) => (s, i))
						.Where(t => canOccupySlot(t.s, t.i))
						.FirstOrDefault();

					if (availableSlot != null)
					{
						crews[vehicleIndex][slotIndex] = availableSlot.Occupy(occupant);
						_ = unassignedOccupants.Remove(occupantIndex);
					}

					Boolean canOccupySlot(ISlot availableSlot, Int32 slotIndex) =>
						crews[vehicleIndex][slotIndex] == null &&
						(!checkForPreferredSlot || availableSlot == occupant.Preference.PreferredSlot);
				}
			}
		}
		/// <inheritdoc/>
		public String Name { get; } = "Optimal";
	}
}
