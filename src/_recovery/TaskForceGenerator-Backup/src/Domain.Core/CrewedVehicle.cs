﻿using System.Collections.Generic;

namespace TaskforceGenerator
{
    sealed record CrewedVehicle(String Name, IReadOnlyList<ISlot> AvailableSlots, IReadOnlyList<IOccupiedSlot> OccupiedSlots) : Vehicle(Name, AvailableSlots), ICrewedVehicle;
}
