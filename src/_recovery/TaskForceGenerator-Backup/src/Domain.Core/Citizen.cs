﻿using TaskforceGenerator.Domain.Core.Abstractions;

namespace TaskforceGenerator.Domain.Core
{
    /// <summary>
    /// Core model implementation of a citizen entity.
    /// </summary>
    /// <param name="Name">The citizens name.</param>
    /// <param name="PublicOrgNames"></param>
    public sealed record Citizen(String Name, IReadOnlySet<String> PublicOrgNames) : ICitizen;
}
