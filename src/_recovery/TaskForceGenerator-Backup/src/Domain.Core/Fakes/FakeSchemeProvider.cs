﻿using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Domain.Core.Fakes
{
    /// <summary>
    /// Provides the fake crewing scheme.
    /// </summary>
    public sealed class FakeSchemeProvider : IValueSetProvider<ICrewingScheme>
    {
        private static readonly HashSet<ICrewingScheme> _schemes = new()
        {
            CrewingSchemeFake.Instance,
            CrewOptimalScheme.Instance
        };
        /// <inheritdoc/>
        public IReadOnlySet<ICrewingScheme> GetValues() => _schemes;
    }
}
