﻿using System.Collections.Generic;
using System.Linq;

namespace TaskforceGenerator
{
    /// <summary>
    /// Default implementation of <see cref="ITaskforceBuilder"/>.
    /// </summary>
    public sealed class TaskforceBuilder : ITaskforceBuilder
    {
        private readonly List<IVehicle> _vehicles = new();
        private readonly HashSet<IOccupant> _citizens = new();
        private ICrewingScheme? _scheme;
        /// <inheritdoc/>
        public ITaskforceBuilder AddVehicle(IVehicle vehicle)
        {
            _vehicles.Add(vehicle);
            return this;
        }
        /// <inheritdoc/>
        public ITaskforceBuilder SetCrewingScheme(ICrewingScheme scheme)
        {
            _scheme = scheme;
            return this;
        }
        /// <inheritdoc/>
        public ITaskforceBuilder AddOccupant(IOccupant citizen)
        {
            _ = _citizens.Add(citizen);
            return this;
        }
        /// <inheritdoc/>
        public ITaskforce Build()
        {
            if(_scheme == null)
                throw new InvalidOperationException("Scheme was not set.");

            var vehicles = _vehicles.ToArray();
            var citizens = _citizens.ToArray();
            var crewedVehicles = _scheme.CrewVehicles(vehicles, citizens).ToArray();
            var result = new TaskForce(crewedVehicles);

            return result;
        }
    }
}
