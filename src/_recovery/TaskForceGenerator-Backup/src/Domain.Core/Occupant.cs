﻿namespace TaskforceGenerator
{
    /// <summary>
    /// Default implementation of <see cref="IOccupant"/>.
    /// </summary>
    /// <param name="CitizenName">The name of the occupant.</param>
    /// <param name="Preference">The occupants preference for slot assignment.</param>
    public readonly record struct Occupant(String CitizenName, ISlotPreference Preference) : IOccupant;
}
