﻿using System.Collections.Generic;

namespace TaskforceGenerator
{
    sealed record TaskForce(IReadOnlyList<ICrewedVehicle> Vehicles) : ITaskforce;
}
