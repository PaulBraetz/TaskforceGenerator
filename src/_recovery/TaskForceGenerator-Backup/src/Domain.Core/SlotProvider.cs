﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Core.Abstractions;

namespace TaskforceGenerator.Domain.Core
{
    /// <summary>
    /// Implementation of <see cref="IValueSetProvider{T}"/> based on the instances defined by <see cref="Slot"/>.
    /// </summary>
    public sealed class SlotProvider : IValueSetProvider<ISlot>
    {
        /// <summary>
        /// Initializes a new instance. If possible, use <see cref="Instance"/> instead.
        /// </summary>
        public SlotProvider() { }
        /// <summary>
        /// Gets the singleton instance.
        /// </summary>
        public static SlotProvider Instance { get; } = new();

        private readonly HashSet<ISlot> _slots = new()
        {
            Slot.Passenger,
            Slot.Pilot,
            Slot.Gunner
        };

        /// <inheritdoc/>
        public IReadOnlySet<ISlot> GetValues() => _slots;
    }
}
