﻿using System.Collections.Generic;

namespace TaskforceGenerator
{
    /// <summary>
    /// Represents a scheme for crewing vehicles with occupants.
    /// </summary>
    public interface ICrewingScheme
    {
        /// <summary>
        /// Gets the name of this crewing scheme.
        /// </summary>
        String Name { get; }
        /// <summary>
        /// Crews the vehicles provided according to a scheme.
        /// </summary>
        /// <param name="vehicles">The vehicles to crew.</param>
        /// <param name="occupants">The occupants to crew vehicles with, according to their preferences.</param>
        /// <returns>An enumeration of newly crewed vehicles.</returns>
        IEnumerable<ICrewedVehicle> CrewVehicles(IReadOnlyList<IVehicle> vehicles, IReadOnlyList<IOccupant> occupants);
    }
}
