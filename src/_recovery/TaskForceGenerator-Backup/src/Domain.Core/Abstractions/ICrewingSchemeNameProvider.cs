﻿namespace TaskforceGenerator
{
    /// <summary>
    /// Provides the names of available crewing schemes.
    /// </summary>
    public interface ICrewingSchemeNameProvider
    {
        /// <summary>
        /// Gets the names of available crewing schemes.
        /// </summary>
        /// <returns></returns>
        IReadOnlySet<String> GetNames();
    }
}
