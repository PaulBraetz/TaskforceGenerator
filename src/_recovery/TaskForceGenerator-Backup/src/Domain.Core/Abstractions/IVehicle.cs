﻿using System;
using System.Collections.Generic;

namespace TaskforceGenerator
{
    /// <summary>
    /// Represents an uncrewed vehicle.
    /// Implements the prototype pattern for crewing vehicles.
    /// </summary>
    public interface IVehicle
    {
        /// <summary>
        /// Gets the name of the vehicle (e.g.: "Redeemer").
        /// </summary>
        String Name { get; }
        /// <summary>
        /// Gets the available slots of the vehicle, including occupied ones.
        /// </summary>
        IReadOnlyList<ISlot> AvailableSlots { get; }
        /// <summary>
        /// Based on this prototype, creates a crewed vehicles with occupied slots.
        /// </summary>
        /// <param name="slots">The slots occupied in the created vehicle.</param>
        /// <returns>A new, crewed vehicle.</returns>
        ICrewedVehicle Crew(IEnumerable<IOccupiedSlot> slots);
    }
}
