﻿namespace TaskforceGenerator
{
    /// <summary>
    /// Represents an occupied slot.
    /// </summary>
    public interface IOccupiedSlot : ISlot
    {
        /// <summary>
        /// Gets the occupant occupying the slot.
        /// </summary>
        IOccupant Occupant { get; }
    }
}
