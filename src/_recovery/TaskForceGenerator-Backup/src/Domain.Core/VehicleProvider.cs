﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Core.Abstractions;

namespace TaskforceGenerator.Domain.Core
{
    /// <summary>
    /// Implementation of <see cref="IValueSetProvider{T}"/> based on the instances defined by <see cref="Vehicle"/>.
    /// </summary>
    public sealed class VehicleProvider : IValueSetProvider<IVehicle>
    {
        /// <summary>
        /// Initializes a new instance. If possible, use <see cref="Instance"/> instead.
        /// </summary>
        public VehicleProvider() { }
        /// <summary>
        /// Gets the singleton instance.
        /// </summary>
        public static VehicleProvider Instance { get; } = new();

        private readonly HashSet<IVehicle> _vehicles = new()
        {
            Vehicle.Redeemer,
            Vehicle.Pisces
        };

        /// <inheritdoc/>
        public IReadOnlySet<IVehicle> GetValues() => _vehicles;
    }
}
