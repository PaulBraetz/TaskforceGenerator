﻿using TaskforceGenerator.Common.Abstractions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Domain.Core.Queries
{
    /// <summary>
    /// Query for creating a citizen (factory command)
    /// </summary>
    /// <param name="CitizenName">The name of tthe citizen to create.</param>
    /// <param name="CancellationToken">The token used to signal the query execution to be cancelled.</param>
    public readonly record struct CreateCitizen(String CitizenName, CancellationToken CancellationToken) : ICqrsQuery;
}
