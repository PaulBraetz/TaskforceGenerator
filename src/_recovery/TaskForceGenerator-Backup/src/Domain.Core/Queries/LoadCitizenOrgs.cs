﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Domain.Core.Queries
{
    /// <summary>
    /// Query for loading a citizens public orgs.
    /// </summary>
    /// <param name="CitizenName">The name of the citizen whose public org names to load.</param>
    /// <param name="CancellationToken">The token used to signal the query execution to be cancelled.</param>
    public readonly record struct LoadCitizenOrgs(String CitizenName, CancellationToken CancellationToken) : ICqrsQuery
    {
        /// <summary>
        /// The result of the query.
        /// </summary>
        /// <param name="PublicOrgNames">The set of orgs this citizen is a member of publicly.</param>
        public readonly record struct Result(IReadOnlySet<String> PublicOrgNames);
    }
}
