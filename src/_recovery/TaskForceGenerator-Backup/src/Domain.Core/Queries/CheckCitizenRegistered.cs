﻿using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Domain.Core.Queries
{
    /// <summary>
    /// Query for checking whether a cititzen is registered to the system.
    /// </summary>
    /// <param name="CitizenName">The name of the citizen whose registration to check.</param>
    /// <param name="CancellationToken">The token used to signal the query execution to be cancelled.</param>
    public readonly record struct CheckCitizenRegistered(String CitizenName, CancellationToken CancellationToken) : ICqrsQuery
    {
        /// <summary>
        /// The result of the query.
        /// </summary>
        /// <param name="Registered">Indicates whether the citizen is registerd to the application. Contains <see langword="true"/> if a citizen with the name provided has been registered; otherwise, <see langword="false"/>.</param>
        public readonly record struct Result(Boolean Registered);
    }
}
