﻿using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Domain.Core.Queries
{
    /// <summary>
    /// Query for checking whether a citizen actually exists, that is, he is an RSI-registered citizen.
    /// </summary>
    /// <param name="CitizenName">The name of the citizen whose existence to check.</param>
    /// <param name="CancellationToken">The token used to signal the query execution to be cancelled.</param>
    public readonly record struct CheckCitizenExists(String CitizenName, CancellationToken CancellationToken) : ICqrsQuery
    {
        /// <summary>
        /// The result of the query.
        /// </summary>
        /// <param name="Exists">Indicates whether the citizen is registerd to RSI. Contains <see langword="true"/> if a citizen with the name provided exists; otherwise, <see langword="false"/>.</param>
        public readonly record struct Result(Boolean Exists);
    }
}
