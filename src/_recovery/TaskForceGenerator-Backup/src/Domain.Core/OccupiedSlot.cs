﻿namespace TaskforceGenerator
{
    sealed record OccupiedSlot(String Name, IOccupant Occupant) : Slot(Name), IOccupiedSlot;
}
