﻿using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Core.Abstractions;
using TaskforceGenerator.Presentation.Models.Abstractions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ObjectiveC;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Presentation.Models
{
    /// <summary>
    /// Model for displaying information on the currently authenticated citizen.
    /// </summary>
    public sealed class UserContextCitizenModel: ObservableModelBase,IUserContextCitizenModel
    {
        private readonly IObservableUserContext _context;
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="context">The context monitored by this model.</param>
        public UserContextCitizenModel(IObservableUserContext context)
        {
            _context = context;
            _context.CitizenChanged += (s, e) => InvokePropertyChanged(nameof(CitizenName));
        }
        /// <inheritdoc/>
        public String? CitizenName =>_context.Citizen?.CitizenName;
    }
}
