﻿using TaskforceGenerator.Presentation.Models.Abstractions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Presentation.Models
{
    /// <summary>
    /// Navigates to a static path upon calling <see cref="Navigate"/>.
    /// </summary>
    public sealed class NavigationModel : INavigationModel
    {
        private readonly INavigationManager _navigationManager;
        private readonly String _path;
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="navigationManager">The navigation manager to use when invoking <see cref="Navigate"/>.</param>
        /// <param name="path">The path to navigate to upon invoking <see cref="Navigate"/>.</param>
        public NavigationModel(INavigationManager navigationManager, String path)
        {
            _navigationManager = navigationManager;
            _path = path;
        }

        /// <inheritdoc/>
        public void Navigate() => _navigationManager.NavigateTo(_path);
    }
}
