﻿using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Presentation.Models.Formatters
{
    /// <summary>
    /// Formatter for instances of type <see cref="IVehicle"/> that yields the vehicle name upon formatting.
    /// </summary>
    /// <typeparam name="TVehicle">The type of vehicle to format.</typeparam>
    public sealed class VehicleNameFormatter<TVehicle> : IStaticFormatter<TVehicle>
        where TVehicle : IVehicle
    {
        /// <inheritdoc/>
        public String Format(TVehicle value) => value.Name;
    }
}
