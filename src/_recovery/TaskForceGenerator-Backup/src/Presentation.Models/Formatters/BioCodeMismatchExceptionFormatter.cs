﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TaskforceGenerator.Application.Exceptions;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Queries;

namespace TaskforceGenerator.Presentation.Models.Formatters
{
    /// <summary>
    /// Ui-friendly formatter for instances of <see cref="BioCodeMismatchException"/>.
    /// </summary>
    public sealed class BioCodeMismatchExceptionFormatter : IStaticFormatter<BioCodeMismatchException>
	{
		/// <inheritdoc/>
		public String Format(BioCodeMismatchException value)
		{
			var result = value.MismatchType == VerifyBioCode.Result.CitizenMismatch ?
					"The code generated does not match the code associated with the citizen anymore. Please generate a new code." :
					"The code generated does not match the code posted in the citizens bio. Post the code in the citizens bio in order to get verified.";

			return result;
		}
	}
}
