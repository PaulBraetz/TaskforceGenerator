﻿using TaskforceGenerator.Application.Commands;
using TaskforceGenerator.Common.Abstractions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Presentation.Models.Formatters
{
    /// <summary>
    /// Toast-specific formatter for password set successes.
    /// </summary>
    public sealed class SetPasswordForConnectionFormatter : IStaticFormatter<SetPasswordForConnection>
    {
        /// <inheritdoc/>
        public String Format(SetPasswordForConnection value) => $"Password for '{value.CitizenName}' set successfully.";
    }
}
