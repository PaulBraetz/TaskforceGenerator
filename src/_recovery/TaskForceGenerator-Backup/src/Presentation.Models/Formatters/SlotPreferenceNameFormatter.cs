﻿using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Presentation.Models.Formatters
{
    /// <summary>
    /// Formatter for instances of type <see cref="ISlotPreference"/> that yields the preferred slots name upon formatting.
    /// </summary>
    /// <typeparam name="TSlotPreference">The type of slot preference to format.</typeparam>
    public sealed class SlotPreferenceNameFormatter<TSlotPreference> : IStaticFormatter<TSlotPreference>
        where TSlotPreference : ISlotPreference
    {
        /// <inheritdoc/>
        public String Format(TSlotPreference value) => value.PreferredSlot.Name;
    }
}
