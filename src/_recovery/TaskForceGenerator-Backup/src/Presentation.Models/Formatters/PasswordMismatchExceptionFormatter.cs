﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Exceptions;

namespace TaskforceGenerator.Presentation.Models.Formatters
{
    /// <summary>
    /// Ui-friendly formatter for instances of <see cref="PasswordMismatchException"/>.
    /// </summary>
    public sealed class PasswordMismatchExceptionFormatter : IStaticFormatter<PasswordMismatchException>
	{
		/// <inheritdoc/>
		public String Format(PasswordMismatchException value) => $"The password provided for '{value.CitizenName}' is incorrect.";
    }
}
