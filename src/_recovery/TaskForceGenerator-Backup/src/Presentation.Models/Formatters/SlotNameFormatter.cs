﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Presentation.Models.Formatters
{
    /// <summary>
    /// Formatter for instances of type <see cref="ISlot"/> that yields the slots name upon formatting.
    /// </summary>
    /// <typeparam name="TSlot">The type of slot to format.</typeparam>
    public sealed class SlotNameFormatter<TSlot> : IStaticFormatter<TSlot>
        where TSlot : ISlot
    {
        /// <inheritdoc/>
        public String Format(TSlot value) => value.Name;
    }
}
