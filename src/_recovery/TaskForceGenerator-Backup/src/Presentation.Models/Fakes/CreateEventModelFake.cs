﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TaskforceGenerator.Presentation.Models.Abstractions;
using TaskforceGenerator.Presentation.Models.Abstractions.CreateEvent;

namespace TaskforceGenerator.Presentation.Models.Fakes
{
    /// <summary>
    /// Empty implementation of <see cref="ICreateEventModel"/>.
    /// </summary>
    public sealed class CreateEventModelFake : ObservableModelBase, ICreateEventModel
    {
        private String? _inviteLink;

        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="create">The button model responsible for creating the event.</param>
        /// <param name="inputs">The input group responsible for configuring the event model.</param>
        public CreateEventModelFake(IButtonModel create, ICreateEventInputGroupModel inputs)
        {
            Create = create;
            Inputs = inputs;

            Create.Label = "Create";
            Create.Clicked += async () =>
            {
                using var _ = Create.Disable();
                await Task.Delay(2500);
                InviteLink = "blabla";
            };

            Inputs.EventName.Label = "Name";
            Inputs.EventName.Description = "Give your event a name.";

            Inputs.EventDescription.Label = "Description";
            Inputs.EventDescription.Description = "Give your event a description.";

            Inputs.EventDate.Label = "Date";
            Inputs.EventDate.Description = "Schedule your event.";

            Inputs.PlannedVehicles.Label = "Planned vehicles";
            Inputs.PlannedVehicles.Description = "Select the vehicles you are planning on using.";
        }
        /// <inheritdoc/>
        public IButtonModel Create { get; }
        /// <inheritdoc/>
        public ICreateEventInputGroupModel Inputs { get; }
        /// <inheritdoc/>
        public String? InviteLink
        {
            get => _inviteLink;
            private set => ExchangeBackingField(ref _inviteLink, value);
        }
    }
}
