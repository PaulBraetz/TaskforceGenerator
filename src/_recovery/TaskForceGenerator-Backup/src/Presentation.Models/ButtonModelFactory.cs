﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Presentation.Models.Abstractions;

namespace TaskforceGenerator.Presentation.Models
{
    /// <summary>
    /// Implementation of <see cref="IFactory{T}"/> that produces instances of <see cref="ButtonModel"/>.
    /// </summary>
    public sealed class ButtonModelFactory : IFactory<IButtonModel>
    {
        /// <inheritdoc/>
        public IButtonModel Create() => new ButtonModel();
    }
}
