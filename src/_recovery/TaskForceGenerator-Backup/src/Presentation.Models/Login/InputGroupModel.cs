﻿using TaskforceGenerator.Presentation.Models.Abstractions;
using TaskforceGenerator.Presentation.Models.Abstractions.Login;

namespace TaskforceGenerator.Presentation.Models.Login
{
    /// <summary>
    /// Default implemenation of <see cref="IInputGroupModel"/>.
    /// </summary>
    public sealed class InputGroupModel : IInputGroupModel
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="name">The model responsible for acquiring the name of the citizen for which to authenticate the user.</param>
        /// <param name="password">The model responsible for acquiring the password uing which the user is attempting to authenticate.</param>
        public InputGroupModel(IInputGroupModel<String, String> name, IInputGroupModel<String, String> password)
        {
            Name = name;
            Password = password;
        }

        /// <inheritdoc/>
        public IInputGroupModel<String, String> Name { get; }
        /// <inheritdoc/>
        public IInputGroupModel<String, String> Password { get; }
    }
}
