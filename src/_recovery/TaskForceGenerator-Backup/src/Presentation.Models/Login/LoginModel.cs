﻿using TaskforceGenerator.Application.Commands;
using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Presentation.Models.Abstractions;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskforceGenerator.Presentation.Models.Abstractions.Login;
using TaskforceGenerator.Domain.Authentication.Exceptions;

namespace TaskforceGenerator.Presentation.Models.Login
{
    /// <summary>
    /// Default implementation of <see cref="ILoginModel"/>.
    /// </summary>
    public sealed class LoginModel : ObservableModelBase, ILoginModel
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="loginService">The service to use when invoking <see cref="Login"/>.</param>
        /// <param name="navigation">The navigation to invoke after successfully logging in.</param>
        /// <param name="login">the button for submitting the login request.</param>
        /// <param name="handler">The handler strategy to use when handling exceptions thrown.</param>
        /// <param name="inputs">The model responsible for obtaining the required inputs.</param>
        public LoginModel(
            IAsyncCqrsCommandService<Application.Commands.Login> loginService,
            INavigationModel navigation,
            IButtonModel login,
            IExceptionHandler handler,
            IInputGroupModel inputs)
        {
            _loginService = loginService;
            _navigation = navigation;
            _handler = handler;

            Login = login;
            Inputs = inputs;
        }
        /// <summary>
        /// Creates, initializes and wires up a new instance.
        /// </summary>
        /// <param name="loginService">The service to use when invoking <see cref="Login"/>.</param>
        /// <param name="navigation">The navigation to invoke after successfully logging in.</param>
        /// <param name="login">the button for submitting the login request.</param>
        /// <param name="handler">The handler strategy to use when handling exceptions thrown.</param>
        /// <param name="inputs">The model responsible for obtaining the required inputs.</param>
        /// <returns>A new, initialized and wired up instance of <see cref="LoginModel"/>.</returns>
        public static LoginModel Create(
            IAsyncCqrsCommandService<Application.Commands.Login> loginService,
            INavigationModel navigation,
            IButtonModel login,
            IExceptionHandler handler,
            IInputGroupModel inputs)
        {
            var result = new LoginModel(loginService, navigation, login, handler, inputs);

            result.Inputs.Name.PropertyChanged += result.OnNamePropertyChanged;
            result.Inputs.Name.Label = "Name";
            result.Inputs.Name.Description = "Enter the name of the citizen you would like to connect as.";
            result.Inputs.Name.Input.Entered += result.OnEntered;

            result.Inputs.Password.PropertyChanged += result.OnPasswordPropertyChanged;
            result.Inputs.Password.Label = "Password";
            result.Inputs.Password.Description = "Enter the password for the citizen you would like to connect as.";
            result.Inputs.Password.Input.Entered += result.OnEntered;

            result.Login.Clicked += result.OnLoginClick;
            result.Login.Label = "Login";

            return result;
        }

        private readonly IAsyncCqrsCommandService<Application.Commands.Login> _loginService;
        private readonly INavigationModel _navigation;
        private readonly IExceptionHandler _handler;

        /// <inheritdoc/>
        public IInputGroupModel Inputs { get; }
        /// <inheritdoc/>
        public IButtonModel Login { get; }

        private Task OnEntered()
        {
            var result = Login.Disabled ?
                Task.CompletedTask :
                Login.Click();

            return result;
        }
        /// <inheritdoc/>
        private async Task OnLoginClick()
        {
            if(Inputs.Name.Input.Validity == InputValidityType.Invalid ||
                Inputs.Password.Input.Validity == InputValidityType.Invalid)
            {
                return;
            }

            using(_ = Login.Disable())
            {
                try
                {
                    await new Application.Commands.Login(
                    Inputs.Name.Input.Value!,
                    Inputs.Password.Input.Value!,
                    CancellationToken.None).Using(_loginService);
                    _navigation.Navigate();
                } catch(Exception ex)
                when(_handler.CanHandle(ex))
                {
                    await _handler.Handle(ex);
                }
            }
        }
        private void OnNamePropertyChanged(Object? sender, PropertyChangedEventArgs e)
        {
            if(e.PropertyName == nameof(Inputs.Name.Input.Value) && !Inputs.Name.Input.IsInvalid())
            {
                UpdateButtonDisabled();
            }
        }
        private void OnPasswordPropertyChanged(Object? sender, PropertyChangedEventArgs e)
        {
            if(e.PropertyName == nameof(Inputs.Password.Input.Value) && !Inputs.Password.Input.IsInvalid())
            {
                UpdateButtonDisabled();
            }
        }

        private void UpdateButtonDisabled()
        {
            Login.Disabled = String.IsNullOrEmpty(Inputs.Name.Input.Value) ||
                 !String.IsNullOrEmpty(Inputs.Name.Input.Error) ||
                 String.IsNullOrEmpty(Inputs.Password.Input.Value) ||
                 !String.IsNullOrEmpty(Inputs.Password.Input.Error);
        }
    }
}
