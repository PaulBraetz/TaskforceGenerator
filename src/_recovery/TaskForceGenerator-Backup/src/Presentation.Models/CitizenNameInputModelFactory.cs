﻿using TaskforceGenerator.Application.Queries;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Core.Queries;
using TaskforceGenerator.Presentation.Models.Abstractions;

namespace TaskforceGenerator.Presentation.Models
{
    /// <summary>
    /// Factory for producing instances of <see cref="CitizenNameInputModel"/>.
    /// </summary>
    public sealed class CitizenNameInputModelFactory : IFactory<IInputModel<String, String>>
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="service">The service passed to the citizen name decorator model.</param>
        /// <param name="valueDefaultProvider">The default value provider to use for initializing <see cref="IInputModel{TValue, TError}.Value"/>.</param>
        /// <param name="errorDefaultProvider">The default value provider to use for initializing <see cref="IInputModel{TValue, TError}.Error"/>.</param>
        public CitizenNameInputModelFactory(
            IAsyncCqrsQueryService<ProxyQuery<CheckCitizenExists>, CheckCitizenExists.Result> service,
            IDefaultValueProvider<String> valueDefaultProvider,
            IDefaultValueProvider<String> errorDefaultProvider)
        {
            _service = service;
            _valueDefaultProvider = valueDefaultProvider;
            _errorDefaultProvider = errorDefaultProvider;
        }

        private readonly IAsyncCqrsQueryService<ProxyQuery<CheckCitizenExists>, CheckCitizenExists.Result> _service;
        private readonly IDefaultValueProvider<String> _valueDefaultProvider;
        private readonly IDefaultValueProvider<String> _errorDefaultProvider;

        /// <inheritdoc/>
        public IInputModel<String, String> Create()
        {
            var result = new CitizenNameInputModel(_service, _valueDefaultProvider, _errorDefaultProvider);

            return result;
        }
    }
}
