﻿using System.Collections.ObjectModel;
using System.Collections.Specialized;

using TaskforceGenerator.Presentation.Models.Abstractions;

namespace TaskforceGenerator.Presentation.Models
{
    /// <summary>
    /// Base implementation of <see cref="IMultiControlModel{TSubControlModel}"/>.
    /// </summary>
    /// <typeparam name="TSubControlModel">The type of subcontrol model encapsulated.</typeparam>
    public class MultiControlModel<TSubControlModel> : ObservableModelBase, IMultiControlModel<TSubControlModel>
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        public MultiControlModel()
        {
            var items = new ObservableCollection<TSubControlModel>();
            items.CollectionChanged += OnCollectionChanged;
            Items = items;
        }

        private String _label = String.Empty;
        private String _description = String.Empty;

        private void OnCollectionChanged(Object? sender, NotifyCollectionChangedEventArgs e) =>
            InvokePropertyChanged(nameof(Items));

        /// <inheritdoc/>
        public ICollection<TSubControlModel> Items { get; }
        /// <inheritdoc/>
        public String Label
        {
            get => _label;
            set => ExchangeBackingField(ref _label, value);
        }
        /// <inheritdoc/>
        public String Description
        {
            get => _description;
            set => ExchangeBackingField(ref _description, value);
        }
    }
}
