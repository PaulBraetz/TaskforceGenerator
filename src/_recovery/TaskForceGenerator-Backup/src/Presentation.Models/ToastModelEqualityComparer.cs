﻿using TaskforceGenerator.Presentation.Models.Abstractions;

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Presentation.Models
{
    /// <summary>
    /// Implementation of <see cref="IEqualityComparer{T}"/> for <see cref="IToastModel"/>, based on <see cref="IToastModel.CreatedAt"/>, <see cref="IToastModel.Header"/> and <see cref="IToastModel.Body"/>.
    /// </summary>
    public sealed class ToastModelEqualityComparer : IEqualityComparer<IToastModel>
    {
        /// <summary>
        /// Initializes a new instance. If possible, use <see cref="Instance"/> instead.
        /// </summary>
        public ToastModelEqualityComparer() { }
        /// <summary>
        /// Gets the singleton instance.
        /// </summary>
        public static ToastModelEqualityComparer Instance { get; } = new();
        /// <inheritdoc/>
        public Boolean Equals(IToastModel? x, IToastModel? y)
        {
            if(x == null)
            {
                return y == null;
            }

            if(y == null)
            {
                return x == null;
            }

            var result = x.CreatedAt == y.CreatedAt &&
                x.Header == y.Header &&
                x.Body == y.Body;

            return result;
        }
        /// <inheritdoc/>
        public Int32 GetHashCode([DisallowNull] IToastModel obj)
        {
            _ = obj ?? throw new ArgumentNullException(nameof(obj));

            var result = HashCode.Combine(obj.CreatedAt, obj.Header, obj.Body);

            return result;
        }
    }
}

