﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

using TaskforceGenerator.Common;
using TaskforceGenerator.Presentation.Models.Abstractions;
using TaskforceGenerator.Presentation.Models.Exceptions;

namespace TaskforceGenerator.Presentation.Models
{
    /// <summary>
    /// Default implementation of <see cref="IButtonModel"/>.
    /// </summary>
    public sealed class ButtonModel : ObservableModelBase, IButtonModel
    {
        private Int32 _loading;
        private Boolean _disabled;
        private Boolean _allowParallelClicks;
        private String? _label;

        /// <inheritdoc/>
        public event AsynchronousEventHandler? Clicked;

        /// <inheritdoc/>
        public String? Label
        {
            get => _label;
            set => ExchangeBackingField(ref _label, value);

        }
        /// <inheritdoc/>
        public Boolean Disabled
        {
            get => _disabled;
            set => ExchangeBackingField(ref _disabled, value);
        }
        /// <inheritdoc/>
        public Boolean Loading => BooleanState.ToBooleanState(_loading);
        /// <inheritdoc/>
        public Boolean AllowParallelClicks
        {
            get => _allowParallelClicks;
            set => ExchangeBackingField(ref _allowParallelClicks, value);
        }

        /// <inheritdoc/>
        public Task Click()
        {
            if(Disabled)
                throw StaticExceptions.InvalidOperation.ButtonDisabled;

            var result = !ExchangeLoading(true) || AllowParallelClicks ?
                GetContinuedClick() :
                Task.CompletedTask;

            return result;
        }
        private async Task GetContinuedClick()
        {
            try
            {
                await Clicked.InvokeAsync();
            } finally
            {
                _ = ExchangeLoading(false);
            }
        }

        /// <summary>
        /// Atomically exchanges the value of <see cref="Loading"/> with the parameter supplied.
        /// </summary>
        /// <param name="loading">The value to write to <see cref="Loading"/>.</param>
        /// <returns>The value previously stored in <see cref="Loading"/>.</returns>
        private Boolean ExchangeLoading(Boolean loading)
        {
            var newState = BooleanState.FromBooleanState(loading);
            var previousState = Interlocked.Exchange(ref _loading, newState);
            if(previousState != newState)
            {
                InvokePropertyChanged(nameof(Loading));
            }

            return BooleanState.ToBooleanState(previousState);
        }
    }
}
