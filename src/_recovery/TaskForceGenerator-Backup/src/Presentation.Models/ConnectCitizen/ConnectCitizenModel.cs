﻿using TaskforceGenerator.Application.Commands;
using TaskforceGenerator.Application.Exceptions;
using TaskforceGenerator.Application.Queries;
using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Authentication.Exceptions;
using TaskforceGenerator.Domain.Authentication.Queries;
using TaskforceGenerator.Domain.Core.Exceptions;
using TaskforceGenerator.Presentation.Models.Abstractions;

using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Reflection;
using TaskforceGenerator.Presentation.Models.Abstractions.ConnectCitizen;
using System.Data.SqlTypes;

namespace TaskforceGenerator.Presentation.Models.ConnectCitizen
{
    /// <summary>
    /// Model for connecting citizens.
    /// </summary>
    public sealed class ConnectCitizenModel : ObservableModelBase, IConnectCitizenModel
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="service">The service to use when connecting a citizen.</param>
        /// <param name="codeService">The service to use for requesting a new bio code.</param>
        /// <param name="successNavigation">The navigation to invoke after successfully connecting a citizen.</param>
        /// <param name="inputs">The required input component model group.</param>
        /// <param name="buttons">The required button model group.</param>
        /// <param name="code">The codedisplay component model.</param>
        /// <param name="handler">The handler strategy to use when handling exceptions thrown.</param>
        public ConnectCitizenModel(
            IAsyncCqrsCommandService<SetPasswordForConnection> service,
            IAsyncCqrsQueryService<GenerateBioCode, BioCode> codeService,
            INavigationModel successNavigation,
            IInputGroupModel inputs,
            IButtonGroupModel buttons,
            ICodeModel code,
            IExceptionHandler handler)
        {
            _connectService = service;
            _codeService = codeService;
            _successNavigation = successNavigation;
            _handler = handler;

            Inputs = inputs;
            Buttons = buttons;
            Code = code;
        }
        /// <summary>
        /// Creates, initializes and wires up a new instance.
        /// </summary>
        /// <param name="service">The service to use when connecting a citizen.</param>
        /// <param name="codeService">The service to use for requesting a new bio code.</param>
        /// <param name="successNavigation">The navigation to invoke after successfully connecting a citizen.</param>
        /// <param name="inputs">The required input component model group.</param>
        /// <param name="buttons">The required button model group.</param>
        /// <param name="code">The codedisplay component model.</param>
        /// <param name="handler">The handler strategy to use when handling exceptions thrown.</param>
        /// <returns>A new, initialized and wired up instance of <see cref="ConnectCitizenModel"/>.</returns>
        public static ConnectCitizenModel Create(
            IAsyncCqrsCommandService<SetPasswordForConnection> service,
            IAsyncCqrsQueryService<GenerateBioCode, BioCode> codeService,
            INavigationModel successNavigation,
            IInputGroupModel inputs,
            IButtonGroupModel buttons,
            ICodeModel code,
            IExceptionHandler handler)
        {
            var result = new ConnectCitizenModel(
                service,
                codeService,
                successNavigation,
                inputs,
                buttons,
                code,
                handler);

            result.Inputs.Password.Label = "Password";
            result.Inputs.Password.Description = "Set the password for your citizen connection. This may be changed at any time later.";
            result.Inputs.Password.PropertyChanged += result.OnInputPropertyChanged;
            result.Inputs.Password.Input.Entered += result.OnEnterPressed;

            result.Inputs.Name.Label = "Name";
            result.Inputs.Name.Description = "Enter the name of the citizen you would like to connect.";
            result.Inputs.Name.PropertyChanged += result.OnInputPropertyChanged;
            result.Inputs.Name.Input.Entered += result.OnEnterPressed;

            result.Buttons.ConnectCitizen.Clicked += result.OnConnectClicked;
            result.Buttons.GenerateBioCode.Clicked += result.OnGenerateClicked;

            result.UpdateButtonDisabledState();

            return result;
        }

        private readonly IAsyncCqrsCommandService<SetPasswordForConnection> _connectService;
        private readonly IAsyncCqrsQueryService<GenerateBioCode, BioCode> _codeService;
        private readonly INavigationModel _successNavigation;
        private readonly IExceptionHandler _handler;

        /// <inheritdoc/>
        public IButtonGroupModel Buttons { get; }
        /// <inheritdoc/>
        public IInputGroupModel Inputs { get; }
        /// <inheritdoc/>
        public ICodeModel Code { get; }

        private Task OnEnterPressed()
        {
            var result = !Buttons.ConnectCitizen.Disabled ?
                Buttons.ConnectCitizen.Click() :
                !Buttons.GenerateBioCode.Disabled ?
                Buttons.GenerateBioCode.Click() :
                Task.CompletedTask;

            return result;
        }

        private async Task OnConnectClicked()
        {
            if(Inputs.Name.Input.Validity == InputValidityType.Invalid ||
                Inputs.Password.Input.Validity == InputValidityType.Invalid)
            {
                return;
            }

            try
            {
                using(_ = Buttons.ConnectCitizen.Disable())
                {
                    await new SetPasswordForConnection(
                        Inputs.Name.Input.Value,
                        Inputs.Password.Input.Value,
                        Code.Value,
                        CancellationToken.None).Using(_connectService);
                    _successNavigation.Navigate();
                }
            } catch(Exception ex)
            when(_handler.CanHandle(ex))
            {
                await _handler.Handle(ex);
            }
        }

        private async Task OnGenerateClicked()
        {
            if(Inputs.Name.Input.Validity == InputValidityType.Invalid)
            {
                return;
            }

            try
            {
                using(_ = Buttons.GenerateBioCode.Disable())
                {
                    Code.Value = await new GenerateBioCode(
                        Inputs.Name.Input.Value,
                        CancellationToken.None).Using(_codeService);
                    Code.CodeError = String.Empty;
                }

                UpdateButtonDisabledState();
            } catch(Exception ex)
            when(_handler.CanHandle(ex))
            {
                await _handler.Handle(ex);
            }
        }

        private void OnInputPropertyChanged(Object? _0, PropertyChangedEventArgs _1)
        {
            UnsetCode();
            UpdateButtonDisabledState();
        }
        private void UnsetCode() => Code.Value = BioCode.Empty;
        private void UpdateButtonDisabledState()
        {
            var nameInvalid = Inputs.Name.Input.IsInvalid();
            var passwordInvalid = Inputs.Password.Input.IsInvalid();
            var codeNotGenerated = Code.Value.IsDefaultOrEmpty();

            Buttons.GenerateBioCode.Disabled = nameInvalid;
            Buttons.ConnectCitizen.Disabled = nameInvalid || passwordInvalid || codeNotGenerated;
        }
    }
}
