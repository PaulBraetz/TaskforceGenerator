﻿using TaskforceGenerator.Domain.Authentication;
using TaskforceGenerator.Presentation.Models.Abstractions;
using TaskforceGenerator.Presentation.Models.Abstractions.ConnectCitizen;

namespace TaskforceGenerator.Presentation.Models.ConnectCitizen
{
    /// <summary>
    /// Default implementation of <see cref="IInputGroupModel"/>.
    /// </summary>
    public sealed class InputGroupModel : IInputGroupModel
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="name">The model used to obtain the citizens name.</param>
        /// <param name="password">The model used to obtain the citizens password.</param>
        public InputGroupModel(IInputGroupModel<String, String> name, IInputGroupModel<String, PasswordValidity> password)
        {
            Name = name;
            Password = password;
        }

        /// <inheritdoc/>
        public IInputGroupModel<String, String> Name { get; }
        /// <inheritdoc/>
        public IInputGroupModel<String, PasswordValidity> Password { get; }
    }
}
