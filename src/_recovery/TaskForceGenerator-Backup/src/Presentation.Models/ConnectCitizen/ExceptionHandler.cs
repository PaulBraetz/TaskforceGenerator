﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Exceptions;
using TaskforceGenerator.Domain.Core.Exceptions;
using TaskforceGenerator.Presentation.Models.Abstractions.ConnectCitizen;

namespace TaskforceGenerator.Presentation.Models.ConnectCitizen
{
    /// <summary>
    /// The default exception handler for the <see cref="IConnectCitizenModel"/>.
    /// </summary>
    public sealed class ExceptionHandler : IExceptionHandler
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="inputs">The inputs model whose error proeprties to set when handling exceptions.</param>
        /// <param name="notExistingExceptionFormatter">The formatter to use when handling a <see cref="CitizenNotExistingException"/>.</param>
        /// <param name="notRegisteredExceptionFormatter">The formatter to use when handling a <see cref="CitizenNotRegisteredException"/>.</param>
        /// <param name="passwordExceptionFormatter">The formatter to use when handling a <see cref="PasswordCreationGuidelineException"/>.</param>
        public ExceptionHandler(
            IInputGroupModel inputs,
            IStaticFormatter<CitizenNotExistingException> notExistingExceptionFormatter,
            IStaticFormatter<CitizenNotRegisteredException> notRegisteredExceptionFormatter,
            IStaticFormatter<PasswordCreationGuidelineException> passwordExceptionFormatter)
        {
            _inputs = inputs;
            _notExistingExceptionFormatter = notExistingExceptionFormatter;
            _notRegisteredExceptionFormatter = notRegisteredExceptionFormatter;
            _passwordExceptionFormatter = passwordExceptionFormatter;
        }

        private readonly IInputGroupModel _inputs;
        private readonly IStaticFormatter<CitizenNotExistingException> _notExistingExceptionFormatter;
        private readonly IStaticFormatter<CitizenNotRegisteredException> _notRegisteredExceptionFormatter;
        private readonly IStaticFormatter<PasswordCreationGuidelineException> _passwordExceptionFormatter;

        /// <inheritdoc/>
        public Boolean CanHandle(Exception ex)
        {
            var result = ex switch
            {
                CitizenNotExistingException => true,
                CitizenNotRegisteredException => true,
                PasswordCreationGuidelineException => true,
                _ => false,
            };

            return result;
        }

        /// <inheritdoc/>
        public ValueTask Handle(Exception ex)
        {
            var error = ex switch
            {
                CitizenNotExistingException notExistingException =>
                    _notExistingExceptionFormatter.Format(notExistingException),
                CitizenNotRegisteredException notRegisteredException =>
                    _notRegisteredExceptionFormatter.Format(notRegisteredException),
                PasswordCreationGuidelineException passwordException =>
                    _passwordExceptionFormatter.Format(passwordException),
                _ => throw new InvalidOperationException($"Unable to handle exception {ex}.", ex)
            };

            _inputs.Name.Input.SetInvalid(error);

            return ValueTask.CompletedTask;
        }
    }
}
