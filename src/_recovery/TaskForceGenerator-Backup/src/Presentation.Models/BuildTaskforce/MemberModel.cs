﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TaskforceGenerator.Application;
using TaskforceGenerator.Application.Queries;
using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Core.Queries;
using TaskforceGenerator.Presentation.Models.Abstractions;
using TaskforceGenerator.Presentation.Models.Abstractions.BuildTaskforce;

namespace TaskforceGenerator.Presentation.Models.BuildTaskforce
{
    /// <summary>
    /// Default implementation of <see cref="IOccupantModel"/>.
    /// </summary>
    public sealed class OccupantModel : IOccupantModel
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="citizenName">The name of the Occupant.</param>
        /// <param name="slotPreferenceName">The name of this Occupants slot preference.</param>
        /// <param name="createService">The service to use when constructing an instance of <see cref="IOccupant"/> in <see cref="GetOccupant"/>.</param>
        public OccupantModel(
            IInputGroupModel<String, String> citizenName,
            ISelectInputGroupModel<ISlotPreference, String> slotPreferenceName,
            IAsyncCqrsQueryService<ProxyQuery<CreateOccupant>, IOccupant> createService)
        {
            _createService = createService;

            CitizenName = citizenName;
            SlotPreference = slotPreferenceName;
        }
        private readonly IAsyncCqrsQueryService<ProxyQuery<CreateOccupant>, IOccupant> _createService;
        /// <inheritdoc/>
        public IInputGroupModel<String, String> CitizenName { get; }
        /// <inheritdoc/>
        public ISelectInputGroupModel<ISlotPreference, String> SlotPreference { get; }
        /// <inheritdoc/>
        public Task<IOccupant> GetOccupant()
        {
            if(SlotPreference.Input.Value == null)
                throw new InvalidOperationException("Unable to construct occupant as slot preference has not been set.");
            if(CitizenName.Input.Validity == InputValidityType.Invalid)
                throw new InvalidOperationException("Unable to construct occupant as citizen name has not been set.");

            var result = new CreateOccupant(CitizenName.Input.Value!, SlotPreference.Input.Value.Value, CancellationToken.None)
                .AsProxyQuery()
                .Using(_createService)
                .AsTask();

            return result;
        }
    }
}
