﻿using Microsoft.Win32.SafeHandles;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using TaskforceGenerator.Application.Queries;
using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Presentation.Models.Abstractions;
using TaskforceGenerator.Presentation.Models.Abstractions.BuildTaskforce;

namespace TaskforceGenerator.Presentation.Models.BuildTaskforce
{
    /// <summary>
    /// Default implementation of <see cref="IBuildTaskforceModel"/>.
    /// </summary>
    public sealed class BuildTaskforceModel : ObservableModelBase, IBuildTaskforceModel
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="build">The button to use to initiate building the taskforce.</param>
        /// <param name="service">The service to use when building the taskforce.</param>
        /// <param name="inputs">The inputs responsible for obtaining the build request data.</param>
        private BuildTaskforceModel(
            IButtonModel build,
            IAsyncCqrsQueryService<Application.Queries.BuildTaskforce, ITaskforce> service,
            IInputGroupModel inputs)
        {
            _service = service;

            Inputs = inputs;
            Build = build;
        }
        /// <summary>
        /// Creates and initializes a new instance.
        /// </summary>
        /// <param name="build">The button to use to initiate building the taskforce.</param>
        /// <param name="service">The service to use when building the taskforce.</param>
        /// <param name="inputs">The inputs responsible for obtaining the build request data.</param>
        /// <returns>The new model instance, initialized and ready.</returns>
        public static BuildTaskforceModel Create(
            IButtonModel build,
            IAsyncCqrsQueryService<Application.Queries.BuildTaskforce, ITaskforce> service,
            IInputGroupModel inputs)
        {
            var result = new BuildTaskforceModel(build, service, inputs);

            result.Inputs.Occupants.Label = "Add Occupants";
            result.Inputs.Occupants.Description = "Add Occupants to your taskforce.";

            result.Inputs.Vehicles.Label = "Add Vehicles";
            result.Inputs.Vehicles.Description = "Add vehicles to your taskforce.";

            result.Inputs.Scheme.Label = "Select Scheme";
            result.Inputs.Scheme.Description = "Select the scheme algorithm to build your taskforce.";

            result.Build.Label = "Build";
            result.Build.Clicked += result.OnClicked;

            return result;
        }

        private readonly IAsyncCqrsQueryService<Application.Queries.BuildTaskforce, ITaskforce> _service;
        private ITaskforce? _taskforce;
        private String _buildError = String.Empty;

        /// <inheritdoc/>
        public ITaskforce? Taskforce
        {
            get => _taskforce;
            private set => ExchangeBackingField(ref _taskforce, value);
        }
        /// <inheritdoc/>
        public IButtonModel Build { get; }
        /// <inheritdoc/>
        public IInputGroupModel Inputs { get; }

        private async Task OnClicked()
        {
            try
            {
                BuildError = String.Empty;

                var occupantTasks = Inputs.Occupants.GetValues(c => c.Control.GetOccupant());
                var occupants = await Task.WhenAll(occupantTasks);
                var vehicles = Inputs.Vehicles.GetValues(c => c.Control)
                    .Where(c => c.Vehicle.Input.Value != null)
                    .SelectMany(c => Enumerable.Repeat(c.Vehicle.Input.Value!.Value, c.Count.Input.Value));
                var schemeName = Inputs.Scheme.Input.Value?.Name ?? String.Empty;

                Taskforce = await new Application.Queries.BuildTaskforce(
                    vehicles,
                    occupants,
                    schemeName,
                    CancellationToken.None)
                    .Using(_service);
            } catch(Exception ex)
            {
                BuildError = ex.Message;
				Taskforce = null;
			}
        }
        /// <inheritdoc/>
        public String BuildError
        {
            get => _buildError;
            set => ExchangeBackingField(ref _buildError, value);
        }
    }
}
