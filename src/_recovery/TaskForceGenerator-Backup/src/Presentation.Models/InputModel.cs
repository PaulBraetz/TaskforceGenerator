﻿using TaskforceGenerator.Common;
using TaskforceGenerator.Presentation.Models.Abstractions;

namespace TaskforceGenerator.Presentation.Models
{
    /// <summary>
    /// Default implementation of <see cref="IInputModel{TValue, TError}"/>.
    /// </summary>
    /// <typeparam name="TValue">The type of value obtained by this model.</typeparam>
    /// <typeparam name="TError">The type of error displayed by this model.</typeparam>
    public class InputModel<TValue, TError> : ObservableModelBase, IInputModel<TValue, TError>
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="valueDefaultProvider">The default value provider to use for initializing <see cref="Value"/>.</param>
        /// <param name="errorDefaultProvider">The default value provider to use for initializing <see cref="Error"/>.</param>
        public InputModel(IDefaultValueProvider<TValue> valueDefaultProvider, IDefaultValueProvider<TError> errorDefaultProvider)
        {
            _value = valueDefaultProvider.GetDefault();
            _error = errorDefaultProvider.GetDefault();

            _errorErrorProvider = errorDefaultProvider;
        }

        private readonly IDefaultValueProvider<TError> _errorErrorProvider;
        private TValue _value;
        private TError _error;
        private InputValidityType _validity;
        private String _placeholder = String.Empty;

        /// <inheritdoc/>
        public virtual TValue Value
        {
            get => _value;
            set => ExchangeBackingField(ref _value, value);
        }
        /// <inheritdoc/>
        public String Placeholder
        {
            get => _placeholder;
            set => ExchangeBackingField(ref _placeholder, value);
        }
        /// <inheritdoc/>
        public TError Error
        {
            get => _error;
            set => ExchangeBackingField(ref _error, value);
        }

        /// <inheritdoc/>
        public event AsynchronousEventHandler? Entered;
        /// <inheritdoc/>
        public Task Enter() => Entered.InvokeAsync();
        /// <inheritdoc/>
        public InputValidityType Validity
        {
            get => _validity;
            private set => ExchangeBackingField(ref _validity, value);
        }
        /// <inheritdoc/>
        public void UnsetValidity()
        {
            Validity = InputValidityType.None;
            Error = _errorErrorProvider.GetDefault();
        }

        /// <inheritdoc/>
        public void SetInvalid(TError error)
        {
            Validity = InputValidityType.Invalid;
            Error = error;
        }
        /// <inheritdoc/>
        public void SetValid()
        {
            Validity = InputValidityType.Valid;
            Error = _errorErrorProvider.GetDefault();
        }
    }
}
