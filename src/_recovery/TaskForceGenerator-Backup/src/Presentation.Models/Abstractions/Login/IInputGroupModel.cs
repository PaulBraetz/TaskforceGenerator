﻿namespace TaskforceGenerator.Presentation.Models.Abstractions.Login
{
    /// <summary>
    /// Contains the inputs required by <see cref="ILoginModel"/>.
    /// </summary>
    public interface IInputGroupModel
    {
        /// <summary>
        /// Gets the model responsible for acquiring the name of the citizen for which to authenticate the user.
        /// </summary>
        IInputGroupModel<String, String> Name { get; }
        /// <summary>
        /// Gets the model responsible for acquiring the password uing which the user is attempting to authenticate.
        /// </summary>
        IInputGroupModel<String, String> Password { get; }
    }
}
