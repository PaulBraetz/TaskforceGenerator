﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Presentation.Models.Abstractions.Login
{
    /// <summary>
    /// Represents a model for login components.
    /// </summary>
    public interface ILoginModel
    {
        /// <summary>
        /// Gets the model responsible for obtaining the required inputs.
        /// </summary>
        IInputGroupModel Inputs { get; }
        /// <summary>
        /// Gets the button for submitting the login request.
        /// </summary>
        IButtonModel Login { get; }
    }
}
