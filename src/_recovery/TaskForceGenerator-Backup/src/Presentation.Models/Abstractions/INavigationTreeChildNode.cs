﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Presentation.Models.Abstractions
{
    /// <summary>
    /// Represents a node in the route navigation tree.
    /// </summary>
    public interface INavigationTreeChildNode : INavigationTreeNode
    {
        /// <summary>
        /// Gets the parent to this node.
        /// </summary>
        INavigationTreeNode Parent { get; }
    }
}
