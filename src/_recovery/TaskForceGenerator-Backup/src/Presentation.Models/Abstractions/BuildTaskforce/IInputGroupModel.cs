﻿namespace TaskforceGenerator.Presentation.Models.Abstractions.BuildTaskforce
{
    /// <summary>
    /// Represents the inputs responsible for obtaining the build request data.
    /// </summary>
    public interface IInputGroupModel
    {
        /// <summary>
        /// Gets the model used for obtaining the Occupant list.
        /// </summary>
        IDynamicMultiControlModel<IOccupantModel> Occupants { get; }
        /// <summary>
        /// Gets the model used for obtaining the vehicles to add to the taskforce.
        /// </summary>
        IDynamicMultiControlModel<ISelectVehiclesModel> Vehicles { get; }
        /// <summary>
        /// Gets the model responsible for obtaining the scheme name to use.
        /// </summary>
        public ISelectInputGroupModel<String, String> Scheme { get; }
    }
}
