﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Presentation.Models.Abstractions.BuildTaskforce
{
    /// <summary>
    /// Represents a model of a taskforce Occupant.
    /// </summary>
    public interface IOccupantModel
    {
        /// <summary>
        /// Gets the name of the Occupant.
        /// </summary>
        IInputGroupModel<String, String> CitizenName { get; }
        /// <summary>
        /// Gets the name of this Occupants slot preference.
        /// </summary>
        ISelectInputGroupModel<ISlotPreference, String> SlotPreference { get; }
        /// <summary>
        /// Obtains a model of the occupant represented.
        /// </summary>
        /// <returns>The occupant represented.</returns>
        Task<IOccupant> GetOccupant();
    }
}
