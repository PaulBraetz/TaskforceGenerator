﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Presentation.Models.Abstractions.BuildTaskforce
{
    /// <summary>
    /// Model for generating taskforces.
    /// </summary>
    public interface IBuildTaskforceModel
    {
        /// <summary>
        /// Gets the inputs responsible for obtaining the build request data.
        /// </summary>
        IInputGroupModel Inputs { get; }
        /// <summary>
        /// Gets the taskforce last built.
        /// </summary>
        ITaskforce? Taskforce { get; }
        /// <summary>
        /// Gets the button responsible for building the taskforce.
        /// </summary>
        IButtonModel Build { get; }
        /// <summary>
        /// Gets the error that occured while attemptng to build the taskforce.
        /// </summary>
        String BuildError { get; set; }
    }
}
