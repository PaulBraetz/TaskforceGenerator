﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Presentation.Models.Abstractions
{
    /// <summary>
    /// Represents a model for displaying information on the citizen currently associated to the users context.
    /// </summary>
    public interface IUserContextCitizenModel
    {
        /// <summary>
        /// Gets the name of the citizen currently associated to the user context, or <see langword="null"/> if no citizen is currently associated.
        /// </summary>
        String? CitizenName { get; }
    }
}
