﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TaskforceGenerator.Common;

namespace TaskforceGenerator.Presentation.Models.Abstractions
{
    /// <summary>
    /// Represents a simple input field that contains information of the validity of the value entered.
    /// </summary>
    /// <typeparam name="TValue">The type of value obtained by this model.</typeparam>
    /// <typeparam name="TError">The type of error displayed by this model.</typeparam>
    public interface IInputModel<TValue, TError> : INotifyPropertyChanged
    {
        /// <summary>
        /// Gets a value indicating the validity of <see cref="Value"/>.
        /// If <see cref="Validity"/> is set to <see cref="InputValidityType.None"/> or <see cref="InputValidityType.Valid"/>, 
        /// any value retrieved from <see cref="Error"/> should be ignored.
        /// </summary>
        InputValidityType Validity { get; }
        /// <summary>
        /// Sets <see cref="Validity"/> to <see cref="InputValidityType.Invalid"/> and <see cref="Error"/> to <paramref name="error"/>.
        /// </summary>
        /// <param name="error">The error to set <see cref="Error"/> to.</param>
        void SetInvalid(TError error);
        /// <summary>
        /// Sets <see cref="Validity"/> to <see cref="InputValidityType.Valid"/> and <see cref="Error"/> to its initial value.
        /// </summary>
        void SetValid();
        /// <summary>
        /// Sets <see cref="Validity"/> to <see cref="InputValidityType.None"/> and <see cref="Error"/> to its initial value.
        /// </summary>
        void UnsetValidity();
        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        TValue Value { get; set; }
        /// <summary>
        /// Gets or sets the error.
        /// </summary>
        TError Error { get; }
        /// <summary>
        /// Gets or sets the placeholde to display before a value has been entered.
        /// </summary>
        String Placeholder { get; set; }
        /// <summary>
        /// Confirms the value set by pressing the enter key.
        /// </summary>
        Task Enter();
        /// <summary>
        /// Invoked after the input has been confirmed by calling <see cref="Enter"/>.
        /// </summary>
        event AsynchronousEventHandler? Entered;
    }
}
