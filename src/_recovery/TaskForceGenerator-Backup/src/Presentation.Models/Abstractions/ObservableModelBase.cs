﻿using System.ComponentModel;
using System.Data.Common;
using System.Runtime.CompilerServices;

namespace TaskforceGenerator.Presentation.Models.Abstractions
{
    /// <summary>
    /// Base class for models exposing property changes.
    /// </summary>
    public abstract class ObservableModelBase : INotifyPropertyChanged
    {
        /// <summary>
        /// <para>
        /// Assigns <paramref name="newValue"/> to <paramref name="location"/> if
        /// the value previously stored at <paramref name="location"/> was not equal
        /// to <paramref name="newValue"/>. Equality is established using the
        /// default equality comparer.
        /// </para>
        /// <para>
        /// Upon assigning <paramref name="newValue"/> to <paramref name="location"/>,
        /// the <see cref="PropertyChanged"/> event will be invoked.
        /// </para>
        /// <para>
        /// This method is not implemented to be safely used in a concurrent access scenario.
        /// </para>
        /// </summary>
        /// <param name="location">The location which to conditionally assign <paramref name="newValue"/>.</param>
        /// <param name="newValue">The new value to assign to <paramref name="location"/>.</param>
        /// <param name="propertyName">The name of the property whose value might change.</param>
        /// <returns>The value previously stored at <paramref name="location"/>.</returns>
        protected T ExchangeBackingField<T>(ref T location, T newValue, [CallerMemberName] String? propertyName = null)
        {
            _ = propertyName ?? throw new ArgumentNullException(nameof(propertyName));

            var previousValue = location;
            if(!EqualityComparer<T>.Default.Equals(previousValue, newValue))
            {
                location = newValue;
                InvokePropertyChanged(propertyName);
            }

            return previousValue;
        }
        /// <summary>
        /// Invokes the <see cref="PropertyChanged"/> event.
        /// </summary>
        /// <param name="propertyName">The name of the property whose value changed.</param>
        protected void InvokePropertyChanged(String propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            OnPropertyChanged(propertyName);
        }
        /// <summary>
        /// Invoked after the <see cref="PropertyChanged"/> event has been invoked by <see cref="ExchangeBackingField{T}(ref T, T, String?)"/>;
        /// </summary>
        /// <param name="propertyName">The name of the property whose value changed.</param>
        protected virtual void OnPropertyChanged(String propertyName) { }

        /// <inheritdoc/>
        public event PropertyChangedEventHandler? PropertyChanged;
    }
}
