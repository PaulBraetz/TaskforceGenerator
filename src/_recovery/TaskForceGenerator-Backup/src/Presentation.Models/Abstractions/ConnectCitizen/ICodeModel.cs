﻿using TaskforceGenerator.Domain.Authentication;

namespace TaskforceGenerator.Presentation.Models.Abstractions.ConnectCitizen
{
    /// <summary>
    /// Represents the code display for the connect citizen control.
    /// </summary>
    public interface ICodeModel
    {
        /// <summary>
        /// Gets the code used for bio verification.
        /// </summary>
        BioCode Value { get; set; }
        /// <summary>
        /// Gets an error message related to the code.
        /// </summary>
        String CodeError { get; set; }
    }
}
