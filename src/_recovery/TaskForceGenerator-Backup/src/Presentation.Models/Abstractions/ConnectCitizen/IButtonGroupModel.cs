﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Presentation.Models.Abstractions.ConnectCitizen
{
    /// <summary>
    /// Represents the buttons to display in a connect citizen form.
    /// </summary>
    public interface IButtonGroupModel
	{
		/// <summary>
		/// Gets the button model for generating a bio code.
		/// </summary>
		IButtonModel GenerateBioCode { get; }
		/// <summary>
		/// Gets the button model for connecting a citizen.
		/// </summary>
		IButtonModel ConnectCitizen { get; }
	}
}
