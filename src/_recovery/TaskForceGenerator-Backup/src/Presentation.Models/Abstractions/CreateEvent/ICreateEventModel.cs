﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Presentation.Models.Abstractions.CreateEvent
{
    /// <summary>
    /// Models the event creation.
    /// </summary>
    public interface ICreateEventModel
    {
        /// <summary>
        /// Gets the invitation link that was created after clicking the <see cref="Create"/> button.
        /// </summary>
        String? InviteLink { get; }
        /// <summary>
        /// Gets the button model responsible for creating the event.
        /// </summary>
        IButtonModel Create { get; }
        /// <summary>
        /// Gets the input group responsible for configuring the event model.
        /// </summary>
        ICreateEventInputGroupModel Inputs { get; }
    }
}
