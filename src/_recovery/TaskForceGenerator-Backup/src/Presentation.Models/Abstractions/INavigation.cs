﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Presentation.Models.Abstractions
{
    /// <summary>
    /// Represents a model that navigates to a static path.
    /// </summary>
    public interface INavigationModel
    {
        /// <summary>
        /// Navigates to the path.
        /// </summary>
        void Navigate();
    }
}
