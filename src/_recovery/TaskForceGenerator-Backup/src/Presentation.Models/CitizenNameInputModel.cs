﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net.Http.Headers;
using System.Reactive;
using System.Reactive.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using TaskforceGenerator.Application;
using TaskforceGenerator.Application.Queries;
using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Core.Queries;
using TaskforceGenerator.Presentation.Models.Abstractions;

namespace TaskforceGenerator.Presentation.Models
{
    /// <summary>
    /// Self validating input model for citizen names.
    /// </summary>
    public sealed class CitizenNameInputModel : InputModel<String, String>
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="service">The service used to validate the entered name.</param>
        /// <param name="valueDefaultProvider">The default value provider to use for initializing <see cref="IInputModel{TValue, TError}.Value"/>.</param>
        /// <param name="errorDefaultProvider">The default value provider to use for initializing <see cref="IInputModel{TValue, TError}.Error"/>.</param>
        public CitizenNameInputModel(
            IAsyncCqrsQueryService<ProxyQuery<CheckCitizenExists>, CheckCitizenExists.Result> service,
            IDefaultValueProvider<String> valueDefaultProvider,
            IDefaultValueProvider<String> errorDefaultProvider)
            : base(valueDefaultProvider, errorDefaultProvider)
        {
            _service = service;

            _valueChangeSubscription = CreateSubscription();
            _ = UpdateError();
        }

        private IDisposable CreateSubscription() =>
            Observable.FromEventPattern(this, nameof(INotifyPropertyChanged.PropertyChanged))
                .Throttle(TimeSpan.FromMilliseconds(100))
                .Select(e => (PropertyChangedEventArgs)e.EventArgs)
                .SelectMany(OnPropertyChanged)
                .Subscribe();

        private readonly IAsyncCqrsQueryService<ProxyQuery<CheckCitizenExists>, CheckCitizenExists.Result> _service;
#pragma warning disable IDE0052 // Remove unread private members
        private readonly IDisposable _valueChangeSubscription;
#pragma warning restore IDE0052 // Remove unread private members

        private Task<Unit> OnPropertyChanged(PropertyChangedEventArgs args) =>
            args.PropertyName != nameof(Value) ? _unitTask : UpdateError();

        private static readonly Task<Unit> _unitTask = Task.FromResult(Unit.Default);

        private async Task<Unit> UpdateError()
        {
            var result = await new CheckCitizenExists(Value, CancellationToken.None)
                .AsProxyQuery()
                .Using(_service);

            if(result.Exists)
                SetValid();
            else
                SetInvalid("The name entered does not appear to be a registered RSI citizen.");

            return Unit.Default;
        }
    }
}
