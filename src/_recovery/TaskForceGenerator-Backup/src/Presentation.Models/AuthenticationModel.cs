﻿using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Core.Abstractions;
using TaskforceGenerator.Presentation.Models.Abstractions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Presentation.Models
{
    /// <summary>
    /// Default implementation of <see cref="IAuthenticationModel"/>.
    /// </summary>
    public sealed class AuthenticationModel : ObservableModelBase, IAuthenticationModel
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="context">The context monitored by this model.</param>
        public AuthenticationModel(IObservableUserContext context)
        {
            _context = context;
            _context.CitizenChanged += OnCitizenChanged;
        }
        private readonly IObservableUserContext _context;
        /// <inheritdoc/>
        public Boolean IsAuthenticated => _isAuthenticated == AUTHENTICATED_STATE;

        private const Int32 AUTHENTICATED_STATE = 1;
        private const Int32 NOT_AUTHENTICATED_STATE = 0;
        private Int32 _isAuthenticated = NOT_AUTHENTICATED_STATE;

        private void OnCitizenChanged(Object? sender, ICitizenConnection? citizen)
        {
            var newState = citizen != null ? AUTHENTICATED_STATE : NOT_AUTHENTICATED_STATE;
            if(Interlocked.Exchange(ref _isAuthenticated, newState) != newState)
                InvokePropertyChanged(nameof(IsAuthenticated));
        }
    }
}
