﻿namespace TaskforceGenerator.Presentation.Models.Exceptions
{
	internal static class StaticExceptions
	{
		public static class InvalidOperation
		{
			public static InvalidOperationException ButtonDisabled { get; } = new("The button may not be clicked while disabled.");
		}
	}
}
