﻿using Microsoft.VisualBasic.FileIO;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TaskforceGenerator.Domain.Core.Abstractions;
using TaskforceGenerator.Presentation.Models.Abstractions;

namespace TaskforceGenerator.Presentation.Models
{
    /// <summary>
    /// Default implementation of <see cref="ISelectInputGroupModel{TValue, TError}"/>.
    /// </summary>
    /// <typeparam name="TValue">The type of value to select.</typeparam>
    /// <typeparam name="TError">The type of error displayed by this model.</typeparam>
    public sealed class SelectInputGroupModel<TValue, TError> :
        InputGroupModel<ISelectInputModel<TValue, TError>, IOptionModel<TValue>?, TError>,
        ISelectInputGroupModel<TValue, TError>
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="input">The input to use.</param>
        public SelectInputGroupModel(
            ISelectInputModel<TValue, TError> input)
            : base(input)
        {

        }
    }
}
