﻿using TaskforceGenerator.Aspects.Abstractions;
using Microsoft.Extensions.Logging;

namespace TaskforceGenerator.Aspects.Logging
{
    /// <summary>
    /// Log entry for logging command executions that have just concluded.
    /// </summary>
    public readonly struct AfterCommandExecutionLogEntry<TCommand> : ILogEntry
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="level">The level at which the execution is to be logged.</param>
        public AfterCommandExecutionLogEntry(LogLevel level = LogLevel.Information)
        {
            _level = level;
        }
        private readonly LogLevel? _level;
        /// <inheritdoc/>
        public LogLevel Level =>_level??LogLevel.Information;

        /// <inheritdoc/>
        public String Evaluate() => $"Executed {typeof(TCommand).Name}";
    }
}