﻿using Microsoft.Extensions.Logging;

using TaskforceGenerator.Aspects.Abstractions;

namespace TaskforceGenerator.Aspects.Logging
{
    /// <summary>
    /// Log entry for logging thread ids.
    /// </summary>
    public readonly struct ThreadIdLogEntry : ILogEntry
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="threadId">The current thread id.</param>
        /// <param name="level">The level at which the thread id is to be logged.</param>
        public ThreadIdLogEntry(Int32 threadId,
                                 LogLevel level = LogLevel.Information)
        {
            Level = level;
            _threadId = threadId;
        }
        /// <inheritdoc/>
        public LogLevel Level { get; }
        private readonly Int32 _threadId;
        /// <summary>
        /// Gets a log entry that has been initialized with the current thread id.
        /// </summary>
        public static ThreadIdLogEntry Current => new(Environment.CurrentManagedThreadId);
        /// <inheritdoc/>
        public String Evaluate() => $"ThreadId: {_threadId}";
    }
}
