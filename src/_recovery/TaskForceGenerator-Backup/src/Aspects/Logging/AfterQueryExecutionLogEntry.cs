﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Aspects.Abstractions;
using Microsoft.Extensions.Logging;

namespace TaskforceGenerator.Aspects.Logging
{
    /// <summary>
    /// Log entry for logging results of query executions.
    /// </summary>
    public readonly struct AfterQueryExecutionLogEntry<TQuery, TResult> : ILogEntry
    {
        /// <inheritdoc/>
        public LogLevel Level { get; }
        private readonly TResult _scope;
        private readonly IStaticFormatter<TResult> _formatter;
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="scope">The result of the query execution.</param>
        /// <param name="formatter">The formatter used to format the result.</param>
        /// <param name="level">The level at which the result is to be logged.</param>
        public AfterQueryExecutionLogEntry(TResult scope,
                                 IStaticFormatter<TResult> formatter,
                                 LogLevel level = LogLevel.Information)
        {
            Level = level;
            _scope = scope;
            _formatter = formatter;
        }

        /// <inheritdoc/>
        public String Evaluate() => $"Executed {typeof(TQuery).Name}: {_formatter.Format(_scope)}";
    }
}
