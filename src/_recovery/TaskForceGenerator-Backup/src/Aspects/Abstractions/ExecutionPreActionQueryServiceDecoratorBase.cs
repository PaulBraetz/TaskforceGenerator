﻿using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Aspects.Abstractions
{
    /// <summary>
    /// Base class for decorators defining actions to be executed before the decorated query services execution.
    /// </summary>
    /// <typeparam name="TQuery">The type of query the decorated service can execute.</typeparam>
    /// <typeparam name="TResult">The type of result yielded by the query service.</typeparam>
    public abstract class ExecutionPreActionQueryServiceDecoratorBase<TQuery, TResult> : ExecutionPreActionServiceDecoratorBase<IAsyncCqrsQueryService<TQuery, TResult>, TQuery>, IAsyncCqrsQueryService<TQuery, TResult>
        where TQuery : ICqrsQuery
    {
        /// <summary>
        /// Initializes a new instance. 
        /// </summary>
        /// <param name="decorated">The decorated query service.</param>
        protected ExecutionPreActionQueryServiceDecoratorBase(IAsyncCqrsQueryService<TQuery, TResult> decorated) : base(decorated)
        {
        }

        /// <inheritdoc/>
        public async ValueTask<TResult> Execute(TQuery query)
        {
            await ExecutePreAction(query);
            var result = await Decorated.Execute(query);
            return result;
        }
    }
}
