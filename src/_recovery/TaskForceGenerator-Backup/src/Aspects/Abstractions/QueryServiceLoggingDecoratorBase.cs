﻿using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Aspects.Abstractions
{
    /// <summary>
    /// Abstract base class for types implementing ambient logging around query service execution.
    /// </summary>
    /// <typeparam name="TQuery">The type of query executed.</typeparam>
    /// <typeparam name="TResult">The type of result yielded by executing a query.</typeparam>
    public abstract class QueryServiceLoggingDecoratorBase<TQuery, TResult> : LoggingDecoratorBase<TQuery, ValueTask<TResult>>, IAsyncCqrsQueryService<TQuery, TResult>
        where TQuery : ICqrsQuery
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="logger">The service used for logging.</param>
        /// <param name="decorated">The decorated service.</param>
        protected QueryServiceLoggingDecoratorBase(
            ILoggingService logger,
            IAsyncCqrsQueryService<TQuery, TResult> decorated) : base(logger)
        {
            Decorated = decorated;
        }
        /// <summary>
        /// Gets the decorated service.
        /// </summary>
        protected IAsyncCqrsQueryService<TQuery, TResult> Decorated { get; }
        /// <inheritdoc/>
        protected override ValueTask<TResult> InvokeDecorated(TQuery arg) => Decorated.Execute(arg);
        /// <inheritdoc/>
        public virtual ValueTask<TResult> Execute(TQuery query) => DecorateInvocation(query);
    }
}
