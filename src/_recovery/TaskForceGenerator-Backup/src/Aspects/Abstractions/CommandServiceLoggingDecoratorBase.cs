﻿using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Aspects.Abstractions
{
    /// <summary>
    /// Abstract base class for types implementing ambient logging around command service execution.
    /// </summary>
    /// <typeparam name="TCommand">The type of command executed.</typeparam>
    public abstract class CommandServiceLoggingDecoratorBase<TCommand> : LoggingDecoratorBase<TCommand, ValueTask>, IAsyncCqrsCommandService<TCommand>
        where TCommand : ICqrsCommand
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="logger">The service used for logging.</param>
        /// <param name="decorated">The decorated service.</param>
        protected CommandServiceLoggingDecoratorBase(
            ILoggingService logger,
            IAsyncCqrsCommandService<TCommand> decorated) : base(logger)
        {
            Decorated = decorated;
        }

        /// <summary>
        /// Gets the decorated service.
        /// </summary>
        protected IAsyncCqrsCommandService<TCommand> Decorated { get; }

        /// <inheritdoc/>
        protected override ValueTask InvokeDecorated(TCommand arg) => Decorated.Execute(arg);
        /// <inheritdoc/>
        public virtual ValueTask Execute(TCommand command) => DecorateInvocation(command);
    }
}
