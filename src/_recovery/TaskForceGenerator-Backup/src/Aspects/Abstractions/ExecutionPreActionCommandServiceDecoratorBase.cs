﻿using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Aspects.Abstractions
{
    /// <summary>
    /// Base class for decorators defining actions to be executed before the decorated command services execution.
    /// </summary>
    /// <typeparam name="TCommand">The type of command the decorated service can execute.</typeparam>
    public abstract class ExecutionPreActionCommandServiceDecoratorBase<TCommand> : ExecutionPreActionServiceDecoratorBase<IAsyncCqrsCommandService<TCommand>, TCommand>, IAsyncCqrsCommandService<TCommand>
        where TCommand : ICqrsCommand
    {
        /// <summary>
        /// Initializes a new instance. 
        /// </summary>
        /// <param name="decorated">The decorated command service.</param>
        protected ExecutionPreActionCommandServiceDecoratorBase(IAsyncCqrsCommandService<TCommand> decorated) : base(decorated)
        {
        }
        /// <inheritdoc/>
        public async ValueTask Execute(TCommand command)
        {
            await ExecutePreAction(command);
            await Decorated.Execute(command);
        }
    }
}
