﻿namespace TaskforceGenerator.Aspects.Abstractions
{
    /// <summary>
    /// Base class for decorators defining actions to be executed before the decorated services execution.
    /// </summary>
    /// <typeparam name="TDecorated">The type of decorated service.</typeparam>
    /// <typeparam name="TPreActionValue">The type of value provided to the pre-action.</typeparam>
    public abstract class ExecutionPreActionServiceDecoratorBase<TDecorated, TPreActionValue>
    {
        /// <summary>
        /// Gets the decorated service.
        /// </summary>
        protected TDecorated Decorated { get; }
        /// <summary>
        /// Executes the pre-action. This method will be invoked before the services execution.
        /// </summary>
        /// <param name="value">The argument provided to the service for execution.</param>
        protected abstract ValueTask ExecutePreAction(TPreActionValue value);
        /// <summary>
        /// Initializes a new instance. 
        /// </summary>
        /// <param name="decorated">The decorated service.</param>
        protected ExecutionPreActionServiceDecoratorBase(TDecorated decorated)
        {
            Decorated = decorated;
        }
    }
}
