﻿using System.Windows.Input;
using TaskforceGenerator.Common;

namespace TaskforceGenerator.Aspects.Abstractions
{
	/// <summary>
	/// Abstract base class for types implementing ambient logging around a decorated functionality.
	/// </summary>
	/// <typeparam name="TArg">The type of argument required by the decorated functionality.</typeparam>
	/// <typeparam name="TResult">The type of result produced by the decorated functionality.</typeparam>
	public abstract class LoggingDecoratorBase<TArg, TResult>
	{
		/// <summary>
		/// Initializes a new instance.
		/// </summary>
		/// <param name="logger">The service used for logging.</param>
		protected LoggingDecoratorBase(ILoggingService logger)
		{
			Logger = logger;
		}
		/// <summary>
		/// Gets the service used for logging.
		/// </summary>
		public ILoggingService Logger { get; }
		/// <summary>
		/// Surrounds a call to <see cref="InvokeDecorated(TArg)"/> with the scope obtained by calling <see cref="GetLogScope(TArg)"/>.
		/// </summary>
		/// <param name="arg">The argument required by the decorated functionality.</param>
		/// <returns>The result of the decorated functionality.</returns>
		protected virtual TResult DecorateInvocation(TArg arg)
		{
			using (_ = GetLogScope(arg))
			{
				return InvokeDecorated(arg);
			}
		}
		/// <summary>
		/// Invokes the decorated functionality using the argument provided.
		/// </summary>
		/// <param name="arg">The argument provided to invoke on the decorated functionality.</param>
		/// <returns>The result of the invokation.</returns>
		protected abstract TResult InvokeDecorated(TArg arg);
		/// <summary>
		/// Gets the log scope to wrap around the service invocation.
		/// </summary>
		/// <param name="command">The command invoked on the service.</param>
		/// <returns>The log scope to wrap around the service invocation.</returns>
		protected virtual IDisposable GetLogScope(TArg command) => NullDisposable.Instance;
	}
}
