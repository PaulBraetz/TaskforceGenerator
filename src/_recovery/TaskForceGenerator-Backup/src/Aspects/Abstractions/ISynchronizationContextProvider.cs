﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Aspects.Abstractions
{
    /// <summary>
    /// Provies a synchronization context.
    /// </summary>
    public interface ISynchronizationContextProvider
    {
        /// <summary>
        /// Gets the synchronization context.
        /// </summary>
        public SynchronizationContext Context { get; }
    }
}
