﻿namespace TaskforceGenerator.Aspects.Abstractions
{
    /// <summary>
    /// Strategy for implementing the citizen name replacement proxy.
    /// </summary>
    /// <typeparam name="T">The type of value containing a citizen name.</typeparam>
    public interface ICitizenNamesReplacementStrategy<T>
    {
        /// <summary>
        /// Creates a replacement value with the citizen names substituted with actual names.
        /// </summary>
        /// <param name="old">The old value.</param>
        /// <param name="cancellationToken">The cancellation token used to cancel the replacement process.</param>
        /// <returns>A new instance of <typeparamref name="T"/>, as obtained from the prototype <paramref name="old"/>, with the citizen names substituted with their actual versions.</returns>
        ValueTask<T> ReplaceNames(T old, CancellationToken cancellationToken);
    }
}
