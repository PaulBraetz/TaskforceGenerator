﻿using TaskforceGenerator.Application.Commands;
using TaskforceGenerator.Aspects.Proxies.ReplacementStrategies.Abstractions;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Queries;

namespace TaskforceGenerator.Aspects.Proxies.ReplacementStrategies
{
    /// <summary>
    /// Name replacement strategy for <see cref="Login"/>.
    /// </summary>
    public sealed class LoginStrategy : CitizenNameReplacementStrategyBase<Login>
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="nameService">The service used to query actual citizen names.</param>
        public LoginStrategy(IAsyncCqrsQueryService<LoadActualName, LoadActualName.Result> nameService) : base(nameService)
        {
        }
        /// <inheritdoc/>
        public override async ValueTask<Login> ReplaceNames(
            Login old,
            CancellationToken cancellationToken)
        {
            var actualName = await LoadActualName(old.CitizenName, cancellationToken);
            var result = old with { CitizenName = actualName };

            return result;
        }
    }
}
