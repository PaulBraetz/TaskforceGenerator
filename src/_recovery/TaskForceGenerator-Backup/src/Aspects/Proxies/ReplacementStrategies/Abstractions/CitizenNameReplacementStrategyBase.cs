﻿using TaskforceGenerator.Aspects.Abstractions;
using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Queries;

namespace TaskforceGenerator.Aspects.Proxies.ReplacementStrategies.Abstractions
{
    /// <summary>
    /// Base class for types implementing <see cref="ICitizenNamesReplacementStrategy{T}"/>.
    /// </summary>
    /// <typeparam name="T">The type of value containing a citizen name.</typeparam>
    public abstract class CitizenNameReplacementStrategyBase<T> : ICitizenNamesReplacementStrategy<T>
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="nameService">The service used to query actual citizen names.</param>
        protected CitizenNameReplacementStrategyBase(IAsyncCqrsQueryService<LoadActualName, LoadActualName.Result> nameService)
        {
            _nameService = nameService;
        }

        private readonly IAsyncCqrsQueryService<LoadActualName, LoadActualName.Result> _nameService;
        /// <summary>
        /// Loads the actual name of the citizen name provided.
        /// </summary>
        /// <param name="name">The name of the citizen for which to load the actual name.</param>
        /// <param name="cancellationToken">The token to cancel the name loading process with.</param>
        /// <returns>The actual name of the citizen.</returns>
        protected async ValueTask<String> LoadActualName(String name, CancellationToken cancellationToken)
        {
            var queryResult = await new LoadActualName(name, cancellationToken).Using(_nameService);
            var result = queryResult.ActualName;

            return result;
        }
        /// <inheritdoc/>
        public abstract ValueTask<T> ReplaceNames(T old, CancellationToken cancellationToken);
    }
}
