﻿using TaskforceGenerator.Common;
using TaskforceGenerator.Application.Queries;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Queries;
using TaskforceGenerator.Domain.Core.Queries;
using TaskforceGenerator.Aspects.Proxies.ReplacementStrategies.Abstractions;

namespace TaskforceGenerator.Aspects.Proxies.ReplacementStrategies
{
    /// <summary>
    /// Name replacement strategy for <see cref="BuildTaskforce"/>.
    /// </summary>
    public sealed class BuildTaskforceStrategy : CitizenNameReplacementStrategyBase<BuildTaskforce>
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="nameService">The service used to query actual citizen names.</param>
        /// <param name="createService">The service to use when replacing occupants in queries for properly named ones.</param>
        public BuildTaskforceStrategy(IAsyncCqrsQueryService<LoadActualName, LoadActualName.Result> nameService, IAsyncCqrsQueryService<CreateOccupant, IOccupant> createService) : base(nameService)
        {
            _createService = createService;
        }

        private readonly IAsyncCqrsQueryService<CreateOccupant, IOccupant> _createService;

        /// <inheritdoc/>
        public override async ValueTask<BuildTaskforce> ReplaceNames(
            BuildTaskforce old,
            CancellationToken cancellationToken)
        {
            var loadNameTasks = old.Occupants
                .Select(o => ReplaceName(o, cancellationToken));
            var occupants = await Task.WhenAll(loadNameTasks);
            var result = old with { Occupants = occupants };

            return result;
        }

        private async Task<IOccupant> ReplaceName(IOccupant occupant, CancellationToken cancellationToken)
        {
            var actualName = await LoadActualName(occupant.CitizenName, cancellationToken);
            var result = await new CreateOccupant(
                actualName,
                occupant.Preference,
                cancellationToken)
                .Using(_createService);

            return result;
        }
    }
}
