﻿using TaskforceGenerator.Application.Queries;
using TaskforceGenerator.Domain.Core.Queries;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Queries;
using TaskforceGenerator.Aspects.Proxies.ReplacementStrategies.Abstractions;

namespace TaskforceGenerator.Aspects.Proxies.ReplacementStrategies
{
    /// <summary>
    /// Name replacement strategy for <see cref="ProxyQuery{TQuery}"/> where <c>TQuery</c> is <see cref="CheckCitizenExists"/>.
    /// </summary>
    public sealed class CheckCitizenExistsStrategy : CitizenNameReplacementStrategyBase<ProxyQuery<CheckCitizenExists>>
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="nameService">The service used to query actual citizen names.</param>
        public CheckCitizenExistsStrategy(IAsyncCqrsQueryService<LoadActualName, LoadActualName.Result> nameService) : base(nameService)
        {
        }
        /// <inheritdoc/>
        public override async ValueTask<ProxyQuery<CheckCitizenExists>> ReplaceNames(
            ProxyQuery<CheckCitizenExists> old,
            CancellationToken cancellationToken)
        {
            var actualName = await LoadActualName(old.Query.CitizenName, cancellationToken);
            var result = old with { Query = old.Query with { CitizenName = actualName } };

            return result;
        }
    }
}
