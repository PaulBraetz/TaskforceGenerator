﻿using TaskforceGenerator.Application.Commands;
using TaskforceGenerator.Aspects.Proxies.ReplacementStrategies.Abstractions;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Queries;

namespace TaskforceGenerator.Aspects.Proxies.ReplacementStrategies
{
    /// <summary>
    /// Name replacement strategy for <see cref="SetPasswordForConnection"/>.
    /// </summary>
    public sealed class SetPasswordForCitizenStrategy : CitizenNameReplacementStrategyBase<SetPasswordForConnection>
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="nameService">The service used to query actual citizen names.</param>
        public SetPasswordForCitizenStrategy(IAsyncCqrsQueryService<LoadActualName, LoadActualName.Result> nameService) : base(nameService)
        {
        }
        /// <inheritdoc/>
        public override async ValueTask<SetPasswordForConnection> ReplaceNames(SetPasswordForConnection old, CancellationToken cancellationToken)
        {
            var actualName = await LoadActualName(old.CitizenName, cancellationToken);
            var result = old with { CitizenName = actualName };

            return result;
        }
    }
}
