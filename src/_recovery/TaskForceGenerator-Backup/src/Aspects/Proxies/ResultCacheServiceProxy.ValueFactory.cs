﻿using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Aspects.Decorators
{
    public sealed partial class ResultCacheServiceProxy<TQuery, TResult> where TQuery : ICqrsQuery
    {
        private sealed class ValueFactory
        {
            private readonly Func<TQuery, Task<TResult>> _factory;

            public ValueFactory(Func<TQuery, Task<TResult>> factory)
            {
                _factory = factory;
            }

            public Boolean FactoryCalled { get; private set; }

            public Task<TResult> CreateValue(TQuery key)
            {
                FactoryCalled = true;
                return _factory.Invoke(key);
            }
        }
    }
}
