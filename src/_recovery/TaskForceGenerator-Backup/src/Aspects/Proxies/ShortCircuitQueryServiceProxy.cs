﻿using TaskforceGenerator.Aspects.Abstractions;
using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Aspects.Proxies
{
    /// <summary>
    /// Adds exception caching functionality to a service.
    /// </summary>
    /// <typeparam name="TQuery">The type of query to execute.</typeparam>
    /// <typeparam name="TResult">The type of result yielded by executing a query.</typeparam>
    /// <typeparam name="TException">The type of exceptions cached.</typeparam>
    public sealed class ShortCircuitQueryServiceProxy<TQuery, TResult, TException> : IAsyncCqrsQueryService<TQuery, TResult>
        where TQuery : ICqrsQuery
        where TException : Exception
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="decorated">The decorated service.</param>
        /// <param name="cache">The cache used to store results.</param>
        public ShortCircuitQueryServiceProxy(IAsyncCqrsQueryService<TQuery, TResult> decorated, ICache<TQuery, ShortCircuitCacheEntry<TException>> cache)
        {
            _decorated = decorated;
            _cache = cache;
        }
        private readonly IAsyncCqrsQueryService<TQuery, TResult> _decorated;
        private readonly ICache<TQuery, ShortCircuitCacheEntry<TException>> _cache;

        /// <inheritdoc/>
        public ValueTask<TResult> Execute(TQuery query)
        {
            var entry = _cache.GetOrAdd(query, k => new ShortCircuitCacheEntry<TException>());
            if (entry.IsSet)
            {
                throw entry.Exception!;
            }

            try
            {
                return _decorated.Execute(query);
            }
            catch (TException ex)
            {
                entry.Set(ex);
                throw ex;
            }
        }
    }
}
