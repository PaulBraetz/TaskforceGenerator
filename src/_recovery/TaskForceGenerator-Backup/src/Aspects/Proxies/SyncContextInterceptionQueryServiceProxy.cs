﻿using TaskforceGenerator.Aspects.Abstractions;
using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Aspects.Proxies
{
    /// <summary>
    /// Proxy for query services that unsets the current synchronization context, executes the query and 
    /// resets the synchronization context to its previous value.
    /// </summary>
    /// <typeparam name="TQuery">The type of command to execute.</typeparam>
    /// <typeparam name="TResult">The type of result yielded by executing a query.</typeparam>
    public sealed class SyncContextInterceptionQueryServiceProxy<TQuery, TResult> : IAsyncCqrsQueryService<TQuery, TResult>
        where TQuery : ICqrsQuery
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="service">The proxied service.</param>
        public SyncContextInterceptionQueryServiceProxy(IAsyncCqrsQueryService<TQuery, TResult> service)
        {
            _service = service;
        }

        private readonly IAsyncCqrsQueryService<TQuery, TResult> _service;
        /// <inheritdoc/>
        public async ValueTask<TResult> Execute(TQuery query)
        {
            var captured = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            try
            {
                var result = await _service.Execute(query);

                return result;
            } finally
            {
                SynchronizationContext.SetSynchronizationContext(captured);
            }
        }
    }
}
