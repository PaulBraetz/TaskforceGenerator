﻿using TaskforceGenerator.Aspects.Abstractions;
using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Queries;

namespace TaskforceGenerator.Aspects.Proxies
{
    /// <summary>
    /// Intercepts commands that pass a citizens name, replacing them with the RSI-registered (actual name) one instead.
    /// </summary>
    /// <typeparam name="TCommand">The type of command to execute.</typeparam>
    public sealed class CitizenNameReplacementCommandProxy<TCommand> : IAsyncCqrsCommandService<TCommand>
        where TCommand : ICqrsCommand
    {
        private readonly IAsyncCqrsCommandService<TCommand> _intercepted;
        private readonly ICitizenNamesReplacementStrategy<TCommand> _replacementStrategy;

        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="intercepted">The service whose commands to intercept.</param>
        /// <param name="replacementStrategy">The strategy to use when analyzing and replacing commands.</param>
        public CitizenNameReplacementCommandProxy(IAsyncCqrsCommandService<TCommand> intercepted,
                                                 ICitizenNamesReplacementStrategy<TCommand> replacementStrategy)
        {
            _intercepted = intercepted;
            _replacementStrategy = replacementStrategy;
        }

        /// <inheritdoc/>
        public async ValueTask Execute(TCommand command)
        {
            var newCommand = await _replacementStrategy.ReplaceNames(command, command.CancellationToken);

            await _intercepted.Execute(newCommand);
        }
    }
}
