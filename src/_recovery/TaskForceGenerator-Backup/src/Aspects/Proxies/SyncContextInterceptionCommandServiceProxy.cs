﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using TaskforceGenerator.Aspects.Abstractions;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Commands;

namespace TaskforceGenerator.Aspects.Proxies
{
    /// <summary>
    /// Proxy for command services that unsets the current synchronization context, executes the command and 
    /// resets the synchronization context to its previous value.
    /// </summary>
    /// <typeparam name="TCommand">The type of command to execute.</typeparam>
    public sealed class SyncContextInterceptionCommandServiceProxy<TCommand> : IAsyncCqrsCommandService<TCommand>
        where TCommand : ICqrsCommand
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="service">The proxied service.</param>
        public SyncContextInterceptionCommandServiceProxy(IAsyncCqrsCommandService<TCommand> service)
        {
            _service = service;
        }

        private readonly IAsyncCqrsCommandService<TCommand> _service;
        /// <inheritdoc/>
        public async ValueTask Execute(TCommand command)
        {
            var captured = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            try
            {
                await _service.Execute(command);
            } finally
            {
                SynchronizationContext.SetSynchronizationContext(captured);
            }
        }
    }
}
