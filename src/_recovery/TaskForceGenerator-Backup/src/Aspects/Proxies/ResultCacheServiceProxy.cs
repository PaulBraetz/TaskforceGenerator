﻿using TaskforceGenerator.Aspects.Abstractions;
using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;

using System.Collections.Concurrent;

using Timer = System.Timers.Timer;
using TaskforceGenerator.Aspects.Logging;

namespace TaskforceGenerator.Aspects.Decorators
{
    /// <summary>
    /// Adds result caching functionality to a service.
    /// </summary>
    /// <typeparam name="TQuery">The type of query to execute.</typeparam>
    /// <typeparam name="TResult">The type of result yielded by executing a query.</typeparam>
    public sealed partial class ResultCacheServiceProxy<TQuery, TResult> : IAsyncCqrsQueryService<TQuery, TResult>
        where TQuery : ICqrsQuery
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="decorated">The decorated service.</param>
        /// <param name="cache">The cache used to store results.</param>
        /// <param name="logger">The logger toi use when logging cache hits.</param>
        public ResultCacheServiceProxy(IAsyncCqrsQueryService<TQuery, TResult> decorated,
                                       ICache<TQuery, Task<TResult>> cache,
                                       ILoggingService logger)
        {
            _decorated = decorated;
            _cache = cache;
            _logger = logger;
        }
        private readonly IAsyncCqrsQueryService<TQuery, TResult> _decorated;
        private readonly ICache<TQuery, Task<TResult>> _cache;
        private readonly ILoggingService _logger;

        /// <inheritdoc/>
        public async ValueTask<TResult> Execute(TQuery query)
        {
            var factory = new ValueFactory(CreateResult);

            using (_ = Logs.LogLate(createLog, _logger))
            {
                return await _cache.GetOrAdd(query, factory.CreateValue);
            }

            ILogEntry createLog() =>
                factory!.FactoryCalled ?
                new ResultCacheMissLogEntry<TQuery>() :
                new ResultCacheHitLogEntry<TQuery>();
        }
        private async Task<TResult> CreateResult(TQuery query)
        {
            var result = await _decorated.Execute(query);
            return result;
        }
    }
}
