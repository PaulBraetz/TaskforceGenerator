﻿using TaskforceGenerator.Aspects.Abstractions;
using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Queries;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace TaskforceGenerator.Aspects.Proxies
{
    /// <summary>
    /// Intercepts queries that pass a citizens name, replacing them with the RSI-registered (actual name) one instead.
    /// </summary>
    /// <typeparam name="TQuery">The type of query to execute.</typeparam>
    /// <typeparam name="TResult">The type of result yielded by executing a query.</typeparam>
    public sealed class CitizenNameReplacementQueryProxy<TQuery, TResult> : IAsyncCqrsQueryService<TQuery, TResult>
        where TQuery : ICqrsQuery
    {
        private readonly IAsyncCqrsQueryService<TQuery, TResult> _intercepted;
        private readonly ICitizenNamesReplacementStrategy<TQuery> _replacementStrategy;

        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="intercepted">The service whose commands to intercept.</param>
        /// <param name="replacementStrategy">The strategy to use when analyzing and replacing queries.</param>
        public CitizenNameReplacementQueryProxy(IAsyncCqrsQueryService<TQuery, TResult> intercepted,
                                                 ICitizenNamesReplacementStrategy<TQuery> replacementStrategy)
        {
            _intercepted = intercepted;
            _replacementStrategy = replacementStrategy;
        }

        /// <inheritdoc/>
        public async ValueTask<TResult> Execute(TQuery command)
        {
            var newCommand = await _replacementStrategy.ReplaceNames(command, command.CancellationToken);
            var result = await _intercepted.Execute(newCommand);

            return result;
        }
    }
}
