﻿using TaskforceGenerator.Application.Commands;
using TaskforceGenerator.Common.Abstractions;
using Microsoft.Extensions.Logging;
using System.Runtime.CompilerServices;
using TaskforceGenerator.Aspects.Logging;
using TaskforceGenerator.Aspects.Abstractions;

namespace TaskforceGenerator.Aspects.Services
{
	/// <summary>
	/// Adapter for logging messages, execution times and exceptions to an instance of <see cref="ILogger"/>.
	/// </summary>
	public sealed class LoggingService : ILoggingService
	{
		private readonly ILogger _logger;

		/// <summary>
		/// Initializes a new instance.
		/// </summary>
		/// <param name="logger">The logger to use.</param>
		public LoggingService(ILogger logger)
		{
			_logger = logger;
		}
		/// <inheritdoc/>
		public void Log(IEnumerable<ILogEntry> logEntries)
		{
			var logGroups = logEntries
				.Where(e => e != null)
				.Select<ILogEntry, (LogLevel level, Func<String> eval)>(e => (e.Level, e.Evaluate))
				.GroupBy(e => e.level)
				.Select(g => (level: g.Key, evals: g.Select(t => t.eval)));

			foreach (var (level, evals) in logGroups)
			{
				_logger.Log(level, default, evals, null, Format);
			}
		}

		private static String Format(IEnumerable<Func<String>> evals, Exception exception)
		{
			var evaluated = evals.Select(e => e.Invoke());
			var result = String.Join('\n', evaluated);

			return result;
		}
		/// <inheritdoc/>
		public void Log(ILogEntry logEntry) =>
			_logger.Log(logEntry.Level, default, logEntry, null, (s, e) => s.Evaluate());
	}
}
