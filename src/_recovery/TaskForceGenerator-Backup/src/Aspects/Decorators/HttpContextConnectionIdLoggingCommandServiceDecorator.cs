﻿using Microsoft.AspNetCore.Http;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TaskforceGenerator.Aspects.Abstractions;
using TaskforceGenerator.Aspects.Logging;
using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Aspects.Decorators
{
    /// <summary>
    /// Decorator for command services that logs the http context connection id before executing the decorated service.
    /// </summary>
    /// <typeparam name="TCommand">The type of command to execute.</typeparam>
    public sealed class HttpContextConnectionIdLoggingCommandServiceDecorator<TCommand> : CommandServiceLoggingDecoratorBase<TCommand>
        where TCommand : ICqrsCommand
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="logger">The service used for logging.</param>
        /// <param name="decorated">The decorated service.</param>
        /// <param name="contextAccessor">The accessor used to obtain the <see cref="HttpContext.Connection"/> and its id.</param>
        public HttpContextConnectionIdLoggingCommandServiceDecorator(
            IAsyncCqrsCommandService<TCommand> decorated,
            IHttpContextAccessor contextAccessor,
            ILoggingService logger) : base(logger, decorated)
        {
            _contextAccessor = contextAccessor;
        }
        private readonly IHttpContextAccessor _contextAccessor;
        /// <inheritdoc/>
        protected override IDisposable GetLogScope(TCommand command)
        {
            ILogEntry logEntry = _contextAccessor.HttpContext != null ?
                new HttpContextConnectionIdLogEntry(_contextAccessor.HttpContext.Connection.Id) :
                new NoHttpContextLogEntry();
            var result = Logs.Log(logEntry, Logger);

            return result;
        }
    }
}
