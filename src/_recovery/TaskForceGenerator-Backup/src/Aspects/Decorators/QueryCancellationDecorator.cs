﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TaskforceGenerator.Aspects.Abstractions;
using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Aspects.Decorators
{
    /// <summary>
    /// Decorator for query services that, if the query to be executed has been cancelled, 
    /// throws an exception of type <see cref="OperationCanceledException"/> before the 
    /// execution of the decorated service.
    /// </summary>
    /// <typeparam name="TQuery">The type of query the decorated service can execute.</typeparam>
    /// <typeparam name="TResult">The type of result yielded by the query service.</typeparam>
    public sealed class QueryCancellationDecorator<TQuery, TResult> : ExecutionPreActionQueryServiceDecoratorBase<TQuery, TResult>
        where TQuery : ICqrsQuery
    {
        /// <summary>
        /// Initializes a new instance. 
        /// </summary>
        /// <param name="decorated">The decorated query service.</param>
        public QueryCancellationDecorator(IAsyncCqrsQueryService<TQuery, TResult> decorated) : base(decorated)
        {
        }

        /// <inheritdoc/>
        protected override ValueTask ExecutePreAction(TQuery value)
        {
            value.CancellationToken.ThrowIfCancellationRequested();
            return ValueTask.CompletedTask;
        }
    }
}
