﻿using TaskforceGenerator.Aspects.Abstractions;
using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Commands;

namespace TaskforceGenerator.Aspects.Decorators
{
    /// <summary>
    /// Decorates a command service with a citizen connection ensuring aspect.
    /// </summary>
    /// <typeparam name="TCommand">The type of query to execute.</typeparam>
    public sealed class EnsureCitizenConnectionCommandServiceDecorator<TCommand> : ExecutionPreActionCommandServiceDecoratorBase<TCommand>
        where TCommand : ICqrsCommand
    {
        private readonly ICitizenNamesExtractionStrategy<TCommand> _extractionStrategy;
        private readonly IAsyncCqrsCommandService<EnsureCitizenConnection> _ensureService;

        /// <summary>
        /// Initializes  a new instance.
        /// </summary>
        /// <param name="extractionStrategy">The strategy used to extract a citizens name from commands.</param>
        /// <param name="ensureService">The service to use when ensuring a connection for the citizen requested exists.</param>
        /// <param name="decorated">The decorated service.</param>
        public EnsureCitizenConnectionCommandServiceDecorator(ICitizenNamesExtractionStrategy<TCommand> extractionStrategy,
                                                              IAsyncCqrsCommandService<EnsureCitizenConnection> ensureService,
                                                              IAsyncCqrsCommandService<TCommand> decorated) : base(decorated)
        {
            _extractionStrategy = extractionStrategy;
            _ensureService = ensureService;
        }
        /// <inheritdoc/>
        protected override async ValueTask ExecutePreAction(TCommand value)
        {
            var replacementTasks = _extractionStrategy.GetNames(value)
                .Select(n => new EnsureCitizenConnection(n, value.CancellationToken).Using(_ensureService).AsTask());
            await Task.WhenAll(replacementTasks);
        }
    }
}
