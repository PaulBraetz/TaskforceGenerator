﻿using TaskforceGenerator.Aspects.Abstractions;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Aspects.Logging;

namespace TaskforceGenerator.Aspects.Decorators
{
	/// <summary>
	/// Exception logging decorator for command services.
	/// </summary>
	/// <typeparam name="TCommand">The type of command to execute and log.</typeparam>
	public sealed class ExceptionLoggingCommandServiceDecorator<TCommand> : CommandServiceLoggingDecoratorBase<TCommand>
		where TCommand : ICqrsCommand
	{
		/// <summary>
		/// Initializes a new instance.
		/// </summary>
		/// <param name="logger">The service used for logging.</param>
		/// <param name="decorated">The decorated service.</param>
		public ExceptionLoggingCommandServiceDecorator(ILoggingService logger, IAsyncCqrsCommandService<TCommand> decorated) : base(logger, decorated)
		{
		}
		
		/// <inheritdoc/>
		public override async ValueTask Execute(TCommand command)
		{
			Exception? exception = null;
			var scope = Logs.LogLateConditional(confirmPush, createLog, Logger);
			try
			{
				//await in order to force exception
				await Decorated.Execute(command);
			}
			catch (Exception ex)
			{
				exception = ex;
				throw;
			}
			finally
			{
				scope.Dispose();
			}

			Boolean confirmPush() => exception!= null;

			ILogEntry createLog()
			{
				var logEntry = new ExceptionLogEntry<TCommand>(exception!);

				return logEntry;
			}
		}
	}
}
