﻿using TaskforceGenerator.Aspects.Abstractions;

using TaskforceGenerator.Aspects.Logging;
using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Aspects.Decorators
{
    /// <summary>
    /// Exception logging decorator for query services.
    /// </summary>
    /// <typeparam name="TQuery">The type of query to execute and log.</typeparam>
    /// <typeparam name="TResult">The type of result to yield and log.</typeparam>
    public sealed class ExceptionLoggingQueryServiceDecorator<TQuery, TResult> : QueryServiceLoggingDecoratorBase<TQuery, TResult>
        where TQuery : ICqrsQuery
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="logger">The service used for logging.</param>
        /// <param name="decorated">The decorated service.</param>
        public ExceptionLoggingQueryServiceDecorator(ILoggingService logger, IAsyncCqrsQueryService<TQuery, TResult> decorated) : base(logger, decorated)
        {
        }

		/// <inheritdoc/>
		public override async ValueTask<TResult> Execute(TQuery query)
		{
			Exception? exception = null;
			var scope = Logs.LogLateConditional(confirmPush, createLog, Logger);
			try
			{
				//await in order to force exception
				var result = await Decorated.Execute(query);

				return result;
			}
			catch (Exception ex)
			{
				exception = ex;
				throw;
			}
			finally
			{
				scope.Dispose();
			}

			Boolean confirmPush() => exception != null;

			ILogEntry createLog()
			{
				var logEntry = new ExceptionLogEntry<TQuery>(exception!);

				return logEntry;
			}
		}
	}
}
