﻿using Microsoft.AspNetCore.Http;

using TaskforceGenerator.Aspects.Abstractions;
using TaskforceGenerator.Aspects.Logging;
using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Aspects.Decorators
{
    /// <summary>
    /// Decorator for query services that logs the http context connection id before executing the decorated service.
    /// </summary>
    /// <typeparam name="TQuery">The type of query to execute.</typeparam>
    /// <typeparam name="TResult">The type of result yielded by executing a query.</typeparam>
    public sealed class HttpContextConnectionIdLoggingQueryServiceDecorator<TQuery, TResult> : QueryServiceLoggingDecoratorBase<TQuery, TResult>
        where TQuery : ICqrsQuery
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="logger">The service used for logging.</param>
        /// <param name="decorated">The decorated service.</param>
        /// <param name="contextAccessor">The accessor used to obtain the <see cref="HttpContext.Connection"/> and its id.</param>
        public HttpContextConnectionIdLoggingQueryServiceDecorator(
            IAsyncCqrsQueryService<TQuery, TResult> decorated,
            IHttpContextAccessor contextAccessor,
            ILoggingService logger) : base(logger, decorated)
        {
            _contextAccessor = contextAccessor;
        }
        private readonly IHttpContextAccessor _contextAccessor;
        /// <inheritdoc/>
        protected override IDisposable GetLogScope(TQuery query)
        {
            ILogEntry logEntry = _contextAccessor.HttpContext!=null?
                new HttpContextConnectionIdLogEntry(_contextAccessor.HttpContext.Connection.Id):
                new NoHttpContextLogEntry();
            var result = Logs.Log(logEntry, Logger);

            return result;
        }
    }
}
