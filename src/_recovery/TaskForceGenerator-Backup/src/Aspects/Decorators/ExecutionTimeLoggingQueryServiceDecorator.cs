﻿using TaskforceGenerator.Aspects.Abstractions;
using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using System.Diagnostics;
using TaskforceGenerator.Aspects.Logging;

namespace TaskforceGenerator.Aspects.Decorators
{
    /// <summary>
    /// Execution time logging decorator for query services.
    /// </summary>
    /// <typeparam name="TQuery">The type of query to execute and log.</typeparam>
    /// <typeparam name="TResult">The type of result to yield and log.</typeparam>
    public sealed class ExecutionTimeLoggingQueryServiceDecorator<TQuery, TResult> : QueryServiceLoggingDecoratorBase<TQuery, TResult>
        where TQuery : ICqrsQuery
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="logger">The service used for logging.</param>
        /// <param name="decorated">The decorated service.</param>
        public ExecutionTimeLoggingQueryServiceDecorator(ILoggingService logger, IAsyncCqrsQueryService<TQuery, TResult> decorated) : base(logger, decorated)
        {
        }

        /// <inheritdoc/>
        protected override async ValueTask<TResult> DecorateInvocation(TQuery arg)
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            using(_ = Logs.LogLate(createLog, Logger))
            {
                var result = await Decorated.Execute(arg);
                return result;
            }

            ILogEntry createLog()
            {
                stopWatch!.Stop();
                var result = new ExecutionTimeLogEntry<TQuery>(stopWatch!.ElapsedTicks);

                return result;
            }
        }
    }
}
