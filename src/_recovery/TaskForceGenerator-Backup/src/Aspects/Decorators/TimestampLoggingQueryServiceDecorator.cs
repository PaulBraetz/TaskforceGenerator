﻿using TaskforceGenerator.Aspects.Abstractions;
using TaskforceGenerator.Aspects.Logging;
using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Aspects.Decorators
{
    /// <summary>
    /// Decorator for query services that logs a timestamp before executing the decorated service.
    /// </summary>
    /// <typeparam name="TQuery">The type of query to execute.</typeparam>
    /// <typeparam name="TResult">The type of result yielded by executing a query.</typeparam>
    public sealed class TimestampLoggingQueryServiceDecorator<TQuery, TResult> : QueryServiceLoggingDecoratorBase<TQuery, TResult>
        where TQuery : ICqrsQuery
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="logger">The service used for logging.</param>
        /// <param name="decorated">The decorated service.</param>
        public TimestampLoggingQueryServiceDecorator(
            IAsyncCqrsQueryService<TQuery, TResult> decorated,
            ILoggingService logger) : base(logger, decorated)
        {
        }
        /// <inheritdoc/>
        protected override IDisposable GetLogScope(TQuery query)
        {
            var logEntry = TimeStampLogEntry.Now;
            var result = Logs.Log(logEntry, Logger);

            return result;
        }
    }
}
