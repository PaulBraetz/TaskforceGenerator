﻿using TaskforceGenerator.Aspects.Abstractions;
using TaskforceGenerator.Aspects.Logging;
using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Aspects.Decorators
{
    /// <summary>
    /// Logger for logging the current thread id at query execution time.
    /// </summary>
    /// <typeparam name="TQuery">The type of query executed.</typeparam>
    /// <typeparam name="TResult">The type of result yielded by executing a query.</typeparam>
    public sealed class ThreadIdLoggingQueryServiceDecorator<TQuery, TResult> : QueryServiceLoggingDecoratorBase<TQuery, TResult>
        where TQuery : ICqrsQuery
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="logger">The service used for logging.</param>
        /// <param name="decorated">The decorated service.</param>
        public ThreadIdLoggingQueryServiceDecorator(ILoggingService logger, IAsyncCqrsQueryService<TQuery, TResult> decorated) : base(logger, decorated)
        {
        }

        /// <inheritdoc/>
        protected override IDisposable GetLogScope(TQuery query)
        {
            var logEntry = ThreadIdLogEntry.Current;
            var result = Logs.Log(logEntry, Logger);

            return result;
        }
    }
}
