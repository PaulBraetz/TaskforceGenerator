﻿using TaskforceGenerator.Application.Commands;
using TaskforceGenerator.Aspects.Abstractions;

namespace TaskforceGenerator.Aspects.Decorators.ExtractionStrategies
{
    /// <summary>
    /// Name extraction strategy for <see cref="SetPasswordForConnection"/>.
    /// </summary>
    public sealed class SetPasswordForCitizenStrategy : ICitizenNamesExtractionStrategy<SetPasswordForConnection>
    {
        /// <summary>
        /// Initializes a new instance. If possible, use <see cref="Instance"/> instead.
        /// </summary>
        public SetPasswordForCitizenStrategy() { }
        /// <summary>
        /// Gets the singleton instance.
        /// </summary>
        public static SetPasswordForCitizenStrategy Instance { get; } = new();
        /// <inheritdoc/>
        public IEnumerable<String> GetNames(SetPasswordForConnection value) =>
            new[] { value.CitizenName };
    }
}
