﻿using TaskforceGenerator.Application.Commands;
using TaskforceGenerator.Aspects.Abstractions;
using TaskforceGenerator.Common;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Aspects.Decorators.ExtractionStrategies
{
    /// <summary>
    /// Name extraction strategy for <see cref="Login"/>.
    /// </summary>
    public sealed class LoginStrategy : ICitizenNamesExtractionStrategy<Login>
    {
        /// <summary>
        /// Initializes a new instance. If possible, use <see cref="Instance"/> instead.
        /// </summary>
        public LoginStrategy() { }
        /// <summary>
        /// Gets the singleton instance.
        /// </summary>
        public static LoginStrategy Instance { get; } = new();
        /// <inheritdoc/>
        public IEnumerable<String> GetNames(Login value) =>
            new[] { value.CitizenName };
    }
}
