﻿using TaskforceGenerator.Aspects.Abstractions;
using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Commands;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskforceGenerator.Domain.Core.Commands;

namespace TaskforceGenerator.Aspects.Decorators
{
    /// <summary>
    /// Decorates a query service with a citizen connection ensuring aspect.
    /// </summary>
    /// <typeparam name="TQuery">The type of query to execute.</typeparam>
    /// <typeparam name="TResult">The type of result yielded by executing a query.</typeparam>
    public sealed class EnsureCitizenConnectionQueryServiceDecorator<TQuery, TResult> : ExecutionPreActionQueryServiceDecoratorBase<TQuery, TResult>
        where TQuery : ICqrsQuery
    {
        private readonly ICitizenNamesExtractionStrategy<TQuery> _extractionStrategy;
        private readonly IAsyncCqrsCommandService<EnsureCitizenConnection> _ensureService;
        /// <summary>
        /// Initializes  a new instance.
        /// </summary>
        /// <param name="extractionStrategy">The strategy used to extract a citizens name form queries.</param>
        /// <param name="ensureService">The service to use when ensuring a connection for the citizen requested exists.</param>
        /// <param name="decorated">The decorated service.</param>
        public EnsureCitizenConnectionQueryServiceDecorator(ICitizenNamesExtractionStrategy<TQuery> extractionStrategy,
                                                              IAsyncCqrsCommandService<EnsureCitizenConnection> ensureService,
                                                              IAsyncCqrsQueryService<TQuery, TResult> decorated) : base(decorated)
        {
            _extractionStrategy = extractionStrategy;
            _ensureService = ensureService;
        }
        /// <inheritdoc/>
        protected override async ValueTask ExecutePreAction(TQuery value)
        {
            var replacementTasks = _extractionStrategy.GetNames(value)
                .Select(n => new EnsureCitizenConnection(n, value.CancellationToken).Using(_ensureService).AsTask());
            await Task.WhenAll(replacementTasks);
        }
    }
}
