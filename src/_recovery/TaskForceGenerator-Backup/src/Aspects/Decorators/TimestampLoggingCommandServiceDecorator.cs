﻿using TaskforceGenerator.Aspects.Abstractions;
using TaskforceGenerator.Aspects.Logging;
using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Aspects.Decorators
{
    /// <summary>
    /// Decorator for command services that logs a timestamp before executing the decorated service.
    /// </summary>
    /// <typeparam name="TCommand">The type of command to execute.</typeparam>
    public sealed class TimestampLoggingCommandServiceDecorator<TCommand> : CommandServiceLoggingDecoratorBase<TCommand>
        where TCommand : ICqrsCommand
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="logger">The service used for logging.</param>
        /// <param name="decorated">The decorated service.</param>
        public TimestampLoggingCommandServiceDecorator(
            IAsyncCqrsCommandService<TCommand> decorated,
            ILoggingService logger) : base(logger, decorated)
        {
        }
        /// <inheritdoc/>
        protected override IDisposable GetLogScope(TCommand command)
        {
            var logEntry = TimeStampLogEntry.Now;
            var result = Logs.Log(logEntry, Logger);

            return result;
        }
    }
}
