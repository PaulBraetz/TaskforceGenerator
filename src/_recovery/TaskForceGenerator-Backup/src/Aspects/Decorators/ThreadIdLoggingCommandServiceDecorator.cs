﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TaskforceGenerator.Aspects.Abstractions;
using TaskforceGenerator.Aspects.Logging;
using TaskforceGenerator.Common.Abstractions;

using static System.Formats.Asn1.AsnWriter;

namespace TaskforceGenerator.Aspects.Decorators
{
    /// <summary>
    /// Logger for logging the current thread id at command execution time.
    /// </summary>
    /// <typeparam name="TCommand">The type of command to execute.</typeparam>
    public sealed class ThreadIdLoggingCommandServiceDecorator<TCommand> : CommandServiceLoggingDecoratorBase<TCommand>
        where TCommand : ICqrsCommand
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="logger">The service used for logging.</param>
        /// <param name="decorated">The decorated service.</param>
        public ThreadIdLoggingCommandServiceDecorator(ILoggingService logger, IAsyncCqrsCommandService<TCommand> decorated) : base(logger, decorated)
        {
        }
        /// <inheritdoc/>
        protected override IDisposable GetLogScope(TCommand command)
        {
            var logEntry = ThreadIdLogEntry.Current;
            var result = Logs.Log(logEntry, Logger);

            return result;
        }
    }
}
