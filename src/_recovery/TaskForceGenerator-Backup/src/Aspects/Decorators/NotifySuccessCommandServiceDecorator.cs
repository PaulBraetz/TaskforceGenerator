﻿using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Aspects.Decorators
{
    /// <summary>
    /// Decorates a command service with event publishing functionality.
    /// </summary>
    /// <typeparam name="TCommand">The type of command to execute.</typeparam>
    public sealed class NotifySuccessCommandServiceDecorator<TCommand> : IAsyncCqrsCommandService<TCommand>
        where TCommand : ICqrsCommand
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="decorated">The service decorated.</param>
        /// <param name="observer">The observer notified about successful executions.</param>
        public NotifySuccessCommandServiceDecorator(IAsyncCqrsCommandService<TCommand> decorated, Common.Abstractions.IObserver<TCommand> observer)
        {
            _decorated = decorated;
            _observer = observer;
        }

        private readonly IAsyncCqrsCommandService<TCommand> _decorated;
        private readonly Common.Abstractions.IObserver<TCommand> _observer;

        /// <inheritdoc/>
        public async ValueTask Execute(TCommand command)
        {
            await _decorated.Execute(command);
            _observer.Notify(command);
        }
    }
}
