﻿using TaskforceGenerator.Aspects.Abstractions;
using TaskforceGenerator.Aspects.Logging;
using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Aspects.Decorators
{
    /// <summary>
    /// Logging decorator for query services.
    /// </summary>
    /// <typeparam name="TQuery">The type of query to execute and log.</typeparam>
    /// <typeparam name="TResult">The type of result to yield and log.</typeparam>
    public sealed class ExecutionLoggingQueryServiceDecorator<TQuery, TResult> : QueryServiceLoggingDecoratorBase<TQuery, TResult>
        where TQuery : ICqrsQuery
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="logger">The logger to use for logging execution status.</param>
        /// <param name="decorated">The decorated service.</param>
        /// <param name="formatter">The formatter to use when determining the logged query value.</param>
        /// <param name="resultFormatter">The formatter to use when determining the logged result value.</param>
        public ExecutionLoggingQueryServiceDecorator(ILoggingService logger,
                                            IAsyncCqrsQueryService<TQuery, TResult> decorated,
                                            IStaticFormatter<TQuery> formatter,
                                            IStaticFormatter<TResult> resultFormatter) : base(logger, decorated)
        {
            _queryFormatter = formatter;
            _resultFormatter = resultFormatter;
        }

        private readonly IStaticFormatter<TQuery> _queryFormatter;
        private readonly IStaticFormatter<TResult> _resultFormatter;

        private static readonly AsyncLocal<(Boolean set, TResult? result)> _localResult = new();
        /// <inheritdoc/>
        protected override async ValueTask<TResult> DecorateInvocation(TQuery query)
        {
            using(_ = Logs.Log(new BeforeExecutionLogEntry<TQuery>(query, _queryFormatter), Logger))
            {
                using(_ = Logs.LogLateConditional(confirmSecondLog, createSecondLog, Logger))
                {
                    var result = await Decorated.Execute(query);

                    _localResult.Value = (true, result);

                    return result;
                }
            }

            Boolean confirmSecondLog() => _localResult.Value.set;
            ILogEntry createSecondLog()
            {
                var queryResult = _localResult.Value.result;
                _localResult.Value = (false, default);
                var result = new AfterQueryExecutionLogEntry<TQuery, TResult>(queryResult, _resultFormatter);

                return result;
            }
        }
    }
}
