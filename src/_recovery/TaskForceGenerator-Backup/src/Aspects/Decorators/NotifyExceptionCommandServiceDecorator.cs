﻿using TaskforceGenerator.Common.Abstractions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Aspects.Decorators
{
    /// <summary>
    /// Decorates a command service with event publishing functionality.
    /// </summary>
    /// <typeparam name="TCommand">The type of command to execute.</typeparam>
    public sealed class NotifyExceptionCommandServiceDecorator<TCommand> : IAsyncCqrsCommandService<TCommand>
        where TCommand : ICqrsCommand
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="decorated">The service decorated.</param>
        /// <param name="observer">The observer notified about exceptions.</param>
        public NotifyExceptionCommandServiceDecorator(IAsyncCqrsCommandService<TCommand> decorated, Common.Abstractions.IObserver<Exception> observer)
        {
            _decorated = decorated;
            _observer = observer;
        }

        private readonly IAsyncCqrsCommandService<TCommand> _decorated;
        private readonly Common.Abstractions.IObserver<Exception> _observer;

        /// <inheritdoc/>
        public async ValueTask Execute(TCommand command)
        {
            try
            {
                await _decorated.Execute(command);
            } catch(Exception ex)
            {
                _observer.Notify(ex);
                throw;
            }
        }
    }
}
