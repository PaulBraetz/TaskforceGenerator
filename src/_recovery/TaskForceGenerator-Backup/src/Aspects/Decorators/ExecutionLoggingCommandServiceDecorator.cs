﻿using TaskforceGenerator.Aspects.Abstractions;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Aspects.Logging;
using Microsoft.Extensions.Logging;

namespace TaskforceGenerator.Aspects.Decorators
{
    /// <summary>
    /// Logging decorator for command services.
    /// </summary>
    /// <typeparam name="TCommand">The type of command to execute and log.</typeparam>
    public sealed class ExecutionLoggingCommandServiceDecorator<TCommand> : CommandServiceLoggingDecoratorBase<TCommand>
        where TCommand : ICqrsCommand
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="logger">The logger to use for logging execution status.</param>
        /// <param name="decorated">The decorated service.</param>
        /// <param name="formatter">The formatter to use when determining the logged value for commands executed.</param>
        public ExecutionLoggingCommandServiceDecorator(ILoggingService logger,
                                              IAsyncCqrsCommandService<TCommand> decorated,
                                              IStaticFormatter<TCommand> formatter) : base(logger, decorated)
        {
            _formatter = formatter;
        }

        private readonly IStaticFormatter<TCommand> _formatter;

        /// <inheritdoc/>
        protected override async ValueTask DecorateInvocation(TCommand arg)
        {
            using(_ = Logs.Log(new BeforeExecutionLogEntry<TCommand>(arg, _formatter), Logger))
            {
                using(_ = Logs.LogLate(createSecondLog, Logger))
                {
                    await Decorated.Execute(arg);
                }
            }

            static ILogEntry createSecondLog() =>
                new AfterCommandExecutionLogEntry<TCommand>();
        }
    }
}
