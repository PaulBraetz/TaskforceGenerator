﻿using TaskforceGenerator.Aspects.Abstractions;
using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Commands;
using TaskforceGenerator.Domain.Core.Commands;

namespace TaskforceGenerator.Aspects.Decorators
{
    /// <summary>
    /// Decorates a query service with a citizen ensuring aspect.
    /// </summary>
    /// <typeparam name="TQuery">The type of query to execute.</typeparam>
    /// <typeparam name="TResult">The type of result yielded by executing a query.</typeparam>
    public sealed class EnsureCitizenQueryServiceDecorator<TQuery, TResult> : ExecutionPreActionQueryServiceDecoratorBase<TQuery, TResult>
        where TQuery : ICqrsQuery
    {
        private readonly ICitizenNamesExtractionStrategy<TQuery> _extractionStrategy;
        private readonly IAsyncCqrsCommandService<EnsureCitizen> _ensureService;
        /// <summary>
        /// Initializes  a new instance.
        /// </summary>
        /// <param name="extractionStrategy">The strategy used to extract a citizens name form queries.</param>
        /// <param name="ensureService">The service to use when ensuring a connection for the citizen requested exists.</param>
        /// <param name="decorated">The decorated service.</param>
        public EnsureCitizenQueryServiceDecorator(ICitizenNamesExtractionStrategy<TQuery> extractionStrategy,
                                                              IAsyncCqrsCommandService<EnsureCitizen> ensureService,
                                                              IAsyncCqrsQueryService<TQuery, TResult> decorated) : base(decorated)
        {
            _extractionStrategy = extractionStrategy;
            _ensureService = ensureService;
        }
        /// <inheritdoc/>
        protected override async ValueTask ExecutePreAction(TQuery value)
        {
            var replacementTasks = _extractionStrategy.GetNames(value)
                .Select(n => new EnsureCitizen(n, value.CancellationToken).Using(_ensureService).AsTask());
            await Task.WhenAll(replacementTasks);
        }
    }
}
