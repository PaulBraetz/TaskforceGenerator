﻿using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Aspects.Decorators
{
    /// <summary>
    /// Decorates a query service with event publishing functionality.
    /// </summary>
    /// <typeparam name="TQuery">The type of query to execute.</typeparam>
    /// <typeparam name="TResult">The type of result yielded by executing a query.</typeparam>
    public sealed class NotifyExceptionQueryServiceDecorator<TQuery, TResult> : IAsyncCqrsQueryService<TQuery, TResult>
        where TQuery : ICqrsQuery
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="decorated">The service decorated.</param>
        /// <param name="observer">The observer notified about exceptions.</param>
        public NotifyExceptionQueryServiceDecorator(IAsyncCqrsQueryService<TQuery, TResult> decorated, Common.Abstractions.IObserver<Exception> observer)
        {
            _decorated = decorated;
            _observer = observer;
        }

        private readonly IAsyncCqrsQueryService<TQuery, TResult> _decorated;
        private readonly Common.Abstractions.IObserver<Exception> _observer;

        /// <inheritdoc/>
        public async ValueTask<TResult> Execute(TQuery command)
        {
            try
            {
                var result = await _decorated.Execute(command);
                return result;
            } catch(Exception ex)
            {
                _observer.Notify(ex);
                throw;
            }
        }
    }
}
