﻿using TaskforceGenerator.Application.Commands;
using TaskforceGenerator.Aspects.Abstractions;
using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using System.Diagnostics;
using TaskforceGenerator.Aspects.Logging;

namespace TaskforceGenerator.Aspects.Decorators
{
    /// <summary>
    /// Execution time logging decorator for command services.
    /// </summary>
    /// <typeparam name="TCommand">The type of command to execute and log.</typeparam>
    public sealed class ExecutionTimeLoggingCommandServiceDecorator<TCommand> : CommandServiceLoggingDecoratorBase<TCommand>
        where TCommand : ICqrsCommand
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="logger">The service used for logging.</param>
        /// <param name="decorated">The decorated service.</param>
        public ExecutionTimeLoggingCommandServiceDecorator(ILoggingService logger, IAsyncCqrsCommandService<TCommand> decorated) : base(logger, decorated)
        {
        }

        /// <inheritdoc/>
        protected override async ValueTask DecorateInvocation(TCommand arg)
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            using(_ = Logs.LogLate(createLog, Logger))
            {
                await Decorated.Execute(arg);
            }

            ILogEntry createLog()
            {
                stopWatch!.Stop();
                var result = new ExecutionTimeLogEntry<TCommand>(stopWatch!.ElapsedTicks);

                return result;
            }
        }
    }
}
