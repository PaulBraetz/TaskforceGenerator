using TaskforceGenerator.Common;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Authentication.Queries;
using TaskforceGenerator.Domain.Authentication.Services;
using TaskforceGenerator.Tests.Common;

namespace Domain.Tests
{
    /// <summary>
    /// Contains tests for <see cref="CreateConnectionService"/>.
    /// </summary>
    [TestClass]
    [TestCategory("Unit Domain")]
    public class CreateConnectionServiceTests : ServiceTestBase<CreateConnectionService>
    {
        private static Object[][] Citizens => Data.Connections;

        /// <summary>
        /// Tests whether the service creates a connection with the parameters provided.
        /// </summary>
        /// <param name="connection">The connection whose properties to use for creating a new connection.</param>
        [TestMethod]
        [DynamicData(nameof(Citizens))]
        public async Task CreatesConnection(ICitizenConnection connection)
        {
            var created = await new CreateConnection(connection.CitizenName, connection.Code, connection.Password, CancellationToken.None).Using(Service);

            Assertions.AreEqual(connection, created);
        }
    }
}