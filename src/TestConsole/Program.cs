﻿internal class Program
{
    private static async Task Main(String[] args)
    {
        var connectionString = "Data Source=memdb;Mode=Memory;Cache=Shared";

        var root = new ProgramTransaction("root");
        await using(root)
        {
            var child1 = await SqliteTransaction.CreateRoot(connectionString, "child1");
            await root.Attach(child1);
            await using(child1)
            {
                var grandChild1 = await child1.CreateChild("grandchild_1.1");
                var grandChild3 = new ProgramTransaction("grandchild_1.3");
                var grandChild4 = new ProgramTransaction("grandchild_1.4");
                await child1.Attach(grandChild3);
                await child1.Attach(grandChild4);

                await using(grandChild1)
                {
                    await grandChild1.Commit();
                }

                var grandChild2 = await child1.CreateChild("grandchild_1.2");
                await using(grandChild2)
                {
                    //await grandChild2.Commit();
                }

                await using(grandChild3)
                {
                    await grandChild3.Commit();
                }

                await using(grandChild4)
                {
                    //await grandChild4.Commit();
                }

                //await child1.Commit();
            }

            var child2 = await root.CreateChild("child2");
            await using(child2)
            {
                var grandChild1 = await child2.CreateChild("grandchild_2.1");
                var grandChild2 = await child2.CreateChild("grandchild_2.2");
                var grandChild3 = await child2.CreateChild("grandchild_2.3");

                await using(grandChild1)
                {
                    await grandChild1.Commit();
                }

                await using(grandChild2)
                {
                    await grandChild2.Commit();
                }

                await using(grandChild3)
                {
                    await grandChild3.Commit();
                }

                //await child2.Commit();
            }

            await root.Commit();
        }

        Console.WriteLine("-");
    }
}