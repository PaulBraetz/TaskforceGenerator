using TaskforceGenerator.Domain.Core;
using TaskforceGenerator.Domain.Core.Abstractions;
using TaskforceGenerator.Tests.Common;

namespace TaskforceGenerator.Domain.Tests;

/// <summary>
/// Contains tests for <see cref="CrewOptimalScheme"/>.
/// </summary>
[TestClass]
public class CrewOptimalSchemeTests
{
    private static Object[][] InputsOnly
    {
        get
        {
            return new Object[][]
            {
                new Object[]
                {
                    new IVehicle[]
                    {
                        Vehicle.Redeemer
                    },
                    new IOccupant[]
                    {
                        new Occupant("SleepWellPupper",SlotPreference.Gunner),
                        new Occupant("YokoArashi",SlotPreference.Pilot),
                        new Occupant("Mar10",SlotPreference.Gunner)
                    }
                },
                new Object[]
                {
                    new IVehicle[]
                    {
                        Vehicle.Redeemer
                    },
                    new IOccupant[]
                    {
                        new Occupant("SleepWellPupper",SlotPreference.Pilot),
                        new Occupant("YokoArashi",SlotPreference.Pilot),
                        new Occupant("Mar10",SlotPreference.Gunner)
                    }
                },
                new Object[]
                {
                    new IVehicle[]
                    {
                        Vehicle.Redeemer,
                        Vehicle.Redeemer
                    },
                    new IOccupant[]
                    {
                        new Occupant("SleepWellPupper",SlotPreference.Pilot),
                        new Occupant("YokoArashi",SlotPreference.Pilot),
                        new Occupant("Mar10",SlotPreference.Gunner)
                    }
                },
                new Object[]
                {
                    new IVehicle[]
                    {
                        Vehicle.Redeemer,
                        Vehicle.Redeemer,
                        Vehicle.Redeemer,
                        Vehicle.Redeemer
                    },
                    new IOccupant[]
                    {
                        new Occupant("SleepWellPupper",SlotPreference.Pilot),
                        new Occupant("YokoArashi",SlotPreference.Gunner),
                        new Occupant("Mar10",SlotPreference.Gunner),
                        new Occupant("Citizen_0",SlotPreference.Pilot),
                        new Occupant("Citizen_1",SlotPreference.Gunner),
                        new Occupant("Citizen_2",SlotPreference.Pilot),
                        new Occupant("Citizen_3",SlotPreference.Gunner),
                        new Occupant("Citizen_4",SlotPreference.Gunner),
                        new Occupant("Citizen_5",SlotPreference.Gunner)
                    }
                },
                new Object[]
                {
                    new IVehicle[]
                    {
                        Vehicle.Redeemer,
                        Vehicle.Redeemer,
                        Vehicle.Redeemer,
                        Vehicle.Redeemer
                    },
                    new IOccupant[]
                    {
                        new Occupant("SleepWellPupper",SlotPreference.Pilot),
                        new Occupant("YokoArashi",SlotPreference.Gunner),
                        new Occupant("Mar10",SlotPreference.Gunner),
                        new Occupant("Citizen_0",SlotPreference.Pilot),
                        new Occupant("Citizen_1",SlotPreference.Pilot),
                        new Occupant("Citizen_2",SlotPreference.Pilot),
                        new Occupant("Citizen_3",SlotPreference.Gunner),
                        new Occupant("Citizen_4",SlotPreference.Gunner),
                        new Occupant("Citizen_5",SlotPreference.Gunner)
                    }
                },
                new Object[]
                {
                    new IVehicle[]
                    {
                        Vehicle.Redeemer,
                        Vehicle.Pisces
                    },
                    new IOccupant[]
                    {
                        new Occupant("SleepWellPupper",SlotPreference.Pilot),
                        new Occupant("YokoArashi",SlotPreference.Gunner),
                        new Occupant("Mar10",SlotPreference.Pilot),
                        new Occupant("Citizen_0",SlotPreference.Passenger),
                        new Occupant("Citizen_1",SlotPreference.Passenger),
                        new Occupant("Citizen_2",SlotPreference.Gunner),
                        new Occupant("Citizen_3",SlotPreference.Passenger),
                        new Occupant("Citizen_4",SlotPreference.Passenger),
                        new Occupant("Citizen_5",SlotPreference.Passenger)
                    }
                },
                new Object[]
                {
                    new IVehicle[]
                    {
                        Vehicle.Redeemer,
                        Vehicle.Redeemer,
                        Vehicle.Redeemer,
                        Vehicle.Redeemer
                    },
                    new IOccupant[]
                    {
                        new Occupant("SleepWellPupper",SlotPreference.Pilot),
                        new Occupant("YokoArashi",SlotPreference.Pilot),
                        new Occupant("Mar10",SlotPreference.Gunner),
                        new Occupant("Citizen_0",SlotPreference.Pilot),
                        new Occupant("Citizen_1",SlotPreference.Pilot),
                        new Occupant("Citizen_2",SlotPreference.Pilot),
                        new Occupant("Citizen_3",SlotPreference.Gunner),
                        new Occupant("Citizen_4",SlotPreference.Gunner),
                        new Occupant("Citizen_5",SlotPreference.Gunner)
                    }
                }
            };
        }
    }
    private static Object[][] InputsAndMaxIgnoredOccupants
    {
        get
        {
            return new Object[][]
            {
                new Object[]
                {
                    0,
                    new IVehicle[]
                    {
                        Vehicle.Redeemer
                    },
                    new IOccupant[]
                    {
                        new Occupant("SleepWellPupper",SlotPreference.Gunner),
                        new Occupant("YokoArashi",SlotPreference.Pilot),
                        new Occupant("Mar10",SlotPreference.Gunner)
                    }
                },
                new Object[]
                {
                    0,
                    new IVehicle[]
                    {
                        Vehicle.Redeemer
                    },
                    new IOccupant[]
                    {
                        new Occupant("SleepWellPupper",SlotPreference.Passenger),
                        new Occupant("YokoArashi",SlotPreference.Pilot),
                    }
                },
                new Object[]
                {
                    1,
                    new IVehicle[]
                    {
                        Vehicle.Pisces
                    },
                    new IOccupant[]
                    {
                        new Occupant("SleepWellPupper",SlotPreference.Gunner)
                    }
                },
                new Object[]
                {
                    0,
                    new IVehicle[]
                    {
                        Vehicle.Redeemer
                    },
                    new IOccupant[]
                    {
                        new Occupant("YokoArashi",SlotPreference.Pilot)
                    }
                },
                new Object[]
                {
                    1,
                    new IVehicle[]
                    {
                        Vehicle.Redeemer
                    },
                    new IOccupant[]
                    {
                        new Occupant("YokoArashi",SlotPreference.Passenger)
                    }
                },
                new Object[]
                {
                    1,
                    new IVehicle[]
                    {
                        Vehicle.Redeemer
                    },
                    new IOccupant[]
                    {
                        new Occupant("SleepWellPupper",SlotPreference.Pilot),
                        new Occupant("YokoArashi",SlotPreference.Pilot),
                        new Occupant("Mar10",SlotPreference.Gunner)
                    }
                },
                new Object[]
                {
                    0,
                    new IVehicle[]
                    {
                        Vehicle.Redeemer,
                        Vehicle.Redeemer
                    },
                    new IOccupant[]
                    {
                        new Occupant("SleepWellPupper",SlotPreference.Pilot),
                        new Occupant("YokoArashi",SlotPreference.Pilot),
                        new Occupant("Mar10",SlotPreference.Gunner)
                    }
                },
                new Object[]
                {
                    0,
                    new IVehicle[]
                    {
                        Vehicle.Redeemer,
                        Vehicle.Redeemer,
                        Vehicle.Redeemer,
                        Vehicle.Redeemer
                    },
                    new IOccupant[]
                    {
                        new Occupant("SleepWellPupper",SlotPreference.Pilot),
                        new Occupant("YokoArashi",SlotPreference.Gunner),
                        new Occupant("Mar10",SlotPreference.Gunner),
                        new Occupant("Citizen_0",SlotPreference.Pilot),
                        new Occupant("Citizen_1",SlotPreference.Gunner),
                        new Occupant("Citizen_2",SlotPreference.Pilot),
                        new Occupant("Citizen_3",SlotPreference.Gunner),
                        new Occupant("Citizen_4",SlotPreference.Gunner),
                        new Occupant("Citizen_5",SlotPreference.Gunner)
                    }
                },
                new Object[]
                {
                    0,
                    new IVehicle[]
                    {
                        Vehicle.Redeemer,
                        Vehicle.Pisces
                    },
                    new IOccupant[]
                    {
                        new Occupant("SleepWellPupper",SlotPreference.Pilot),
                        new Occupant("YokoArashi",SlotPreference.Gunner),
                        new Occupant("Mar10",SlotPreference.Pilot),
                        new Occupant("Citizen_0",SlotPreference.Passenger),
                        new Occupant("Citizen_1",SlotPreference.Passenger),
                        new Occupant("Citizen_2",SlotPreference.Gunner),
                        new Occupant("Citizen_3",SlotPreference.Passenger),
                        new Occupant("Citizen_4",SlotPreference.Passenger),
                        new Occupant("Citizen_5",SlotPreference.Passenger)
                    }
                },
                new Object[]
                {
                    1,
                    new IVehicle[]
                    {
                        Vehicle.Redeemer,
                        Vehicle.Pisces
                    },
                    new IOccupant[]
                    {
                        new Occupant("SleepWellPupper",SlotPreference.Pilot),
                        new Occupant("YokoArashi",SlotPreference.Gunner),
                        new Occupant("Mar10",SlotPreference.Pilot),
                        new Occupant("Citizen_0",SlotPreference.Pilot),
                        new Occupant("Citizen_1",SlotPreference.Passenger),
                        new Occupant("Citizen_2",SlotPreference.Gunner),
                        new Occupant("Citizen_3",SlotPreference.Passenger),
                        new Occupant("Citizen_4",SlotPreference.Passenger),
                        new Occupant("Citizen_5",SlotPreference.Passenger)
                    }
                },
                new Object[]
                {
                    2,
                    new IVehicle[]
                    {
                        Vehicle.Redeemer,
                        Vehicle.Pisces
                    },
                    new IOccupant[]
                    {
                        new Occupant("SleepWellPupper",SlotPreference.Pilot),
                        new Occupant("YokoArashi",SlotPreference.Gunner),
                        new Occupant("Mar10",SlotPreference.Pilot),
                        new Occupant("Citizen_0",SlotPreference.Gunner),
                        new Occupant("Citizen_1",SlotPreference.Passenger),
                        new Occupant("Citizen_2",SlotPreference.Gunner),
                        new Occupant("Citizen_3",SlotPreference.Passenger),
                        new Occupant("Citizen_4",SlotPreference.Passenger),
                        new Occupant("Citizen_5",SlotPreference.Pilot)
                    }
                },
                new Object[]
                {
                    0,
                    new IVehicle[]
                    {
                        Vehicle.Redeemer,
                        Vehicle.Redeemer,
                        Vehicle.Redeemer,
                        Vehicle.Redeemer
                    },
                    new IOccupant[]
                    {
                        new Occupant("SleepWellPupper",SlotPreference.Pilot),
                        new Occupant("YokoArashi",SlotPreference.Gunner),
                        new Occupant("Mar10",SlotPreference.Gunner),
                        new Occupant("Citizen_0",SlotPreference.Pilot),
                        new Occupant("Citizen_1",SlotPreference.Pilot),
                        new Occupant("Citizen_2",SlotPreference.Pilot),
                        new Occupant("Citizen_3",SlotPreference.Gunner),
                        new Occupant("Citizen_4",SlotPreference.Gunner),
                        new Occupant("Citizen_5",SlotPreference.Gunner)
                    }
                },
                new Object[]
                {
                    1,
                    new IVehicle[]
                    {
                        Vehicle.Redeemer,
                        Vehicle.Redeemer,
                        Vehicle.Redeemer,
                        Vehicle.Redeemer
                    },
                    new IOccupant[]
                    {
                        new Occupant("SleepWellPupper",SlotPreference.Pilot),
                        new Occupant("YokoArashi",SlotPreference.Pilot),
                        new Occupant("Mar10",SlotPreference.Gunner),
                        new Occupant("Citizen_0",SlotPreference.Pilot),
                        new Occupant("Citizen_1",SlotPreference.Pilot),
                        new Occupant("Citizen_2",SlotPreference.Pilot),
                        new Occupant("Citizen_3",SlotPreference.Gunner),
                        new Occupant("Citizen_4",SlotPreference.Gunner),
                        new Occupant("Citizen_5",SlotPreference.Gunner)
                    }
                }
            };
        }
    }

    /// <summary>
    /// Asserts that the amount of occupied slots created by the scheme matches the amount of occupants passed.
    /// </summary>
    /// <param name="inputVehicles">The vehicles to pass to the scheme.</param>
    /// <param name="inputOccupants">The occupants to pass to the scheme.</param>
    [TestMethod]
    [DynamicData(nameof(InputsOnly))]
    public void OutputOccupantCountEqualsInputOccupantCount(IVehicle[] inputVehicles, IOccupant[] inputOccupants)
    {
        //Arrange
        var scheme = CreateScheme();

        //Act
        var taskforce = scheme.CrewVehicles(inputVehicles, inputOccupants);
        var actual = taskforce.SelectMany(v => v.OccupiedSlots)
            .Select(s => s.Occupant)
            .Count();
        var expected = inputOccupants.Length;

        //Assert
        Assert.AreEqual(expected, actual);
    }

    /// <summary>
    /// Asserts that the crewed vehicles created by the algorithm are a subset of the vehicles passed. 
    /// </summary>
    /// <param name="inputVehicles">The vehicles to pass to the scheme.</param>
    /// <param name="inputOccupants">The occupants to pass to the scheme.</param>
    [TestMethod]
    [DynamicData(nameof(InputsOnly))]
    public void OutputVehiclesAreSubsetOfInputVehicles(IVehicle[] inputVehicles, IOccupant[] inputOccupants)
    {
        //Arrange
        var scheme = CreateScheme();

        //Act
        var taskforce = scheme.CrewVehicles(inputVehicles, inputOccupants);
        var actualCounts = taskforce.GroupBy(v => v.Name)
            .Select(g => (name: g.Key, amount: g.Count()));
        var expectedCounts = inputVehicles.GroupBy(v => v.Name)
            .Select(g => (name: g.Key, amount: g.Count()))
            .ToDictionary(t => t.name, t => t.amount);
        var condition = actualCounts.All(t => expectedCounts[t.name] >= t.amount);

        //Assert
        Assert.IsTrue(condition);
    }

    /// <summary>
    /// Asserts that the scheme will produce results whose preference mismatch characteristics do not exceed a known maximum.
    /// </summary>
    /// <param name="maxIgnoredOccupants">The maximum amount of citizens whose preference has been ignored.</param>
    /// <param name="inputVehicles">The vehicles to pass to the scheme.</param>
    /// <param name="inputOccupants">The occupants to pass to the scheme.</param>
    [TestMethod]
    [DynamicData(nameof(InputsAndMaxIgnoredOccupants))]
    public void CrewsVehiclesOptimally(Int32 maxIgnoredOccupants, IVehicle[] inputVehicles, IOccupant[] inputOccupants)
    {
        //Arrange
        var scheme = CreateScheme();

        //Act
        var crewedVehicles = scheme.CrewVehicles(inputVehicles, inputOccupants);
        var ignoredOccupants = crewedVehicles
            .SelectMany(v => v.OccupiedSlots)
            .Where(Extensions.IsPreferenceMismatch)
            .Count();

        //Assert
        Assert.IsTrue(maxIgnoredOccupants >= ignoredOccupants);
    }

    /// <summary>
    /// Asserts that the scheme does not return null values.
    /// </summary>
    /// <param name="inputVehicles">The vehicles to pass to the scheme.</param>
    /// <param name="inputOccupants">The occupants to pass to the scheme.</param>
    [TestMethod]
    [DynamicData(nameof(InputsOnly))]
    public void DoesNotReturnNull(IVehicle[] inputVehicles, IOccupant[] inputOccupants)
    {
        //Arrange
        var scheme = CreateScheme();

        //Act
        var value = scheme.CrewVehicles(inputVehicles, inputOccupants);

        //Assert
        Assert.IsNotNull(value);
    }

    /// <summary>
    /// Asserts that the scheme does not return null values.
    /// </summary>
    /// <param name="inputVehicles">The vehicles to pass to the scheme.</param>
    /// <param name="inputOccupants">The occupants to pass to the scheme.</param>
    [TestMethod]
    [DynamicData(nameof(InputsOnly))]
    public void DoesNotReturnNullVehicles(IVehicle[] inputVehicles, IOccupant[] inputOccupants)
    {
        //Arrange
        var scheme = CreateScheme();

        //Act
        var vehicles = scheme.CrewVehicles(inputVehicles, inputOccupants);

        //Assert
        foreach(var value in vehicles)
            Assert.IsNotNull(value);
    }

    /// <summary>
    /// Asserts that the scheme does not return null values.
    /// </summary>
    /// <param name="inputVehicles">The vehicles to pass to the scheme.</param>
    /// <param name="inputOccupants">The occupants to pass to the scheme.</param>
    [TestMethod]
    [DynamicData(nameof(InputsOnly))]
    public void DoesNotReturnNullOccupiedSlots(IVehicle[] inputVehicles, IOccupant[] inputOccupants)
    {
        //Arrange
        var scheme = CreateScheme();

        //Act
        var slots = scheme.CrewVehicles(inputVehicles, inputOccupants).SelectMany(v => v.OccupiedSlots);

        //Assert
        foreach(var value in slots)
            Assert.IsNotNull(value);
    }

    /// <summary>
    /// Asserts that the scheme does not return null values.
    /// </summary>
    /// <param name="inputVehicles">The vehicles to pass to the scheme.</param>
    /// <param name="inputOccupants">The occupants to pass to the scheme.</param>
    [TestMethod]
    [DynamicData(nameof(InputsOnly))]
    public void DoesNotReturnNullOccupants(IVehicle[] inputVehicles, IOccupant[] inputOccupants)
    {
        //Arrange
        var scheme = CreateScheme();

        //Act
        var occupants = scheme.CrewVehicles(inputVehicles, inputOccupants)
            .SelectMany(v => v.OccupiedSlots)
            .Select(s => s.Occupant);

        //Assert
        foreach(var value in occupants)
            Assert.IsNotNull(value);
    }

    /// <summary>
    /// Asserts that the scheme does not return null values.
    /// </summary>
    /// <param name="inputVehicles">The vehicles to pass to the scheme.</param>
    /// <param name="inputOccupants">The occupants to pass to the scheme.</param>
    [TestMethod]
    [DynamicData(nameof(InputsOnly))]
    public void DoesNotReturnNullAvailableSlots(IVehicle[] inputVehicles, IOccupant[] inputOccupants)
    {
        //Arrange
        var scheme = CreateScheme();

        //Act
        var slots = scheme.CrewVehicles(inputVehicles, inputOccupants).SelectMany(v => v.AvailableSlots);

        //Assert
        foreach(var value in slots)
            Assert.IsNotNull(value);
    }

    /// <summary>
    /// Asserts that the scheme will throw an ArgumentNullException if vehicles parameter is null
    /// </summary>
    [TestMethod]
    public void ThrowsExceptionForVehiclesNullArgument()
    {
        //Arrange
        var scheme = CreateScheme();

        //Act and Assert
        Assertions.AssertArgumentNullException("vehicles", () => scheme.CrewVehicles(null, Array.Empty<IOccupant>()));
    }

    /// <summary>
    /// Asserts that the scheme will throw an ArgumentNullException if occupants parameter is null
    /// </summary>
    [TestMethod]
    public void ThrowsExceptionForOccupantsNullArgument()
    {
        //Arrange
        var scheme = CreateScheme();

        //Act and Assert
        Assertions.AssertArgumentNullException("occupants", () => scheme.CrewVehicles(Array.Empty<IVehicle>(), null));
    }

    private static CrewOptimalScheme CreateScheme() => new();
}