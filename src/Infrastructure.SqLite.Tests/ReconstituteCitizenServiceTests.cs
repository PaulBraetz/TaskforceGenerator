﻿using FluentAssertions;

using System.Data;

using TaskforceGenerator.Common;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Authentication.Requests;
using TaskforceGenerator.Domain.Authentication.Results;
using TaskforceGenerator.Domain.Core.Results;
using TaskforceGenerator.Infrastructure.SqLite.Services.Authentication;
using TaskforceGenerator.Infrastructure.SqLite.Tests.Abstractions;
using TaskforceGenerator.Tests.Common;

namespace TaskforceGenerator.Infrastructure.SqLite.Tests;

/// <summary>
/// Contains tests for <see cref="ReconstituteConnectionService"/>.
/// </summary>
[TestClass]
[TestCategory("Unit Infrastructure.SqLite")]
public class ReconstituteCitizenServiceTests : DatabaseCommitRequiredTestBase<ReconstituteConnectionService>
{
    private static Object[][] Citizens => Data.Connections;

    /// <summary>
    /// Tests whether the service returns a <see cref="CitizenNotRegisteredResult"/> when 
    /// requesting a citizen that has not been inserted before.
    /// </summary>
    /// <param name="citizen">The citizen to request, but not insert.</param>
    [TestMethod]
    [DynamicData(nameof(Citizens))]
    public async Task YieldsCNRRWhenCitizenNotInserted(ICitizenConnection citizen)
    {
        //Arrange
        var request = new ReconstituteConnection(citizen.CitizenName, CancellationToken.None);

        //Act
        var actual = await request.Using(Service);

        //Assert
        var expected = CitizenNotRegisteredResult.Instance;
        _ = actual.IsT1.Should().BeTrue();
        _ = actual.AsT1.Should().Be(expected);
    }

    /// <summary>
    /// Tests whether the service reconstitutes citizens previously inserted into the database.
    /// </summary>
    /// <param name="citizen">The citizen to create and reconstitute.</param>
    [TestMethod]
    [DynamicData(nameof(Citizens))]
    public async Task ReconstitutesCitizenInsertedBefore(ICitizenConnection citizen)
    {
        //Arrange
        _ = await Commit(citizen);
        var request = new ReconstituteConnection(citizen.CitizenName, CancellationToken.None);

        //Act
        var actual = await request.Using(Service);

        //Assert
        _ = actual.IsT0.Should().BeTrue();
        Assertions.AreEqual(citizen, actual.AsT0);
    }

    /// <summary>
    /// Tests whether the service returns a <see cref="CitizenNotRegisteredResult"/> 
    /// upon attempting to reconstitute a citizen with an invalid name.
    /// </summary>
    /// <param name="name">The name passed to the service.</param>
    [TestMethod]
    [DataRow("SleepWe_ llPupper")]
    [DataRow(" YokoArashi")]
    [DataRow("Terrore$%nte")]
    [DataRow("SpaêceDirk")]
    public async Task YieldsCNERForInvalidNameCitizens(String name)
    {
        //Arrange
        var request = new ReconstituteConnection(name, CancellationToken.None);

        //Act
        var actual = await request.Using(Service);

        //Assert
        _ = actual.IsT1.Should().BeTrue();
        _ = actual.AsT1.Should().Be(CitizenNotRegisteredResult.Instance);
    }
    /// <summary>
    /// Tests whether the service throws a <see cref="OperationCanceledException"/> when passed a cancelled token.
    /// </summary>
    /// <param name="name">The name passed to the service.</param>
    [TestMethod]
    [DataRow("SleepWellPupper")]
    [DataRow("YokoArashi")]
    [DataRow("Terrorente")]
    [DataRow("SpaceDirk")]
    [ExpectedException(typeof(OperationCanceledException), AllowDerivedTypes = true)]
    public async Task ThrowsOCEForNotRegisteredCitizens(String name)
    {
        var token = new CancellationToken(true);
        _ = await new ReconstituteConnection(name, token).Using(Service);
    }
}