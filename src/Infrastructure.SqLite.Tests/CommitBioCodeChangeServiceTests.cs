﻿using FluentAssertions;

using System.Data;

using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Transactions;
using TaskforceGenerator.Domain.Authentication;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Authentication.Requests;
using TaskforceGenerator.Infrastructure.SqLite.Services.Authentication;
using TaskforceGenerator.Infrastructure.SqLite.Tests.Abstractions;
using TaskforceGenerator.Tests.Common;

namespace TaskforceGenerator.Infrastructure.SqLite.Tests;

/// <summary>
/// COntains tests for <see cref="CommitBioCodeChangeService"/>.
/// </summary>
[TestClass]
[TestCategory("Unit Infrastructure.SqLite")]
public class CommitBioCodeChangeServiceTests : DatabaseCommitRequiredTestBase<CommitBioCodeChangeService>
{
    private static Object[][] BioChanges => Data.Connections.Select(p => p.Append(Data.CreateBioCode()).ToArray()).ToArray();

    /// <summary>
    /// Tests whether the service will commit bio code changes to the database.
    /// </summary>
    /// <param name="citizen">The citizen whose bio code to change.</param>
    /// <param name="newCode">The code to commit using the service.</param>
    [TestMethod]
    [DynamicData(nameof(BioChanges))]
    public async Task CommitsBioCodeChanges(ICitizenConnection citizen, BioCode newCode)
    {
        //Arrange
        _ = await Commit(citizen);

        //Act
        _ = await new CommitBioCodeChange(citizen.CitizenName, newCode, CancellationToken.None).Using(Service);

        //Assert
        var queryText = $"select ({Schema.CitizenConnections.Code}) from " +
            $"{Schema.CitizenConnections.TableName} where " +
            $"{Schema.CitizenConnections.CitizenName}='{citizen.CitizenName}'";

        var transaction = await TransactionFactory.CreateRoot(default);
        await using var _0 = transaction.AsAutoFlush();

        using var query = transaction.Context.Connection.CreateCommand(queryText);
        var reader = await query.ExecuteReaderAsync();
        var code = reader.OfType<IDataRecord>().Single().GetString(0);

        _ = newCode.Value.Should().Be(code);
    }

    /// <summary>
    /// Tests whether the service will throw an <see cref="OperationCanceledException"/>.
    /// Since there is a valid default result of a pre-cancelled operation (code does not get updated) the service should not throw the exception.
    /// </summary> 
    /// <param name="citizen">The citizen whose bio code to change.</param>
    /// <param name="newCode">The code to commit using the service.</param>
    [TestMethod]
    [DynamicData(nameof(BioChanges))]
    public async Task DoesNotThrowOCEWhenPassedCancelledToken(ICitizenConnection citizen, BioCode newCode)
    {
        _ = await Commit(citizen);

        var token = new CancellationToken(true);
        _ = await new CommitBioCodeChange(citizen.CitizenName, newCode, token).Using(Service);

        var queryText = $"select ({Schema.CitizenConnections.Code}) from " +
            $"{Schema.CitizenConnections.TableName} where " +
            $"{Schema.CitizenConnections.CitizenName}='{citizen.CitizenName}'";

        var transaction = await TransactionFactory.CreateRoot(default);
        await using var _0 = transaction.AsAutoFlush();

        using var query = transaction.Context.Connection.CreateCommand(queryText);

        var reader = await query.ExecuteReaderAsync();
        var code = reader.OfType<IDataRecord>().Single().GetString(0);

        _ = citizen.Code.Value.Should().Be(code);
    }
}
