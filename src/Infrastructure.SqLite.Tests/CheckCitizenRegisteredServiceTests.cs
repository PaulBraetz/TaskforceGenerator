﻿using FluentAssertions;

using TaskforceGenerator.Common;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Authentication.Queries;
using TaskforceGenerator.Infrastructure.SqLite.Services.Authentication;
using TaskforceGenerator.Infrastructure.SqLite.Tests.Abstractions;
using TaskforceGenerator.Tests.Common;

namespace TaskforceGenerator.Infrastructure.SqLite.Tests;
/// <summary>
/// Contains tests for <see cref="CheckConnectionExistsService"/>.
/// </summary>
[TestClass]
[TestCategory("Unit Infrastructure.SqLite")]
public class CheckConnectionExistsServiceTests : DatabaseCommitRequiredTestBase<CheckConnectionExistsService>
{
    private static Object[][] Citizens => Data.Connections;

    /// <summary>
    /// Tests whether the service yields correctly for connections actually registered to the database.
    /// </summary>
    [TestMethod]
    [DynamicData(nameof(Citizens))]
    public async Task YieldsExistsForExistingConnections(ICitizenConnection citizen)
    {
        //Arrange
        _ = await Commit(citizen);

        //Act
        var result = await new CheckConnectionExists(citizen.CitizenName, CancellationToken.None).Using(Service);

        //Assert
        _ = result.Should().BeTrue();
    }
    /// <summary>
    /// Tests whether the service yields correctly for citizens not registered to the database.
    /// </summary>
    [TestMethod]
    [DynamicData(nameof(Citizens))]
    public async Task YieldsNotExistsForExistingConnections(ICitizenConnection citizen)
    {
        //Arrange
        var request = new CheckConnectionExists(citizen.CitizenName, CancellationToken.None);

        //Act
        var result = await request.Using(Service);

        //Assert
        _ = result.Should().BeFalse();
    }
    /// <summary>
    /// Tests whether the service will throw an <see cref="OperationCanceledException"/> upon receiving a cancelled token.
    /// Since there is no logical result to return when cancelled, the exception must be thrown.
    /// </summary>
    [TestMethod]
    [DynamicData(nameof(Citizens))]
    [ExpectedException(typeof(OperationCanceledException), AllowDerivedTypes = true)]
    public async Task ThrowsOCEWhenCancelled(ICitizenConnection citizen)
    {
        _ = await Commit(citizen);

        var token = new CancellationToken(true);
        _ = await new CheckConnectionExists(citizen.CitizenName, token).Using(Service);
    }
}
