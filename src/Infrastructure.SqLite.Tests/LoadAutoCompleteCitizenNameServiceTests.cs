﻿using FluentAssertions;

using TaskforceGenerator.Common;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Authentication.Queries;
using TaskforceGenerator.Domain.Authentication.Requests;
using TaskforceGenerator.Infrastructure.SqLite.Services.Authentication;
using TaskforceGenerator.Infrastructure.SqLite.Tests.Abstractions;
using TaskforceGenerator.Tests.Common;

namespace TaskforceGenerator.Infrastructure.SqLite.Tests;

/// <summary>
/// COntains tests for <see cref="LoadAutoCompleteCitizenNameService"/>.
/// </summary>
[TestClass]
[TestCategory("Unit Infrastructure.SqLite")]
public class LoadAutoCompleteCitizenNameServiceTests :
    DatabaseCommitRequiredTestBase<LoadAutoCompleteCitizenNameService>
{
    private static Object[][] Citizens => Data.Connections;

    /// <summary>
    /// Asserts that the service yields the citizen prompted for.
    /// </summary>
    /// <param name="connection">The citizen connection whose name to query for.</param>
    [TestMethod]
    [DynamicData(nameof(Citizens))]
    public async Task YieldsCitizen(ICitizenConnection connection)
    {
        //Arrange
        foreach(var conn in Data.Connections.SelectMany(a => a).Cast<ICitizenConnection>())
        {
            _ = await Commit(conn);
        }

        var prompt = connection.CitizenName[..^3];

        //Act
        var result = await new LoadAutoCompleteCitizenName(prompt, CancellationToken.None)
            .Using(Service);

        //Assert
        _ = result.Should().Be(connection.CitizenName);
    }
}
