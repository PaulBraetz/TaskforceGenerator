﻿using FluentAssertions;

using OneOf;

using SimpleInjector;

using System.Data;

using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Results;
using TaskforceGenerator.Domain.Authentication;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Authentication.Requests;
using TaskforceGenerator.Domain.Authentication.Results;
using TaskforceGenerator.Domain.Core.Results;
using TaskforceGenerator.Infrastructure.SqLite.Services.Authentication;
using TaskforceGenerator.Infrastructure.SqLite.Tests.Abstractions;
using TaskforceGenerator.Tests.Common;

namespace TaskforceGenerator.Infrastructure.SqLite.Tests;

/// <summary>
/// Contains tests for <see cref="CommitPasswordChangeService"/>.
/// </summary>
[TestClass]
[TestCategory("Unit Infrastructure.SqLite")]
public class CommitPasswordChangeServiceTests : DatabaseCommitRequiredTestBase<CommitPasswordChangeService>
{
    private static Object[][] PasswordData => Data.Connections.Select(p => p.Append(Data.CreatePassword()).ToArray()).ToArray();

    private IService<ReconstituteConnection, OneOf<ICitizenConnection, CitizenNotRegisteredResult>> ReconstituteService { get; set; }

    /// <inheritdoc/>
    protected override void Configure(Container container)
    {
        base.Configure(container);
        container.Register<
            IService<ReconstituteConnection, OneOf<ICitizenConnection, CitizenNotRegisteredResult>>,
            ReconstituteConnectionService>();
    }
    /// <inheritdoc/>
    protected override void OnAfterConfigure(Container container)
    {
        base.OnAfterConfigure(container);
        ReconstituteService = container.GetInstance<
            IService<ReconstituteConnection, OneOf<ICitizenConnection, CitizenNotRegisteredResult>>>();
    }

    /// <summary>
    /// Tests whether the service will correctly commit password changes to the database.
    /// </summary>
    /// <param name="citizen">The citizen to create and whose password to set.</param>
    /// <param name="newPassword">The new password to commit using the service.</param>
    [TestMethod]
    [DynamicData(nameof(PasswordData))]
    public async Task CommitsPasswordChanges(ICitizenConnection citizen, Password newPassword)
    {
        //Arrange
        _ = await Commit(citizen);

        //Act
        _ = await new CommitPasswordChange(citizen.CitizenName, citizen.Code, newPassword, CancellationToken.None).Using(Service);

        //Assert
        var reconstituted = await new ReconstituteConnection(
            citizen.CitizenName,
            CancellationToken.None)
            .Using(ReconstituteService);

        _ = reconstituted.IsT0.Should().BeTrue();

        var reconstitutedConnection = reconstituted.AsT0;

#pragma warning disable FluentAssertions0600 // Simplify Assertion
#pragma warning disable FluentAssertions0606 // Simplify Assertion
        Assert.IsTrue(
            reconstitutedConnection.Password.Hash
            .SequenceEqual(newPassword.Hash), "hash");

        Assert.IsTrue(
            reconstitutedConnection.Password.Parameters.Data.KnownSecret
            .SequenceEqual(newPassword.Parameters.Data.KnownSecret), "known secret");
        Assert.IsTrue(
            reconstitutedConnection.Password.Parameters.Data.Salt
            .SequenceEqual(newPassword.Parameters.Data.Salt), "salt");
        Assert.IsTrue(
            reconstitutedConnection.Password.Parameters.Data.AssociatedData
            .SequenceEqual(newPassword.Parameters.Data.AssociatedData), "associated data");

        Assert.AreEqual(
            reconstitutedConnection.Password.Parameters.Numerics.Iterations,
            newPassword.Parameters.Numerics.Iterations, "iterations");
        Assert.AreEqual(
            reconstitutedConnection.Password.Parameters.Numerics.DegreeOfParallelism,
            newPassword.Parameters.Numerics.DegreeOfParallelism, "degree of parallelism");
        Assert.AreEqual(
            reconstitutedConnection.Password.Parameters.Numerics.MemorySize,
            newPassword.Parameters.Numerics.MemorySize, "memory size");
        Assert.AreEqual(
            reconstitutedConnection.Password.Parameters.Numerics.OutputLength,
            newPassword.Parameters.Numerics.OutputLength, "output length");
#pragma warning restore FluentAssertions0600 // Simplify Assertion
#pragma warning restore FluentAssertions0606 // Simplify Assertion
    }

    /// <summary>
    /// Tests whether the service will return a <see cref="CompliantlyCancelledResult"/> upon receiving a cancelled token.
    /// </summary>
    /// <param name="citizen">The citizen to create and whose password to set.</param>
    /// <param name="newPassword">The new password to commit using the service.</param>
    [TestMethod]
    [DynamicData(nameof(PasswordData))]
    public async Task YieldsOCCRWhenPassedCancelledToken(ICitizenConnection citizen, Password newPassword)
    {
        //Arrange
        _ = await Commit(citizen);

        //Act
        var token = new CancellationToken(true);
        var actual = await new CommitPasswordChange(citizen.CitizenName, citizen.Code, newPassword, token).Using(Service);

        //Assert
        _ = actual.IsT1.Should().BeTrue();
        _ = actual.AsT1.Should().Be(CompliantlyCancelledResult.Instance);
    }
}
