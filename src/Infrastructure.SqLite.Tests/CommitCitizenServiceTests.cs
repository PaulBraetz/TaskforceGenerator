﻿using SimpleInjector;

using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Authentication.Requests;
using TaskforceGenerator.Infrastructure.SqLite.Services.Authentication;
using TaskforceGenerator.Infrastructure.SqLite.Tests.Abstractions;
using TaskforceGenerator.Tests.Common;
using TaskforceGenerator.Common.Transactions;
using OneOf;
using TaskforceGenerator.Domain.Authentication.Results;
using FluentAssertions;
using TaskforceGenerator.Domain.Core.Results;

namespace TaskforceGenerator.Infrastructure.SqLite.Tests;

/// <summary>
/// Contains tests for <see cref="CommitConnectionService"/>.
/// </summary>
[TestClass]
[TestCategory("Unit Infrastructure.SqLite")]
public class CommitCitizenServiceTests : DatabaseTestBase<CommitConnectionService>
{
    private static Object[][] Citizens => Data.Connections;

    private IService<ReconstituteConnection, OneOf<ICitizenConnection, CitizenNotRegisteredResult>> ReconstituteService { get; set; }

    /// <inheritdoc/>
    protected override void Configure(Container container)
    {
        base.Configure(container);
        container.Register<
            IService<ReconstituteConnection, OneOf<ICitizenConnection, CitizenNotRegisteredResult>>,
            ReconstituteConnectionService>();
    }
    /// <inheritdoc/>
    protected override void OnAfterConfigure(Container container)
    {
        base.OnAfterConfigure(container);
        ReconstituteService = container.GetInstance<
            IService<ReconstituteConnection, OneOf<ICitizenConnection, CitizenNotRegisteredResult>>>();
    }

    /// <summary>
    /// Tests whether the service commits citizens to the database.
    /// </summary>
    /// <param name="citizen">The citizen to commit.</param>
    [TestMethod]
    [DynamicData(nameof(Citizens))]
    public async Task CommitsCitizenPassed(ICitizenConnection citizen)
    {
        //Arrange
        var request = new CommitConnection(citizen, CancellationToken.None);

        //Act
        _ = await request.Using(Service);

        //Assert
        var actual = await new ReconstituteConnection(
            citizen.CitizenName,
            CancellationToken.None)
            .Using(ReconstituteService);
        _ = actual.IsT0.Should().BeTrue();
        Assertions.AreEqual(citizen, actual.AsT0);
    }

    /// <summary>
    /// Tests whether the service returns a <see cref="ConnectionAlreadyCommittedResult"/> upon committing a connection twice.
    /// </summary>
    /// <param name="citizen">The connection to commit twice.</param>
    [TestMethod]
    [DynamicData(nameof(Citizens))]
    public async Task ReturnsCACRForDuplicateCommits(ICitizenConnection citizen)
    {
        //Arrange
        var request = new CommitConnection(citizen, default);
        _ = await request.Using(Service);

        //Act
        var actual = await request.Using(Service);

        //Assert
        _ = actual.IsT1.Should().BeTrue();
        _ = actual.AsT1.Should().Be(ConnectionAlreadyCommittedResult.Instance);
    }

    /// <summary>
    /// Tests whether the service throws an <see cref="OperationCanceledException"/> upon receiving a cancelled token.
    /// Since the operation being cancelled before initiating the transaction yields a valid state (citizen does not get inserted), the service should simply return to the caller.
    /// </summary>
    /// <param name="citizen">The citizen to commit.</param>
    [TestMethod]
    [DynamicData(nameof(Citizens))]
    public async Task DoesNotThrowOCEWhenPassedCancelledToken(ICitizenConnection citizen)
    {
        //Arrange
        var token = new CancellationToken(true);
        var request = new CommitConnection(citizen, token);

        //Act
        _ = await request.Using(Service);

        //Assert
        var commandText = $"select Count(*) from {Schema.CitizenConnections.TableName} where " +
            $"{Schema.CitizenConnections.CitizenName}='{citizen.CitizenName}'";

        var transaction = await TransactionFactory.CreateRoot(default);
        await using var _0 = transaction.AsAutoFlush();

        using var command = transaction.Context.Connection.CreateCommand(commandText);
        var count = await command.ExecuteScalarAsync();

        _ = count.Should().Be(0);
    }
}
