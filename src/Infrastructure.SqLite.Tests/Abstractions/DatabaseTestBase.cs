﻿using Microsoft.Data.Sqlite;

using SimpleInjector;

using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Transactions.Abstractions;
using TaskforceGenerator.Infrastructure.SqLite.Abstractions;
using TaskforceGenerator.Infrastructure.SqLite.Schema;
using TaskforceGenerator.Tests.Common;

using SqliteTransaction = TaskforceGenerator.Infrastructure.SqLite.Transactions.SqliteTransaction;
using MsSqliteTransaction = Microsoft.Data.Sqlite.SqliteTransaction;
using TaskforceGenerator.Common.Transactions;
using TaskforceGenerator.Common;

namespace TaskforceGenerator.Infrastructure.SqLite.Tests.Abstractions;

/// <summary>
/// Base class for tests whose service require a database to execute.
/// </summary>
/// <typeparam name="TService"></typeparam>
public abstract class DatabaseTestBase<TService> : ServiceTestBase<TService>
    where TService : class
{
    private sealed class MemoryScript : IScript
    {
        private readonly String _commandText;

        public MemoryScript(String scriptName, String commandText)
        {
            _commandText = commandText;
            ScriptName = scriptName;
        }

        public String ScriptName { get; }

        public void Execute(SqliteConnection connection)
        {
            using var command = new SqliteCommand(_commandText, connection);
            _ = command.ExecuteNonQuery();
        }
    }

    /// <summary>
    /// Gets the database registered for injection into the service.
    /// </summary>
    protected ITransactionFactory<ITransaction<MsSqliteTransaction>, MsSqliteTransaction> TransactionFactory { get; private set; }
    /// <summary>
    /// Gets the database schema.
    /// </summary>
    protected Tables Schema { get; private set; }
    /// <inheritdoc/>
    protected override void OnAfterConfigure(Container container)
    {
        TransactionFactory = container.GetInstance<ITransactionFactory<ITransaction<MsSqliteTransaction>, MsSqliteTransaction>>();
        Schema = container.GetInstance<Tables>();
    }
    /// <inheritdoc/>
    protected override void Configure(Container container)
    {
        base.Configure(container);
        container.Register<CitizenConnections>(Lifestyle.Singleton);
        container.Register<Citizens>(Lifestyle.Singleton);
        container.Register<Tables>(Lifestyle.Singleton);
        container.Register<IFactory<IObservableTransactionStateMachine, ITransactionStateMachineSettings>, ObservableTransactionStateMachineFactory>();
        container.Register<IFactory<ITransactionStateMachineSettings>>(() =>
        {
            var result = new FactoryStrategy<ITransactionStateMachineSettings>(() => new TransactionStateMachineSettings());

            return result;
        });
        container.Register<ITransactionFactory<ITransaction<MsSqliteTransaction>, MsSqliteTransaction>>(() =>
        {
            var tables = container.GetInstance<Tables>();
            var connectionString = $"Data Source=inMemoryDb;Mode=Memory;Cache=Shared";

            var scripts = new IScript[]
            {
                new MemoryScript("Up",
@$"create table if not exists {tables.CitizenConnections.TableName}(
    {tables.CitizenConnections.CitizenName} text primary key, 
    {tables.CitizenConnections.Code} text,
    {tables.CitizenConnections.PasswordHash} blob, 
    {tables.CitizenConnections.PasswordDegreeOfParallelism} integer, 
    {tables.CitizenConnections.PasswordIterations} integer, 
    {tables.CitizenConnections.PasswordMemorySize} integer, 
    {tables.CitizenConnections.PasswordOutputLength} integer, 
    {tables.CitizenConnections.PasswordSalt} blob, 
    {tables.CitizenConnections.PasswordAssociatedData} blob, 
    {tables.CitizenConnections.PasswordKnownSecret} blob)")
            };

            var stateMachineFactory = container.GetInstance<IFactory<IObservableTransactionStateMachine, ITransactionStateMachineSettings>>();
            var settingsFactory = container.GetInstance<IFactory<ITransactionStateMachineSettings>>();

            var result = SqliteTransaction.Factory.Create(
                connectionString,
                stateMachineFactory,
                settingsFactory);

            result.Initialize(scripts);

            return result;
        }, Lifestyle.Singleton);
    }
}
