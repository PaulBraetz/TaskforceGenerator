﻿using OneOf;

using SimpleInjector;

using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Results;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Authentication.Requests;
using TaskforceGenerator.Domain.Authentication.Results;
using TaskforceGenerator.Infrastructure.SqLite.Services.Authentication;

namespace TaskforceGenerator.Infrastructure.SqLite.Tests.Abstractions;

/// <summary>
/// Base class for tests whose service require a database to execute. A service for committing citizens is provided via the <see cref="CommitService"/> property.
/// </summary>
/// <typeparam name="TService">The type of service tested.</typeparam>
public abstract class DatabaseCommitRequiredTestBase<TService> : DatabaseTestBase<TService>
    where TService : class
{
    /// <summary>
    /// Gets the service used for committing citizens to the database.
    /// </summary>
    protected IService<CommitConnection, OneOf<ServiceResult, ConnectionAlreadyCommittedResult>> CommitService { get; set; }

    /// <inheritdoc/>
    protected override void Configure(Container container)
    {
        base.Configure(container);
        container.Register<
            IService<CommitConnection, OneOf<ServiceResult, ConnectionAlreadyCommittedResult>>,
            CommitConnectionService>(Lifestyle.Transient);
    }

    /// <inheritdoc/>
    protected override void OnAfterConfigure(Container container)
    {
        base.OnAfterConfigure(container);
        CommitService = container.GetInstance<
            IService<CommitConnection, OneOf<ServiceResult, ConnectionAlreadyCommittedResult>>>();
    }
    /// <summary>
    /// Commits a citizen to the database using <see cref="CommitService"/>.
    /// </summary>
    /// <param name="connection">The cicitzen to submit.</param>
    protected ValueTask<OneOf<ServiceResult, ConnectionAlreadyCommittedResult>> Commit(ICitizenConnection connection) =>
        new CommitConnection(connection, CancellationToken.None).Using(CommitService);
}
