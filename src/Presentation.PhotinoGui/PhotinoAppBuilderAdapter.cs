﻿using Microsoft.Extensions.DependencyInjection;

using Photino.Blazor;

using TaskforceGenerator.Presentation.Views.Blazor.DependencyInjection;

namespace TaskforceGenerator.Presentation.LocalGui;

internal sealed class PhotinoAppBuilderAdapter : IApplicationBuilder<PhotinoAppAdapter>
{
    private readonly PhotinoBlazorAppBuilder _builder;

    public PhotinoAppBuilderAdapter(PhotinoBlazorAppBuilder builder)
    {
        _builder = builder;
    }

    public IServiceCollection Services => _builder.Services;

    public PhotinoAppAdapter Build()
    {
        var app = _builder.Build();
        var result = new PhotinoAppAdapter(app);

        return result;
    }
}
