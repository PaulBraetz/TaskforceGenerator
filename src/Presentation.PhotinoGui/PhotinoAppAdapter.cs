﻿using Photino.Blazor;

using TaskforceGenerator.Presentation.Views.Blazor.DependencyInjection;

namespace TaskforceGenerator.Presentation.LocalGui;

internal sealed class PhotinoAppAdapter : IApplication
{
    public PhotinoBlazorApp App { get; }

    public PhotinoAppAdapter(PhotinoBlazorApp app)
    {
        App = app;
    }

    public IServiceProvider Services => App.Services;
}
