﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

using Photino.Blazor;

using TaskforceGenerator.Composition;
using TaskforceGenerator.Presentation.Views.Blazor.DependencyInjection;
using TaskforceGenerator.Presentation.Views.Blazor.DependencyInjection.Logging;

namespace TaskforceGenerator.Presentation.LocalGui;

internal class Program
{
    [STAThread]
    private static void Main(String[] args)
    {
        var builder = PhotinoBlazorAppBuilder.CreateDefault(args);

        AddConfigs(builder);
        AddLogging(builder);

        // register root component and selector
        builder.RootComponents.Add<EntryPoint>("app");

        using var integrator = CreateIntegrator();
        //integrate into the app
        PhotinoBlazorApp app;
        try
        {
            app = AdaptIntegrate(builder, integrator);
        } catch(Exception ex)
        {
            PrintFastExit(ex.ToString());
            return;
        }

        ConfigureWindow(app);

        AppDomain.CurrentDomain.UnhandledException += (sender, error)
            => app.MainWindow.OpenAlertWindow("Fatal exception", error.ExceptionObject.ToString());

        app.Run();
    }
    private static void AddLogging(PhotinoBlazorAppBuilder builder)
    {
        var logFilePath = builder.Configuration
                        .Build()
                        .GetRequiredSection("LogFilePath")
                        .Value;
        _ = builder.Logging.AddFile(logFilePath);
    }
    private static void AddConfigs(PhotinoBlazorAppBuilder builder)
    {
        _ = builder.Configuration.AddJsonFile("appsettings.json");
        var environmentAppsettings = $"appsettings.{builder.Environment.EnvironmentName}.json";
        if(File.Exists(environmentAppsettings))
        {
            _ = builder.Configuration.AddJsonFile(environmentAppsettings);
        }
    }
    private static SimpleInjectorBlazorIntegrator CreateIntegrator()
    {
        //configure DI logging
        var loggers = new IContainerLogger[]
        {
            new ContainerDiagnosticsLogger(),
            new FakeWarningsLogger()
#if !DEBUG
                ,new RootGraphLogger()
#endif
        };

        var integrator =
                        new SimpleInjectorBlazorIntegrator(
                            new CompositeContainerLogger(
                                loggers));
        return integrator;
    }
    private static PhotinoBlazorApp AdaptIntegrate(PhotinoBlazorAppBuilder builder, SimpleInjectorBlazorIntegrator integrator)
    {
        PhotinoBlazorApp app;
        var adapter = new PhotinoAppBuilderAdapter(builder);

        app = integrator.Integrate(
           adapter,
           Root.LocalGui.Compose,
           typeof(Presentation.Views.Blazor._Imports).Assembly,
           typeof(Presentation.LocalGui.EntryPoint).Assembly).App;
        return app;
    }
    private static void ConfigureWindow(PhotinoBlazorApp app)
    {
        _ = app.MainWindow
            .SetIconFile("favicon.ico")
            .SetTitle("Taskforce Builder")
            .SetMaximized(true)
#if !DEBUG
            .SetDevToolsEnabled(false)
            .SetContextMenuEnabled(false)
#endif
            ;
    }
    private static void PrintFastExit(String message)
    {
        //await integrator logs
        Thread.Sleep(2500);
        Console.WriteLine(String.Concat(Enumerable.Repeat('─', 100)));
        Console.WriteLine(message);
    }
}
