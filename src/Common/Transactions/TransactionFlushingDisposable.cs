﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TaskforceGenerator.Common.Transactions.Abstractions;

namespace TaskforceGenerator.Common.Transactions;
/// <summary>
/// Adapter for instances of <typeparamref name="TTransaction"/> onto <see cref="IAsyncDisposable"/>, so 
/// that transactions may be used inside a <see langword="await"/> <see langword="using"/> statement.
/// </summary>
/// <typeparam name="TTransaction">The type of transaction to adapt.</typeparam>
public readonly struct TransactionFlushingDisposable<TTransaction> : IAsyncDisposable
    where TTransaction : ITransaction
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="transaction">The transaction whose <see cref="ITransaction.Flush(CancellationToken)"/> to call upon disposal.</param>
    public TransactionFlushingDisposable(TTransaction transaction)
    {
        Transaction = transaction;
    }
    /// <summary>
    /// Gets the adapted transaction.
    /// </summary>
    public TTransaction Transaction { get; }
    /// <inheritdoc/>
    public async ValueTask DisposeAsync()
    {
        if(Transaction != null)
        {
            await Transaction.Flush(default);
        }
    }
}
