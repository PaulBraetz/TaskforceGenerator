﻿using TaskforceGenerator.Common.Transactions.Abstractions;

namespace TaskforceGenerator.Common.Transactions;
/// <summary>
/// Non-contextual programmatic transaction.
/// </summary>
public sealed partial class ProgramTransaction : TransactionBase, ITransaction, ITransactionStateMachine
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="stateMachine">The state machine using which to represent the internal state.</param>
    public ProgramTransaction(ITransactionStateMachine stateMachine) : base(stateMachine)
    {
    }

    /// <inheritdoc/>
    public override Task Commit(CancellationToken cancellationToken = default) =>
        StateMachine.Commit(cancellationToken);

    /// <inheritdoc/>
    public override async Task Flush(CancellationToken cancellationToken) =>
        await StateMachine.RequestFlush(cancellationToken);
}
