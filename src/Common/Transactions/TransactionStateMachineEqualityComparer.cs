﻿using System.Diagnostics.CodeAnalysis;

using TaskforceGenerator.Common.Transactions.Abstractions;

namespace TaskforceGenerator.Common.Transactions;

/// <summary>
/// Reference-Identity-based equality comparer for transactional state machines.
/// </summary>
public sealed class TransactionStateMachineEqualityComparer : IEqualityComparer<ITransactionStateMachine>
{
    /// <summary>
    /// Initializes a new instance. If possible, use <see cref="Instance"/> instead.
    /// </summary>
    public TransactionStateMachineEqualityComparer() { }
    /// <summary>
    /// Gets the singleton instance.
    /// </summary>
	public static TransactionStateMachineEqualityComparer Instance { get; } = new();

    /// <inheritdoc/>
    public Boolean Equals(ITransactionStateMachine? x, ITransactionStateMachine? y)
    {
        if(x == null)
        {
            return y == null;
        }
        
        if(y == null)
        {
            return x == null;
        }
        
        var result = y.Identity == x.Identity;

        return result;
    }
    /// <inheritdoc/>
    public Int32 GetHashCode([DisallowNull] ITransactionStateMachine obj)
    {
        var result = obj?.Identity.GetHashCode() ?? throw new ArgumentNullException(nameof(obj));

        return result;
    }
}
