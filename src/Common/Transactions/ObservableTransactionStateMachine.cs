﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Transactions.Abstractions;

namespace TaskforceGenerator.Common.Transactions;
/// <summary>
/// Default implementation of <see cref="IObservableTransactionStateMachine"/>.
/// </summary>
public sealed class ObservableTransactionStateMachine : TransactionStateMachine, IObservableTransactionStateMachine
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="comparer">The comparer used to determine children equality.</param>
    public ObservableTransactionStateMachine(IEqualityComparer<ITransactionStateMachine> comparer) : base(comparer)
    {
    }

    /// <inheritdoc/>
    public event AsyncEventHandler? Committed;
    /// <inheritdoc/>
    public event AsyncEventHandler? RolledBack;

    /// <inheritdoc/>
    protected override Task OnCommit(CancellationToken cancellationToken) =>
        Committed.InvokeAsync(this, cancellationToken);
    /// <inheritdoc/>
    protected override Task OnRollback(CancellationToken cancellationToken) =>
        RolledBack.InvokeAsync(this, cancellationToken);
}
