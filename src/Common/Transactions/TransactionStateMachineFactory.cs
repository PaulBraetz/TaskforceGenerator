﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Transactions.Abstractions;

namespace TaskforceGenerator.Common.Transactions;

/// <summary>
/// Factory for creating instances of <see cref="ITransactionStateMachine"/>.
/// </summary>
public sealed class TransactionStateMachineFactory : IFactory<ITransactionStateMachine, ITransactionStateMachineSettings>
{
    /// <summary>
    /// Initializes a new instance. If possible, use <see cref="Instance"/> instead.
    /// </summary>
    public TransactionStateMachineFactory() { }
    /// <summary>
    /// Gets the singleton instance.
    /// </summary>
	public static TransactionStateMachineFactory Instance { get; } = new();

    /// <inheritdoc/>
    public ITransactionStateMachine Create(ITransactionStateMachineSettings @params)
    {
        var (comparer, ignore, timeout) = @params;
        var result = new TransactionStateMachine(comparer)
        {
            FamilySynchronizationTimeout = timeout,
            IgnoreMultipleFlushes = ignore
        };

        return result;
    }
}
