﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Transactions.Abstractions;

namespace TaskforceGenerator.Common.Transactions;
public sealed partial class ProgramTransaction
{
    /// <summary>
    /// The factory used to instantiate root and child instances of <see cref="ProgramTransaction"/>.
    /// </summary>
    public sealed class Factory : ITransactionFactory<ITransaction>
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="stateMachineFactory">The factory to use when instantiating new state machines for transactions to use.</param>
        /// <param name="settingsFactory">The factory touse when creating new settings for new state machines.</param>
        public Factory(IFactory<ITransactionStateMachine, ITransactionStateMachineSettings> stateMachineFactory, IFactory<ITransactionStateMachineSettings> settingsFactory)
        {
            _stateMachineFactory = stateMachineFactory;
            _settingsFactory = settingsFactory;
        }

        private readonly IFactory<ITransactionStateMachineSettings> _settingsFactory;
        private readonly IFactory<ITransactionStateMachine, ITransactionStateMachineSettings> _stateMachineFactory;

        /// <inheritdoc/>
        public Task<ITransaction> CreateRoot(CancellationToken cancellationToken)
        {
            var stateMachine = CreateStateMachine();
            var result = new ProgramTransaction(stateMachine);

            return Task.FromResult<ITransaction>(result);
        }

        /// <inheritdoc/>
        public async Task<ITransaction> CreateChild(ITransaction parent, CancellationToken cancellationToken)
        {
            var stateMachine = CreateStateMachine();
            var result = new ProgramTransaction(stateMachine);
            await parent.Attach(result, cancellationToken);

            return result;
        }

        private ITransactionStateMachine CreateStateMachine()
        {
            var settings = _settingsFactory.Create();
            var stateMachine = _stateMachineFactory.Create(settings);
            return stateMachine;
        }
    }
}
