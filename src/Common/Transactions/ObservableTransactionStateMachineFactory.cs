﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Transactions.Abstractions;

namespace TaskforceGenerator.Common.Transactions;

/// <summary>
/// Factory for creating instances of <see cref="IObservableTransactionStateMachine"/>.
/// </summary>
public sealed class ObservableTransactionStateMachineFactory : IFactory<IObservableTransactionStateMachine, ITransactionStateMachineSettings>
{
    /// <summary>
    /// Initializes a new instance. If possible, use <see cref="Instance"/> instead.
    /// </summary>
    public ObservableTransactionStateMachineFactory() { }
    /// <summary>
    /// Gets the singleton instance.
    /// </summary>
	public static ObservableTransactionStateMachineFactory Instance { get; } = new();

    /// <inheritdoc/>
    public IObservableTransactionStateMachine Create(ITransactionStateMachineSettings @params)
    {
        var (comparer, ignore, timeout) = @params;
        var result = new ObservableTransactionStateMachine(comparer)
        {
            FamilySynchronizationTimeout = timeout,
            IgnoreMultipleFlushes = ignore
        };

        return result;
    }
}
