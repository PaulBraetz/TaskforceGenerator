﻿using TaskforceGenerator.Common.Exceptions;

namespace TaskforceGenerator.Common;

/// <summary>
/// Callback-based implementation of <see cref="IDisposable"/>.
/// </summary>
public readonly struct CallbackDisposable : IDisposable
{
    private readonly Action _callback;
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="callback">The callback to invoke upon <see cref="Dispose"/> being called.</param>
    public CallbackDisposable(Action callback)
    {
        _callback = callback;
    }
    /// <inheritdoc/>
    public void Dispose() => _callback.Invoke();
    /// <inheritdoc/>
    public override Boolean Equals(Object? obj) => throw Throw.NotSupportedException.CallbackDisposableEquals;
    /// <inheritdoc/>
    public override Int32 GetHashCode() => throw Throw.NotSupportedException.CallbackDisposableGetHashCode;
    /// 
    public static Boolean operator ==(CallbackDisposable left, CallbackDisposable right) =>
        left.Equals(right);
    /// 
    public static Boolean operator !=(CallbackDisposable left, CallbackDisposable right) =>
        !(left == right);
}
