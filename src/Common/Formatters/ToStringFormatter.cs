﻿using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Common.Formatters;

/// <summary>
/// <see cref="Object.ToString"/>-based implementation of <see cref="IStaticFormatter{T}"/>.
/// </summary>
/// <typeparam name="T">The type of object to format.</typeparam>
public sealed class ToStringFormatter<T> : IStaticFormatter<T>
{
    /// <summary>
    /// Initializes a new instance. If possible, use <see cref="Instance"/> instead.
    /// </summary>
    public ToStringFormatter() { }
    /// <summary>
    /// Gets the singleton instance.
    /// </summary>
    public static ToStringFormatter<T> Instance { get; } = new();
    /// <inheritdoc/>
    public String Format(T value) => value?.ToString() ?? String.Empty;
}
