﻿using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Common.Formatters;

/// <summary>
/// Formats values into an the formatted values type name.
/// </summary>
/// <typeparam name="T">The type of object to format.</typeparam>
public sealed class TypeNameFormatter<T> : IStaticFormatter<T>
{
    /// <summary>
    /// Initializes a new instance. If possible, use <see cref="Instance"/> instead.
    /// </summary>
    public TypeNameFormatter() { }
    /// <summary>
    /// Gets the singleton instance.
    /// </summary>
    public static TypeNameFormatter<T> Instance { get; } = new();
    /// <inheritdoc/>
    public String Format(T value) => value?.GetType().Name ?? String.Empty;
}
