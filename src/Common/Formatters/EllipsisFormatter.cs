﻿using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Common.Formatters;

/// <summary>
/// Ellipsis based formatter for <see cref="String"/>.
/// </summary>
public sealed class EllipsisFormatter : IStaticFormatter<String>
{
    /// <summary>
    /// Initializes a new instance. If possible, use <see cref="Instance"/> instead.
    /// </summary>
    public EllipsisFormatter() { }
    /// <summary>
    /// Gets the singleton instance.
    /// </summary>
    public static EllipsisFormatter Instance { get; } = new();

    private const Int32 ELLIPSIS_LIMIT = 10;
    private const Int32 ELLIPSIS_LIMIT_HALF = ELLIPSIS_LIMIT / 2;

    /// <inheritdoc/>
    public String Format(String value)
    {
        var result = value.Length > ELLIPSIS_LIMIT ?
            $"{value[..ELLIPSIS_LIMIT_HALF]} [...] {value[^ELLIPSIS_LIMIT_HALF..]}" :
            value;

        return result;
    }
}
