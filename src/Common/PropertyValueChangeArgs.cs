﻿using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Common;

/// <summary>
/// Concrete args for the <see cref="INotifyPropertyValueChanging.PropertyValueChanging"/>
/// or <see cref="INotifyPropertyValueChanged.PropertyValueChanged"/> events.
/// </summary>
/// <typeparam name="TProperty">The type of property that has changed or is changing.</typeparam>
public sealed class PropertyValueChangeArgs<TProperty> : PropertyValueChangeArgsBase
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="oldValue">The properties value before the change.</param>
    /// <param name="newValue">The properties value after the change.</param>
    /// <param name="propertyName">The name of the property.</param>
    public PropertyValueChangeArgs(
        TProperty oldValue,
        TProperty newValue,
        String propertyName) : base(propertyName)
    {
        OldValue = oldValue;
        NewValue = newValue;
    }
    /// <summary>
    /// Gets the properties old value.
    /// </summary>
    public TProperty OldValue { get; }
    /// <summary>
    /// Gets the properties new value.
    /// </summary>
    public TProperty NewValue { get; }
}
