﻿using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Common;

/// <summary>
/// Strategy-based implementation of <see cref="IExceptionHandler"/>.
/// </summary>
public sealed class ExceptionHandlerStrategy : IExceptionHandler
{
    private readonly Func<Exception, Boolean> _canHandle;
    private readonly Func<Exception, ValueTask> _handle;

    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="canHandle">The strategy to invoke when calling <see cref="CanHandle(Exception)"/>.</param>
    /// <param name="handle">The strategy to invoke when calling <see cref="Handle"/>.</param>
    public ExceptionHandlerStrategy(Func<Exception, Boolean> canHandle, Func<Exception, ValueTask> handle)
    {
        _canHandle = canHandle;
        _handle = handle;
    }
    /// <inheritdoc/>
    public Boolean CanHandle(Exception ex) => _canHandle(ex);
    /// <inheritdoc/>
    public ValueTask Handle(Exception ex) =>
        CanHandle(ex) ?
        _handle(ex) :
        throw new InvalidOperationException($"Unable to handle exception {ex}", ex);
}
