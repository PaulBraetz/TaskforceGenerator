﻿using TaskforceGenerator.Common.Transactions.Abstractions;

namespace TaskforceGenerator.Common.Abstractions;

/// <summary>
/// Represents an observable transactional unit of work.
/// </summary>
public interface IObservableTransactionStateMachine : ITransactionStateMachine
{
    /// <summary>
    /// Invoked after the state machine has been flushed while set to commit.
    /// </summary>
    event AsyncEventHandler? Committed;
    /// <summary>
    /// Invoked after the state machine has been flushed while set to rollback.
    /// </summary>
    event AsyncEventHandler? RolledBack;
}
