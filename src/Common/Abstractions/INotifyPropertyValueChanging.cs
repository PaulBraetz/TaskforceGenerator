﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskforceGenerator.Common.Abstractions;
/// <summary>
/// Declares an event that notifies observers about property values changing.
/// </summary>
public interface INotifyPropertyValueChanging
{
    /// <summary>
    /// Invoked before a property is changed.
    /// </summary>
    event EventHandler<PropertyValueChangeArgsBase> PropertyValueChanging;
}
