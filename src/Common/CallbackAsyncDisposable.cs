﻿using TaskforceGenerator.Common.Exceptions;

namespace TaskforceGenerator.Common;

/// <summary>
/// Callback-based implementation of <see cref="IAsyncDisposable"/>.
/// </summary>
public readonly struct CallbackAsyncDisposable : IAsyncDisposable
{
    private readonly Func<ValueTask> _callback;
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="callback">The callback to invoke upon <see cref="DisposeAsync"/> being called.</param>
    public CallbackAsyncDisposable(Func<ValueTask> callback)
    {
        _callback = callback;
    }
    /// <inheritdoc/>
    public ValueTask DisposeAsync() => _callback.Invoke();
    /// <inheritdoc/>
    public override Boolean Equals(Object? obj) => throw Throw.NotSupportedException.CallbackDisposableEquals;
    /// <inheritdoc/>
    public override Int32 GetHashCode() => throw Throw.NotSupportedException.CallbackDisposableGetHashCode;
    /// 
    public static Boolean operator ==(CallbackAsyncDisposable left, CallbackAsyncDisposable right) =>
        left.Equals(right);
    /// 
    public static Boolean operator !=(CallbackAsyncDisposable left, CallbackAsyncDisposable right) =>
        !(left == right);
}
