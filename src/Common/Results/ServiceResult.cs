﻿using OneOf;

namespace TaskforceGenerator.Common.Results;
/// <summary>
/// Represents a general service result.
/// </summary>
public sealed class ServiceResult : OneOfBase<CompletedSuccessResult, CompliantlyCancelledResult>
{
    private ServiceResult(OneOf<CompletedSuccessResult, CompliantlyCancelledResult> input) : base(input)
    {
    }

    private Task<ServiceResult>? _task;

    /// <summary>
    /// Gets this instance wrapped in a <see cref="System.Threading.Tasks.Task"/>.
    /// </summary>
    public Task<ServiceResult> Task => _task ??= System.Threading.Tasks.Task.FromResult(this);
    /// <summary>
    /// Gets the singleton instance for a fully completed operations result.
    /// </summary>
    public static ServiceResult Completed { get; } = new ServiceResult(CompletedSuccessResult.Instance);
    /// <summary>
    /// Gets the singleton instance for a compliantly cancelled operations result.
    /// </summary>
    public static ServiceResult CompliantlyCancelled { get; } = new(CompliantlyCancelledResult.Instance);
}
