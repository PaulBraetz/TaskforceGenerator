﻿using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Common;

/// <summary>
/// Abstract event args for the <see cref="INotifyPropertyValueChanging.PropertyValueChanging"/>
/// or <see cref="INotifyPropertyValueChanged.PropertyValueChanged"/> events.
/// </summary>
public abstract class PropertyValueChangeArgsBase
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="propertyName">The name of the property.</param>
    protected PropertyValueChangeArgsBase(String propertyName)
    {
        PropertyName = propertyName;
    }
    /// <summary>
    /// Gets the name of the property.
    /// </summary>
    public String PropertyName { get; }
    /// <summary>
    /// Attempts to convert the current instance to an instance of <see cref="PropertyValueChangeArgs{TProperty}"/>.
    /// </summary>
    /// <typeparam name="TProperty">The type of property.</typeparam>
    /// <returns>
    /// This instance converted to <see cref="PropertyValueChangeArgs{TProperty}"/> if it 
    /// is of type <see cref="PropertyValueChangeArgs{TProperty}"/>; otherwise,
    /// <see langword="null"/>.
    /// </returns>
    public PropertyValueChangeArgs<TProperty>? As<TProperty>() =>
        this as PropertyValueChangeArgs<TProperty>;
    /// <summary>
    /// Gets a value indicating if the current instance can be converted to an 
    /// instance of <see cref="PropertyValueChangeArgs{TProperty}"/>.
    /// </summary>
    /// <typeparam name="TProperty">The type of property.</typeparam>
    /// <returns>
    /// <see langword="true"/> if this instance can be converted to
    /// <see cref="PropertyValueChangeArgs{TProperty}"/>; otherwise,
    /// <see langword="false"/>.
    /// </returns>
    public Boolean Is<TProperty>() =>
        this is PropertyValueChangeArgs<TProperty>;
}
