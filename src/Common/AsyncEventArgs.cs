﻿using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Common;

/// <summary>
/// Event arguments for asynchronous events.
/// </summary>
public class AsyncEventArgs : IAsyncEventArgs
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="cancellationToken">The cancellation token used to signal event handlers to cancel their operation.</param>
    public AsyncEventArgs(CancellationToken cancellationToken)
    {
        CancellationToken = cancellationToken;
    }
    /// <inheritdoc/>
    public CancellationToken CancellationToken { get; }
    /// <summary>
    /// Gets an instance with its <see cref="CancellationToken"/> set to <see cref="CancellationToken.None"/>.
    /// </summary>
    public static AsyncEventArgs Default { get; } = new(CancellationToken.None);
}

/// <summary>
/// Event arguments for asynchronous events that communicate some additional data.
/// </summary>
/// <typeparam name="T">The type fo additional data communicated to event handlers.</typeparam>
public class AsyncEventArgs<T> : AsyncEventArgs, IAsyncEventArgs<T>
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="value">The additional data to communicate to handlers.</param>
    /// <param name="cancellationToken">The cancellation token used to signal event handlers to cancel their operation.</param>
    public AsyncEventArgs(T value, CancellationToken cancellationToken)
        : base(cancellationToken)
    {
        Value = value;
    }
    /// <inheritdoc/>
    public T Value { get; }
}