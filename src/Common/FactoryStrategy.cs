﻿using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Common;

/// <summary>
/// Strategy-based implementation of <see cref="IFactory{TProduct}"/>.
/// </summary>
/// <typeparam name="TProduct">The type of object to create.</typeparam>
public sealed class FactoryStrategy<TProduct> : IFactory<TProduct>
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="strategy">The strategy to invoke when calling <see cref="Create"/>.</param>
    public FactoryStrategy(Func<TProduct> strategy)
    {
        _strategy = strategy;
    }
    private readonly Func<TProduct> _strategy;
    /// <inheritdoc/>
    public TProduct Create() => _strategy.Invoke();
}

/// <summary>
/// Strategy-based implementation of <see cref="IFactory{TProduct, TParams}"/>.
/// </summary>
/// <typeparam name="TProduct">The type of object to create.</typeparam>
/// <typeparam name="TParams">The type of parameters required to create a new instance.</typeparam>
public sealed class FactoryStrategy<TProduct, TParams> : IFactory<TProduct, TParams>
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="strategy">The strategy to use when calling <see cref="Create(TParams)"/>.</param>
    public FactoryStrategy(Func<TParams, TProduct> strategy)
    {
        _strategy = strategy;
    }
    private readonly Func<TParams, TProduct> _strategy;
    /// <inheritdoc/>
    public TProduct Create(TParams @params) => _strategy.Invoke(@params);
}