﻿using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Exceptions;

namespace TaskforceGenerator.Common;

public static partial class AmbientCallbackStateQueue<T>
{
    private sealed partial class Context
    {
        public struct LateScope : IDisposable
        {
            private Int32 _disposedValue = BooleanState.FALSE_STATE;

            private readonly Func<T> _itemFactory;

            private LateScope(Func<T> itemFactory)
            {
                _itemFactory = itemFactory;
            }
            public static LateScope Create(Func<T> itemFactory, Action<IEnumerable<T>> callback)
            {
                var result = new LateScope(itemFactory);

                Instance.OnScopeCreated();

                Instance.SetCallback(callback);

                return result;
            }

            public void Dispose()
            {
                if(Interlocked.CompareExchange(
                    ref _disposedValue,
                    BooleanState.TRUE_STATE,
                    BooleanState.FALSE_STATE) == BooleanState.FALSE_STATE)
                {
                    var item = _itemFactory.Invoke();
                    Instance.Enqueue(item);
                    Instance.OnScopeDisposed();
                } else
                {
                    throw Throw.ObjectDisposedException.LateScope;
                }
            }
        }
    }
}
