﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Exceptions;

namespace TaskforceGenerator.Common;

/// <summary>
/// <para>
/// A closure around a request and the service using which it should be executed.
/// </para>
/// <para>
/// Attention: Make sure that code utilizing this interface is not violating the CQRS pattern.
/// </para>
/// </summary>
/// <typeparam name="TResult">The type of result produced by the request.</typeparam>
/// <typeparam name="TRequest">The type of request captured.</typeparam>
public readonly struct RequestClosure<TRequest, TResult> : IRequestClosure<TResult>
    where TRequest : IServiceRequest<TResult>
{
    private readonly TRequest _command;
    private readonly IService<TRequest, TResult> _service;
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="request">The request captured.</param>
    /// <param name="service">The service captured.</param>
    public RequestClosure(TRequest request, IService<TRequest, TResult> service)
    {
        _command = request;
        _service = service;
    }

    /// <summary>
    /// Executes the captured request using the captured service.
    /// </summary>
    public ValueTask<TResult> Execute() => _service.Execute(_command);
    /// <inheritdoc/>
    public override Boolean Equals(Object? obj) => throw Throw.NotSupportedException.ClosureEquals;
    /// <inheritdoc/>
    public override Int32 GetHashCode() => throw Throw.NotSupportedException.ClosureGetHashCode;
    /// 
    public static Boolean operator ==(RequestClosure<TRequest, TResult> left, RequestClosure<TRequest, TResult> right) =>
        left.Equals(right);
    /// 
    public static Boolean operator !=(RequestClosure<TRequest, TResult> left, RequestClosure<TRequest, TResult> right) =>
        !(left == right);
}
