﻿using OneOf;

using SimpleInjector;

using TaskforceGenerator.Application.Requests;
using TaskforceGenerator.Application.Results;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Results;
using TaskforceGenerator.Domain.Authentication;
using TaskforceGenerator.Domain.Authentication.Results;
using TaskforceGenerator.Domain.Core.Results;
using TaskforceGenerator.Presentation.Models.Abstractions;
using TaskforceGenerator.Presentation.Models.Abstractions.ConnectCitizen;
using TaskforceGenerator.Presentation.Models.ConnectCitizen;
using TaskforceGenerator.Tests.Common;

namespace TaskforceGenerator.Presentation.Models.Tests;

/// <summary>
/// Contains tests for <see cref="ConnectCitizenModel"/>.
/// </summary>
[TestClass]
public class ConectCitizenModelTests : ServiceTestBase<ConnectCitizenModel>
{
    /// <inheritdoc/>
    protected override void Configure(Container container)
    {
        base.Configure(container);

        container.Register<Stubs.ButtonModel>();
        container.Register<IButtonModel, Stubs.ButtonModel>();

        container.Register<Stubs.NavigationModel>();
        container.Register<INavigationModel, Stubs.NavigationModel>();

        container.Register<Stubs.ExceptionHandler>();
        container.Register<IExceptionHandler, Stubs.ExceptionHandler>();

        container.Register<Stubs.ConnectCitizen.ButtonGroupModel>();
        container.Register<IButtonGroupModel, Stubs.ConnectCitizen.ButtonGroupModel>();

        container.Register<Stubs.ConnectCitizen.InputGroupModel>();
        container.Register<IInputGroupModel, Stubs.ConnectCitizen.InputGroupModel>();

        container.Register<Stubs.ConnectCitizen.CodeModel>();
        container.Register<ICodeModel, Stubs.ConnectCitizen.CodeModel>();

        container.Register<ServiceStub<GenerateBioCode, GenerateBioCode.Result>>();
        container.Register<
            IService<GenerateBioCode, GenerateBioCode.Result>,
            ServiceStub<GenerateBioCode, GenerateBioCode.Result>>();

        container.Register<ServiceStub<SetPasswordForConnection, SetPasswordForConnection.Result>>();
        container.Register<
            IService<SetPasswordForConnection, SetPasswordForConnection.Result>,
            ServiceStub<SetPasswordForConnection, SetPasswordForConnection.Result>>();
    }
    /// <summary>
    /// Asserts ?
    /// </summary>
    [TestMethod]
    public void MyTestMethod()
    {

    }
}
