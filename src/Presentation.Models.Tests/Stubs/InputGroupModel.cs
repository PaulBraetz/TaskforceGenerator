﻿using System.ComponentModel;

using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Presentation.Models.Abstractions;

namespace TaskforceGenerator.Presentation.Models.Tests.Stubs;

internal sealed class InputGroupModel<TValue, TError> : HasObservableProperties, IInputGroupModel<TValue, TError>
{
    private String _label = String.Empty;
    private String _description = String.Empty;

    public InputGroupModel(InputModel<TValue, TError> inputStub)
    {
        InputStub = inputStub;
        InputStub.PropertyValueChanged += (s, e) =>
        {
            InvokePropertyValueChanged(nameof(InputStub), InputStub, InputStub);
            InvokePropertyValueChanged(nameof(Input), Input, Input);
        };
    }

    public InputModel<TValue, TError> InputStub { get; }
    public IInputModel<TValue, TError> Input => InputStub;
    public String Label
    {
        get => _label;
        set => ExchangeBackingField(ref _label, value, nameof(Label));
    }
    public String Description
    {
        get => _description;
        set => ExchangeBackingField(ref _description, value, nameof(Description));
    }
}
