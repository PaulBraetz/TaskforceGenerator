﻿using TaskforceGenerator.Common;
using TaskforceGenerator.Presentation.Models.Abstractions;

namespace TaskforceGenerator.Presentation.Models.Tests.Stubs;

internal sealed class ButtonModel : IButtonModel
{
    public String Label { get; set; }
    public Boolean Disabled { get; set; }
    public Boolean Loading { get; }
    public Boolean AllowParallelClicks { get; set; }

    public event AsyncEventHandler Clicked;

    public Boolean ClickCalled { get; set; }
    public Func<Task> ClickStrategy { get; set; }

    public async Task Click()
    {
        ClickCalled = true;
        await Clicked.InvokeAsync(this);
        await ClickStrategy();
    }
    public Task Click(CancellationToken cancellationToken)
    {
        Disabled = true;
        _ = cancellationToken.Register(() => Disabled = false);

        return Task.CompletedTask;
    }
}
