﻿using Microsoft.AspNetCore.Http;

using TaskforceGenerator.Presentation.Models.Abstractions;

namespace TaskforceGenerator.Presentation.Models.Tests.Stubs;

internal sealed class NavigationModel : INavigationModel
{
    public Boolean Navigated { get; private set; }
    public void Navigate(QueryString? query = null) => Navigated = true;
}
