﻿using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Presentation.Models.Tests.Stubs;

internal sealed class ExceptionHandler : IExceptionHandler
{
    public Boolean CanHandleCalled { get; private set; }
    public Exception CanHandleArgument { get; private set; }
    public Boolean HandleCalled { get; private set; }
    public Exception HandleArgument { get; private set; }

    public Func<Exception, Boolean> CanHandleStrategy { get; set; } = e => true;
    public Func<Exception, ValueTask> HandleStrategy { get; set; } = e => ValueTask.CompletedTask;

    public Boolean CanHandle(Exception ex)
    {
        CanHandleArgument = ex;
        CanHandleCalled = true;
        var result = CanHandleStrategy.Invoke(ex);

        return result;
    }

    public ValueTask Handle(Exception ex)
    {
        HandleArgument = ex;
        HandleCalled = true;
        var result = HandleStrategy.Invoke(ex);

        return result;
    }
}
