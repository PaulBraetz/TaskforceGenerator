﻿using System.ComponentModel;

using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Presentation.Models.Abstractions;

namespace TaskforceGenerator.Presentation.Models.Tests.Stubs;

internal sealed class InputModel<TValue, TError> : HasObservableProperties, IInputModel<TValue, TError>
{
    private TError _error;
    private TValue _value;
    private InputValidityType _validity;
    private String _placeholder;

    public InputValidityType Validity
    {
        get => _validity;
        private set
        {
            var oldValue = _validity;
            _validity = value;
            InvokePropertyValueChanged(
                nameof(Validity),
                oldValue: oldValue,
                newValue: value);
        }
    }

    public void SetInvalid(TError error)
    {
        Validity = InputValidityType.Invalid;
        Error = error;
    }
    public void SetValid() => Validity = InputValidityType.Valid;

    public void UnsetValidity() => Validity = InputValidityType.None;

    public TValue Value
    {
        get => _value;
        set
        {
            var oldValue = _value;
            _value = value;
            InvokePropertyValueChanged(
                nameof(Value),
                oldValue: oldValue,
                newValue: value);
        }
    }
    public String Placeholder
    {
        get => _placeholder;
        set
        {
            var oldValue = _placeholder;
            _placeholder = value;
            InvokePropertyValueChanged(
                nameof(Placeholder),
                oldValue: oldValue,
                newValue: value);
        }
    }
    public TError Error
    {
        get => _error;
        private set
        {
            var oldValue = _error;
            _error = value;
            InvokePropertyValueChanged(
                nameof(Error),
                oldValue: oldValue,
                newValue: value);
        }
    }

    public Boolean EnterCalled { get; private set; }
    public Func<Task> EnterStrategy { get; set; } = () => Task.CompletedTask;

    public async Task Enter()
    {
        EnterCalled = true;
        await InvokeEntered();
        await EnterStrategy();
    }

    public Task InvokeEntered() =>
        Entered.InvokeAsync(this, Value);

    public event AsyncEventHandler<IAsyncEventArgs<TValue>> Entered;

    public TValue AutoCompleteValue { get; set; }
    public Boolean AutoCompleteEnabled { get; set; }

    public Task RequestAutoComplete() => AutoCompleteRequested.InvokeAsync(this, Value);

    public event AsyncEventHandler<IAsyncEventArgs<TValue>> AutoCompleteRequested;
}
