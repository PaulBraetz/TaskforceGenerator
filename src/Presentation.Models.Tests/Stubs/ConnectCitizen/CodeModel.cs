﻿using TaskforceGenerator.Domain.Authentication;
using TaskforceGenerator.Presentation.Models.Abstractions.ConnectCitizen;

namespace TaskforceGenerator.Presentation.Models.Tests.Stubs.ConnectCitizen;

internal sealed class CodeModel : ICodeModel
{
    public BioCode Value { get; set; }
    public String CodeError { get; set; }
}
