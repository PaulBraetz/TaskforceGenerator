﻿namespace TaskforceGenerator.Aspects.Abstractions;

/// <summary>
/// Strategy for extracting citizen names from a value, typically a request Object.
/// </summary>
/// <typeparam name="T">The type of value containing citizen names.</typeparam>
public interface ICitizenNameExtractionStrategy<T>
{
    /// <summary>
    /// Gets the citizen names contained in an instance of <typeparamref name="T"/>.
    /// </summary>
    /// <param name="value">
    /// The value from which to extract the citizen names.
    /// </param>
    /// <returns>
    /// The names contained in <paramref name="value"/>.
    /// </returns>
    IEnumerable<String> GetNames(T value);
}
