﻿using OneOf;

using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Requests;
using TaskforceGenerator.Domain.Core.Results;

namespace TaskforceGenerator.Aspects.Abstractions;

/// <summary>
/// Base class for types implementing <see cref="ICitizenNameReplacementStrategy{T}"/>.
/// </summary>
/// <typeparam name="T">The type of value containing citizen names.</typeparam>
public abstract class CitizenNameReplacementStrategyBase<T>
    : ICitizenNameReplacementStrategy<T>
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="nameService">The service used to determine citizen names.</param>
    protected CitizenNameReplacementStrategyBase(
        IService<LoadActualName, OneOf<String, CitizenNotExistingResult>> nameService)
    {
        _nameService = nameService;
    }

    private readonly IService<LoadActualName, OneOf<String, CitizenNotExistingResult>> _nameService;

    /// <summary>
    /// Determines the actual name of a citizen.
    /// </summary>
    /// <param name="name">The name of the citizen whose actual name to determine.</param>
    /// <param name="cancellationToken">The token used to signal cancellation of the operation.</param>
    /// <returns>The actual name of the citizen if he exists; otherwise, <paramref name="name"/>.</returns>
    protected async ValueTask<String> LoadName(String name, CancellationToken cancellationToken)
    {
        var loadResult = await new LoadActualName(name, cancellationToken).Using(_nameService);
        var result = loadResult.Match(
            actualName => actualName,
            notExists => name);

        return result;
    }
    /// <inheritdoc/>
    public abstract ValueTask<T> ReplaceNames(T old, CancellationToken cancellationToken);
}
