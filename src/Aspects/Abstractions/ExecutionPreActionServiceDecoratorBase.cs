﻿using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Aspects.Abstractions;

/// <summary>
/// Base class for decorators defining actions to be executed before the decorated query services execution.
/// </summary>
/// <typeparam name="TRequest">The type of query the decorated service can execute.</typeparam>
/// <typeparam name="TResult">The type of result yielded by the query service.</typeparam>
public abstract class ExecutionPreActionServiceDecoratorBase<TRequest, TResult>
    : IService<TRequest, TResult>
    where TRequest : IServiceRequest<TResult>
{
    /// <summary>
    /// Initializes a new instance. 
    /// </summary>
    /// <param name="decorated">The decorated query service.</param>
    protected ExecutionPreActionServiceDecoratorBase(IService<TRequest, TResult> decorated)
    {
        _decorated = decorated;
    }

    private readonly IService<TRequest, TResult> _decorated;

    /// <summary>
    /// Executes the pre-action.
    /// </summary>
    protected virtual ValueTask ExecutePreAction(TRequest request) => ValueTask.CompletedTask;

    /// <inheritdoc/>
    public async ValueTask<TResult> Execute(TRequest query)
    {
        await ExecutePreAction(query);
        var result = await _decorated.Execute(query);

        return result;
    }
}