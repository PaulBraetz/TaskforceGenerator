﻿namespace TaskforceGenerator.Aspects.Abstractions;

/// <summary>
/// Strategy for implementing the citizen name replacement proxy.
/// </summary>
/// <typeparam name="T">The type of value containing citizen names.</typeparam>
public interface ICitizenNameReplacementStrategy<T>
{
    /// <summary>
    /// Creates a replacement value with citizen names substituted by their actual names.
    /// </summary>
    /// <param name="old">The old value.</param>
    /// <param name="cancellationToken">The token used to signal cancellation of the operation.</param>
    /// <returns>
    /// A new instance of <typeparamref name="T"/>, as obtained from the 
    /// prototype <paramref name="old"/>, with citizen names substituted by their actual names.
    /// </returns>
    ValueTask<T> ReplaceNames(T old, CancellationToken cancellationToken);
}
