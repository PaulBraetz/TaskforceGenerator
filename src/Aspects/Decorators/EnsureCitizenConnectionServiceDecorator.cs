﻿using TaskforceGenerator.Aspects.Abstractions;
using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Results;
using TaskforceGenerator.Domain.Authentication.Requests;

namespace TaskforceGenerator.Aspects.Decorators;

/// <summary>
/// Decorates a query service with a citizen connection ensuring aspect.
/// </summary>
/// <typeparam name="TRequest">The type of query to execute.</typeparam>
/// <typeparam name="TResult">The type of result yielded by executing a query.</typeparam>
public sealed class EnsureCitizenConnectionServiceDecorator<TRequest, TResult>
    : ExecutionPreActionServiceDecoratorBase<TRequest, TResult>
    where TRequest : IServiceRequest<TResult>
{
    /// <summary>
    /// Initializes  a new instance.
    /// </summary>
    /// <param name="extractionStrategy">The strategy used to extract a citizens name form queries.</param>
    /// <param name="ensureService">The service to use when ensuring a connection for the citizen requested exists.</param>
    /// <param name="decorated">The decorated service.</param>
    public EnsureCitizenConnectionServiceDecorator(ICitizenNameExtractionStrategy<TRequest> extractionStrategy,
                                                          IService<EnsureCitizenConnection, ServiceResult> ensureService,
                                                          IService<TRequest, TResult> decorated) : base(decorated)
    {
        _extractionStrategy = extractionStrategy;
        _ensureService = ensureService;
    }

    private readonly ICitizenNameExtractionStrategy<TRequest> _extractionStrategy;
    private readonly IService<EnsureCitizenConnection, ServiceResult> _ensureService;

    /// <inheritdoc/>
    protected override async ValueTask ExecutePreAction(TRequest value)
    {
        var ensureTasks = _extractionStrategy.GetNames(value)
            .Select(name =>
                new EnsureCitizenConnection(name, value.CancellationToken)
                .Using(_ensureService)
                .AsTask());

        _ = await Task.WhenAll(ensureTasks);
    }
}
