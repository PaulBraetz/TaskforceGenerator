﻿using OneOf;

using TaskforceGenerator.Aspects.Abstractions;
using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Results;
using TaskforceGenerator.Domain.Core.Requests;
using TaskforceGenerator.Domain.Core.Results;

namespace TaskforceGenerator.Aspects.Decorators;

/// <summary>
/// Decorates a query service with a citizen ensuring aspect.
/// </summary>
/// <typeparam name="TRequest">The type of query to execute.</typeparam>
/// <typeparam name="TResult">The type of result yielded by executing a query.</typeparam>
public sealed class EnsureCitizenServiceDecorator<TRequest, TResult>
    : ExecutionPreActionServiceDecoratorBase<TRequest, TResult>
    where TRequest : IServiceRequest<TResult>
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="extractionStrategy">The strategy used to extract citizen names from queries.</param>
    /// <param name="ensureService">The service to use when ensuring a connection for the citizen requested exists.</param>
    /// <param name="decorated">The decorated service.</param>
    public EnsureCitizenServiceDecorator(
        ICitizenNameExtractionStrategy<TRequest> extractionStrategy,
        IService<EnsureCitizen, OneOf<ServiceResult, CitizenNotExistingResult>> ensureService,
        IService<TRequest, TResult> decorated) : base(decorated)
    {
        _extractionStrategy = extractionStrategy;
        _ensureService = ensureService;
    }

    private readonly ICitizenNameExtractionStrategy<TRequest> _extractionStrategy;
    private readonly IService<EnsureCitizen, OneOf<ServiceResult, CitizenNotExistingResult>> _ensureService;

    /// <inheritdoc/>
    protected override async ValueTask ExecutePreAction(TRequest value)
    {
        var ensureTasks = _extractionStrategy.GetNames(value)
            .Select(name =>
                new EnsureCitizen(name, value.CancellationToken)
                .Using(_ensureService)
                .AsTask());

        _ = await Task.WhenAll(ensureTasks);
    }
}
