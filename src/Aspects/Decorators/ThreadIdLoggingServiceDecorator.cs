﻿using TaskforceGenerator.Aspects.Abstractions;
using TaskforceGenerator.Aspects.Logging;
using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Aspects.Decorators;

/// <summary>
/// Logger for logging the current thread id at query execution time.
/// </summary>
/// <typeparam name="TRequest">The type of query executed.</typeparam>
/// <typeparam name="TResult">The type of result yielded by executing a query.</typeparam>
public sealed class ThreadIdLoggingServiceDecorator<TRequest, TResult> :
    IService<TRequest, TResult>
    where TRequest : IServiceRequest<TResult>
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="logger">The service used for logging.</param>
    /// <param name="decorated">The decorated service.</param>
    public ThreadIdLoggingServiceDecorator(
        IService<TRequest, TResult> decorated,
        ILoggingService logger)
    {
        _logger = logger;
        _decorated = decorated;
    }

    private readonly ILoggingService _logger;
    private readonly IService<TRequest, TResult> _decorated;

    /// <inheritdoc/>
    public ValueTask<TResult> Execute(TRequest request)
    {
        using(_ = Logs.Log(ThreadIdLogEntry.Current, _logger))
        {
            return _decorated.Execute(request);
        }
    }
}
