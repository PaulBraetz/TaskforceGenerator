﻿using TaskforceGenerator.Aspects.Abstractions;
using TaskforceGenerator.Aspects.Logging;
using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Aspects.Decorators;
/// <summary>
/// Decorates a service with the service type logging aspect.
/// </summary>
/// <typeparam name="TRequest">The type of request to execute.</typeparam>
/// <typeparam name="TResult">The type of result yielded by executing a request.</typeparam>
/// <typeparam name="TService">The type of service decorated.</typeparam>
/// <remarks>
/// <typeparamref name="TService"/> must not be constrained so as to allow correct composition of decorators.
/// It is only used to provide the service type underlying the decorator hierarchy.
/// </remarks>
public sealed class ServiceTypeLoggingDecorator<TRequest, TResult, TService> : IService<TRequest, TResult>
    where TRequest : IServiceRequest<TResult>
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="logger">The logger to use for logging execution status.</param>
    /// <param name="decorated">The decorated service.</param>
    public ServiceTypeLoggingDecorator(ILoggingService logger, IService<TRequest, TResult> decorated)
    {
        _logger = logger;
        _decorated = decorated;
    }

    private readonly ILoggingService _logger;
    private readonly IService<TRequest, TResult> _decorated;

    /// <inheritdoc/>
    public ValueTask<TResult> Execute(TRequest request)
    {
        var entry = new ServiceTypeLogEntry(typeof(TService));
        using var _ = Logs.Log(entry, _logger);
        return _decorated.Execute(request);
    }
}
