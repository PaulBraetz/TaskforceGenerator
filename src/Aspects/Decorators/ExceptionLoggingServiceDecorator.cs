﻿using TaskforceGenerator.Aspects.Abstractions;

using TaskforceGenerator.Aspects.Logging;
using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Aspects.Decorators;

/// <summary>
/// Exception logging decorator for query services.
/// </summary>
/// <typeparam name="TRequest">The type of query to execute and log.</typeparam>
/// <typeparam name="TResult">The type of result to yield and log.</typeparam>
public sealed class ExceptionLoggingServiceDecorator<TRequest, TResult>
    : IService<TRequest, TResult>
    where TRequest : IServiceRequest<TResult>
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="logger">The logger to use for logging execution status.</param>
    /// <param name="decorated">The decorated service.</param>
    public ExceptionLoggingServiceDecorator(ILoggingService logger,
                                                 IService<TRequest, TResult> decorated)
    {
        _logger = logger;
        _decorated = decorated;
    }

    private readonly ILoggingService _logger;
    private readonly IService<TRequest, TResult> _decorated;

    private static readonly AsyncLocal<Exception?> _localException = new();

    /// <inheritdoc/>
    public async ValueTask<TResult> Execute(TRequest query)
    {
        using(_ = Logs.LogLateConditional(confirmPush, createLog, _logger))
        {
            try
            {
                //await in order to force exception
                var result = await _decorated.Execute(query);

                return result;
            } catch(Exception ex)
            {
                _localException.Value = ex;
                throw;
            }
        }

        Boolean confirmPush() => _localException.Value != null;

        ILogEntry createLog()
        {
            var exception = _localException.Value!;
            _localException.Value = null;
            var logEntry = new ExceptionLogEntry<TRequest>(exception);

            return logEntry;
        }
    }
}
