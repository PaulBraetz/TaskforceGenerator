﻿using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Aspects.Decorators;

/// <summary>
/// Decorates a query service with event publishing functionality.
/// </summary>
/// <typeparam name="TRequest">The type of command to execute.</typeparam>
/// <typeparam name="TResult">The type of result to be produced by a service.</typeparam>
public sealed class NotifyExceptionServiceDecorator<TRequest, TResult>
    : IService<TRequest, TResult>
    where TRequest : IServiceRequest<TResult>
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="decorated">The service decorated.</param>
    /// <param name="observer">The observer notified about exceptions.</param>
    public NotifyExceptionServiceDecorator(
        IService<TRequest, TResult> decorated,
        Common.Abstractions.IObserver<Exception> observer)
    {
        _decorated = decorated;
        _observer = observer;
    }

    private readonly IService<TRequest, TResult> _decorated;
    private readonly Common.Abstractions.IObserver<Exception> _observer;

    /// <inheritdoc/>
    public async ValueTask<TResult> Execute(TRequest command)
    {
        try
        {
            //await to force exception
            var result = await _decorated.Execute(command);

            return result;
        } catch(Exception ex)
        {
            _observer.Notify(ex);
            throw;
        }
    }
}
