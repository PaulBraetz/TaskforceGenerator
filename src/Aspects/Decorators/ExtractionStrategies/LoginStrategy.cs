﻿using TaskforceGenerator.Application.Requests;
using TaskforceGenerator.Aspects.Abstractions;

namespace TaskforceGenerator.Aspects.Decorators.ExtractionStrategies;

/// <summary>
/// Name replacement strategy for <see cref="Login"/>.
/// </summary>
public sealed class LoginStrategy : ICitizenNameExtractionStrategy<Login>
{
    /// <summary>
    /// Initializes a new instance. If possible, use <see cref="Instance"/> instead.
    /// </summary>
    public LoginStrategy() { }
    /// <summary>
    /// Gets the singleton instance.
    /// </summary>
    public static LoginStrategy Instance { get; } = new();
    /// <inheritdoc/>
    public IEnumerable<String> GetNames(Login value) =>
        new[] { value.CitizenName };
}
