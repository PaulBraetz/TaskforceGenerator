﻿using TaskforceGenerator.Application.Requests;
using TaskforceGenerator.Aspects.Abstractions;

namespace TaskforceGenerator.Aspects.Decorators.ExtractionStrategies;

/// <summary>
/// Name extraction strategy for <see cref="GenerateBioCode"/>.
/// </summary>
public sealed class GenerateBioCodeStrategy : ICitizenNameExtractionStrategy<GenerateBioCode>
{
    /// <summary>
    /// Initializes a new instance. If possible, use <see cref="Instance"/> instead.
    /// </summary>
    public GenerateBioCodeStrategy() { }
    /// <summary>
    /// Gets the singleton instance.
    /// </summary>
    public static GenerateBioCodeStrategy Instance { get; } = new();
    /// <inheritdoc/>
    public IEnumerable<String> GetNames(GenerateBioCode value) =>
        new[] { value.CitizenName };
}
