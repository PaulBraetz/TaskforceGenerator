﻿using TaskforceGenerator.Application.Requests;
using TaskforceGenerator.Aspects.Abstractions;

namespace TaskforceGenerator.Aspects.Decorators.ExtractionStrategies;

/// <summary>
/// Name replacement strategy for <see cref="SetPasswordForConnection"/>.
/// </summary>
public sealed class SetPasswordForCitizenStrategy
    : ICitizenNameExtractionStrategy<SetPasswordForConnection>
{
    /// <summary>
    /// Initializes a new instance. If possible, use <see cref="Instance"/> instead.
    /// </summary>
    public SetPasswordForCitizenStrategy() { }
    /// <summary>
    /// Gets the singleton instance.
    /// </summary>
    public static SetPasswordForCitizenStrategy Instance { get; } = new();
    /// <inheritdoc/>
    public IEnumerable<String> GetNames(SetPasswordForConnection value) =>
        new[] { value.CitizenName };
}
