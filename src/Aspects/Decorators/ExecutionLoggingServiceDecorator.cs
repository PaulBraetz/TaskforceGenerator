﻿using TaskforceGenerator.Aspects.Abstractions;
using TaskforceGenerator.Aspects.Logging;
using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Aspects.Decorators;

/// <summary>
/// Logging decorator for query services.
/// </summary>
/// <typeparam name="TRequest">The type of query to execute and log.</typeparam>
/// <typeparam name="TResult">The type of result to yield and log.</typeparam>
public sealed class ExecutionLoggingServiceDecorator<TRequest, TResult>
    : IService<TRequest, TResult>
    where TRequest : IServiceRequest<TResult>
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="logger">The logger to use for logging execution status.</param>
    /// <param name="decorated">The decorated service.</param>
    /// <param name="formatter">The formatter to use when determining the logged query value.</param>
    /// <param name="resultFormatter">The formatter to use when determining the logged result value.</param>
    public ExecutionLoggingServiceDecorator(
        ILoggingService logger,
        IService<TRequest, TResult> decorated,
        IStaticFormatter<TRequest> formatter,
        IStaticFormatter<TResult> resultFormatter)
    {
        _logger = logger;
        _decorated = decorated;
        _queryFormatter = formatter;
        _resultFormatter = resultFormatter;
    }

    private readonly ILoggingService _logger;
    private readonly IService<TRequest, TResult> _decorated;
    private readonly IStaticFormatter<TRequest> _queryFormatter;
    private readonly IStaticFormatter<TResult> _resultFormatter;

    private static readonly AsyncLocal<(Boolean set, TResult? result)> _localResult = new();

    /// <inheritdoc/>
    public async ValueTask<TResult> Execute(TRequest query)
    {
        using(_ = Logs.Log(new BeforeExecutionLogEntry<TRequest>(query, _queryFormatter), _logger))
        {
            using(_ = Logs.LogLateConditional(confirmSecondLog, createSecondLog, _logger))
            {
                var result = await _decorated.Execute(query);

                _localResult.Value = (true, result);

                return result;
            }
        }

        Boolean confirmSecondLog() => _localResult.Value.set;
        ILogEntry createSecondLog()
        {
            var queryResult = _localResult.Value.result;
            _localResult.Value = (false, default);
            var result = new AfterQueryExecutionLogEntry<TRequest, TResult>(queryResult, _resultFormatter);

            return result;
        }
    }
}
