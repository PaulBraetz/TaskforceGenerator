﻿using Microsoft.AspNetCore.Http;

using TaskforceGenerator.Aspects.Abstractions;
using TaskforceGenerator.Aspects.Logging;
using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Aspects.Decorators;

/// <summary>
/// Decorator for query services that logs the http context connection id before executing the decorated service.
/// </summary>
/// <typeparam name="TRequest">The type of query to execute.</typeparam>
/// <typeparam name="TResult">The type of result yielded by executing a query.</typeparam>
public sealed class HttpContextConnectionIdLoggingServiceDecorator<TRequest, TResult> :
    IService<TRequest, TResult>
    where TRequest : IServiceRequest<TResult>
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="logger">The service used for logging.</param>
    /// <param name="decorated">The decorated service.</param>
    /// <param name="contextAccessor">The accessor used to obtain the <see cref="HttpContext.Connection"/> and its id.</param>
    public HttpContextConnectionIdLoggingServiceDecorator(
        IService<TRequest, TResult> decorated,
        IHttpContextAccessor contextAccessor,
        ILoggingService logger)
    {
        _contextAccessor = contextAccessor;
        _logger = logger;
        _decorated = decorated;
    }

    private readonly ILoggingService _logger;
    private readonly IHttpContextAccessor _contextAccessor;
    private readonly IService<TRequest, TResult> _decorated;

    /// <inheritdoc/>
    public ValueTask<TResult> Execute(TRequest request)
    {
        ILogEntry log = _contextAccessor.HttpContext != null ?
            new HttpContextConnectionIdLogEntry(_contextAccessor.HttpContext.Connection.Id) :
            new NoHttpContextLogEntry();

        using(_ = Logs.Log(log, _logger))
        {
            return _decorated.Execute(request);
        }
    }
}
