﻿using System.Diagnostics;

using TaskforceGenerator.Aspects.Abstractions;
using TaskforceGenerator.Aspects.Logging;
using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Aspects.Decorators;

/// <summary>
/// Execution time logging decorator for query services.
/// </summary>
/// <typeparam name="TRequest">The type of query to execute and log.</typeparam>
/// <typeparam name="TResult">The type of result to yield and log.</typeparam>
public sealed class ExecutionTimeLoggingServiceDecorator<TRequest, TResult>
    : IService<TRequest, TResult>
    where TRequest : IServiceRequest<TResult>
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="logger">The logger to use for logging execution time.</param>
    /// <param name="decorated">The decorated service.</param>
    public ExecutionTimeLoggingServiceDecorator(
        ILoggingService logger,
        IService<TRequest, TResult> decorated)
    {
        _logger = logger;
        _decorated = decorated;
    }

    private readonly ILoggingService _logger;
    private readonly IService<TRequest, TResult> _decorated;

    /// <inheritdoc/>
    public async ValueTask<TResult> Execute(TRequest query)
    {
        var stopWatch = new Stopwatch();
        stopWatch.Start();
        using(_ = Logs.LogLate(createLog, _logger))
        {
            var result = await _decorated.Execute(query);

            return result;
        }

        ILogEntry createLog()
        {
            stopWatch!.Stop();
            var result = new ExecutionTimeLogEntry<TRequest>(stopWatch!.ElapsedTicks);

            return result;
        }
    }
}
