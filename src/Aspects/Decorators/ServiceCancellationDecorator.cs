﻿using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Results;

namespace TaskforceGenerator.Aspects.Decorators;

/// <summary>
/// Decorates simple services with the cancellation short-circuit aspect.
/// </summary>
/// <typeparam name="TRequest">The type of request to execute.</typeparam>
public sealed class ServiceCancellationDecorator<TRequest>
    : IService<TRequest>
    where TRequest : IServiceRequest
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="decorated">The decorated service.</param>
    public ServiceCancellationDecorator(IService<TRequest> decorated)
    {
        _decorated = decorated;
    }

    private readonly IService<TRequest> _decorated;

    /// <inheritdoc/>
    public ValueTask<ServiceResult> Execute(TRequest query)
    {
        var result = query.CancellationToken.IsCancellationRequested ?
            ServiceResult.CompliantlyCancelled.Task.AsValueTask() :
            _decorated.Execute(query);

        return result;
    }
}
