﻿using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Aspects.Decorators;

/// <summary>
/// Decorates a command service with success event publishing functionality.
/// </summary>
/// <typeparam name="TRequest">The type of command to execute.</typeparam>
/// <typeparam name="TResult">The type of result to be produced by a service.</typeparam>
public sealed class NotifySuccessServiceDecorator<TRequest, TResult>
    : IService<TRequest, TResult>
    where TRequest : IServiceRequest<TResult>
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="decorated">The service decorated.</param>
    /// <param name="observer">The observer notified about successful executions.</param>
    public NotifySuccessServiceDecorator(
        IService<TRequest, TResult> decorated,
        Common.Abstractions.IObserver<TRequest> observer)
    {
        _decorated = decorated;
        _observer = observer;
    }

    private readonly IService<TRequest, TResult> _decorated;
    private readonly Common.Abstractions.IObserver<TRequest> _observer;

    /// <inheritdoc/>
    public async ValueTask<TResult> Execute(TRequest command)
    {
        var result = await _decorated.Execute(command);
        _observer.Notify(command);

        return result;
    }
}
