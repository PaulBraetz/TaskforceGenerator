﻿using TaskforceGenerator.Aspects.Abstractions;
using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Aspects.Proxies;

/// <summary>
/// Intercepts queries that pass a citizens name, replacing them with the RSI-registered (actual name) one instead.
/// </summary>
/// <typeparam name="TRequest">The type of query to execute.</typeparam>
/// <typeparam name="TResult">The type of result yielded by executing a query.</typeparam>
public sealed class CitizenNameReplacementServiceProxy<TRequest, TResult>
    : IService<TRequest, TResult>
    where TRequest : IServiceRequest<TResult>
{
    private readonly IService<TRequest, TResult> _intercepted;
    private readonly ICitizenNameReplacementStrategy<TRequest> _replacementStrategy;

    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="intercepted">The service whose commands to intercept.</param>
    /// <param name="replacementStrategy">The strategy to use when analyzing and replacing queries.</param>
    public CitizenNameReplacementServiceProxy(
        IService<TRequest, TResult> intercepted,
        ICitizenNameReplacementStrategy<TRequest> replacementStrategy)
    {
        _intercepted = intercepted;
        _replacementStrategy = replacementStrategy;
    }

    /// <inheritdoc/>
    public async ValueTask<TResult> Execute(TRequest command)
    {
        command = await _replacementStrategy.ReplaceNames(command, command.CancellationToken);
        var result = await _intercepted.Execute(command);

        return result;
    }
}
