﻿using TaskforceGenerator.Aspects.Abstractions;
using TaskforceGenerator.Aspects.Logging;
using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Aspects.Decorators;

/// <summary>
/// Adds result caching functionality to a service.
/// </summary>
/// <typeparam name="TRequest">The type of query to execute.</typeparam>
/// <typeparam name="TResult">The type of result yielded by executing a query.</typeparam>
public sealed partial class ResultCacheServiceProxy<TRequest, TResult>
    : IService<TRequest, TResult>
    where TRequest : IServiceRequest<TResult>
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="decorated">The decorated service.</param>
    /// <param name="cache">The cache used to store results.</param>
    /// <param name="logger">The logger toi use when logging cache hits.</param>
    public ResultCacheServiceProxy(IService<TRequest, TResult> decorated,
                                   ICache<TRequest, Task<TResult>> cache,
                                   ILoggingService logger)
    {
        _decorated = decorated;
        _cache = cache;
        _logger = logger;
    }
    private readonly IService<TRequest, TResult> _decorated;
    private readonly ICache<TRequest, Task<TResult>> _cache;
    private readonly ILoggingService _logger;

    /// <inheritdoc/>
    public async ValueTask<TResult> Execute(TRequest query)
    {
        var factory = new ValueFactory(CreateResult);

        using(_ = Logs.LogLate(createLog, _logger))
        {
            return await _cache.GetOrAdd(query, factory.CreateValue);
        }

        ILogEntry createLog() =>
            factory!.FactoryCalled ?
            new ResultCacheMissLogEntry<TRequest>() :
            new ResultCacheHitLogEntry<TRequest>();
    }
    private async Task<TResult> CreateResult(TRequest query)
    {
        var result = await _decorated.Execute(query);
        return result;
    }
}
