﻿using OneOf;

using TaskforceGenerator.Application.Requests;
using TaskforceGenerator.Aspects.Abstractions;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Requests;
using TaskforceGenerator.Domain.Core.Results;

namespace TaskforceGenerator.Aspects.Proxies.ReplacementStrategies;

/// <summary>
/// Name replacement strategy for <see cref="SetPasswordForConnection"/>.
/// </summary>
public sealed class SetPasswordForCitizenStrategy
    : CitizenNameReplacementStrategyBase<SetPasswordForConnection>
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="nameService">The service used to determine citizen names.</param>
    public SetPasswordForCitizenStrategy(IService<LoadActualName, OneOf<String, CitizenNotExistingResult>> nameService)
        : base(nameService)
    {
    }

    /// <inheritdoc/>
    public override async ValueTask<SetPasswordForConnection> ReplaceNames(SetPasswordForConnection old, CancellationToken cancellationToken)
    {
        var actualName = await LoadName(old.CitizenName, cancellationToken);
        var result = old with { CitizenName = actualName };

        return result;
    }
}
