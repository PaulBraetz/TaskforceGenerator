﻿using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Aspects.Decorators;

public sealed partial class ResultCacheServiceProxy<TRequest, TResult>
    where TRequest : IServiceRequest<TResult>
{
    private sealed class ValueFactory
    {
        private readonly Func<TRequest, Task<TResult>> _factory;

        public ValueFactory(Func<TRequest, Task<TResult>> factory)
        {
            _factory = factory;
        }

        public Boolean FactoryCalled { get; private set; }

        public Task<TResult> CreateValue(TRequest key)
        {
            FactoryCalled = true;
            return _factory.Invoke(key);
        }
    }
}
