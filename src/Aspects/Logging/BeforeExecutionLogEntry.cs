﻿using Microsoft.Extensions.Logging;

using TaskforceGenerator.Aspects.Abstractions;
using TaskforceGenerator.Common.Abstractions;

using static System.Formats.Asn1.AsnWriter;

namespace TaskforceGenerator.Aspects.Logging;

/// <summary>
/// Log entry for logging executions (command or query) that are about to commence.
/// </summary>
public readonly struct BeforeExecutionLogEntry<TScope> : ILogEntry
{
    /// <inheritdoc/>
    public LogLevel Level { get; }
    private readonly TScope _scope;
    private readonly IStaticFormatter<TScope> _formatter;
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="scope">The scope executed.</param>
    /// <param name="formatter">The formatter used to format the scope.</param>
    /// <param name="level">The level at which the execution is to be logged.</param>
    public BeforeExecutionLogEntry(
        TScope scope,
        IStaticFormatter<TScope> formatter,
        LogLevel level = LogLevel.Information)
    {
        Level = level;
        _scope = scope;
        _formatter = formatter;
    }

    /// <inheritdoc/>
    public String Evaluate() => $"Execute {typeof(TScope).Name}: {_formatter.Format(_scope)}";
}
