﻿using Microsoft.Extensions.Logging;

using TaskforceGenerator.Aspects.Abstractions;

namespace TaskforceGenerator.Aspects.Logging;

/// <summary>
/// Command for logging execution times.
/// </summary>
public readonly struct ExecutionTimeLogEntry<TScope> : ILogEntry
{
    private readonly Lazy<String> _evaluation;

    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="executionTimeTicks">The execution time measured.</param>
    /// <param name="level">The level at which the execution time is to be logged.</param>
    public ExecutionTimeLogEntry(Int64 executionTimeTicks,
                                 LogLevel level = LogLevel.Information)
    {
        Level = level;
        _evaluation = new(() => Format(executionTimeTicks));
    }

    /// <inheritdoc/>
    public LogLevel Level { get; }

    private static readonly Int64 _microSecondTicks = TimeSpan.FromMilliseconds(0.001).Ticks;
    private static readonly Int64 _milliSecondTicks = TimeSpan.FromMilliseconds(1).Ticks;
    private static readonly Int64 _secondTicks = TimeSpan.FromSeconds(1).Ticks;
    private static readonly Int64 _minuteTicks = TimeSpan.FromMinutes(1).Ticks;

    /// <inheritdoc/>
    public String Evaluate() => _evaluation.Value;

    private static String Format(Int64 executionTimeTicks)
    {
        var executionTimeMicros = executionTimeTicks / _microSecondTicks;
        var executionTimeFormatted =
            executionTimeTicks >= _minuteTicks ?
            $"{executionTimeMicros / 60e6:F1}m" :
            executionTimeTicks >= _secondTicks ?
            $"{executionTimeMicros / 1e6:F1}s" :
            executionTimeTicks >= _milliSecondTicks ?
            $"{executionTimeMicros / 1e3:F1}ms" :
            $"{executionTimeMicros}μs";

        return $"Done with {typeof(TScope).Name}: {executionTimeFormatted}";
    }
}
