﻿using Microsoft.Extensions.Logging;

using TaskforceGenerator.Aspects.Abstractions;

namespace TaskforceGenerator.Aspects.Logging;

/// <summary>
/// Wrapper for logging exceptions.
/// </summary>
public readonly struct ExceptionLogEntry<TScope> : ILogEntry
{
    private readonly Exception _exception;
    /// <inheritdoc/>
    public LogLevel Level { get; }

    /// <summary>
    /// Initializes a new instance with the <see cref="LogLevel.Error"/> level.
    /// </summary>
    /// <param name="exception">The exception to be logged.</param>
    /// <param name="level">The level at which the entry is to be logged.</param>
    public ExceptionLogEntry(Exception exception,
                             LogLevel level = LogLevel.Error)
    {
        _exception = exception;
        Level = level;
    }

    /// <inheritdoc/>
    public String Evaluate() => $"Error in {typeof(TScope).Name}: {_exception}";
}