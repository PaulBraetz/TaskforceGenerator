﻿using Microsoft.Extensions.Logging;

using TaskforceGenerator.Aspects.Abstractions;

namespace TaskforceGenerator.Aspects.Logging;

/// <summary>
/// Log entry for logging a services type.
/// </summary>
public readonly struct ServiceTypeLogEntry : ILogEntry
{
    /// <inheritdoc/>
    public LogLevel Level { get; }
    private readonly Type _serviceType;
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="serviceType">The type of service to log.</param>
    /// <param name="level">The level at which the execution is to be logged.</param>
    public ServiceTypeLogEntry(
        Type serviceType,
        LogLevel level = LogLevel.Information)
    {
        Level = level;
        _serviceType = serviceType;
    }

    /// <inheritdoc/>
    public String Evaluate() => $"Service Type: {_serviceType.Name}";
}