﻿using Microsoft.Extensions.Logging;

using TaskforceGenerator.Aspects.Abstractions;

namespace TaskforceGenerator.Aspects.Logging;

/// <summary>
/// Log entry for cache misses.
/// </summary>
/// <typeparam name="TRequest">The type of query whose cache was missed.</typeparam>
public readonly struct ResultCacheMissLogEntry<TRequest> : ILogEntry
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="level">The level at which the cache miss is to be logged.</param>
    public ResultCacheMissLogEntry(LogLevel level = LogLevel.Information)
    {
        _level = level;
    }

    private readonly LogLevel? _level;
    /// <inheritdoc/>
    public LogLevel Level => _level ?? LogLevel.Information;
    /// <inheritdoc/>
    public String Evaluate() => $"Cache miss for {typeof(TRequest).Name}";
}
