﻿using Microsoft.Extensions.Logging;

using TaskforceGenerator.Aspects.Abstractions;

namespace TaskforceGenerator.Aspects.Logging;

/// <summary>
/// Log entry for not executing in the context of a http request.
/// </summary>
public readonly struct NoHttpContextLogEntry : ILogEntry
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="level">The level at which the connection id is to be logged.</param>
    public NoHttpContextLogEntry(LogLevel level = LogLevel.Information)
    {
        _level = level;
    }
    private readonly LogLevel? _level;
    /// <inheritdoc/>
    public LogLevel Level => _level ?? LogLevel.Information;
    /// <inheritdoc/>
    public String Evaluate() => "No http context";
}
