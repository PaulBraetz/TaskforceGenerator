﻿using Microsoft.Extensions.Logging;

using TaskforceGenerator.Aspects.Abstractions;

namespace TaskforceGenerator.Aspects.Logging;

/// <summary>
/// Log entry for cache hits.
/// </summary>
/// <typeparam name="TRequest">The type of query whose cache was hit.</typeparam>
public readonly struct ResultCacheHitLogEntry<TRequest> : ILogEntry
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="level">The level at which the cache hit is to be logged.</param>
    public ResultCacheHitLogEntry(LogLevel level = LogLevel.Information)
    {
        _level = level;
    }

    private readonly LogLevel? _level;
    /// <inheritdoc/>
    public LogLevel Level => _level ?? LogLevel.Information;
    /// <inheritdoc/>
    public String Evaluate() => $"Cache hit for {typeof(TRequest).Name}";
}
