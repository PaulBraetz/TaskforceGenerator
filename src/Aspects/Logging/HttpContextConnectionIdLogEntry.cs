﻿using Microsoft.Extensions.Logging;

using TaskforceGenerator.Aspects.Abstractions;

namespace TaskforceGenerator.Aspects.Logging;

/// <summary>
/// Log entry for the current http requests connection id.
/// </summary>
public readonly struct HttpContextConnectionIdLogEntry : ILogEntry
{
    private readonly String _connectionId;
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="connectionId">The current requests connection id.</param>
    /// <param name="level">The level at which the connection id is to be logged.</param>
    public HttpContextConnectionIdLogEntry(String connectionId, LogLevel level = LogLevel.Information)
    {
        _connectionId = connectionId;
        Level = level;
    }
    /// <inheritdoc/>
    public LogLevel Level { get; }
    /// <inheritdoc/>
    public String Evaluate() => $"Connection id: {_connectionId}";
}
