﻿using FluentAssertions;

using SimpleInjector;

using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Authentication.Queries;
using TaskforceGenerator.Domain.Authentication.Requests;
using TaskforceGenerator.Infrastructure.Queries;
using TaskforceGenerator.Infrastructure.Services;
using TaskforceGenerator.Tests.Common;

namespace TaskforceGenerator.Infrastructure.Tests;

/// <summary>
/// Contains tests for <see cref="LoadBioService"/>.
/// </summary>
[TestClass]
[TestCategory("Unit Infrastructure")]
public class LoadBioServiceTests : ServiceTestBase<LoadBioService>
{
    /// <inheritdoc/>
    protected override void Configure(Container container)
    {
        base.Configure(container);
        container.Register<IService<LoadCitizenPage, LoadCitizenPage.Result>, LoadCitizenPageService>(Lifestyle.Singleton);
        container.Register<IService<CreateBio, IBio>, CreateBioService>(Lifestyle.Transient);
    }

    /// <summary>
    /// Tests whether the service will throw an <see cref="OperationCanceledException"/> upon being called with a cancelled token.
    /// There is no valid default result to be returned, thus the exception must be thrown.
    /// </summary>
    /// <param name="name">The name of the citizen passed to the service.</param>
    [TestMethod]
    [DataRow("SleepWellPupper")]
    [DataRow("SpaceDirk")]
    [DataRow("")]
    [DataRow("-")]
    [DataRow("B95686B7-C7FA-4C1D-9663-1370A287AE63")]
    [ExpectedException(typeof(OperationCanceledException), AllowDerivedTypes = true)]
    public async Task ThrowsOCEWhenCancelled(String name)
    {
        var token = new CancellationToken(true);
        _ = await new LoadBio(name, token).Using(Service);
    }
    /// <summary>
    /// Tests whether the service loads the actual bio of a citizen by asserting it contains a known code.
    /// </summary>
    /// <param name="name">The name of the citizen whose bio to retrieve.</param>
    /// <param name="code">The code whose being contained in the bio loaded is to be asserted.</param>
    /// <returns></returns>
    [TestMethod]
    [DataRow("SleepWellPupper", "NINN-OJFN-XVHY-TOFL")]
    public async Task LoadsBioFromCitizen(String name, String code)
    {
        //Arrange
        var request = new LoadBio(name, CancellationToken.None);

        //Act
        var bio = await request.Using(Service);

        //Assert
        _ = bio.IsT0.Should().BeTrue();
        _ = bio.AsT0.Contains(code).Should().BeTrue();
    }
}