﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Authentication.Requests;

namespace TaskforceGenerator.Infrastructure.Tests;

internal sealed class CreateBioService : IService<CreateBio, IBio>
{
    private sealed class Bio : IBio
    {
        private readonly String _bio;

        public Bio(String bio)
        {
            _bio = bio;
        }

        public Boolean Contains(BioCode code) => _bio.Contains(code);
    }
    public ValueTask<IBio> Execute(CreateBio query)
    {
        IBio result = new Bio(query.BioText);

        return ValueTask.FromResult(result);
    }
}