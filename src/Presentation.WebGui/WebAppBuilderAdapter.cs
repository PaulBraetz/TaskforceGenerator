﻿using TaskforceGenerator.Presentation.Views.Blazor.DependencyInjection;

namespace TaskforceGenerator.Presentation.WebGui;

internal sealed class WebAppBuilderAdapter : IApplicationBuilder<WebAppAdapter>
{
    private readonly WebApplicationBuilder _builder;

    public WebAppBuilderAdapter(WebApplicationBuilder builder)
    {
        _builder = builder;
    }

    public IServiceCollection Services => _builder.Services;

    public WebAppAdapter Build()
    {
        var app = _builder.Build();
        var result = new WebAppAdapter(app);

        return result;
    }
}