﻿using TaskforceGenerator.Presentation.Views.Blazor.DependencyInjection;

namespace TaskforceGenerator.Presentation.WebGui;

internal sealed class WebAppAdapter : IApplication
{
    public WebApplication App { get; }

    public WebAppAdapter(WebApplication app)
    {
        App = app;
    }

    public IServiceProvider Services => App.Services;
}