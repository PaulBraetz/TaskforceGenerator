﻿using OneOf;

using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Results;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Authentication.Results;

namespace TaskforceGenerator.Domain.Authentication.Requests;

/// <summary>
/// Command for committing connections to the infrastructure.
/// </summary>
/// <param name="Connection">The connection to commit.</param>
/// <param name="CancellationToken">The token used to signal the service execution to be cancelled.</param>
public readonly record struct CommitConnection(
    ICitizenConnection Connection,
    CancellationToken CancellationToken) : 
    IServiceRequest<OneOf<ServiceResult, ConnectionAlreadyCommittedResult>>;
