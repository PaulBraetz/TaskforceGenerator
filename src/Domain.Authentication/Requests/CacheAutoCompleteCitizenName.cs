﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Domain.Authentication.Requests;
/// <summary>
/// Command for caching citizen names for use by autocomplete features.
/// </summary>
public readonly record struct CacheAutoCompleteCitizenName(
    String CitizenName,
    CancellationToken CancellationToken) : IServiceRequest;
