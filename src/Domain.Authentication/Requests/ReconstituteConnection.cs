﻿using OneOf;

using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Authentication.Results;
using TaskforceGenerator.Domain.Core.Results;

namespace TaskforceGenerator.Domain.Authentication.Requests;

/// <summary>
/// Query for reconstituting a single citizen connection matching the query.
/// </summary>
/// <param name="CitizenName">The name of the citizen whose connection to retrieve.</param>
/// <param name="CancellationToken">The token used to signal the service execution to be cancelled.</param>
public readonly record struct ReconstituteConnection(
    String CitizenName,
    CancellationToken CancellationToken) :
    IServiceRequest<OneOf<ICitizenConnection, CitizenNotRegisteredResult>>;
