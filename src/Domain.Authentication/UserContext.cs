﻿using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Abstractions;

namespace TaskforceGenerator.Domain.Authentication;

/// <summary>
/// Default implementation of <see cref="IUserContext"/> and <see cref="IObservableUserContext"/>.
/// </summary>
public sealed class UserContext : IUserContext, IObservableUserContext
{
    private ICitizenConnection? _citizen;

    /// <inheritdoc/>
    public ICitizenConnection? Citizen
    {
        get => _citizen;
        set
        {
            var oldValue = _citizen;
            _citizen = value;
            CitizenChanged?.Invoke(
                this,
                new(
                    oldValue: oldValue,
                    newValue: value,
                    nameof(Citizen)
                ));
        }
    }
    /// <inheritdoc/>
    public event EventHandler<PropertyValueChangeArgs<ICitizenConnection?>>? CitizenChanged;
}
