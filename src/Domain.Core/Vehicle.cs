﻿using TaskforceGenerator.Domain.Core.Abstractions;

namespace TaskforceGenerator.Domain.Core;

/// <summary>
/// Default implementation of <see cref="IVehicle"/>.
/// </summary>
/// <param name="Name">The name of the vehicle (e.g.: "Redeemer").</param>
/// <param name="AvailableSlots">The available slots of the vehicle, including occupied ones.</param>
public record Vehicle(String Name, IReadOnlyList<ISlot> AvailableSlots) : IVehicle
{
    /// <summary>
    /// Gets the vehicle prototype of the Redeemer gunship.
    /// </summary>
    public static IVehicle Redeemer { get; } =
        new Vehicle("Redeemer",
            new[] {
                Slot.Pilot, Slot.Gunner, Slot.Gunner,
                Slot.Passenger, Slot.Passenger, Slot.Passenger, Slot.Passenger, Slot.Passenger
            });
    /// <summary>
    /// Gets the vehicle prototype of the Pisces shuttle.
    /// </summary>
    public static IVehicle Pisces { get; } =
        new Vehicle("Pisces",
            new[] {
                Slot.Pilot, Slot.Passenger, Slot.Passenger,
                Slot.Passenger, Slot.Passenger, Slot.Passenger
            });

    /// <inheritdoc/>
    public ICrewedVehicle Crew(IEnumerable<IOccupiedSlot> occupiedSlots)
    {
        var result = new CrewedVehicle(Name, AvailableSlots, occupiedSlots.ToArray());

        return result;
    }
}
