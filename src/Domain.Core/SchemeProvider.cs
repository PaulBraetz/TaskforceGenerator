﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Core.Abstractions;

namespace TaskforceGenerator.Domain.Core;

/// <summary>
/// Provides the available crewing schemes.
/// </summary>
public sealed class SchemeProvider : IValueSetProvider<ICrewingScheme>
{
    private static readonly HashSet<ICrewingScheme> _schemes = new()
    {
        CrewOptimalScheme.Instance
    };
    /// <inheritdoc/>
    public IReadOnlySet<ICrewingScheme> GetValues() => _schemes;
}
