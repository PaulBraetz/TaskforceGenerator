﻿using TaskforceGenerator.Domain.Core.Abstractions;

namespace TaskforceGenerator.Domain.Core;

internal sealed record CrewedVehicle(String Name, IReadOnlyList<ISlot> AvailableSlots, IReadOnlyList<IOccupiedSlot> OccupiedSlots) : Vehicle(Name, AvailableSlots), ICrewedVehicle;
