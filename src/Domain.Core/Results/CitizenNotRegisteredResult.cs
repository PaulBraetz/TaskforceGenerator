﻿namespace TaskforceGenerator.Domain.Core.Results;

/// <summary>
/// Result for when a citizen that is not registered to the system is attempted to be accessed.
/// </summary>
public sealed class CitizenNotRegisteredResult
{
    private CitizenNotRegisteredResult() { }
    /// <summary>
    /// Gets the singleton instance.
    /// </summary>
    public static CitizenNotRegisteredResult Instance { get; } = new();
}
