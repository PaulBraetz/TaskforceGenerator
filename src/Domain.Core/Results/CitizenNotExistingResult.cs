﻿namespace TaskforceGenerator.Domain.Core.Results;

/// <summary>
/// Result for when a citizen that does not exist (is not registered to RSI) is attempted to be accessed.
/// </summary>
public sealed class CitizenNotExistingResult
{
    private CitizenNotExistingResult() { }
    /// <summary>
    /// Gets the singleton instance.
    /// </summary>
    public static CitizenNotExistingResult Instance { get; } = new();
}
