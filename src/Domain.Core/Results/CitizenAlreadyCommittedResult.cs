﻿namespace TaskforceGenerator.Domain.Core.Results;

/// <summary>
/// Result if the system is unable to commit a citizen to the infrastructure 
/// because a citizen with the same name has already been committed.
/// </summary>
public sealed class CitizenAlreadyCommittedResult
{
    private CitizenAlreadyCommittedResult() { }
    /// <summary>
    /// Gets the singleton instance.
    /// </summary>
    public static CitizenAlreadyCommittedResult Instance { get; } = new();
}
