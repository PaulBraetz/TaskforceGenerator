﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Core.Abstractions;

namespace TaskforceGenerator.Domain.Core;

/// <summary>
/// Implementation of <see cref="IValueSetProvider{T}"/> based on the instances defined by <see cref="Career"/>.
/// </summary>
public sealed class CareerProvider : IValueSetProvider<ICareer>
{
    /// <summary>
    /// Initializes a new instance. If possible, use <see cref="Instance"/> instead.
    /// </summary>
    public CareerProvider() { }
    /// <summary>
    /// Gets the singleton instance.
    /// </summary>
    public static CareerProvider Instance { get; } = new();

    private readonly HashSet<ICareer> _careers = new()
    {
        Career.None,
        Career.Pilot,
        Career.Gunner,
        Career.Operator,
        Career.Engineer,
        Career.Merchant,
        Career.Mechanic,
        Career.Commodore
    };

    /// <inheritdoc/>
    public IReadOnlySet<ICareer> GetValues() => _careers;
}
