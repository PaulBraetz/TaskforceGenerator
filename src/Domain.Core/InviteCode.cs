﻿namespace TaskforceGenerator.Domain.Core;
/// <summary>
/// Represents an event invite code.
/// </summary>
/// <param name="Value">The value of the invite code.</param>
public readonly record struct InviteCode(String Value)
{
    /// <summary>
    /// Gets the empty invite code.
    /// </summary>
    public static InviteCode Empty { get; } = new();

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
    public static implicit operator String(InviteCode code) => code.Value;
    public static implicit operator InviteCode(String value) => new(value);
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
}
