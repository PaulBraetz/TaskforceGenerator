﻿using TaskforceGenerator.Domain.Core.Abstractions;

namespace TaskforceGenerator.Domain.Core;

/// <summary>
/// Default implementation of <see cref="ISlotPreference"/>.
/// </summary>
/// <param name="PreferredSlot">The preferred slot the occupnt wishes to be assigned to.</param>
public sealed record SlotPreference(ISlot PreferredSlot) : ISlotPreference
{
    /// <summary>
    /// Gets the prototype for a pilot slot.
    /// </summary>
    public static ISlotPreference Pilot { get; } =
        new SlotPreference(Slot.Pilot);
    /// <summary>
    /// Gets the prototype for a gunner slot.
    /// </summary>
    public static ISlotPreference Gunner { get; } =
        new SlotPreference(Slot.Gunner);
    /// <summary>
    /// Gets the prototype for a passenger slot.
    /// </summary>
    public static ISlotPreference Passenger { get; } =
        new SlotPreference(Slot.Passenger);
}
