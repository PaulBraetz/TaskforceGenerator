﻿namespace TaskforceGenerator.Domain.Core.Abstractions;

/// <summary>
/// Represents a citizen.
/// </summary>
public interface ICitizen
{
    /// <summary>
    /// Gets the citizens name.
    /// </summary>
    String Name { get; }
    /// <summary>
    /// Gets the set of orgs this citizen is a member of publicly.
    /// </summary>
    IReadOnlySet<String> PublicOrgNames { get; }
}
