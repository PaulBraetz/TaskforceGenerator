﻿namespace TaskforceGenerator.Domain.Core.Abstractions;

/// <summary>
/// Represents the gameplay interest of a citizen.
/// </summary>
public interface ICareer
{
    /// <summary>
    /// Gets the name of the gameplay represented.
    /// </summary>
    String Name { get; }
}