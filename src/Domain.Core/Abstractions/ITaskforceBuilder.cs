﻿namespace TaskforceGenerator.Domain.Core.Abstractions;

/// <summary>
/// Representa a builder for creating taskforces, made up of vehicles, occupants and defined by a crewing scheme.
/// </summary>
public interface ITaskforceBuilder
{
    /// <summary>
    /// Sets the crewing scheme to use when crewing vehicles for the taskforce.
    /// </summary>
    /// <param name="scheme">The scheme to use.</param>
    /// <returns>An instance to the builder for chaining of further calls.</returns>
    ITaskforceBuilder SetCrewingScheme(ICrewingScheme scheme);
    /// <summary>
    /// Adds a vehicle to the builder, to be included in the taskforce creation.
    /// </summary>
    /// <param name="vehicle">The vehicle to add.</param>
    /// <returns>An instance to the builder for chaining of further calls.</returns>
    ITaskforceBuilder AddVehicle(IVehicle vehicle);
    /// <summary>
    /// Adds an occupant to the builder, to be included in the taskforce creation.
    /// </summary>
    /// <param name="occupant">The occupant to add.</param>
    /// <returns>An instance to the builder for chaining of further calls.</returns>
    ITaskforceBuilder AddOccupant(IOccupant occupant);
    /// <summary>
    /// Builds a taskforce based on the vehicles, occupants and scheme provided.
    /// </summary>
    /// <returns>A new taskforce, based on the crewing scheme, vehicles and occupants provided.</returns>
    ITaskforce Build();
}
