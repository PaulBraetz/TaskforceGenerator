﻿namespace TaskforceGenerator.Domain.Core.Abstractions;

/// <summary>
/// Represents a taskforce consisting of (possibly partially) occupied vehicles.
/// </summary>
public interface ITaskforce
{
    /// <summary>
    /// Gets the vehicles in the taskforce.
    /// </summary>
    IReadOnlyList<ICrewedVehicle> Vehicles { get; }
}
