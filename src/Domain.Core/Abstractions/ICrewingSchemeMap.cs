﻿namespace TaskforceGenerator.Domain.Core.Abstractions;

/// <summary>
/// Maps crewing scheme names onto their corresponding scheme.
/// </summary>
public interface ICrewingSchemeMap
{
    /// <summary>
    /// Attempts to retrieve the scheme with a given name.
    /// </summary>
    /// <param name="name">The name for which to retrieve the scheme.</param>
    /// <param name="scheme">If one could be located, the scheme; otherwise <see langword="null"/>.</param>
    /// <returns><see langword="true"/> if a scheme could be located; otehrwise <see langword="false"/>.</returns>
    Boolean TryGetScheme(String name, out ICrewingScheme? scheme);
}
