﻿namespace TaskforceGenerator.Domain.Core.Abstractions;

/// <summary>
/// Represents an occupant to a vehicle slot. This represents a specific view onto a citizen entity.
/// </summary>
public interface IOccupant
{
    /// <summary>
    /// Gets the name of the occupant.
    /// </summary>
    String CitizenName { get; }
    /// <summary>
    /// Gets the occupants preference for slot assignment.
    /// </summary>
    ISlotPreference Preference { get; }
}
