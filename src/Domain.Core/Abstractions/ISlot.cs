﻿namespace TaskforceGenerator.Domain.Core.Abstractions;

/// <summary>
/// Represents a vehicle slot, to be occupied by an occupant.
/// Implements the prototype pattern for occupying slots.
/// </summary>
public interface ISlot
{
    /// <summary>
    /// Gets the name of the slot.
    /// </summary>
    String Name { get; }
    /// <summary>
    /// Occupies the slot, creating a new occupied slot.
    /// </summary>
    /// <param name="occupant">The occupant to occupy the new slot.</param>
    /// <returns>A new slot, occupied with the occupant provided.</returns>
    IOccupiedSlot Occupy(IOccupant occupant);
}
