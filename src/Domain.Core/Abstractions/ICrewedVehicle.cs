﻿namespace TaskforceGenerator.Domain.Core.Abstractions;

/// <summary>
/// Represents a (possibly partially) crewed vehicle.
/// </summary>
public interface ICrewedVehicle : IVehicle
{
    /// <summary>
    /// Gets the vehicles occupied slots.
    /// </summary>
    IReadOnlyList<IOccupiedSlot> OccupiedSlots { get; }
}
