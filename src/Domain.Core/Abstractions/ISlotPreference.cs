﻿namespace TaskforceGenerator.Domain.Core.Abstractions;

/// <summary>
/// Represents an occupants preference of slot assignment.
/// </summary>
public interface ISlotPreference
{
    /// <summary>
    /// Gets the preferred slot the occupnt wishes to be assigned to.
    /// </summary>
    ISlot PreferredSlot { get; }
}
