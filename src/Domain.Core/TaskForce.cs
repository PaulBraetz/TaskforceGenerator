﻿using TaskforceGenerator.Domain.Core.Abstractions;

namespace TaskforceGenerator.Domain.Core;

internal sealed record TaskForce(IReadOnlyList<ICrewedVehicle> Vehicles) : ITaskforce;
