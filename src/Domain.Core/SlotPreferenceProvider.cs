﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Core.Abstractions;

namespace TaskforceGenerator.Domain.Core;

/// <summary>
/// Implementation of <see cref="IValueSetProvider{T}"/> based on the instances defined by <see cref="SlotPreference"/>.
/// </summary>
public sealed class SlotPreferenceProvider : IValueSetProvider<ISlotPreference>
{
    /// <summary>
    /// Initializes a new instance. If possible, use <see cref="Instance"/> instead.
    /// </summary>
    public SlotPreferenceProvider() { }
    /// <summary>
    /// Gets the singleton instance.
    /// </summary>
    public static SlotPreferenceProvider Instance { get; } = new();
    private readonly HashSet<ISlotPreference> _preferences = new()
    {
        SlotPreference.Passenger,
        SlotPreference.Pilot,
        SlotPreference.Gunner
    };
    /// <inheritdoc/>
    public IReadOnlySet<ISlotPreference> GetValues() =>
        _preferences;
}
