﻿using TaskforceGenerator.Domain.Core.Abstractions;

namespace TaskforceGenerator.Domain.Core;

/// <summary>
/// Represents the gameplay interest of a citizen.
/// </summary>
/// <param name="Name">The name of the gameplay represented.</param>
public sealed record Career(String Name) : ICareer
{
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
    public static readonly Career None = new(String.Empty);
    public static readonly Career Pilot = new("Pilot");
    public static readonly Career Gunner = new("Gunner");
    public static readonly Career Operator = new("Operator");
    public static readonly Career Engineer = new("Engineer");
    public static readonly Career Merchant = new("Merchant");
    public static readonly Career Mechanic = new("Mechanic");
    public static readonly Career Commodore = new("Commodore");
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
}