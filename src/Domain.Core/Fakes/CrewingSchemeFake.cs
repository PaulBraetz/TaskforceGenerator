﻿using TaskforceGenerator.Domain.Core.Abstractions;

namespace TaskforceGenerator.Domain.Core.Fakes;

/// <summary>
/// Fake crewing scheme that always returns the same occupied vehicles.
/// </summary>
internal sealed class CrewingSchemeFake : ICrewingScheme
{
    /// <summary>
    /// Initializes a new instance. If possible, use <see cref="Instance"/> instead.
    /// </summary>
    public CrewingSchemeFake() { }
    /// <summary>
    /// Gets the singleton instance.
    /// </summary>
    public static CrewingSchemeFake Instance { get; } = new();
    public IEnumerable<ICrewedVehicle> CrewVehicles(IReadOnlyList<IVehicle> vehicles, IReadOnlyList<IOccupant> occupants)
    {
        return new[]{
            Vehicle.Redeemer.Crew(new[]
            {
                Slot.Gunner.Occupy(
                    new Occupant("SleepWellPupper",
                    SlotPreference.Gunner)),
                Slot.Pilot.Occupy(
                    new Occupant("YokoArashi",
                    SlotPreference.Pilot)),
                Slot.Gunner.Occupy(
                    new Occupant("Mar10",
                    SlotPreference.Gunner))
            })
        };
    }

    public String Name { get; } = "Fake Scheme";
}
