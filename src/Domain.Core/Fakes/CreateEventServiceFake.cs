﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Core.Abstractions;
using TaskforceGenerator.Domain.Core.Queries;

namespace TaskforceGenerator.Domain.Core.Fakes;
/// <summary>
/// Fake service for creating events.
/// </summary>
public sealed class CreateEventServiceFake : IService<CreateEvent, IEvent>
{
    private sealed class EventFake : IEvent { }
    /// <inheritdoc/>
    public ValueTask<IEvent> Execute(CreateEvent query) =>
        ValueTask.FromResult((IEvent)new EventFake());
}
