﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Results;
using TaskforceGenerator.Domain.Core.Requests;

namespace TaskforceGenerator.Domain.Core.Fakes;
/// <summary>
/// Fake service for committing events to the infrastructure.
/// </summary>
public sealed class CommitEventServiceFake : IService<CommitEvent>
{
    /// <inheritdoc/>
    public ValueTask<ServiceResult> Execute(CommitEvent command) => ServiceResult.Completed.Task.AsValueTask();
}
