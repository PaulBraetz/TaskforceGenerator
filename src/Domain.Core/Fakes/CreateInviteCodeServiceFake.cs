﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Core.Queries;

namespace TaskforceGenerator.Domain.Core.Fakes;

/// <summary>
/// Fake service for creating invite codes.
/// </summary>
public sealed class CreateInviteCodeServiceFake : IService<CreateInviteCode, InviteCode>
{
    /// <inheritdoc/>
    public ValueTask<InviteCode> Execute(CreateInviteCode query) =>
        ValueTask.FromResult<InviteCode>("FAKE_CODE");
}
