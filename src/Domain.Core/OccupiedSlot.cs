﻿using TaskforceGenerator.Domain.Core.Abstractions;

namespace TaskforceGenerator.Domain.Core;

internal sealed record OccupiedSlot(String Name, IOccupant Occupant) : Slot(Name), IOccupiedSlot;
