﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Domain.Core.Queries;
/// <summary>
/// Query for creating new unique invite codes for events.
/// </summary>
/// <param name="CancellationToken">
/// The token used to signal the service execution to be cancelled.
/// </param>
public readonly record struct CreateInviteCode(
    CancellationToken CancellationToken) : 
    IServiceRequest<InviteCode>;
