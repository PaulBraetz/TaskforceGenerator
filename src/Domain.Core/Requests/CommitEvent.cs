﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Core.Abstractions;

namespace TaskforceGenerator.Domain.Core.Requests;
/// <summary>
/// Command for committing an event to the infrastructure.
/// </summary>
/// <param name="Event">The event to commit to the infrastructure.</param>
/// <param name="CancellationToken">The token used to signal the service execution to be cancelled.</param>
public readonly record struct CommitEvent(
    IEvent Event,
    CancellationToken CancellationToken) : IServiceRequest;
