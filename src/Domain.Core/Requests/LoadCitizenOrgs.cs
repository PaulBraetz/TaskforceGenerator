﻿using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Domain.Core.Queries;

/// <summary>
/// Query for loading a citizens public orgs.
/// </summary>
/// <param name="CitizenName">The name of the citizen whose public org names to load.</param>
/// <param name="CancellationToken">The token used to signal the service execution to be cancelled.</param>
public readonly record struct LoadCitizenOrgs(String CitizenName, CancellationToken CancellationToken) : IServiceRequest<IReadOnlySet<String>>;
