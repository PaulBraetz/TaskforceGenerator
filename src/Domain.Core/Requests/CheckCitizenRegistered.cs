﻿using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Domain.Core.Queries;

/// <summary>
/// Query for checking whether a cititzen is registered to the system.
/// </summary>
/// <param name="CitizenName">The name of the citizen whose registration to check.</param>
/// <param name="CancellationToken">The token used to signal the service execution to be cancelled.</param>
public readonly record struct CheckCitizenRegistered(String CitizenName, CancellationToken CancellationToken) : 
    IServiceRequest<Boolean>;

