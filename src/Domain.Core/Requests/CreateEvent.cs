﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Core.Abstractions;

namespace TaskforceGenerator.Domain.Core.Queries;
/// <summary>
/// Query for creating an event entity instance.
/// </summary>
/// <param name="InviteCode">The invite code of the event to create.</param>
/// <param name="Name">The name of the event to create.</param>
/// <param name="Description">The description of the event to create.</param>
/// <param name="Date">The date of the event to create.</param>
/// <param name="PlannedVehicles">The vehicles and their respective counts planned for the event to create.</param>
/// <param name="CreatorName">The name of the creator of the event to create.</param>
/// <param name="CancellationToken">The token used to signal the service execution to be cancelled.</param>
public readonly record struct CreateEvent(
    String InviteCode,
    String Name,
    String Description,
    DateTimeOffset Date,
    IReadOnlyDictionary<IVehicle, Int32> PlannedVehicles,
    String CreatorName,
    CancellationToken CancellationToken) : IServiceRequest<IEvent>;