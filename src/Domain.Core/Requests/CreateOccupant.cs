﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Core.Abstractions;

namespace TaskforceGenerator.Domain.Core.Queries;

/// <summary>
/// Query for creating an occupant.
/// </summary>
/// <param name="CitizenName">The name of the occupant.</param>
/// <param name="SlotPreference">The occupants preferred slot.</param>
/// <param name="CancellationToken">The token used to signal the service execution to be cancelled.</param>
public readonly record struct CreateOccupant(
    String CitizenName,
    ISlotPreference SlotPreference,
    CancellationToken CancellationToken) : IServiceRequest<IOccupant>;
