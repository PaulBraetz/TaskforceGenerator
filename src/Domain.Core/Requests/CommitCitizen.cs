﻿using OneOf;

using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Results;
using TaskforceGenerator.Domain.Core.Abstractions;
using TaskforceGenerator.Domain.Core.Results;

namespace TaskforceGenerator.Domain.Core.Requests;

/// <summary>
/// Command for committing a citizen to the infrastructure.
/// </summary>
/// <param name="Citizen">The citizen to commit.</param>
/// <param name="CancellationToken">The token used to signal the service execution to be cancelled.</param>
public readonly record struct CommitCitizen(
    ICitizen Citizen,
    CancellationToken CancellationToken) 
    : IServiceRequest<OneOf<ServiceResult, CitizenAlreadyCommittedResult>>;
