﻿using OneOf;

using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Results;
using TaskforceGenerator.Domain.Core.Results;

namespace TaskforceGenerator.Domain.Core.Requests;

/// <summary>
/// Command for ensuring a citizen exists withing the application.
/// </summary>
/// <param name="CitizenName">The name of the citizen whose existence in the application to ensure.</param>
/// <param name="CancellationToken">The token used to signal the service execution to be cancelled.</param>
public readonly record struct EnsureCitizen(
    String CitizenName,
    CancellationToken CancellationToken) :
    IServiceRequest<OneOf<ServiceResult, CitizenNotExistingResult>>;
