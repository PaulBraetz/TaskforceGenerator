﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Core.Abstractions;

namespace TaskforceGenerator.Domain.Core.Queries;

/// <summary>
/// Query for creating a citizen (factory command)
/// </summary>
/// <param name="CitizenName">The name of tthe citizen to create.</param>
/// <param name="CancellationToken">The token used to signal the service execution to be cancelled.</param>
public readonly record struct CreateCitizen(String CitizenName, CancellationToken CancellationToken) : IServiceRequest<ICitizen>;
