﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Core.Abstractions;

namespace TaskforceGenerator.Domain.Core;

/// <summary>
/// Provides the names of available schemes.
/// </summary>
public sealed class SchemeNameProvider : IValueSetProvider<String>
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="schemeProvider">The provider to use when obtaining the available schemes.</param>
    public SchemeNameProvider(IValueSetProvider<ICrewingScheme> schemeProvider)
    {
        _schemeProvider = schemeProvider;
    }

    private readonly IValueSetProvider<ICrewingScheme> _schemeProvider;

    /// <inheritdoc/>
    public IReadOnlySet<String> GetValues() => _schemeProvider.GetValues().Select(s => s.Name).ToHashSet();
}
