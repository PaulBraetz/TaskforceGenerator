﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Core.Abstractions;
using TaskforceGenerator.Domain.Core.Queries;

namespace TaskforceGenerator.Domain.Core.Services;

/// <summary>
/// Service for creating occupants.
/// </summary>
public sealed class CreateOccupantService : IService<CreateOccupant, IOccupant>
{
    /// <inheritdoc/>
    public ValueTask<IOccupant> Execute(CreateOccupant query) =>
        ValueTask.FromResult<IOccupant>(new Occupant(query.CitizenName, query.SlotPreference));
}
