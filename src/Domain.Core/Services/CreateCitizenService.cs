﻿using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Core.Abstractions;
using TaskforceGenerator.Domain.Core.Queries;

namespace TaskforceGenerator.Domain.Core.Services;

/// <summary>
/// Service for creating a citizen.
/// </summary>
public sealed class CreateCitizenService : IService<CreateCitizen, ICitizen>
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="orgsService">The service to use when loading a citizens public orgs.</param>
    public CreateCitizenService(IService<LoadCitizenOrgs, IReadOnlySet<String>> orgsService)
    {
        _orgsService = orgsService;
    }
    private readonly IService<LoadCitizenOrgs, IReadOnlySet<String>> _orgsService;
    /// <inheritdoc/>
    public async ValueTask<ICitizen> Execute(CreateCitizen query)
    {
        query.CancellationToken.ThrowIfCancellationRequested();

        var orgs = await new LoadCitizenOrgs(query.CitizenName, query.CancellationToken)
            .Using(_orgsService);

        ICitizen result = new Citizen(query.CitizenName, orgs);

        return result;
    }
}
