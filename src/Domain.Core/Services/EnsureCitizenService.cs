﻿using OneOf;

using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Results;
using TaskforceGenerator.Domain.Core.Abstractions;
using TaskforceGenerator.Domain.Core.Results;
using TaskforceGenerator.Domain.Core.Queries;
using TaskforceGenerator.Domain.Core.Requests;

namespace TaskforceGenerator.Domain.Core.Services;

/// <summary>
/// Service for ensuring a citizen exists withing the application.
/// </summary>
public sealed class EnsureCitizenService : IService<EnsureCitizen, OneOf<ServiceResult, CitizenNotExistingResult>>
{
    private readonly IService<CheckCitizenRegistered, Boolean> _registeredService;
    private readonly IService<CheckCitizenExists, Boolean> _existsService;
    private readonly IService<CreateCitizen, ICitizen> _createService;
    private readonly IService<CommitCitizen, OneOf<ServiceResult, CitizenAlreadyCommittedResult>> _commitService;

    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="registeredService">The service to use when checking whether the citizen is already registered.</param>
    /// <param name="existsService">The service to use when checking whether the citizen actually exists (registered to RSI).</param>
    /// <param name="commitService">The service to use when committing a newly created citizn to the infrastructure.</param>
    /// <param name="createService">The service to use when creating a new citizen.</param>
    public EnsureCitizenService(
        IService<CheckCitizenRegistered, Boolean> registeredService,
        IService<CheckCitizenExists, Boolean> existsService,
        IService<CommitCitizen, OneOf<ServiceResult, CitizenAlreadyCommittedResult>> commitService,
        IService<CreateCitizen, ICitizen> createService)
    {
        _registeredService = registeredService;
        _existsService = existsService;
        _commitService = commitService;
        _createService = createService;
    }
    /// <inheritdoc/>
    public async ValueTask<OneOf<ServiceResult, CitizenNotExistingResult>> Execute(EnsureCitizen command)
    {
        var (name, token) = command;
        var registered = await new CheckCitizenRegistered(name, token).Using(_registeredService);
        if(registered)
        {
            return ServiceResult.Completed;
        }

        var exists = await new CheckCitizenExists(name, token).Using(_existsService);
        if(!exists)
        {
            return CitizenNotExistingResult.Instance;
        }

        var citizen = await new CreateCitizen(name, token).Using(_createService);
        var commitResult = await new CommitCitizen(citizen, token).Using(_commitService);

        var result = commitResult.Match(
            r => r,
            _ => ServiceResult.Completed);

        return result;
    }
}
