﻿using TaskforceGenerator.Domain.Core.Abstractions;

namespace TaskforceGenerator.Domain.Core;

/// <summary>
/// Contains extensions for the <c>TaskforceGenerator</c> namespace.
/// </summary>
public static class Extensions
{
    /// <summary>
    /// Indicates whether a slot is occupied by a citizen whose slot preference is in contradiction to this slot.
    /// </summary>
    /// <param name="slot">The slot to examine.</param>
    /// <returns><see langword="true"/> if the slot is occupied by a citizen whose slot preference is in contradiction to it; otherwise, <see langword="false"/>.</returns>
    public static Boolean IsPreferenceMismatch(this IOccupiedSlot slot) =>
        slot.Name != slot.Occupant.Preference.PreferredSlot.Name;
}
