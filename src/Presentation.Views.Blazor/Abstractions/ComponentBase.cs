﻿using Microsoft.AspNetCore.Components;

using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;

using TaskforceGenerator.Common;
using TaskforceGenerator.Presentation.Views.Blazor.DependencyInjection;
using TaskforceGenerator.Presentation.Views.Blazor.Exceptions;

namespace TaskforceGenerator.Presentation.Views.Blazor.Abstractions;

/// <summary>
/// Base component providing non captured parameters via <see cref="Attributes"/>
/// as well as providing automated checking for required parameters in <see cref="OnParametersSet"/>.
/// Required parameters must be annotated using the <see cref="ParameterAttribute"/> and the <see cref="RequiredAttribute"/>.
/// Upon encountering a required parameter with value <see langword="null"/>,
/// a <see cref="ParameterNullException"/> will be thrown.
/// </summary>
public abstract class ComponentBase : SimpleInjectorIntegratedComponent, IDisposable
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    public ComponentBase()
    {
        _nullChecks = new(GetNullChecks);
        _attributes = new();
#if DEBUG
        _attributes.Add("component-type", GetType().Name);
#endif
    }

    private readonly CancellationTokenSource _disposalCts = new();
    private readonly Lazy<IReadOnlyList<Action>> _nullChecks;
    private Dictionary<String, Object> _attributes;
    private Int32 _disposed = BooleanState.FALSE_STATE;

#pragma warning disable BL0007 // Component parameters should be auto properties
    /// <summary>
    /// Gets or sets the otherwise unmatched attributes passed to the component.
    /// </summary>
    [Parameter(CaptureUnmatchedValues = true)]
    public required Dictionary<String, Object> Attributes
    {
        get => _attributes;
        set
        {
#if DEBUG
            // Allow assigning null value in order to force
            // ParameterNullException in OnParametersSet and avoiding
            // NullReferenceException here
            _ = value?.TryAdd("component-type", GetType().Name);
            _attributes = value!;
#else
            _attributes = value;
#endif
        }
    }
#pragma warning restore BL0007 // Component parameters should be auto properties

    /// <summary>
    /// Gets a cancellation token that will be cancelled upon <see cref="Dispose"/> being called.
    /// </summary>
    protected CancellationToken ComponentDisposalToken => _disposalCts.Token;

    /// <summary>
    /// Ensures that each of the class names provided are removed from the <c>class</c> attribute.
    /// </summary>
    /// <param name="classNames">
    /// The class names ensure are removed from the <c>class</c> attribute.
    /// </param>
    protected void RemoveClassNames(params String[] classNames) =>
        ConfigureClassNames(Array.Empty<String>(), classNames);
    /// <summary>
    /// Ensures that each of the class names provided are removed from the <c>class</c> attribute.
    /// </summary>
    /// <param name="classNames">
    /// The class names ensure are removed from the <c>class</c> attribute.
    /// </param>
    protected void RemoveClassNames(IEnumerable<String> classNames) =>
        ConfigureClassNames(Array.Empty<String>(), classNames);
    /// <summary>
    /// Ensures that each of the class names provided are present in the <c>class</c> attribute.
    /// </summary>
    /// <param name="classNames">
    /// The class names ensure are present in the <c>class</c> attribute.
    /// </param>
    protected void EnsureClassNames(IEnumerable<String> classNames) =>
        ConfigureClassNames(classNames, Array.Empty<String>());
    /// <summary>
    /// Ensures that each of the class names provided are present in the <c>class</c> attribute.
    /// </summary>
    /// <param name="classNames">
    /// The class names ensure are present in the <c>class</c> attribute.
    /// </param>
    protected void EnsureClassNames(params String[] classNames) =>
        ConfigureClassNames(classNames, Array.Empty<String>());
    /// <summary>
    /// Configures the class names available in the <c>class</c> attribute.
    /// </summary>
    /// <param name="ensure">The class names to ensure.</param>
    /// <param name="remove">The class names to ensure are not available.</param>
    protected void ConfigureClassNames(IEnumerable<String> ensure, IEnumerable<String> remove)
    {
        var removeSet = remove
            .Select(n => n.Trim())
            .ToHashSet();

        var trimmedClassNames = ensure
            .Select(n => n.Trim())
            .Where(n => !removeSet.Contains(n));

        if(!Attributes.TryGetValue("class", out var existingClassNames) ||
            existingClassNames == null ||
            existingClassNames is not String)
        {
            Attributes["class"] = String.Join(' ', trimmedClassNames);
            return;
        }

        var classSet = (existingClassNames as String)!
            .Split(' ', StringSplitOptions.TrimEntries)
            .Where(n => !removeSet.Contains(n))
            .ToHashSet();

        foreach(var className in trimmedClassNames)
        {
            _ = classSet.Add(className);
        }

        var newClassNames = String.Join(' ', classSet);
        Attributes["class"] = newClassNames;
    }
    /// <inheritdoc/>
    protected override void OnParametersSet() => CheckNullParameters();
    private void CheckNullParameters()
    {
        var nullChecks = _nullChecks.Value;
        foreach(var nullCheck in nullChecks)
        {
            nullCheck.Invoke();
        }
    }
    private IReadOnlyList<Action> GetNullChecks()
    {
        //only check parameters with required keyword
        var result = GetType()
            .GetProperties()
            .Where(p => p.CustomAttributes.Any(a => a.AttributeType == typeof(ParameterAttribute)))
            .Where(p => p.CustomAttributes.Any(a => a.AttributeType == typeof(RequiredMemberAttribute)))
            .Where(p => !p.PropertyType.IsValueType || Nullable.GetUnderlyingType(p.PropertyType) != null)
            .Select(CreateNullCheck)
            .ToList();

        return result;
    }

    private Action CreateNullCheck(PropertyInfo info)
    {
        var thisType = GetType();
        //this
        var thisExpr = Expression.Constant(this, thisType);
        //this.Prop
        var propertyExpr = Expression.Property(thisExpr, info);
        //null
        var nullExpr = Expression.Constant(null, info.PropertyType);
        //this.Prop == null
        var equalityExpr = Expression.Equal(propertyExpr, nullExpr);
        //"Prop"
        var paramNameExpr = Expression.Constant(info.Name);
        //Type
        var thisTypeExpr = Expression.Constant(thisType);
        //Type
        var propertyTypeExpr = Expression.Constant(info.PropertyType);
        var exceptionCtor = typeof(ParameterNullException)
            .GetConstructor(new[] { typeof(String), typeof(Type), typeof(Type) })!;
        //new ParameterNullException("Prop", Type, Type)
        var exceptionExpr = Expression.New(exceptionCtor, paramNameExpr, propertyTypeExpr, thisTypeExpr);
        //throw new ParameterNullException("Prop", Type, Type)
        var throwExpr = Expression.Throw(exceptionExpr);
        //void
        var voidExpr = Expression.Constant(typeof(void));
        //if(this.Prop == null)
        //	throw new ParameterNullException("Prop", Type, Type)
        //else
        //	void
        var ifThenExpr = Expression.IfThenElse(equalityExpr, throwExpr, voidExpr);

        //()=>{
        //if(this.Prop == null)
        //		throw new ParameterNullException("Prop", Type, Type)
        //void (return)
        //}
        var lambdaExpr = Expression.Lambda(ifThenExpr);

        var result = (Action)lambdaExpr.Compile();

        return result;
    }

    /// <summary>
    /// Called the first time the instance is disposed.
    /// </summary>
    protected virtual void OnDisposing() { }

    /// <inheritdoc/>
#pragma warning disable CA1816 // Dispose methods should call SuppressFinalize
    public void Dispose()
#pragma warning restore CA1816 // Dispose methods should call SuppressFinalize
    {
        if(Interlocked.Exchange(
            ref _disposed,
            BooleanState.TRUE_STATE) == BooleanState.FALSE_STATE)
        {
            _disposalCts.Cancel();
            _disposalCts.Dispose();
            OnDisposing();
        }
    }
}
