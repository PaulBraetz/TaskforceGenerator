﻿using TaskforceGenerator.Presentation.Views.Blazor.DependencyInjection;

namespace TaskforceGenerator.Presentation.Views.Blazor.Abstractions;

/// <summary>
/// Base component view for a model of <typeparamref name="TModel"/>. The model will be injected.
/// </summary>
/// <typeparam name="TModel">The type of model to render a component for.</typeparam>
public abstract class InjectedModelComponentBase<TModel>
    : ModelComponentBase<TModel>, IDisposable
{
    /// <inheritdoc/>
    [Injected]
    public override TModel Value { get => base.Value; set => base.Value = value; }

    /// <summary>
    /// Gets or sets the strategy used to possibly dispose the component model.
    /// </summary>
    [Injected]
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
    public IDisposeStrategy<TModel> ModelDisposeStrategy { get; set; }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
    
    /// <inheritdoc/>
    protected override void OnDisposing()
    {
        ModelDisposeStrategy.Dispose(Value);
        base.OnDisposing();
    }
}
