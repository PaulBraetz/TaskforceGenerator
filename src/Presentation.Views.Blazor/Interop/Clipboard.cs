﻿using Microsoft.JSInterop;

namespace TaskforceGenerator.Presentation.Views.Blazor.Interop;

/// <summary>
/// Clipboard that accesses the underlying clipboard through a JavaScript runtime.
/// </summary>
public sealed class Clipboard
{
    private readonly IJSRuntime _jsRuntime;

    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="jsRuntime">The runtime used for invoking the javascript clipboard functionality.</param>
    public Clipboard(IJSRuntime jsRuntime)
    {
        _jsRuntime = jsRuntime;
    }

    /// <summary>
    /// Reads text stored in the clipboard.
    /// </summary>
    /// <returns>A task that, upon completion, will contain the text read from the clipboard.</returns>
    public ValueTask<String> ReadAsync() =>
        _jsRuntime.InvokeAsync<String>("navigator.clipboard.readText");

    /// <summary>
    /// Writes text to the clipboard.
    /// </summary>
    /// <param name="text">The text to write to the clipboard.</param>
    /// <returns>A task that will complete upon the text having been written to the clipboard.</returns>
    public ValueTask WriteTextAsync(String text) =>
        _jsRuntime.InvokeVoidAsync("navigator.clipboard.writeText", text);
}
