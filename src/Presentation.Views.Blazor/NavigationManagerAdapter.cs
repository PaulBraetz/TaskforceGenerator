﻿using Microsoft.AspNetCore.Components;

using TaskforceGenerator.Presentation.Models.Abstractions;

namespace TaskforceGenerator.Presentation.Views.Blazor;

/// <summary>
/// Adapts <see cref="NavigationManager"/> onto <see cref="INavigationManager"/>.
/// </summary>
public sealed class NavigationManagerAdapter : INavigationManager
{
    private readonly NavigationManager _navigationManager;
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="navigationManager">The navigation manager to adapt to <see cref="INavigationManager"/>.</param>
    public NavigationManagerAdapter(NavigationManager navigationManager)
    {
        _navigationManager = navigationManager;
    }

    /// <inheritdoc/>
    public void NavigateTo(String route) => 
        _navigationManager.NavigateTo(route);
}
