﻿using TaskforceGenerator.Presentation.Models.Abstractions;

namespace TaskforceGenerator.Presentation.Views.Blazor.Components.Primitives;

/// <summary>
/// Base class for input controls that provide a label, description and error.
/// </summary>
/// <typeparam name="TValue">The type of value obtained by the model.</typeparam>
/// <typeparam name="TError">The type of error displayed by the model.</typeparam>
public class InputGroup<TValue, TError> : InputGroupSpecialized<IInputModel<TValue, TError>, TValue, TError>
{

}
