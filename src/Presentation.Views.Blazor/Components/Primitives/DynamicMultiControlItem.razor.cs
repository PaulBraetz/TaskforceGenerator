﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Rendering;

using TaskforceGenerator.Presentation.Models.Abstractions;
using TaskforceGenerator.Presentation.Views.Blazor.Abstractions;

namespace TaskforceGenerator.Presentation.Views.Blazor.Components.Primitives;

/// <summary>
/// </summary>
/// <typeparam name="TSubControlModel"></typeparam>
public partial class DynamicMultiControlItem<TSubControlModel> : ModelComponentBase<IDynamicMultiControlItemModel<TSubControlModel>>
{
    private static RenderFragment DefaultRemoveButton(IButtonModel button)
    {
        return result;
        void result(RenderTreeBuilder b)
        {
            b.OpenElement(0, "div");
            b.AddAttribute(1, "class", "mr-1 flex items-center");
            b.OpenComponent<Button>(2);
            b.AddAttribute(3, "Value", button);
            b.CloseComponent();
            b.CloseElement();
        }
    }
}
