﻿using Microsoft.AspNetCore.Components;

using TaskforceGenerator.Presentation.Models.Abstractions;

using TaskforceGenerator.Presentation.Views.Blazor.Abstractions;

namespace TaskforceGenerator.Presentation.Blazor.Components;
internal sealed class Navigate : InjectedModelComponentBase<INavigationManager>
{
    [Parameter]
    public required String Route { get; set; }

    protected override void OnParametersSet() => Model.NavigateTo(Route);
}
