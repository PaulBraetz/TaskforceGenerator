﻿using TaskforceGenerator.Presentation.Views.Blazor.Abstractions;

namespace TaskforceGenerator.Presentation.Views.Blazor;

/// <summary>
/// Dispose strategy that never disposes instances any time <see cref="Dispose(T)"/> is called.
/// </summary>
/// <typeparam name="T">The type of objects never to dispose of.</typeparam>
public sealed class NonDisposingStrategy<T> : IDisposeStrategy<T>
{
    /// <summary>
    /// Initializes a new instance. If possible, use <see cref="Instance"/> instead.
    /// </summary>
    public NonDisposingStrategy() { }
    /// <summary>
    /// Gets the singleton instance.
    /// </summary>
    public static NonDisposingStrategy<T> Instance { get; } = new();
    /// <inheritdoc/>
    public void Dispose(T disposable)
    {
    }
}
