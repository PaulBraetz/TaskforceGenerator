using Microsoft.Extensions.DependencyInjection;

using PhotinoNET;

using System;
using System.IO;

namespace Photino.Blazor
{
    public class PhotinoBlazorApp
    {
        /// <summary>
        /// Gets configuration for the service provider.
        /// </summary>
        public IServiceProvider Services { get; private set; }

        /// <summary>
        /// Gets configuration for the root components in the window.
        /// </summary>
        public BlazorWindowRootComponents RootComponents { get; private set; }

        internal void Initialize(IServiceProvider services, RootComponentList rootComponents)
        {
            Services = services;
            RootComponents = Services.GetService<BlazorWindowRootComponents>();
            MainWindow = Services.GetService<PhotinoWindow>();
            WindowManager = Services.GetService<PhotinoWebViewManager>();

            _ = MainWindow
                .SetTitle("Photino.Blazor App")
                .SetUseOsDefaultLocation(false)
                .SetWidth(1000)
                .SetHeight(900)
                .SetLeft(450)
                .SetTop(100);

            _ = MainWindow.RegisterCustomSchemeHandler(PhotinoWebViewManager.BlazorAppScheme, HandleWebRequest);

            foreach(var component in rootComponents)
            {
                RootComponents.Add(component.Item1, component.Item2);
            }
        }

        public PhotinoWindow MainWindow { get; private set; }

        public PhotinoWebViewManager WindowManager { get; private set; }

        public void Run()
        {
            WindowManager.Navigate("/");
            MainWindow.WaitForClose();
        }

        public Stream HandleWebRequest(Object sender, String scheme, String url, out String contentType)
        {
            return WindowManager.HandleWebRequest(sender, scheme, url, out contentType!)!;
        }
    }
}
