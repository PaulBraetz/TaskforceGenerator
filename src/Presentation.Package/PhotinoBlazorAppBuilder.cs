﻿using Microsoft.Extensions.DependencyInjection;

using System;

namespace Photino.Blazor
{
    public class PhotinoBlazorAppBuilder
    {
        internal PhotinoBlazorAppBuilder()
        {
            RootComponents = new RootComponentList();
            Services = new ServiceCollection();
        }

        public static PhotinoBlazorAppBuilder CreateDefault(String[] args = default)
        {
            // We don't use the args for anything right now, but we want to accept them
            // here so that it shows up this way in the project templates.

            var builder = new PhotinoBlazorAppBuilder();
            _ = builder.Services.AddPhotino();

            // Right now we don't have conventions or behaviors that are specific to this method
            // however, making this the default for the template allows us to add things like that
            // in the future, while giving `new BlazorDesktopHostBuilder` as an opt-out of opinionated
            // settings.

            return builder;
        }

        public RootComponentList RootComponents { get; }

        public IServiceCollection Services { get; }

        public PhotinoBlazorApp Build(Action<IServiceProvider> serviceProviderOptions = null)
        {
            // register root components with DI container
            // Services.AddSingleton(RootComponents);

            var sp = Services.BuildServiceProvider();
            var app = sp.GetRequiredService<PhotinoBlazorApp>();

            serviceProviderOptions?.Invoke(sp);

            app.Initialize(sp, RootComponents);

            return app;
        }
    }
}
