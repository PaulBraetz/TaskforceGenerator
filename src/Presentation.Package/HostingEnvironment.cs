﻿using System;
using System.IO;

namespace Photino.Blazor
{
    public sealed class HostingEnvironment : IHostingEnvironment
    {
        public String ContentRootPath { get; set; } = Directory.GetCurrentDirectory();
        public String EnvironmentName { get; set; } = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
    }
}
