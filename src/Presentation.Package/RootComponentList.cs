﻿using Microsoft.AspNetCore.Components;

using System;
using System.Collections;
using System.Collections.Generic;

namespace Photino.Blazor
{
    public class RootComponentList : IEnumerable<(Type, String)>
    {
        private readonly List<(Type componentType, String domElementSelector)> _components = new();

        public void Add<TComponent>(String selector) where TComponent : IComponent
        {
            _components.Add((typeof(TComponent), selector));
        }

        public IEnumerator<(Type, String)> GetEnumerator()
        {
            return _components.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _components.GetEnumerator();
        }
    }
}
