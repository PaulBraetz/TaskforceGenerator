﻿using OneOf;

using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Results;
using TaskforceGenerator.Domain.Authentication;
using TaskforceGenerator.Domain.Authentication.Results;
using TaskforceGenerator.Domain.Core.Results;

namespace TaskforceGenerator.Application.Requests;

/// <summary>
/// Request for generating new bio codes.
/// </summary>
/// <param name="CitizenName">The citizen whose connection to generate a new bio code for.</param>
/// <param name="CancellationToken">The token used to signal the service execution to be cancelled.</param>
public readonly record struct GenerateBioCode(
    String CitizenName,
    CancellationToken CancellationToken) :
    //compliant cancellation is enabled because this request violates the cqrs principle.
    IServiceRequest<GenerateBioCode.Result>
{
    /// <summary>
    /// The result of a <see cref="GenerateBioCode"/> request.
    /// </summary>
    public sealed class Result : OneOfBase<BioCode, CitizenNotRegisteredResult, CitizenNotExistingResult, CompliantlyCancelledResult>
    {
        private Result(OneOf<BioCode, CitizenNotRegisteredResult, CitizenNotExistingResult, CompliantlyCancelledResult> input) : base(input)
        {
        }

        private Task<Result>? _task;

        /// <summary>
        /// Gets this instance wrapped in a <see cref="System.Threading.Tasks.Task"/>.
        /// </summary>
        public Task<Result> Task => _task ??= System.Threading.Tasks.Task.FromResult(this);
        /// <summary>
        /// Gets a new instance for a fully completed bio code result.
        /// </summary>
        public static Result BioCode(BioCode code) => new(code);
        /// <summary>
        /// Gets the singleton instance for a compliantly cancelled operations result.
        /// </summary>
        public static Result CompliantlyCancelled { get; } = new(CompliantlyCancelledResult.Instance);
        /// <summary>
        /// Gets the singleton instance for a result indicating that the requested citizen does not exist.
        /// </summary>
        public static Result CitizenNotExisting { get; } = new(CitizenNotExistingResult.Instance);
        /// <summary>
        /// Gets the singleton instance for a result indicating that the requested citizen is not registered.
        /// </summary>
        public static Result CitizenNotRegistered { get; } = new(CitizenNotRegisteredResult.Instance);
    }
}
