﻿using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Results;

namespace TaskforceGenerator.Application.Requests;

/// <summary>
/// Represents a proxy request for request execution. 
/// The request is thus signalled to be executed by a proxy wrapping the actual service.
/// Requests should be proxied in order to force dependency injection of application 
/// services instead of domain or infrastructure services. This leads to proper application 
/// of cross-cutting concerns, even though the underlying executing service might have 
/// been injected directly instead.
/// </summary>
/// <typeparam name="TRequest">The type of request to proxy.</typeparam>
/// <typeparam name="TResult">The type of result produced by the proxied request.</typeparam>
/// <param name="Request">The proxied request.</param>
public readonly record struct ProxyServiceRequest<TRequest, TResult>(TRequest Request) : IServiceRequest<TResult>
    where TRequest : IServiceRequest<TResult>
{
    /// <inheritdoc/>
    public CancellationToken CancellationToken => Request.CancellationToken;
}
/// <summary>
/// Contains static factory methods for the <see cref="ProxyServiceRequest{TRequest, TResult}"/> type.
/// </summary>
public static class ProxyServiceRequest
{
    /// <summary>
    /// Creates a new proxy request.
    /// </summary>
    /// <typeparam name="TRequest">The type of request to proxy.</typeparam>
    /// <typeparam name="TResult">The type of result produced by the proxied request.</typeparam>
    /// <param name="request">The proxied request.</param>
    /// <returns>A new proxy request.</returns>
    public static ProxyServiceRequest<TRequest, TResult> Create<TRequest, TResult>(TRequest request)
        where TRequest : IServiceRequest<TResult>
    => new(request);
    /// <summary>
    /// Creates a new proxy request.
    /// </summary>
    /// <typeparam name="TRequest">The type of request to proxy.</typeparam>
    /// <param name="request">The proxied request.</param>
    /// <returns>A new proxy request.</returns>
    public static ProxyServiceRequest<TRequest, ServiceResult> Create<TRequest>(TRequest request)
        where TRequest : IServiceRequest
    => new(request);
}