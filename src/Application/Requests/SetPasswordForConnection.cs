﻿using OneOf;

using TaskforceGenerator.Application.Results;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Results;
using TaskforceGenerator.Domain.Authentication;
using TaskforceGenerator.Domain.Authentication.Requests;
using TaskforceGenerator.Domain.Authentication.Results;
using TaskforceGenerator.Domain.Core.Results;

namespace TaskforceGenerator.Application.Requests;

/// <summary>
/// Request for setting a citizens connection password.
/// </summary>
/// <param name="CitizenName">The name of the citizen whose connection to modify.</param>
/// <param name="Password">The password to associate to the connection.</param>
/// <param name="Code">The bio code to verify the password change with.</param>
/// <param name="CancellationToken">The token used to signal the service execution to be cancelled.</param>
public readonly record struct SetPasswordForConnection(
    String CitizenName,
    String Password,
    BioCode Code,
    CancellationToken CancellationToken) :
    IServiceRequest<SetPasswordForConnection.Result>
{
    /// <summary>
    /// The result of a <see cref="SetPasswordForConnection"/> request.
    /// </summary>
    public sealed class Result : OneOfBase<
            ServiceResult,
            BioCodeMismatchResult,
            PasswordCreationGuidelineViolatedResult,
            CitizenNotExistingResult,
            CitizenNotRegisteredResult>
    {
        private Result(OneOf<
            ServiceResult,
            BioCodeMismatchResult,
            PasswordCreationGuidelineViolatedResult,
            CitizenNotExistingResult,
            CitizenNotRegisteredResult> input) : base(input)
        {
        }

        private Task<Result>? _task;

        /// <summary>
        /// Gets this instance wrapped in a <see cref="System.Threading.Tasks.Task"/>.
        /// </summary>
        public Task<Result> Task => _task ??= System.Threading.Tasks.Task.FromResult(this);
        /// <summary>
        /// Gets the singleton instance for a fully completed operations result.
        /// </summary>
        public static Result Completed { get; } = new(ServiceResult.Completed);
        /// <summary>
        /// Gets the singleton instance for a compliantly cancelled operations result.
        /// </summary>
        public static Result CompliantlyCancelled { get; } = new(ServiceResult.CompliantlyCancelled);
        /// <summary>
        /// Gets the singleton instance for a result indicating that the requested citizen does not exist.
        /// </summary>
        public static Result CitizenNotExisting { get; } = new(CitizenNotExistingResult.Instance);
        /// <summary>
        /// Gets the singleton instance for a result indicating that the requested citizen is not registered.
        /// </summary>
        public static Result CitizenNotRegistered { get; } = new(CitizenNotRegisteredResult.Instance);
        /// <summary>
        /// Gets a new result indicating that the operation encountered a bio code mismatch.
        /// </summary>
        public static Result BioCodeMismatch(BioCodeMismatchResult wrappedResult) => new(wrappedResult);
        /// <summary>
        /// Gets a new result indicating the password provided violated password guidelines.
        /// </summary>
        public static Result PasswordCreationGuidelineViolated(PasswordCreationGuidelineViolatedResult wrappedResult) =>
            new(wrappedResult);
    }
}