﻿using OneOf;

using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Results;
using TaskforceGenerator.Domain.Authentication.Results;
using TaskforceGenerator.Domain.Core;
using TaskforceGenerator.Domain.Core.Abstractions;

namespace TaskforceGenerator.Application.Requests;

/// <summary>
/// Request for creating an event and yielding the invite code created for it.
/// </summary>
/// <param name="EventName">The name of the event to create.</param>
/// <param name="Description">The description of the event to create.</param>
/// <param name="Date">The date of the event to create.</param>
/// <param name="PlannedVehicles">The vehicles and their respective amounts planned for the event to create.</param>
/// <param name="CancellationToken">The token used to signal the service execution to be cancelled.</param>
public readonly record struct CreateEvent(
    String EventName,
    String Description,
    DateTimeOffset Date,
    IReadOnlyDictionary<IVehicle, Int32> PlannedVehicles,
    CancellationToken CancellationToken) 
    : IServiceRequest<OneOf<InviteCode, CompliantlyCancelledResult, NotAuthenticatedResult>>;