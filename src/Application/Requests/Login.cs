﻿using OneOf;

using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Results;
using TaskforceGenerator.Domain.Authentication.Results;
using TaskforceGenerator.Domain.Core.Results;

namespace TaskforceGenerator.Application.Requests;

/// <summary>
/// Request for authenticating the user for using a citizen connection.
/// </summary>
/// <param name="CitizenName">The name of the citizen for which to authenticate the user.</param>
/// <param name="ClearPassword">The password provided by the user.</param>
/// <param name="CancellationToken">The token used to signal the service execution to be cancelled.</param>
public readonly record struct Login(
    String CitizenName,
    String ClearPassword,
    CancellationToken CancellationToken)
    : IServiceRequest<Login.Result>
{
    /// <summary>
    /// The result of a <see cref="Login"/> request.
    /// </summary>
    public sealed class Result : 
        OneOfBase<
            ServiceResult, 
            CitizenNotRegisteredResult, 
            CitizenNotExistingResult, 
            PasswordMismatchResult>
    {
        private Result(OneOf<ServiceResult, CitizenNotRegisteredResult, CitizenNotExistingResult, PasswordMismatchResult> input) : base(input)
        {
        }

        private Task<Result>? _task;

        /// <summary>
        /// Gets this instance wrapped in a <see cref="System.Threading.Tasks.Task"/>.
        /// </summary>
        public Task<Result> Task => _task ??= System.Threading.Tasks.Task.FromResult(this);
        /// <summary>
        /// Gets the singleton instance for a fully completed operations result.
        /// </summary>
        public static Result Completed { get; } = new(ServiceResult.Completed);
        /// <summary>
        /// Gets the singleton instance for a compliantly cancelled operations result.
        /// </summary>
        public static Result CompliantlyCancelled { get; } = new(ServiceResult.CompliantlyCancelled);
        /// <summary>
        /// Gets the singleton instance for a result indicating that the requested citizen does not exist.
        /// </summary>
        public static Result CitizenNotExisting { get; } = new(CitizenNotExistingResult.Instance);
        /// <summary>
        /// Gets the singleton instance for a result indicating that the requested citizen is not registered.
        /// </summary>
        public static Result CitizenNotRegistered { get; } = new(CitizenNotRegisteredResult.Instance);
        /// <summary>
        /// Gets the singleton instance for a result indicating that the password provided did not match the one set to the citizen requested.
        /// </summary>
        public static Result PasswordMismatch { get; } = new(PasswordMismatchResult.Instance);
    }
}

