﻿using TaskforceGenerator.Domain.Authentication;
using TaskforceGenerator.Domain.Authentication.Requests;

namespace TaskforceGenerator.Application.Results;

/// <summary>
/// Result forn when a bio code provided does not match the one expected.
/// </summary>
/// <param name="Code">The invalid code provided.</param>
/// <param name="MismatchType">The mismatch type causing the exception.</param>
/// <param name="Message">The message detailing the mismatch.</param>
public readonly record struct BioCodeMismatchResult(BioCode Code, VerifyBioCodeResult MismatchType, String Message)
{
    /// <summary>
    /// Creates and initializes a new instance.
    /// </summary>
    /// <param name="code">The </param>
    /// <param name="mismatchType"></param>
    /// <returns></returns>
    /// <exception cref="ArgumentException"></exception>
    public static BioCodeMismatchResult Create(BioCode code, VerifyBioCodeResult mismatchType)
    {
        var message = mismatchType switch
        {
            VerifyBioCodeResult.BioMismatch => $"The required code does not match the bio retrieved for the citizen. Make sure the required code is posted to the citizens RSI bio.",
            VerifyBioCodeResult.CitizenMismatch => $"The required code does not match the system entities code. Please try again.",
            _ => throw new ArgumentException($"Invalid mismatch type received: {mismatchType}", nameof(mismatchType))
        };
        var result = new BioCodeMismatchResult(code, mismatchType, message);

        return result;
    }
}
