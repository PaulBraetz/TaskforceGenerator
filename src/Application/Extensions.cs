﻿using TaskforceGenerator.Application.Requests;
using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Application;

/// <summary>
/// Extensions for the <c>TaskforceGenerator.Application</c> namespace.
/// </summary>
public static class Extensions
{
    /// <summary>
    /// Wraps a query in a proxy request.
    /// </summary>
    /// <typeparam name="TRequest">The type of query to proxy.</typeparam>
    /// <typeparam name="TResult">The type of result produced by the proxied query.</typeparam>
    /// <param name="query">The query to proxy.</param>
    /// <returns>A new instance of <see cref="ProxyServiceRequest{TRequest, TResult}"/>, wrapping the query.</returns>
    public static ProxyServiceRequest<TRequest, TResult> AsProxyQuery<TRequest, TResult>(this TRequest query)
        where TRequest : IServiceRequest<TResult> => new(query);
    /// <summary>
    /// Wraps a command in a proxy request.
    /// </summary>
    /// <typeparam name="TRequest">The type of command to proxy.</typeparam>
    /// <typeparam name="TResult">The type of result produced by the proxied command.</typeparam>
    /// <param name="command">The command to proxy.</param>
    /// <returns>A new instance of <see cref="ProxyServiceRequest{TRequest, TResult}"/>, wrapping the command.</returns>
    public static ProxyServiceRequest<TRequest, TResult> AsProxyCommand<TRequest, TResult>(this TRequest command)
        where TRequest : IServiceRequest<TResult>
        => new(command);
}
