﻿using TaskforceGenerator.Application.Requests;
using TaskforceGenerator.Common.Abstractions;

namespace TaskforceGenerator.Application.Services;

/// <summary>
/// General proxy request service.
/// Requests should be proxied in order to force dependency injection 
/// of application services instead of domain or infrsatructure services.
/// This leads to proper application of cross-cutting concerns, even 
/// though the underlying executing service might have been injected 
/// directly instead.
/// </summary>
/// <typeparam name="TRequest">The type of request to proxy.</typeparam>
/// <typeparam name="TResult">The type of result produced by the proxied request.</typeparam>
/// <typeparam name="TService">The type of service proxied.</typeparam>
/// <remarks>
/// <typeparamref name="TService"/> must not be constrained so as to allow correct composition of decorators.
/// It is only used to obtain the service type underlying the decorator hierarchy.
/// </remarks>
public sealed class ProxyService<TRequest, TResult, TService> : IService<ProxyServiceRequest<TRequest, TResult>, TResult>
    where TRequest : IServiceRequest<TResult>
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="service">The proxied service.</param>
    public ProxyService(IService<TRequest, TResult> service)
    {
        _service = service;
    }

    private readonly IService<TRequest, TResult> _service;

    /// <inheritdoc/>
    public ValueTask<TResult> Execute(ProxyServiceRequest<TRequest, TResult> query) =>
        _service.Execute(query.Request);
}
