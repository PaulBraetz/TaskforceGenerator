﻿using OneOf;

using TaskforceGenerator.Application.Requests;
using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Results;
using TaskforceGenerator.Domain.Authentication;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Authentication.Requests;
using TaskforceGenerator.Domain.Core.Results;

namespace TaskforceGenerator.Application.Services;

/// <summary>
/// Service for generating new bio codes. New bio codes are registered to the citizen connection they are generated for.
/// This service resides in the application layer because it violates the CQRS principle by simultaneously retrieving a
/// new bio code (query) and assigning it to a connection (command).
/// In that sense, it also acts as <c>Facade</c>, as it orchestrates domain services.
/// </summary>
public sealed class GenerateBioCodeService :
    IService<GenerateBioCode, GenerateBioCode.Result>
{
    private readonly IService<CommitBioCodeChange> _commitService;
    private readonly IService<ReconstituteConnection, OneOf<ICitizenConnection, CitizenNotRegisteredResult>> _citizenService;
    private readonly IService<GenerateRandomBioCode, BioCode> _generateCodeService;

    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="citizenService">The service used for retrieving the citizen for whom to generate a new code.</param>
    /// <param name="codeService">The service to set the citizens bio code to after generating it.</param>
    /// <param name="generateCodeService">The service to use when generating new bio codes.</param>
    public GenerateBioCodeService(
        IService<ReconstituteConnection, OneOf<ICitizenConnection, CitizenNotRegisteredResult>> citizenService,
        IService<CommitBioCodeChange> codeService,
        IService<GenerateRandomBioCode, BioCode> generateCodeService)
    {
        _citizenService = citizenService;
        _commitService = codeService;
        _generateCodeService = generateCodeService;
    }
    /// <inheritdoc/>
    public async ValueTask<GenerateBioCode.Result> Execute(GenerateBioCode query)
    {
        if(query.CancellationToken.IsCancellationRequested)
        {
            return GenerateBioCode.Result.CompliantlyCancelled;
        }

        var reconstituteResult = await new ReconstituteConnection(query.CitizenName, query.CancellationToken).Using(_citizenService);

        if(reconstituteResult.TryPickT1(out var _, out var citizen))
        {
            return GenerateBioCode.Result.CitizenNotRegistered;
        }

        var previousCode = citizen.Code;
        BioCode code;
        do
        {
            code = await new GenerateRandomBioCode().Using(_generateCodeService);
        } while(previousCode == code);

        var commitResult = await new CommitBioCodeChange(query.CitizenName, code, query.CancellationToken).Using(_commitService);

        var result = commitResult.Match(
            s => GenerateBioCode.Result.BioCode(code),
            c => GenerateBioCode.Result.CompliantlyCancelled);

        return result;
    }
}
