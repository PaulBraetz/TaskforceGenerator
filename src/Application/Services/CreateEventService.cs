﻿using OneOf;

using TaskforceGenerator.Application.Requests;
using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Results;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Authentication.Results;
using TaskforceGenerator.Domain.Core;
using TaskforceGenerator.Domain.Core.Abstractions;
using TaskforceGenerator.Domain.Core.Requests;

using CreateEventEntity = TaskforceGenerator.Domain.Core.Queries.CreateEvent;
using CreateInviteCodeQuery = TaskforceGenerator.Domain.Core.Queries.CreateInviteCode;

namespace TaskforceGenerator.Application.Services;
/// <summary>
/// Service for creating and committing an event, and returning the invite code generated for it.
/// This service resides in the application layer as it violates the CQRS-principle by
/// simultaneously returning a newly created code (query) as well as committing an event to 
/// the infrastructure (command).
/// </summary>
public sealed class CreateEventService : IService<CreateEvent, OneOf<InviteCode, CompliantlyCancelledResult, NotAuthenticatedResult>>
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="userContext">The context used to determine the creator of the event.</param>
    /// <param name="codeService">The service used to generate a new invite code.</param>
    /// <param name="eventService">The factory service used to create a new event entity.</param>
    /// <param name="commitService">The service used to commit the newly created event.</param>
    public CreateEventService(
        IUserContext userContext,
        IService<CreateInviteCodeQuery, InviteCode> codeService,
        IService<CreateEventEntity, IEvent> eventService,
        IService<CommitEvent> commitService)
    {
        _userContext = userContext;
        _codeService = codeService;
        _eventService = eventService;
        _commitService = commitService;
    }

    private readonly IUserContext _userContext;
    private readonly IService<CreateInviteCodeQuery, InviteCode> _codeService;
    private readonly IService<CreateEventEntity, IEvent> _eventService;
    private readonly IService<CommitEvent> _commitService;

    /// <inheritdoc/>
    public async ValueTask<OneOf<InviteCode, CompliantlyCancelledResult, NotAuthenticatedResult>> Execute(CreateEvent request)
    {
        if(request.CancellationToken.IsCancellationRequested)
        {
            return CompliantlyCancelledResult.Instance;
        }

        if(_userContext.Citizen == null)
        {
            return NotAuthenticatedResult.Instance;
        }

        var code = await new CreateInviteCodeQuery(request.CancellationToken)
            .Using(_codeService);

        var creatorName = _userContext.Citizen.CitizenName;

        var @event = await new CreateEventEntity(
            code,
            request.EventName,
            request.Description,
            request.Date,
            request.PlannedVehicles,
            creatorName,
            request.CancellationToken).Using(_eventService);

        var commitResult = await new CommitEvent(@event, request.CancellationToken)
             .Using(_commitService);

        return commitResult.IsT1 ?
            commitResult.AsT1 :
            code;
    }
}
