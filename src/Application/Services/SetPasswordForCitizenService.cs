﻿using OneOf;

using TaskforceGenerator.Application.Requests;
using TaskforceGenerator.Application.Results;
using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Results;
using TaskforceGenerator.Domain.Authentication;
using TaskforceGenerator.Domain.Authentication.Requests;
using TaskforceGenerator.Domain.Authentication.Results;
using TaskforceGenerator.Domain.Core.Results;

namespace TaskforceGenerator.Application.Services;

/// <summary>
/// Service for setting a citizens password.
/// This service resides in the application layer because it coordinates domain actions.
/// </summary>
public sealed class SetPasswordForCitizenService : IService<SetPasswordForConnection, SetPasswordForConnection.Result>
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="verifyService">The service used for verifying password setting commands.</param>
    /// <param name="passwordService">The service used to create a new password.</param>
    /// <param name="commitService">The service used to commit the new password to the infrastructure.</param>
    /// <param name="parametersService">The service to use when creating new password parameters.</param>
    public SetPasswordForCitizenService(IService<VerifyBioCode, OneOf<VerifyBioCodeResult, CitizenNotRegisteredResult, CitizenNotExistingResult>> verifyService,
                                        IService<CreatePassword, OneOf<Password, PasswordCreationGuidelineViolatedResult>> passwordService,
                                        IService<CommitPasswordChange> commitService,
                                        IService<CreatePasswordParameters, PasswordParameters> parametersService)
    {
        _verifyService = verifyService;
        _passwordService = passwordService;
        _commitService = commitService;
        _parametersService = parametersService;
    }

    private readonly IService<VerifyBioCode, OneOf<VerifyBioCodeResult, CitizenNotRegisteredResult, CitizenNotExistingResult>> _verifyService;
    private readonly IService<CreatePasswordParameters, PasswordParameters> _parametersService;
    private readonly IService<CreatePassword, OneOf<Password, PasswordCreationGuidelineViolatedResult>> _passwordService;
    private readonly IService<CommitPasswordChange> _commitService;

    /// <inheritdoc/>
    public async ValueTask<SetPasswordForConnection.Result> Execute(SetPasswordForConnection command)
    {
        if(command.CancellationToken.IsCancellationRequested)
        {
            return SetPasswordForConnection.Result.CompliantlyCancelled;
        }

        var (name, password, code, token) = command;
        var verifyResult = await new VerifyBioCode(code, name, token)
            .Using(_verifyService);

        if(verifyResult.TryPickT1(out var _, out var verificationOrNotRegistered))
        {
            return SetPasswordForConnection.Result.CitizenNotRegistered;
        }

        if(verificationOrNotRegistered.TryPickT1(out var _, out var verification))
        {
            return SetPasswordForConnection.Result.CitizenNotRegistered;
        }

        if(verification != VerifyBioCodeResult.Match)
        {
            var mismatchResult = BioCodeMismatchResult.Create(code, verification);
            return SetPasswordForConnection.Result.BioCodeMismatch(mismatchResult);
        }

        var passwordParameters = await new CreatePasswordParameters().Using(_parametersService);

        var passwordResult = await new CreatePassword(
            password,
            passwordParameters,
            IsInitialPassword: true,
            token).Using(_passwordService);

        if(passwordResult.TryPickT1(out var guidelineError, out var createdPassword))
        {
            return SetPasswordForConnection.Result.PasswordCreationGuidelineViolated(guidelineError);
        }

        var commitResult = await new CommitPasswordChange(name, code, createdPassword, token).Using(_commitService);

        var result = commitResult.Match(
            s => SetPasswordForConnection.Result.Completed,
            c => SetPasswordForConnection.Result.CompliantlyCancelled);

        return result;
    }
}
