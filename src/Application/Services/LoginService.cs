﻿using OneOf;

using TaskforceGenerator.Application.Requests;
using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Results;
using TaskforceGenerator.Domain.Authentication.Abstractions;
using TaskforceGenerator.Domain.Authentication.Requests;
using TaskforceGenerator.Domain.Authentication.Results;
using TaskforceGenerator.Domain.Core.Results;

namespace TaskforceGenerator.Application.Services;

/// <summary>
/// Service for logging in a user.
/// This service resides in the application layer because it coordinates domain actions.
/// </summary>
public sealed class LoginService : IService<Login, Login.Result>
{
    private readonly IService<ReconstituteConnection, OneOf<ICitizenConnection, CitizenNotRegisteredResult>> _repository;
    private readonly IService<Authenticate, OneOf<ServiceResult, PasswordMismatchResult>> _authenticateService;
    private readonly IService<SetContextConnection> _contextService;

    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="repository">The repository to use when reconstituting the requested citizen.</param>
    /// <param name="authenticateService">The service used to authenticate the citizen with the password provided.</param>
    /// <param name="contextService">The service used to set the authenticated citizen.</param>
    public LoginService(
        IService<ReconstituteConnection, OneOf<ICitizenConnection, CitizenNotRegisteredResult>> repository,
        IService<Authenticate, OneOf<ServiceResult, PasswordMismatchResult>> authenticateService,
        IService<SetContextConnection> contextService)
    {
        _repository = repository;
        _authenticateService = authenticateService;
        _contextService = contextService;
    }

    /// <inheritdoc/>
    public async ValueTask<Login.Result> Execute(Login command)
    {
        var reconstituteResult = await new ReconstituteConnection(
            command.CitizenName,
            command.CancellationToken).Using(_repository);

        if(reconstituteResult.TryPickT1(out var _, out var connection))
        {
            return Login.Result.CitizenNotRegistered;
        }

        var authenticateResult = await new Authenticate(
                connection,
                command.ClearPassword,
                command.CancellationToken)
            .Using(_authenticateService);

        if(authenticateResult.TryPickT1(out var _, out var authenticateServiceResult))
        {
            return Login.Result.PasswordMismatch;
        } else if(authenticateServiceResult.TryPickT1(out _, out _))
        {
            return Login.Result.CompliantlyCancelled;
        }

        var setContextResult = await new SetContextConnection(connection, command.CancellationToken)
             .Using(_contextService);

        var result = setContextResult.Match(
            s => Login.Result.Completed,
            c => Login.Result.CompliantlyCancelled);

        return result;
    }
}
