﻿using OneOf;

using TaskforceGenerator.Application.Requests;
using TaskforceGenerator.Application.Results;
using TaskforceGenerator.Common;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Results;
using TaskforceGenerator.Domain.Authentication.Results;
using TaskforceGenerator.Domain.Core.Results;

namespace TaskforceGenerator.Application.Fakes;

/// <summary>
/// Fake service for setting a citizens connection password.
/// </summary>
public sealed class SetPasswordForCitizenServiceFake :
    IService<SetPasswordForConnection, SetPasswordForConnection.Result>
{
    /// <inheritdoc/>
    public ValueTask<SetPasswordForConnection.Result> Execute(SetPasswordForConnection command) =>
        ValueTask.FromResult(SetPasswordForConnection.Result.Completed);
}
