﻿using OneOf;

using TaskforceGenerator.Application.Requests;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Results;
using TaskforceGenerator.Domain.Authentication.Results;
using TaskforceGenerator.Domain.Core.Results;

namespace TaskforceGenerator.Application.Fakes;

/// <summary>
/// Fake service for logging in.
/// </summary>
public sealed class LoginServiceFake :
    IService<Login, Login.Result>
{
    /// <inheritdoc/>
    public ValueTask<Login.Result> Execute(Login command) =>
        ValueTask.FromResult(Login.Result.Completed);
}
