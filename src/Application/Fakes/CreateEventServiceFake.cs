﻿using OneOf;

using TaskforceGenerator.Application.Requests;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Common.Results;
using TaskforceGenerator.Domain.Authentication.Results;
using TaskforceGenerator.Domain.Core;
using TaskforceGenerator.Domain.Core.Abstractions;

namespace TaskforceGenerator.Application.Fakes;

/// <summary>
/// Service for creating an event and yielding the invite code created for it.
/// </summary>
public sealed class CreateEventServiceFake : IService<CreateEvent, OneOf<InviteCode, CompliantlyCancelledResult, NotAuthenticatedResult>>
{
    /// <inheritdoc/>
    public async ValueTask<OneOf<InviteCode, CompliantlyCancelledResult, NotAuthenticatedResult>> Execute(CreateEvent query)
    {
        await Task.Delay(2500);
        InviteCode result = "FAKE_CODE";

        return result;
    }
}
