﻿using TaskforceGenerator.Application.Requests;
using TaskforceGenerator.Common.Abstractions;
using TaskforceGenerator.Domain.Authentication.Requests;

namespace TaskforceGenerator.Application.Formatters;

/// <summary>
/// Password-hiding implementation for formatting instances of <see cref="SetPasswordForConnection"/>.
/// </summary>
public sealed class SetPasswordForConnectionFormatter : IStaticFormatter<SetPasswordForConnection>
{
    /// <inheritdoc/>
    public String Format(SetPasswordForConnection value)
    {
        var result =
            $"{nameof(SetPasswordForConnection)} " +
            $"{{ " +
                $"{nameof(SetPasswordForConnection.CitizenName)} = {value.CitizenName}, " +
                $"{nameof(SetPasswordForConnection.Password)} = ***, " +
                $"{nameof(SetPasswordForConnection.Code)} = {value.Code} " +
            $"}}";

        return result;
    }
}
/// <summary>
/// Password-hiding implementation for formatting instances of <see cref="CreatePassword"/>.
/// </summary>
public sealed class LoginFormatter : IStaticFormatter<Login>
{
    /// <inheritdoc/>
    public String Format(Login value)
    {
        var result =
            $"{nameof(Login)} " +
            $"{{ " +
                $"{nameof(Login.ClearPassword)} = ***, " +
                $"{nameof(Login.CitizenName)} = {value.CitizenName} " +
            $"}}";

        return result;
    }
}
