﻿namespace TaskforceGenerator.Application.Exceptions;

/// <summary>
/// Exception thrown when an invalid or unknown scheme name was provided.
/// </summary>
public sealed class InvalidSchemeNameException : ApplicationException
{
    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    /// <param name="schemeName">The invalid scheme name provided.</param>
    public InvalidSchemeNameException(String schemeName)
        : base($"The provided scheme name '{schemeName}' is invalid.")
    {
        SchemeName = schemeName;
    }
    /// <summary>
    /// Gets the invalid scheme name provided.
    /// </summary>
    public String SchemeName { get; }
}
